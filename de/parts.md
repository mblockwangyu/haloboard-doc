# Verpackungsinhalt

<table style="table-layout:fixed;width:100%;text-align:center;" cellpadding=20px; cellspacing=12px;>

<tr>
<td><img src="../fr/images/halo.png" width="300px;" style="padding:5px 5px 5px 5px;"><br>HaloCode * 1</td>
<td><img src="../fr/images/battery-box.png" width="300px;" style="padding:5px 5px 5px 5px;"><br>AAA-Batteriefach * 1</td>
<td><img src="../fr/images/battery.png" width="300px;" style="padding:5px 5px 5px 5px;"><br>AAA-Batterie * 3</td>
</tr>

<tr>
<td><img src="../fr/images/clip.png" width="300px;" style="padding:5px 5px 5px 5px;"><br>Krokodilklemme * 4</td>
<td><img src="../fr/images/tape.png" width="300px;" style="padding:5px 5px 5px 5px;"><br>Kupferfolienklebband * 1</td>
<td><img src="../fr/images/strap.png" width="300px;" style="padding:5px 5px 5px 5px;"><br>Zauberarmband * 1</td>
</tr>

<tr>
<td><img src="../fr/images/fastener.png" width="300px;" style="padding:5px 5px 5px 5px;"><br>Klettverschluss * 4</td>
<td><img src="../fr/images/usb.png" width="300px;" style="padding:5px 5px 5px 5px;"><br>Micro-USB-Kabel * 1</td>
</tr>

</table>