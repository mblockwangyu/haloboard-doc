# Batterie

### Batterie einlegen

<img src="../fr/images/install-battery.png" width="600px" style="padding:5px 5px 5px 5px;">

### Gebrauchsanweisung

- Ziehen Sie nicht direkt am Batteriekabel, um Batterien abzuschalten. Fingern Sie den Anschluss fest und entfernen Sie ihn vorsichtig.
- Verwenden Sie keine Batterien unterschiedlicher Marken, oder keine alten und neuen Batterien gleichzeitig.
- Stellen Sie sicher, dass die Batterie in der richtigen Richtung eingelegt ist.
- Bitte entfernen Sie die verbrauchte Batterie aus dem Batteriefach.
- Legen Sie keine metallischen Gegenstände zwischen die Klemmen des Batteriefachs, da dies zu einem Kurzschluss in der Stromversorgung führen kann.
- Laden Sie keine normalen Batterien (nicht wiederaufladbare Batterien).
