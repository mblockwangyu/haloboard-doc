# Kurzanleitung

## 1. Software herunterladen

Besuchen Sie [http://www.mblock.cc](http://www.mblock.cc), um die neueste Version der mBlock 5 herunterzuladen und zu installieren.

<img src="../fr/images/download.png" width="600px;" style="padding:5px 5px 20px 5px;">

## 2. die HaloCode an den Computer anschließen

Verwenden Sie das Micro-USB-Kabel, um die HaloCode an den Computer anzuschließen.

<img src="../fr/images/connect.png" width="600px;" style="padding:5px 5px 20px 5px;">

## 3. die Programmierung starten

<span style="color:white;font-size:18;border-radius:100%;background-color:lightblue;font-wieght:bold;">&nbsp; 1 &nbsp;</span> Öffnen Sie die mBlock 5 und fügen Sie die HaloCode aus der Gerätebibliothek hinzu.

<table style="table-layout:fixed;width:100%;text-align:center;" cellpadding=20px; cellspacing=12px;>
<tr>
<td width="50%"><img src="images/connect-halo-1.png" height="120px;" style="padding:5px 5px 5px 5px;"></td>
<td width="10%;"><span style="font-size:30px;padding:5px;color:lightblue;">&rarr;</span></td>
<td width="40%"><img src="../fr/images/connect-halo-2.png" height="120px;" style="padding:5px 5px 5px 5px;"></td>
</tr>
</table>

<span style="color:white;font-size:18;border-radius:100%;background-color:lightblue;font-wieght:bold;">&nbsp; 2 &nbsp;</span> Schließen Sie das Gerät an.

<table style="table-layout:fixed;width:100%;text-align:center;" cellpadding=20px; cellspacing=12px;>
<tr>
<td width="50%"><img src="images/connect-halo-3.png" height="120px;" style="padding:5px 5px 5px 5px;"></td>
<td width="10%;"><span style="font-size:30px;padding:5px;color:lightblue;">&rarr;</span></td>
<td width="40%"><img src="images/connect-halo-4.png" height="120px;" style="padding:5px 5px 5px 5px;"></td>
</tr>
</table>

<span style="color:white;font-size:18;border-radius:100%;background-color:lightblue;font-wieght:bold;">&nbsp; 3 &nbsp;</span> Nun programmieren wir zusammen eine kleine Aufgabe! Versuchen Sie, alle LEDs auf der HaloCode rot leuchtend zu machen.

<div style="margin:20px;">
<p>1) Fügen Sie gemäß unterer Abbildung einen Ereignisbaustein hinzu.</p>
<img src="images/program-1.png" width="600px;" style="padding:0px 5px 15px 5px;">
<p>2) Fügen Sie den folgenden violetten Baustein aus dem Beleuchtungsbausteinbereich hinzu und ändern Sie die Lichtfarbe ins Rot, um das Programm abzuschließen.</p>
<img src="images/program-2.png" width="600px;" style="padding:0px 5px 15px 5px;">
</div>

## 4. Programm ausführen

Sie können entweder das Programm auf die HaloCode hochladen oder direkt in der mBlock 5 ausführen.

### Programm auf die HaloCode hochladen

Programmausführung getrennt vom Software ist möglich 

Aktivieren Sie den Hochladen-Modus und klicken Sie auf auf Gerät hochladen. Drücken Sie nach dem Hochladen die blaue Taste auf der HaloCode, um zu sehen, ob die LED rot leuchtet.

<img src="images/program-3.png" style="padding:5px 5px 5px 5px;">

### Führen Sie das Programm in der Software aus

Kein Hochladen erforderlich. Die Ergebnisse können in Echtzeit gesehen werden.

Schalten Sie den Hochladen-Modus aus, ändern Sie die Farbe im Programm ins Grün und drücken Sie die blaue Taste auf der HaloCode, um zu sehen, ob sich die LED-Leuchtfarbe geändert hat.

<img src="images/program-4.png" style="padding:5px 5px 5px 5px;">


