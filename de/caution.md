# Vorsicht

- Die HaloCode kann nur mit einer 3V- bis 5V-Gleichstromversorgung betrieben werden.
- Stecken oder ziehen Sie den Stromkreis nicht unter Spannung. Stellen Sie sicher, dass die HaloCode ausgeschaltet ist, auch wenn Sie die E / A-Erweiterungsschnittstelle an den externen Stromkreis  anschließen.
- Die vier E / A-Erweiterungsschnittstellen 0, 1, 2, 3 mit einer Belastbarkeit von 10 mA.
- Der 3,3-V-Fuß hat eine Belastbarkeit von 500 mA.
- Die Erweiterungsschnittstelle des Elektronikmoduls hat eine Belastbarkeit von 1A. Achtung: nur wenn die Eingangsspannung des Netzteils mehr als 3 V und der Eingangsstrom mehr als 3,5 A beträgt, kann die Belastbarkeit der Erweiterungsschnittstelle 1A erreichen. Zu diesem Zeitpunkt können mehr elektronische Module angeschlossen werden.
- Setzen Sie die HaloCode weder Wasser noch Feuchtigkeit aus.
- Betreiben Sie die HaloCode in einer gut belüfteten Umgebung.
- Bewahren Sie die HaloCode nicht in einer überkalten oder überheißen Umgebung auf.
- Legen Sie keine Metallgegenstände auf die HaloCode, da sonst die HaloCode defekt wird oder sogar dauerhaft beschädigt werden kann.
- Wenn Sie die HaloCode an ein nicht zugelassenes Gerät anschließen, kann die HaloCode beschädigt werden.
- Setzen Sie die HaloCode nicht unbeaufsichtigt in ein Gerät ein.
- Verbinden Sie den Fuß 3,3 V auf keinen Fall direkt mit dem GND-Fuß durch Krokodilklemmen. Dies kann einen Kurzschluss verursachen und die HaloCode beschädigen.
