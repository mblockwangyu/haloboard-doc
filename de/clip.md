# Ausführung mit Krokodilklemmen

Die Metallteile an den Enden der Krokodilklemme sind elektrisch leitend. Mit dem Berührungssensor der HaloCode und dem doppelseitig leitfähigen Kupferfolienklebeband können verschiedene interessante Projektartikel hergestellt werden, wie z.B. Farbpalette, Interaktion mit der mBlock 5.

Lassen Sie uns gemeinsam ein einfaches Projekt abschließen! Durch dieses Projekt können Sie lernen, wie die Krokodilklemme zum kreativen Herstellen verwendet wird.

Verwenden Sie den Karton, das Kupferfolienklebeband usw. innerhalb der Verpackung, um die entsprechende Farbanzeige der Farb-LEDs zu realisieren, wenn Sie eine der Krokodilklemmen berühren.

<img src="../fr/images/clip-1.png" width="300px;" style="padding:5px 5px 20px 5px;">

### 1 Finden Sie die nötigen Artikel.

<img src="images/clip-2.png" width="800px;" style="padding:5px 5px 20px 5px;">

### 2 Installieren Sie die Krokodilklemme.

<span style="color:white;font-size:18;border-radius:100%;background-color:lightblue;font-wieght:bold;">&nbsp; 1 &nbsp;</span> Klemmen Sie die rote Krokodilklemme am Berührungssensor 0.

<img src="../fr/images/clip-3.png" width="300px;" style="padding:5px 5px 20px 5px;">

<span style="color:white;font-size:18;border-radius:100%;background-color:lightblue;font-wieght:bold;">&nbsp; 2 &nbsp;</span> Klemmen Sie die gelbe, blaue und grüne Krokodilklemme jeweils nacheinander an den Berührungssensoren 1, 2 und 3.

<img src="../fr/images/clip-4.png" width="300px;" style="padding:5px 5px 20px 5px;">

<div style="background-color:#FFFFCC;padding:10px;border-left:5px solid #FFEB3B;margin:20px;">Verbinden Sie den Fuß 3,3 V auf keinen Fall direkt mit dem GND-Fuß durch Krokodilklemmen. Dies kann einen Kurzschluss verursachen und die HaloCode beschädigen.
</div>

<br>

<span style="color:white;font-size:18;border-radius:100%;background-color:lightblue;font-wieght:bold;">&nbsp; 3 &nbsp;</span> Schneiden und falten Sie Karton aus der Verpackung gemäß Abbildung.

<img src="../fr/images/clip-5.png" width="800px;" style="padding:5px 5px 20px 5px;">

<span style="color:white;font-size:18;border-radius:100%;background-color:lightblue;font-wieght:bold;">&nbsp; 4 &nbsp;</span> Kleben Sie das Kupferfolienklebeband am Karton.

<img src="../fr/images/clip-6.png" width="300px;" style="padding:5px 5px 20px 5px;">

<div style="background-color:#FFFFCC;padding:10px;border-left:5px solid #FFEB3B;margin:20px;">* Kupferfolienklebeband ist auf beide Seiten leitfähig. Die Krokodilklemme kann damit den Berührungsbereich verlängern und vergrößern. 
</div>

<br>

<span style="color:white;font-size:18;border-radius:100%;background-color:lightblue;font-wieght:bold;">&nbsp; 5 &nbsp;</span> Decken Sie den Schutzfilm auf dem Kupferfolienklebeband ab.

<img src="../fr/images/clip-7.png" width="300px;" style="padding:5px 5px 20px 5px;">

<div style="background-color:#FFFFCC;padding:10px;border-left:5px solid #FFEB3B;margin:20px;">* Der Schutzfilm schützt das Kupferfolienklebeband vor Oxidation.
</div>

<br>

<span style="color:white;font-size:18;border-radius:100%;background-color:lightblue;font-wieght:bold;">&nbsp; 6 &nbsp;</span> Klemmen Sie das andere Ende der Krokodilklemmen an das Kupferfolienklebeband.

<img src="../fr/images/clip-8.png" width="500px;" style="padding:5px 5px 20px 5px;">

### 3 Verbinden Sie die HaloCode mit dem Computer durch das Micro-USB-Kabel.

<img src="../fr/images/clip-9.png" width="600px;" style="padding:5px 5px 20px 5px;">

### 4 Starten Sie die Programmierung.

<img src="images/clip-11.png" width="600px;" style="padding:5px 5px 20px 5px;">

### Fertig!

Berühren Sie das Metallteil der roten Krokodilklemme oder das Kupferfolienklebeband. Sehen Sie, dass die Farb-LED rot leuchtet?

<img src="../fr/images/clip-10.png" width="600px;" style="padding:5px 5px 20px 5px;">

Sie haben bestimmt erlernt, wie man die Krokodilklemme verwendet. Stellen Sie sich mal vor, welche interessanten Projekte man noch verwirklichen kann!

