# Ausführung mit dem Zauberarmband

Das Armband kann zur Befestigung der HaloCode am Handgelenk verwendet werden. Mit farbigen LEDs, Berührungssensoren usw. sind viele interessante tragbare Artikel wie z.B. Schrittzähler zu erstellen.

Lassen Sie uns gemeinsam ein einfaches Projekt über einen tragbaren Artikel abschließen! Durch dieses Projekt lernen Sie, wie das Zauberarmband zum kreativen Herstellen verwendet wird.

Tragen Sie die HaloCode an der Hand.  Die Zufallsanzeige einer Farbe von Farb-LEDs ist zu realisieren, wenn das Schütteln der HaloCode vom Bewegungssensor erkannt wird.

<img src="../fr/images/strap-1.png" width="300px;" style="padding:5px 5px 20px 5px;">

### 1 Finden Sie die nötigen Artikel.

<img src="../fr/images/strap-2.png" width="800px;" style="padding:5px 5px 20px 5px;">

### 2 Programmieren Sie und laden Sie es auf die HaloCode hoch.

<img src="images/strap-3.png" width="800px;" style="padding:5px 5px 20px 5px;">

### 3 Legen Sie die Batterien ein.

<img src="../fr/images/strap-4.png" width="800px;" style="padding:5px 5px 20px 5px;">

### 4 Führen Sie die folgenden Schritte aus, um den Tragevorgang abzuschließen (die linke Hand als Beispiel).

<span style="color:white;font-size:18;border-radius:100%;background-color:lightblue;font-wieght:bold;">&nbsp; 1 &nbsp;</span> Verbinden Sie die Batterie und die HaloCode.

<img src="images/strap-5.png" width="600px;" style="padding:5px 5px 20px 5px;">

<span style="color:white;font-size:18;border-radius:100%;background-color:lightblue;font-wieght:bold;">&nbsp; 2 &nbsp;</span> Kleben Sie einen Klettverschluss an der hinteren Abdeckung  des Batteriefachs und an der Rückseite der HaloCode.

<img src="images/strap-6.png" width="600px;" style="padding:5px 5px 20px 5px;">

<span style="color:white;font-size:18;border-radius:100%;background-color:lightblue;font-wieght:bold;">&nbsp; 3 &nbsp;</span>

<img src="images/strap-7.png" width="800px;" style="padding:5px 5px 20px 5px;">

<span style="color:white;font-size:18;border-radius:100%;background-color:lightblue;font-wieght:bold;">&nbsp; 4 &nbsp;</span>

<img src="images/strap-8.png" width="600px;" style="padding:5px 5px 20px 5px;">

<span style="color:white;font-size:18;border-radius:100%;background-color:lightblue;font-wieght:bold;">&nbsp; 5 &nbsp;</span>

<img src="images/strap-9.png" width="800px;" style="padding:5px 5px 20px 5px;">

<span style="color:white;font-size:18;border-radius:100%;background-color:lightblue;font-wieght:bold;">&nbsp; 6 &nbsp;</span>

<img src="images/strap-10.png" width="600px;" style="padding:5px 5px 20px 5px;">

### Fertig!

Schalten Sie den Batterieschalter ein und schütteln Sie Ihre Hand. Sehen Sie, dass die Farb-LED leuchtet?

<img src="images/strap-11.png" width="600px;" style="padding:5px 5px 20px 5px;">

Sie haben bestimmt erlernt, wie man das Zauberarmband verwendet. Stellen Sie sich mal vor, welche interessanten Projekte man noch verwirklichen kann!
