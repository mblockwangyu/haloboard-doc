# Summary

## Docs

* [Standard Kit HaloCode](README.md)
    * [Verpackungsinhalt](parts.md)
    * [Kurzanleitung](quick-start.md)
    * [Überblick über Aussehen](appearance.md)
    * [Batterie](battery.md)
    * [Ausführung mit dem Zauberarmband](strap.md)
    * [Ausführung mit Krokodilklemmen](clip.md)
    * [FAQ](faq.md)
    * [Vorsicht](caution.md)