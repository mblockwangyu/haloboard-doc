# FAQ

### Nach dem Hochladen des Programms funktioniert die HaloCode nicht.

Da die Firmware-Version möglicherweise nicht auf dem neuesten Stand ist. Klicken Sie auf **Firmware-Upgrade** gemäß unterer Abbildung, um die Firmware zu aktualisieren.

<img src="images/update-halo.png" width="300px;" style="padding:5px 5px 20px 5px;"> <span style="font-size:30px;padding:5px;color:lightblue;">&rarr;</span><img src="images/update-halo-1.png" width="300px;" style="padding:5px 5px 20px 5px;">

### Wie ist es mit der Stromversorgung für die HaloCode?

Es gibt zwei Möglichkeiten, die HaloCode mit Strom zu versorgen:

1. Anschließen an externes Batteriefach durch Batterieanschluss
2. Anschließen an Ihren Computer oder Ihre mobile Stromversorgung mit einem Micro-USB-Kabel

Versorgungsspannungsanforderungen: DC 3-5V
