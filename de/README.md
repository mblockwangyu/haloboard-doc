# Standard Kit HaloCode

* [Verpackungsinhalt](parts.md)
* [Kurzanleitung](quick-start.md)
* [Überblick über Aussehen](appearance.md)
* [Batterie](battery.md)
* [Ausführung mit dem Zauberarmband](strap.md)
* [Ausführung mit Krokodilklemmen](clip.md)
* [FAQ](faq.md)
* [Vorsicht](caution.md)

### Mehr Hilfe und Ressourcen

Wir nehmen Ihre Erfahrungen sehr ernst. Wenn Sie Fragen oder Anregungen haben, nehmen Sie bitte Kontakt mit uns auf.

Technische Unterstützung: <support@makeblock.com>

Makeblock-Bildungswebseite: [http://education.makeblock.com/](http://education.makeblock.com/)