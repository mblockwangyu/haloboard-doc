# Languages

* [简体中文](zh/)
* [English](en/)
* [繁體中文](tc/)
* [日本語](jp/)
* [Deutsch](de/)
* [Español](es/)
* [Française](fr/)
* [Italiano](it/)
* [Português](pt/)
* [Pусский](ru/)
* [한국어](kr/)



