# Summary

## Docs

* [Equipamento padrão para Makeblock HaloCode](README.md)
    * [Lista de peças](parts.md)
    * [Guia de Iniciação Rápida](quick-start.md)
    * [Introdução à aparência](appearance.md)
    * [Pilha](battery.md)
    * [Criar com a alça de gancho e laço](strap.md)
    * [Criar com alicates de crocodilo](clip.md)
    * [Perguntas Frequentes](faq.md)
    * [Cuidado](caution.md)