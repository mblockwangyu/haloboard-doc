# Criar com alicates de crocodilo

As partes metálicas nas extremidades do clipe de crocodilo podem ser eletricamente condutivas. Com o sensor de toque da HaloCode e a fita dupla face de cobre condutor, vários itens de toque interessantes podem ser feitos, como a paleta de cores e interação com o mBlock 5.

Agora vamos fazer um projeto simples utilizando o HaloCode. Neste projeto, aprenderá a utilizar os alicates de crocodilo para criar um projeto.

Com o cartão no interior da embalagem e as folhas de cobre, as LED podem transmitir a cor correspondente quando tocar num alicate de crocodilo específico.

<img src="../fr/images/clip-1.png" width="300px;" style="padding:5px 5px 20px 5px;">

### Passo 1 Encontre os materiais que precisa.

<img src="images/clip-2.png" width="800px;" style="padding:5px 5px 20px 5px;">

### Passo 2 Junte os alicates de crocodilo.

<span style="color:white;font-size:18;border-radius:100%;background-color:lightblue;font-wieght:bold;">&nbsp; 1 &nbsp;</span> Junte o alicate de crocodilo vermelho ao sensor tátil 0.

<img src="../fr/images/clip-3.png" width="300px;" style="padding:5px 5px 20px 5px;">

<span style="color:white;font-size:18;border-radius:100%;background-color:lightblue;font-wieght:bold;">&nbsp; 2 &nbsp;</span> Junte os alicates de crocodilo amarelo, azul e verde ao sensor tátil 1, 2 e 3 respetivamente.

<img src="../fr/images/clip-4.png" width="300px;" style="padding:5px 5px 20px 5px;">

<div style="background-color:#FFFFCC;padding:10px;border-left:5px solid #FFEB3B;margin:20px;">NÃO utilize o clipe de crocodilo para ligar o pino de 3,3V ao pino GND, caso contrário pode ocorrer um curto circuito e o HaloCode pode ficar danificado.
</div>

<br>

<span style="color:white;font-size:18;border-radius:100%;background-color:lightblue;font-wieght:bold;">&nbsp; 3 &nbsp;</span> Corte o cartão no interior da embalagem e dobre-o conforme apresentado.

<img src="../fr/images/clip-5.png" width="800px;" style="padding:5px 5px 20px 5px;">

<span style="color:white;font-size:18;border-radius:100%;background-color:lightblue;font-wieght:bold;">&nbsp; 4 &nbsp;</span> Junte as folhas de cobre ao cartão.

<img src="../fr/images/clip-6.png" width="300px;" style="padding:5px 5px 20px 5px;">

<div style="background-color:#FFFFCC;padding:10px;border-left:5px solid #FFEB3B;margin:20px;">*As folhas de cobre são condutoras em ambos os lados. Anexar os alicates de crocodilo às folhas pode aumentar a área condutora. 
</div>

<br>

<span style="color:white;font-size:18;border-radius:100%;background-color:lightblue;font-wieght:bold;">&nbsp; 5 &nbsp;</span> Remova as películas protetoras nas folhas de cobre.

<img src="../fr/images/clip-7.png" width="300px;" style="padding:5px 5px 20px 5px;">

<div style="background-color:#FFFFCC;padding:10px;border-left:5px solid #FFEB3B;margin:20px;">*A película protetora é utilizada para evitar que a folha fique oxidada.
</div>

<br>

<span style="color:white;font-size:18;border-radius:100%;background-color:lightblue;font-wieght:bold;">&nbsp; 6 &nbsp;</span> Junte a outra ponta dos alicates de crocodilo às folhas de cobre.

<img src="../fr/images/clip-8.png" width="500px;" style="padding:5px 5px 20px 5px;">

### Passo 3 Ligue o HaloCode a um computador com o cabo micro USB.

<img src="../fr/images/clip-9.png" width="600px;" style="padding:5px 5px 20px 5px;">

### Passo 4 Inicie a codificação.

<img src="images/clip-11.png" width="600px;" style="padding:5px 5px 20px 5px;">

### Terminado!

Toque na peça de metal do alicate de crocodilo vermelho ou na folha de cobre para ver se os LED ficam vermelhos.

<img src="../fr/images/clip-10.png" width="600px;" style="padding:5px 5px 20px 5px;">

Agora sabe como utilizar os alicates de crocodilo. Use a sua imaginação para criar projetos mais divertidos.
