# Cuidado

- Ligue apenas o seu HaloCode a uma fonte de energia DC classificada de 3 Volts a 5 Volts.
- Evite manusear o HaloCode enquanto este está ligado a uma fonte de energia. O HaloCode deve ser desligado quando conectado a um circuito externo I/O.
- A corrente máxima seguramente fornecida a um circuito externo utilizando o I/O é de 10mA.
- A corrente máxima seguramente fornecida a um circuito externo utilizando a interface de 3,3V é de 500mA. Certifique-se de que este limite não é ultrapassado.
- A corrente máxima seguramente fornecida a um circuito externo utilizando a interface de expansão de bloco eletrónico é de 1A: apenas quando a entrada de corrente de energia é maior do que 3,5 A é que a corrente máxima será alcançada. Neste caso, podem ser ligados mais blocos eletrónicos ao HaloCode.
- Não utilize o seu HaloCode na água ou com as mãos molhadas.
- Opere o seu HaloCode numa sala suficientemente ventilada.
- Não guarde ou utilize o seu HaloCode em ambientes extremamente quentes ou frios.
- Não coloque quaisquer objetos de metal através dos circuitos impressos na placa visto que pode causar um curto circuito danificando o seu HaloCode. Isto pode causar risco de danos permanentes.
- Ligar o seu HaloCode a quaisquer periféricos não aprovados pode danificá-lo.
- Não deixe o seu HaloCode ligado a um computador ou a qualquer outro dispositivo sem supervisão.
- NÃO utilize o alicate de crocodilo para ligar o pino de 3,3V ao pino GND, caso contrário pode ocorrer um curto circuito e o HaloCode pode ficar danificado. 
