# Pilha

### Instalar as pilhas

<img src="../fr/images/install-battery.png" width="600px" style="padding:5px 5px 5px 5px;">

### Avisos

- Não puxe o cabo da bateria diretamente para desconectar a bateria. Use os dedos para apertar o conector e remova-o com cuidado.
- Não misture vários tipos de pilhas nem misture pilhas novas e usadas.
- Insira as pilhas da forma correta (com a polaridade correta).
- Remova a bateria gasta da caixa das pilhas.
- Não coloque qualquer objeto de metal através da cavidade das pilhas do HaloCode visto que isto pode provocar um curto circuito e aquecer a bateria.
- Não tente carregar pilhas normais (não recarregáveis).
