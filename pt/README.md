# Equipamento padrão para Makeblock HaloCode

* [Lista de peças](parts.md)
* [Guia de Iniciação Rápida](quick-start.md)
* [Introdução à aparência](appearance.md)
* [Pilha](battery.md)
* [Criar com a alça de gancho e laço](strap.md)
* [Criar com alicates de crocodilo](clip.md)
* [Perguntas Frequentes](faq.md)
* [Cuidado](caution.md)

### Ajuda e Recursos

Importamo-nos como você se sente  em relação ao nosso produto e como você usa o nosso produto. Não hesite em nos contatar caso tenha algum problema ou sugestão.

Suporte técnico: <support@makeblock.com>

Educação Makeblock: [http://education.makeblock.com/](http://education.makeblock.com/)