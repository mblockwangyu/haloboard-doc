# Criar com a alça de gancho e laço

Com a alça de gancho e laço pode prender o HaloCode ao seu pulso. As LED e o sensor tátil no HaloCode permitem-lhe torná-lo num dispositivo utilizável, como um pedómetro.

Vamos tornar o HaloCode num dispositivo utilizável. Neste projeto, aprenderá a criar um projeto utilizando a alça de gancho e laço.

Isto é o que encontrará: HaloCode utilizável no seu pulso. Conforme programado, as LED transmitem uma cor aleatória quando o sensor de movimento no HaloCode deteta os movimentos da sua mão.

<img src="../fr/images/strap-1.png" width="300px;" style="padding:5px 5px 20px 5px;">

### Passo 1 Encontre os materiais que precisa.

<img src="../fr/images/strap-2.png" width="800px;" style="padding:5px 5px 20px 5px;">

### Passo 2 Escreva o código e carregue-o para o HaloCode.

<img src="images/strap-3.png" width="800px;" style="padding:5px 5px 20px 5px;">

### Passo 3 Coloque as pilhas no suporte de pilhas. 

<img src="../fr/images/strap-4.png" width="800px;" style="padding:5px 5px 20px 5px;">

### Passo 4 Coloque o HaloCode no seu pulso conforme é apresentado abaixo (mão esquerda como exemplo). 

<span style="color:white;font-size:18;border-radius:100%;background-color:lightblue;font-wieght:bold;">&nbsp; 1 &nbsp;</span> Ligue o suporte de pilhas ao HaloCode.

<img src="images/strap-5.png" width="600px;" style="padding:5px 5px 20px 5px;">

<span style="color:white;font-size:18;border-radius:100%;background-color:lightblue;font-wieght:bold;">&nbsp; 2 &nbsp;</span> Coloque os fechos de gancho e laço na caixa deslizante do suporte de pilhas e na parte de trás do HaloCode. 

<img src="images/strap-6.png" width="600px;" style="padding:5px 5px 20px 5px;">

<span style="color:white;font-size:18;border-radius:100%;background-color:lightblue;font-wieght:bold;">&nbsp; 3 &nbsp;</span>

<img src="images/strap-7.png" width="800px;" style="padding:5px 5px 20px 5px;">

<span style="color:white;font-size:18;border-radius:100%;background-color:lightblue;font-wieght:bold;">&nbsp; 4 &nbsp;</span>

<img src="images/strap-8.png" width="600px;" style="padding:5px 5px 20px 5px;">

<span style="color:white;font-size:18;border-radius:100%;background-color:lightblue;font-wieght:bold;">&nbsp; 5 &nbsp;</span>

<img src="images/strap-9.png" width="800px;" style="padding:5px 5px 20px 5px;">

<span style="color:white;font-size:18;border-radius:100%;background-color:lightblue;font-wieght:bold;">&nbsp; 6 &nbsp;</span>

<img src="images/strap-10.png" width="600px;" style="padding:5px 5px 20px 5px;">

### Concluído!

Agora ligue o interruptor das pilhas. Abane a sua mão para ver as LED ligadas!

<img src="images/strap-11.png" width="600px;" style="padding:5px 5px 20px 5px;">

Agora sabe como utilizar a alça de gancho e laço. Use a sua imaginação para criar projetos mais divertidos.  

