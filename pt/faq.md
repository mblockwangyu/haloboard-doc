# Perguntas Frequentes

### Porque é que o programa não funciona conforme esperado após carregar o programa para o HaloCode?

O motivo pode ser que a versão do firmware não está atualizada. Clique em **Atualizar** para atualizar o firmware.

<img src="images/update-halo.png" width="300px;" style="padding:5px 5px 20px 5px;"> <span style="font-size:30px;padding:5px;color:lightblue;">&rarr;</span><img src="images/update-halo-1.png" width="300px;" style="padding:5px 5px 20px 5px;">

### Como ligar o HaloCode?

Há dois métodos para ligar o HaloCode:

1. Conecte a uma caixa de pilhas através da interface das pilhas. 
2. Conecte-o a um computador ou carregador portátil usando um cabo micro USB.

Requisitos de tensão da alimentação: 3-5 V DC
