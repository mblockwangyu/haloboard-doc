# Guia de Iniciação Rápida

## 1. Baixe o software

Para baixar a última versão do mBlock 5, visite: [http://www.mblock.cc](http://www.mblock.cc).

<img src="../fr/images/download.png" width="600px;" style="padding:5px 5px 20px 5px;">

## 2. Conecte o HaloCode a um computador

Conecte o HaloCode ao seu computador através de um cabo micro USB.

<img src="../fr/images/connect.png" width="600px;" style="padding:5px 5px 20px 5px;">

## 3. Inicie a programação

<span style="color:white;font-size:18;border-radius:100%;background-color:lightblue;font-wieght:bold;">&nbsp; 1 &nbsp;</span> Abra o mBlock 5 e adicionao HaloCode a partir da Biblioteca de Dispositivos.

<table style="table-layout:fixed;width:100%;text-align:center;" cellpadding=20px; cellspacing=12px;>
<tr>
<td width="50%"><img src="images/connect-halo-1.png" height="120px;" style="padding:5px 5px 5px 5px;"></td>
<td width="10%;"><span style="font-size:30px;padding:5px;color:lightblue;">&rarr;</span></td>
<td width="40%"><img src="../fr/images/connect-halo-2.png" height="120px;" style="padding:5px 5px 5px 5px;"></td>
</tr>
</table>

<span style="color:white;font-size:18;border-radius:100%;background-color:lightblue;font-wieght:bold;">&nbsp; 2 &nbsp;</span> Conecte o dispositivo.

<table style="table-layout:fixed;width:100%;text-align:center;" cellpadding=20px; cellspacing=12px;>
<tr>
<td width="50%"><img src="images/connect-halo-3.png" height="120px;" style="padding:5px 5px 5px 5px;"></td>
<td width="10%;"><span style="font-size:30px;padding:5px;color:lightblue;">&rarr;</span></td>
<td width="40%"><img src="images/connect-halo-4.png" height="120px;" style="padding:5px 5px 5px 5px;"></td>
</tr>
</table>

<span style="color:white;font-size:18;border-radius:100%;background-color:lightblue;font-wieght:bold;">&nbsp; 3 &nbsp;</span> Agora vamos concluir uma tarefa de programação! Tente fazer com que as LED no HaloCode fiquem vermelhas.

<div style="margin:20px;">
<p>1) Adicione um bloco de evento conforme apresentado abaixo.</p>
<img src="images/program-1.png" width="600px;" style="padding:0px 5px 15px 5px;">
<p>2) Adicione o bloco roxo abaixo a partir da categoria blocos de iluminação. Depois altere a cor para vermelho.</p>
<img src="images/program-2.png" width="600px;" style="padding:0px 5px 15px 5px;">
</div>

## 4. Executa o Programa

Você pode escolher carregar o programa para o HaloCode ou executar o programa no mBlock 5. 

### Carregue o programa no HaloCode: execute o programa sem software.

Ative o modo de Carregar e clique em Carregar. Após o carregamento ser bem sucedido, prima o botão azul no HaloCode para verificar se as LED ficam continuamente vermelhas.

<img src="images/program-3.png" style="padding:5px 5px 5px 5px;">

### Execute o programa no mBlock: Não é necessário carregar. Pode ver como é que o seu código trabalha exatamente em tempo real.

Desative o **modo Carregar e altere a cor no seu programa de vermelho para verde**. Prima o botão azul no HaloCode para verificar se as LED alteram de cor.

<img src="images/program-4.png" style="padding:5px 5px 5px 5px;">