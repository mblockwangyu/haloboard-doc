# Get Started

This section includes a quick start guide to Halocode.

## Connect Halocode

1\. Under "Devices", click "+" to add device.

<img src="images/get-started-1.png" style="padding:5px 5px 20px 5px;">

2\. From the pop-up Device Library page, select "Halocode" and click "OK".

<img src="images/get-started-2.png" width="800px;" style="padding:5px 5px 20px 5px;">

<div style="background-color:#E7F1F9;padding:20px;border-top:5px solid #6EACDB;font-size:14px;line-height:28px;margin-left:10px;margin-bottom:15px;border-radius:5px;width:360px;"><strong>Tip:</strong><br>
&#9755; Click &star; to set Halocode as the mostly used device.<br>
<img src="images/frequent-device.png" style="padding:16px 0px 15px 10px;"><br>
</div>

3\. Use a micro-USB cable to connect your Halocode to a USB port of your computer.

<img src="images/get-started-3.png" width="300px;" style="padding:5px 5px 20px 5px;">

4\. Under "Devices", choose "Halocode", and click "Connect".

<img src="images/get-started-4.png" style="padding:5px 5px 20px 5px;">

5\. The Device Connection window will pop up. The serial port of Halocode will automatically be detected. Please click "Connect".

<img src="images/get-started-5.png"  style="padding:5px 5px 20px 5px;">

6\. Toggle on Upload Mode.

<img src="images/upload-mode-1.png" style="padding:5px 5px 20px 5px;">

## Create a Halocode Project

Let's start with a simple project. When we shake Halocode, the LED ring will light up and then go off.

1\. Drag an Events block <span style="background-color:#FFBF00;color:white;padding:4px; font-size: 12px;border-radius:3px;">when HaloCode is shaking</span> to the Scripts area.

<img src="images/get-started-6.gif" width="800px;" style="padding:5px 5px 20px 5px;">

2\. Add a Lighting block <span style="background-color:#8521D3;color:white;padding:4px; font-size: 12px;border-radius:3px;">show ()</span> to control the LED ring of Halocode.

<img src="images/get-started-7.gif" width="800px;" style="padding:5px 5px 20px 5px;">

3\. Add a Control block <span style="background-color:#FFAB19;color:white;padding:4px; font-size: 12px;border-radius:3px;">wait () seconds</span>, and a Lighting block <span style="background-color:#8521D3;color:white;padding:4px; font-size: 12px;border-radius:3px;">light off all the LEDs</span> to make the LED ring go off after 1 second.

<img src="images/get-started-8.gif" width="800px;" style="padding:5px 5px 20px 5px;">

4\. Click "Upload" to upload the program to Halocode.

<img src="images/get-started-9.gif" width="800px;" style="padding:5px 5px 20px 5px;">

5\. Try shaking Halocode!

![](get-started-10.gif)