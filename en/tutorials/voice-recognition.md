# Speech Recognition

With Halocode's microphone and built-in Wi-Fi, we can make a simple speech recognition application. The internet enables Halocode to use Microsoft's speech recognition service. You need to sign in/up to mBlock 5 to connect your Halocode to the internet.

## Sign in to mBlock 5

Halocode needs to connect to the internet to use online speech recognition service. We need to sign in to mBlock 5 first.

<img src="images/login.gif" width="800px;" style="padding:5px 5px 20px 5px;">

## Toggle on Upload mode

Click to toggle on Upload mode.

<img src="images/toggle-on-upload.gif" width="800px;" style="padding:5px 5px 20px 5px;">

## Connect to the internet

1\. Drag an Events block <span style="background-color:#FFBF00;color:white;padding:4px; font-size: 12px;border-radius:3px;">when Halocode starts up</span> and a Wi-Fi block <span style="background-color:#76CE14;;color:white;padding:4px; font-size: 12px;border-radius:3px;">connect to Wi-Fi () password ()</span>. Input the Wi-Fi name and password.

<img src="images/connect-wifi-1.gif" width="800px;" style="padding:5px 5px 20px 5px;">

2\. We want to know when the Wi-Fi is successfully connected. Add a Control block <span style="background-color:#FFAB19;color:white;padding:4px; font-size: 12px;border-radius:3px;">wait ()</span>, a Wi-Fi block <span style="background-color:#76CE14;color:white;padding:4px; font-size: 12px;border-radius:3px;">Wi-Fi is connected?</span>, and a Lighting block <span style="background-color:#8521D3;color:white;padding:4px; font-size: 12px;border-radius:3px;">all LEDs light up ()</span>.

<img src="images/connect-wifi-2.gif" width="800px;" style="padding:5px 5px 20px 5px;">

## Speech recognition

When the button is pressed, Halocode will start speech recognition and all LEDs will light up white. The recognition process lasts for 3 seconds. When it's done, all LEDs will go off as a signal.

3\. Add an Events block <span style="background-color:#FFBF00;color:white;padding:4px; font-size: 12px;border-radius:3px;">when button is pressed</span> and a Lighting block <span style="background-color:#8521D3;color:white;padding:4px; font-size: 12px;border-radius:3px;">all LEDs light up ()</span>. Change the color to white. Then add a Wi-Fi block <span style="background-color:#76CE14;color:white;padding:4px; font-size: 12px;border-radius:3px;">recognize (English) for (3) seconds</span>.

<img src="images/speech-recognition-1.gif" width="800px;" style="padding:5px 5px 20px 5px;">

4\. Add a Lighting block <span style="background-color:#8521D3;color:white;padding:4px; font-size: 12px;border-radius:3px;">light off all LEDs</span> to light off all LEDs after the speech recognition is done.

<img src="images/speech-recognition-2.gif" width="800px;" style="padding:5px 5px 20px 5px;">

## Recognize "red"

5\. Add a Control block <span style="background-color:#FFAB19;color:white;padding:4px; font-size: 12px;border-radius:3px;">if () then ()</span> and an Operators block <span style="background-color:#59C059;color:white;padding:4px; font-size: 12px;border-radius:3px;">() contains ()?</span>. Add Wi-Fi block <span style="background-color:#76CE14;color:white;padding:4px; font-size: 12px;border-radius:3px;">speech recognition result</span> to the first box and input "red" to the second box.

<img src="images/speech-recognition-3.gif" width="800px;" style="padding:5px 5px 20px 5px;">

6\. If the speech recognition result contains "red", all LEDs will light red, and go off in 1 second. Add these blocks: <span style="background-color:#8521D3;color:white;padding:4px; font-size: 12px;border-radius:3px;">all LEDs light up ()</span>, <span style="background-color:#FFAB19;color:white;padding:4px; font-size: 12px;border-radius:3px;">wait () seconds</span>, and <span style="background-color:#8521D3;color:white;padding:4px; font-size: 12px;border-radius:3px;">light off all LEDs</span>.

<img src="images/speech-recognition-4.gif" width="800px;" style="padding:5px 5px 20px 5px;">

7\. Click "Upload" to upload the program to Halocode.

<img src="images/upload-program.gif" width="800px;" style="padding:5px 5px 20px 5px;">

8\. Wait until all the LEDs light up green. Then press the button and say "red". Check the LEDs of Halocode.

<p><span style="background-color:#66CDAA;line-height:30px;padding:12px;font-size:16px;border-radius:3px;"><a href="speech-recognition.mblock" download style="color:white;font-weight:bold;">Download code</a></span></p>

---