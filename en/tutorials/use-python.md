# Use Python with Halocode

Halocode is a MicroPython-based single board computer.  
Python code need to be uploaded to Halocode to run.

## Start Using Python

Switch the programming mode from "Blocks" to "Python" to start using Python.

<img src="images/use-python-1.png" style="padding:5px 5px 20px 5px;">

**Note: please make sure that "Halocode" is currently selected.**

<img src="images/use-python-3.png" style="padding:5px 5px 20px 5px;">

Here's an example code:

```py
import halocode
import event

@event.start
def on_start():
    halocode.led.show_all(122, 239, 10)
    time.sleep(3)
    halocode.led.off_all()
```

After programming, click "Upload" to upload the program to Halocode.

<img src="images/use-python-2.png" style="padding:5px 5px 20px 5px;">

## Convert Blocks to Python Code

In the Scripts area, click &nbsp; <img src="../../zh/tutorials/images/use-python-5.png" width="20px"> &nbsp; to covert blocks to Python. The following is an example:

<img src="images/use-python-4.png" style="padding:5px 5px 20px 5px;">

## Use Halocode's LEDs

**LED Ring**

The ID and position of each of the 12 LEDs are as follows:

![](../python-api/led-id.png)

### led.show_all(r, g, b)

Set the color of all the LEDs, mixed by red, green, and blue, each color with a value range 0-255.

```py
halocode.led.show_all(255, 0, 0) # Set all the LEDs to color red
```

### led.off_all()

Turn off all the LEDs.

```py
halocode.led.off_all() # Turn off all the LEDs
```

### led.show_ring(color)

Set the color of all 12 LEDs at the same time. There are ten colors: red, green, blue, yellow, cyan, purple, white, orange, black, and gray.

```py
halocode.led.show_ring('red orange yellow green cyan blue purple white white white white white') # Set the 12 LEDs to color red, orange, yellow, green, cyan, blue, purple, white, white, white, white, and white respectively
```

### led.show_single(led_id, r, g, b)

Set the color of one specified LED.

```py
halocode.led.show_single(1, 255, 0, 0) # Set the color of the first LED to red
```

### led.off_single(led_id)

Turn off one specified LED.

```py
halocode.led.off_single(4) # Turn off the fourth LED
```

### led.show_animation(name)

Show the default LED animation. There are four options: `spoondrift`, `meteor`, `rainbow`, and `firefly`.

```py
halocode.led.show_animation('rainbow') # Show the "rainbow" LED animation
```

### led.ring_graph(percentage)

Use the status of the LED ring to display percentage.

```py
halocode.led.ring_graph(60) # Use the LED ring to display 60%
```

## Use the Sensors of Halocode

### button.is_pressed()

If the button is pressed, return True; otherwise, return False.

```py
print(halocode.button.is_pressed()) # Print True if the button is pressed
```

### microphone.get_loudness()

Get the loudness of the microphone. The range is 0-100.

```py
print(halocode.microphone.get_loudness()) # Output the loudness
```

### motion_sensor.is_shaked()

Tell whether Halocode is being shaken. The result is True or False.

```py
print(halocode.motion_sensor.is_shaked()) # If Halocode is being shaken, return True
```

### motion_sensor.is_tilted_left()/motion_sensor.is_tilted_right()

Tell whether Halocode is tilted to the right or left. The result is True or False.

```py
print(halocode.motion_sensor.is_tilted_left()) # If Halocode is left-tilted, return True
```

### motion_sensor.is_arrow_up()/motion_sensor.is_arrow_down()

Tell whether Halocode is placed arrow-up or arrow-down. The result is True or False.

```py
print(halocode.motion_sensor.is_arrow_up()) # If Halocode is placed arrow-up, return True
```

### Gyroscope

Get the roll, pitch or yaw value of Halocode's gyroscope
- motion_sensor.get_roll()
- motion_sensor.get_pitch()
- motion_sensor.get_yaw()

Get the gyroscope's rotation value (in degrees) around a certain axis.
- motion_sensor.get_rotation(axis): x, y, or z axis
- motion_sensor.reset_rotation(axis="all"): reset the rotation angles of the gyro

You can use motion_sensor.get_shake_strength() to get the intensity of the shaking.

```py
print("The yaw and pitch is:", halocode.motion_sensor.get_yaw(), halocode.motion_sensor.get_pitch()) # output the yaw and pitch value of the gyro
print("The rotation value is:", halocode.motion_sensor.get_rotation(x), halocode.motion_sensor.get_rotation(y), halocode.motion_sensor.get_rotation(z))
halocode.motion_sensor.reset_roation() # reset the rotation value.
print("Shake strength:", halocode.motion_sensor.get_shake_strength())
```

### get_timer()

Get the timer value in seconds (since startup or last reset).

```py
print(halocode.get_timer()) # print the timer value since startup or last reset
```

### reset_timer()

Reset the timer.

```py
halocode.reset_timer() 
print(halocode.get_timer()) # prints 0
```

## Halocode's Event and Flow Control

Halocode supports events (like when the button is pressed), and it also supports multi-threading.  

If you wish to use event, declare a function and register it to the event. A program can only register no more than 6 event functions.

Example: 
```py
def on_button_pressed(): # define a function
    print("The button is pressed!")

halocode.on_button_pressed() # register it to "when button is pressed" event
```

### on_button_pressed()

When the button is pressed, run the function.

```py
def on_button_pressed():
    print("The button is pressed!")

halocode.on_button_pressed()
```

### on_shaked()

When Halocode is being shaken, call the function.

```py
def on_shaked():
    print("I'm shaken!")

halocode.on_shaked()
```

### on_tilted_left()

Call the function when Halocode is tilted to the left.

```py
def on_tilted_left():
    print("I'm left-tilted!")

halocode.on_tilted_left()
```

### on_greater_than(volume, 'microphone')

When the loudness is over a certain value, call the function.

```py
def on_greater_than():
    print("The loudness is over 50! Too loud!")

halocode.on_greater_than(50, 'microphone')
```

### on_received(message_name)

When the specified message is received, call the function.

```py
def on_received():
    print("Game start!")

halocode.on_received("game_start")
```