# Code with Mu: A Simple Python Editor

Mu is a simple Python editor for beginner programmers. With the support of Mu, you can write Python and Micro Python code in the editor to control the Halocode and other electronic modules.

In addition to the basic features of Mu, this version supports more features. Like  firmware upgrade, code uploading, online debugging through the REPL controller, and real-time viewing of variables and sensor values through plotter.

## Preparation Before Use
Visit the website below to download the latest version of Mu and firmware of Halocode from GitHub:
https://github.com/FFtust/mu/releases/tag/V0.0.1
<img src="images/download.png" style="padding:5px 5px 20px 5px;">
Unzip the file **mu-editor.rar** after downloading.
<img src="images/unzip.png" style="padding:5px 5px 20px 5px;">

## Quick Start Guide
After installation, the programming learning journey will begin!
### 1、Connect Halocode to your computer
Connect Halocode to your computer via the Micro USB cable.
<img src="images/connect.png" style="padding:5px 5px 20px 5px;">
Note: You can also connect Halocode to your computer via Makeblock Bluetooth dongle.

### 2、Open the Software
Open the unzipped folder **mu-editor**, then double-click on the the file **.main.exe** to open Mu.
<img src="images/open-mu.png" style="padding:5px 5px 20px 5px;">

### 3、Select Mode
From the pop-up Select Mode page, select **Makeblock Halocode** and click **OK**.
<img src="images/mode.png" style="padding:5px 5px 20px 5px;">

Note：You can also click **Mode** to select mode.

### 4、Update the Firmware
Before using the Mu editor, you need to use the latest Halocode firmware that we haven't released yet. Click **Firmware** and select the file **firefly_firmware_25_01_004-ht1.bin** from the pop-up dialog box. Then you can check the upgrade progress in the lower left corner of the software.
<img src="images/update-firmware.png" style="padding:5px 5px 20px 5px;">
<img src="images/update-progress.png" style="padding:5px 5px 20px 5px;">
### 5、Start Programming
Now let's complete a programming task! Try to make the LEDs on the Halocode turn green. Here is the sample code:
```py
import halo
halo.led.show_all(0, 255, 0)
```
Note：Please refer to [Python API Reference](http://docs.makeblock.com/halocode/en/python-api/python-api.html) to learn more about the function instructions.

### 6、Upload Your Program to the Halocode
Click **Uploading** to upload your program to the Halocode, then check if the LEDs turn green?
<img src="images/upload.png" style="padding:5px 5px 20px 5px;">
<img src="images/led-green.png" width="300px" style="padding:5px 5px 20px 5px;">

## More Features: Use the REPL Platform to Debug the Program
### Reporting information
You can use the **print()** function in your program as long as the Halocode is connected to your computer (via the Micro USB cable or Makeblock Bluetooth dongle).Then check the real-time returns of **print()** in the REPL platform.

Here is a simple, write code as shown below and upload it to the Halocode, then click **REPL platform** and check the changes of microphone values in the platform.
```py
import halo
while True:
    print(halo.microphone.get_loudness("maximum"))
```
<img src="images/controller.png" style="padding:5px 5px 20px 5px;">

### Sending a Command
You can also use the platform to debug the program in real time.

Here is a simple, write code in the platform and press the enter key, now check if the LEDs of Halocode turn red?
<img src="images/controller-adjust.png" style="padding:5px 5px 20px 5px;">
You can also continue to send commands to check the loudness detected by the microphone:
<img src="images/controller-adjust-2.png" style="padding:5px 5px 20px 5px;">


## More Features: Plotter

### Check the variable values in real time
The plotter can draw variable values or sensor output values into a graph to make it easier to check the changes of the values.

Here is a simple, write code as shown below and upload it to the Halocode, click **Plotter** to check the value of microphone in real time.
```py
import halo
while True:
    halo.print_plot(halo.microphone.get_loudness())
    time.sleep(0.1)
```
<img src="images/plotter.png" style="padding:5px 5px 20px 5px;">

Note：When you connect Halocode to the computer via Makeblock Bluetooth dongle, the plotter may have some delays due to the limit of Bluetooth communication speed.

### Check changes of multiple variables simultaneously

The plotter supports checking the changes of multiple variables simultaneously. 

Here is a simple, write code as shown below and upload it to the Halocode, then click **Plotter** to check the changes of loudness and i%100.

```py
import halo
i=1
while True:
    halo.print_plot(halo.microphone.get_loudness(),i%100)
    i=i+1
    time.sleep(0.1)
```
<img src="images/plotter-2.png" style="padding:5px 5px 20px 5px;">

## More Documentation
### Basic documentation
If you want to implement more features of Halocode and its extension modules in Mu, please refer to [Python API Reference](http://docs.makeblock.com/halocode/en/python-api/python-api.html).

**Note: We are simplifying our APIs to improve the user experience. (API is too long to use!) It will be expected to be completed in August. We will keep the current APIs as possible as we can though some changes will take place.**

If you are not familiar with Python APIs of Halocode, please refer to [Convert Blocks to Python Code](http://docs.makeblock.com/halocode/en/tutorials/use-python.html#convert-blocks-to-python-code).

### Advanced documentation
The Halocode has powerful performance and networking capabilities, and we provide some sophisticated APIs for you, so you can build exciting hardware projects with the Halocode and its associated electronic modules, and learn from such things as network communication, IoT applications, APP development, etc. But be aware that these features are often more difficult to navigate.

The Halocode has powerful performance and networking capabilities, and we provide some sophisticated APIs for you, so you can build exciting hardware projects with the Halocode and its associated electronic modules, and learn from such things as network communication, IoT applications, APP development, etc. But be aware that these features are often more difficult to navigate.

**network**

Micropython has implemented a network module and provides the corresponding documentation, which can be found at the following link:
http://docs.micropython.org/en/latest/library/network.html

Using the APIs in the network will help you make fully use of the Halocode networking capabilities.

**network2**

Using the APIs in the network will help you make fully use of the Halocode networking capabilities.

Default config:
```py
DEFAULT_AP_IP    = "192.168.4.1"
DEFAULT_STA_IP   = "192.168.4.2"
DEFAULT_NETMARK  = "255.255.255.0"
DEFAULT_GATEWAY  = "192.168.1.1"
DEFAULT_DNS      = "172.16.50.20"
DEFAULT_AUTHMODE = AUTH_WPA2_PSK
DEFAULT_PORT     = 5050
```
**API Reference**
```py
network2.config_ap(ssid, password)
```
Set the Halocode as an AP.

Note: The Halocode can not be AP and STA at the same time currently.

<table border="1" cellpadding="10px">
<tr>
<td>Parameters:</td>
<td>ssid (str) – wifi ssid
<br/>password(str) – wifi password</td></tr>
<tr>
<td>Returns:</td>
<td>None</td></tr>
</table>

```py
config_sta(ssid, password)
```
Set the Halocode as an STA.

Note: The Halocode can not be AP and STA at the same time currently.

<table border="1" cellpadding="10px">
<tr>
<td>Parameters:</td>
<td>ssid (str) – wifi ssid<br/>
password(str)  – wifi password</td></tr>
<tr>
<td>Returns:</td>
<td>None</td></tr>
</table>

```py
is_connected()
```
Check first whether the Halocode is connected to other devices through WIFI. The API is effective when the Halocode functions as an AP or STA. Therefore, you can always obtain the connection status of the Halocode through this API.
<table border="1" cellpadding="10px">
<tr>
<td>Returns:</td>
<td>None</td></tr>
</table>

```py
set_ip(ip)
```
Set the IP address of the Halocode. The Halocode has a default IP address before using this API.
DEFAULT_AP_IP    = "192.168.4.1"<br/>
DEFAULT_STA_IP   = "192.168.4.2"
<table border="1" cellpadding="10px">
<tr>
<td>Parameters:</td>
<td>ip (str) – ipv4</td></tr>
<tr>
<td>Returns:</td>
<td>None</td></tr>
</table>

```py
get_ip()
```
Gain Halocode own IP address.
<table border="1" cellpadding="10px">
<tr>
<td>Returns:</td>
<td>	
ipv4</td></tr>
</table>

```py
set_subnet_mark(mark)
```
Set Halocode subnet mask.
<table border="1" cellpadding="10px">
<tr>
<td>Parameters:</td>
<td>mark (str) – wifi mark, 255.255.255.0 as defualt</td></tr>
<tr>
<td>Returns:</td>
<td>None</td></tr>
</table>

```py
get_subnet_mark()
```
Get Halocode subnet mask.

```py
set_gateway(gw)
```
Get Halocode subnet mask.
<table border="1" cellpadding="10px">
<tr>
<td>Parameters:</td>
<td>	
gateway (str) –</td></tr>
<tr>
<td>Returns:</td>
<td>None</td></tr>
</table>

```py
get_gateway()
```
Get Halocode gateway.

```py
create_client()
```
Set Halocode as a client.

```py
client_connect_to(ip_to, port = DEFAULT_PORT)
```
Connect the Halocode to the server with the specified IP address and port.

```py
create_server(port = DEFAULT_PORT)
```
Set Halocode as a server.

```py
server_wait_connection(port = DEFAULT_PORT)
```
This API blocks the thread until a client connects to the server via a specific port, which returns information about the client.

```py
server_get_connections(port = DEFAULT_PORT)
```
Get the IP addresses of all connected clients in list format.

```py
server_get_latest_connection(port = DEFAULT_PORT)
```
Get the IP address of the client that was last connected.

```py
write(data, mode, ip_to, port = DEFAULT_PORT)
```
Send data.

```py
write_line(data, mode, ip_to, port = DEFAULT_PORT)
```
Send data line.

```py
read(mode, ip_from, port = DEFAULT_PORT)
```
Accept data.

```py
read_line(mode, ip_from, port = DEFAULT_PORT)
```
Accept data line.

**communication**

We have developed the serial communication interface of the Ring Board, so that you can use these interfaces with a variety of hardware with serial communication capability (such as Raspberry Pie, some camera modules) .
<img src="images/communication.png" style="padding:5px 5px 20px 5px;">

```py
disable_channel_default(channel)
```
The Halocode communicates with its electronic module through the hardware serial port. The specific communication protocol is used between the two. If the communication protocol is to be closed, the serial port command of the optical ring board can be “customized”. Please use this instruction.
<table border="1" cellpadding="10px">
<tr>
<td>Parameters:</td>
<td>	
channel (str) – “uart1/ble”</td></tr>
<tr>
<td>Returns:</td>
<td>None</td></tr>
</table>

```py
enable_channel_default(channel)
```
The Halocode communicates with its electronic module through the hardware serial port. A specific communication protocol is used between the two. To restore the communication command, use this command.
<table border="1" cellpadding="10px">
<tr>
<td>Parameters:</td>
<td>	
channel (str) – “uart1/ble”</td></tr>
<tr>
<td>Returns:</td>
<td>None</td></tr>
</table>

```py
read(channel)
```
Read data through a specific channel.
<table border="1" cellpadding="10px">
<tr>
<td>Parameters:</td>
<td>	
channel (str) – “uart1/ble”</td></tr>
<tr>
<td>Returns:</td>
<td>bytearray</td></tr>
</table>

```py
send(channel, data)
```
Send data through the set channel.
<table border="1" cellpadding="10px">
<tr>
<td>Parameters:</td>
<td>	
channel (str) – “uart1/ble”<br/>
data (bytearray) – the data to send</td></tr>
<tr>
<td>Returns:</td>
<td>bytearray</td></tr>
</table>
