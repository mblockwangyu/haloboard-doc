![](halocode.png)

# Introduction

Makeblock Halocode is a powerful single board computer, with built-in Wi-Fi and versatile electronic modules. Specially designed for programming education, Halocode is extremely beginners-friendly. Creation can be done through simple coding. Use mBlock 5 with Halocode. You can start with graphical programming, simple as building blocks, and further delve into textual programming, the cutting-edge Python. Wireless connectivity enables Halocode to connect to the internet, to realize IoT functions. You can make your own smart-home devices.

![](halocode-01.png)
![](haloboard-2.png)
![](halocode-3.png)
![](halocode-4.png)

### Use mBlock 5

Use mBlock 5 to play with Halocode. You can make all kinds of creations, such as a wearable somatosensory pedometer, a touch sensitive light box, and more.

<p style="text-align:left;"><img style="width:600px;" src="mblock5.png"></p>


- **mBlock 5 PC**: visit [http://www.mblock.cc/mblock-software/](http://www.mblock.cc/mblock-software/) to download
- **mBlock 5 Web**: [https://ide.mblock.cc/](https://ide.mblock.cc/)
- **mBlock App**: search for "mBlock" on any app store to download and install

<small>Note: mBlock 5 Web requires an extra software mLink to connect to your devices. Please refer to <a href="http://www.mblock.cc/doc/en/part-one-basics/mlink-quick-start-guide.html" target="_blank">mLink Guide</a>。</small>