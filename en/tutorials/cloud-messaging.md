# User Cloud Message

User Cloud Message syncs your data across devices with your mBlock 5 account. As long as your Halocode is connected to the internet, you can program it remotely.

## Sign UP / Sign In to mBlock 5

Click the sign in/sign up icon at the tool bar, and then follow the instructions to finish signing up or signing in.

<img src="images/login.gif" width="800px;" style="padding:5px 5px 20px 5px;">

## Toggle on Upload mode

Click to toggle on Upload mode.

<img src="images/toggle-on-upload.gif" width="800px;" style="padding:5px 5px 20px 5px;">

## Use Stage Button to Control Halocode Remotely

We will create a new project combined with Scratch Stage Programming. Use the button on stage to control Halocode remotely.

### Add User Cloud Message Blocks

Under "Sprites", click "+" in the Blocks area. The Extension Center page will pop up. Click "Add" to add User Cloud Message blocks.

<img src="images/user-cloud-message-1.gif" width="800px;" style="padding:5px 5px 20px 5px;">

### Step One: Program Stage Button

1\. Under "Sprites", click "&times;" to delete "Panda" (the default sprite).

<img src="images/user-cloud-message-2.gif" width="800px;" style="padding:5px 5px 20px 5px;">

2\. Click "+" to add a new sprite. From the pop-up Sprite Library page, search and choose "Empty Button1". Click "OK".

<img src="images/user-cloud-message-3.gif" width="800px;" style="padding:5px 5px 20px 5px;">

3\. Choose "Empty Button1", and drag an Events block <span style="background-color:#FFBF00;color:white;padding:4px; font-size: 12px;border-radius:3px;">when this sprite clicked</span> to the Scripts area.

<img src="images/user-cloud-message-4.gif" width="800px;" style="padding:5px 5px 20px 5px;">

4\. Add a User Cloud Message block <span style="background-color:#4EAA83;color:white;padding:4px; font-size: 12px;border-radius:3px;">send user cloud message ()</span> and name the message "light_on".

<img src="images/user-cloud-message-5.gif" width="800px;" style="padding:5px 5px 20px 5px;">

5\. Let's add some special effects to the button. Add a Motion block <span style="background-color:#4C97FF;color:white;padding:4px; font-size: 12px;border-radius:3px;">change y by ()</span> and set the value to "-2". Add a Looks block <span style="background-color:#9966FF;color:white;padding:4px; font-size: 12px;border-radius:3px;">set color effect to ()</span> and set the value to "-10".

<img src="images/user-cloud-message-6.gif" width="800px;" style="padding:5px 5px 20px 5px;">

6\. Add a Control block <span style="background-color:#FFAB19;color:white;padding:4px; font-size: 12px;border-radius:3px;">wait () seconds</span> and input "0.2". Drag another Motion block <span style="background-color:#4C97FF;color:white;padding:4px; font-size: 12px;border-radius:3px;">change y by ()</span> and set the value to "-2". Then add another Looks block <span style="background-color:#9966FF;color:white;padding:4px; font-size: 12px;border-radius:3px;">set color effect to ()</span>.

<img src="images/user-cloud-message-7.gif" width="800px;" style="padding:5px 5px 20px 5px;">

### Step Two: Program Halocode

#### Connect to the Internet

1\. Under "Devices", make sure "Halocode" is selected. Drag an Events block <span style="background-color:#FFBF00;color:white;padding:4px; font-size: 12px;border-radius:3px;">when Halocode starts up</span> to the Scripts area. Add a Wi-Fi block <span style="background-color:#76CE14;;color:white;padding:4px; font-size: 12px;border-radius:3px;">connect to Wi-Fi () password ()</span>. Input Wi-Fi SSID (Wi-Fi name) and password.

<img src="images/connect-wifi-1.gif" width="800px;" style="padding:5px 5px 20px 5px;">

2\. To ensure Wi-Fi is successfully connected, we need to add a Control block <span style="background-color:#FFAB19;color:white;padding:4px; font-size: 12px;border-radius:3px;">wait ()</span> and a Wi-Fi block <span style="background-color:#76CE14;color:white;padding:4px; font-size: 12px;border-radius:3px;">Wi-Fi is connected?</span>. We want to know when Wi-Fi is connected, so let's add a Lighting block <span style="background-color:#8521D3;color:white;padding:4px; font-size: 12px;border-radius:3px;">all LEDs light up ()</span> to make the LED ring lights up green.

<img src="images/connect-wifi-2.gif" width="800px;" style="padding:5px 5px 20px 5px;">

#### Cloud Message Script

When Halocode receives the user cloud message "light_on", we want the LED ring to light up red and then go off.

1\. Drag a Wi-Fi block <span style="background-color:#76CE14;color:white;padding:4px; font-size: 12px;border-radius:3px;">when receiving user cloud message ()</span> and input "light_on". Add a Lighting block <span style="background-color:#8521D3;color:white;padding:4px; font-size: 12px;border-radius:3px;">all LEDs light up ()</span> and set the color to red.

<img src="images/user-cloud-message-8.gif" width="800px;" style="padding:5px 5px 20px 5px;">

3\. Add a Control block <span style="background-color:#FFAB19;color:white;padding:4px; font-size: 12px;border-radius:3px;">wait () seconds</span> and a Lighting block <span style="background-color:#8521D3;color:white;padding:4px; font-size: 12px;border-radius:3px;">light off all LEDs</span>.

<img src="images/user-cloud-message-9.gif" width="800px;" style="padding:5px 5px 20px 5px;">

4\. Click "Upload" to upload the program to Halocode.

<img src="images/user-cloud-message-10.gif" width="800px;" style="padding:5px 5px 20px 5px;">

5\. Try clicking "Empty Button1" on the stage! 

As long as Halocode is connected to the internet, you can sign in to mBlock 5 in another computer and open the same project to control Halocode remotely.

<p><span style="background-color:#66CDAA;line-height:30px;padding:12px;font-size:16px;border-radius:3px;"><a href="user-cloud-message.mblock" download style="color:white;font-weight:bold;">Download code</a></span></p>

---

{% note %}
You have to sign in with the same mBlock 5 account.
{% endnote %}