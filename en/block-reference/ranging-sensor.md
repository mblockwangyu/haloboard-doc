# Ranging Sensor

You can connect the <a href="http://docs.makeblock.com/diy-platform/en/mbuild/hardware/sensors/ranging-sensor.html" target="_blank">[Ranging Sensor]</a> block of [mbuild](http://docs.makeblock.com/diy-platform/en/mbuild.html) to Halocode, and make more creations.

## 1. ranging sensor (1) distance to an object (cm)

Reports the distance of an object detected the specified ranging sensor

<img src="mbuild-images/ranging-1-1.png">

**Example**

<img src="mbuild-images/ranging-1-2.png">

When the button of Halocode is pressed, if the distance of the obstacle detected by ranging sensor 1 is less than 15cm, the LED ring of Halocode will light up red.

---

## 2. ranging sensor (1) outside distance range?

If the distance of an obstacle is outside the detecting range of the specified ranging sensor, the report condition is met. 

<img src="mbuild-images/ranging-2-1.png">

**Example**

<img src="mbuild-images/ranging-2-2.png">

When the button of Halocode is pressed, if the obstacle is outside the detecting range of ranging sensor 1, the LED ring of Halocode will light up red.

---