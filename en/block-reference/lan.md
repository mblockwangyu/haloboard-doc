# LAN

## 1. set up LAN named ()

Set up a LAN  with a specified name

![](images/lan-1-1.png)

**示例：**

![](images/lan-1-2.png)

When Halocode starts up, set up a LAN  which named "mesh1".

---

## 2. join LAN named ()

Join the specified LAN named 

![](images/lan-2-1.png)

**示例：**

![](images/lan-2-2.png)

When Halocode starts up, set up and join a LAN named "mesh1".

---
## 3. broadcast () on LAN

broadcast a specific message on LAN

![](images/lan-3-1.png)

**示例：**

![](images/lan-3-2.png)

When Halocode starts up, set up and join a LAN named "mesh1",and then broadcast "message" on LAN.


## 4. broadcast () on LAN with value ()

Broadcast the specified message with the specified value on LAN.

![](images/lan-4-1.png)

**Example:**

![](images/lan-4-2.png)

When Halocode starts up, set up and join a LAN named "mesh1", and then broadcast "message" with the value "1" on LAN.

---

## 5. when receiving LAN broadcast ()

When the specified LAN broadcast is received, run the script.

![](images/lan-5-1.png)

**Example:**

![](images/lan-5-2.png)

When Halocode receives LAN broadcast "message", all the LEDs will light up green.

---

## 6. LAN message () value received

Report the value sent with the specified LAN broadcast.

![](images/lan-6-1.png)

**Example:**

![](images/lan-6-2.png)

When Halocode receives LAN broadcast "message", if the value received with the message is "1", all the LEDs will light up green.

---


