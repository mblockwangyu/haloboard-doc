# Humiture Sensor

You can connect the <a href="http://docs.makeblock.com/diy-platform/en/mbuild/hardware/sensors/humiture-sensor.html" target="_blank">[Humiture Sensor]</a> block of [mbuild](http://docs.makeblock.com/diy-platform/en/mbuild.html) to Halocode, and make more creations.

## 1. humiture sensor (1) temperature(℃)

Reports the temperature detected by the specified humiture sensor, measured in degree Celsius (℃).

<img src="mbuild-images/humiture-1-1.png">

**Example**

<img src="mbuild-images/humiture-1-2.png">

When the button of Halocode is pressed, the temperature detected by humiture sensor 1 will be displayed on blue LED matrix 1, measured in degree Celsius.

---

## 2. humiture sensor (1) temperature(℉)

Reports the temperature detected by the specified humiture sensor, measured in Fahrenheit (℉).

<img src="mbuild-images/humiture-2-1.png">

**Example**

<img src="mbuild-images/humiture-2-2.png">

When the button of Halocode is pressed, the temperature detected by humiture sensor 1 will be displayed on blue LED matrix 1, measured in Fahrenheit.

---

## 3. humiture sensor (1) air humidity(%)

Reports the air humidity detected by the specified humiture sensor.

<img src="mbuild-images/humiture-3-1.png">

**Example**

<img src="mbuild-images/humiture-3-2.png">

When the button of Halocode is pressed, the air humidity detected by humiture sensor 1 will be displayed on blue LED matrix 1.

---