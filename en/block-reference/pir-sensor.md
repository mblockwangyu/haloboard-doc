# PIR Sensor 

You can connect the <a href="http://docs.makeblock.com/diy-platform/en/mbuild/hardware/sensors/pir-sensor.html" target="_blank">[PIR Sensor]</a> block of [mbuild](http://docs.makeblock.com/diy-platform/en/mbuild.html) to Halocode, and make more creations.

## 1. PIR sensor (1) detects human body?

If the specified PIR sensor detects human body, the report condition is met.

<img src="mbuild-images/pir-1-1.png">

**Example**

<img src="mbuild-images/pir-1-2.png">

When the button of Halocode is pressed, if PIR sensor 1 detects human body, the LED ring of Halocode will light up red.

---

## 2. PIR sensor (1) number of times a human body detected

Reports the number of times a human body is detected by the specified PIR sensor.

<img src="mbuild-images/pir-2-1.png">

**Example**

<img src="mbuild-images/pir-2-2.png">

When the button of Halocode is pressed, if the number of times a human body is detected by PIR sensor 1 is greater than 16, the LED ring of Halocode will light up red.


---

## 3. PIR sensor (1) resets number of times

Resets the number of times a human body is detected by the specified PIR sensor.

<img src="mbuild-images/pir-3-1.png">

**Example**

<img src="mbuild-images/pir-3-2.png">

When the button of Halocode is pressed, the number of times a human body is detected by PIR sensor 1 will be reset.

---