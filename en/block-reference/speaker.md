# Speaker

You can connect the <a href="http://docs.makeblock.com/diy-platform/en/mbuild/hardware/output-modules/speaker.html" target="_blank">[Speaker]</a> block of [mbuild](http://docs.makeblock.com/diy-platform/en/mbuild.html) to Halocode, and make more creations.

## 1. speaker (1) plays note (C4) for (0.25) beats

The specified speaker plays the specified note for the specified number of beats.

<img src="mbuild-images/speaker-1-1.png">

**Example**

<img src="mbuild-images/speaker-1-2.png">

When the button of Halocode is pressed, speaker 1 will play note "C4" for 0.25 beat.

---

## 2. speaker (1) plays sound at frequency of (700) HZ for (1) secs

The specified speaker plays sound at the specified frequency for a specified period of time.

<img src="mbuild-images/speaker-2-1.png">

**Example**

<img src="mbuild-images/speaker-2-2.png">

When the button of Halocode is pressed, speaker 1 will play sound at frequency of 700HZ for 1 second.

---

## 3. speaker (1) plays sound at frequency of (700) HZ

The specified speaker plays sound at the specified frequency.

<img src="mbuild-images/speaker-3-1.png">

**Example**

<img src="mbuild-images/speaker-3-2.png">

When the button of Halocode is pressed, speaker 1 will play sound at frequency of 700HZ.

---

## 4. speaker (1) plays ()

The specified speaker plays the specified sound effect.

<img src="mbuild-images/speaker-4-1.png">

**Example**

<img src="mbuild-images/speaker-4-2.png">

When the button of Halocode is pressed, speaker 1 will play emotional sound "hello".

---

## 5. speaker (1) plays () until done

The specified speaker plays the specified sound effect until it finishes.

<img src="mbuild-images/speaker-5-1.png">

**Example**

<img src="mbuild-images/speaker-5-2.png">

When the button of Halocode is pressed, speaker 1 will play emotional sound "hello" until it finishes.

---

## 6. emotional sound (hello)

The specified emotional sound.

<small>Note: this is a <a href="http://www.mblock.cc/doc/en/block-reference/shape.html#3-reporter-blocks" target="_blank">Reporter Block</a>, which is used with other blocks, and can't stand alone.</small>

<img src="mbuild-images/speaker-6-1.png">

**Example**

<img src="mbuild-images/speaker-5-2.png">

When the button of Halocode is pressed, speaker 1 will play emotional sound "hello" until it finishes.

---


## 7. electronic sound (start)

The specified electronic sound.

<small>Note: this is a <a href="http://www.mblock.cc/doc/en/block-reference/shape.html#3-reporter-blocks" target="_blank">Reporter Block</a>, which is used with other blocks, and can't stand alone.</small>

<img src="mbuild-images/speaker-7-1.png">

**Example**

<img src="mbuild-images/speaker-7-2.png">

When the button of Halocode is pressed, speaker 1 will play electronic sound "start" until it finishes.

---

## 8. physical sound (metal-dash)

The specified physical sound.

<small>Note: this is a <a href="http://www.mblock.cc/doc/en/block-reference/shape.html#3-reporter-blocks" target="_blank">Reporter Block</a>, which is used with other blocks, and can't stand alone.</small>

<img src="mbuild-images/speaker-8-1.png">

**Example**

<img src="mbuild-images/speaker-8-2.png">

When the button of Halocode is pressed, speaker 1 will play physical sound "metal-dash" until it finishes.

---

## 9. number and letter sound (0)

The specified number and letter sound.

<small>Note: this is a <a href="http://www.mblock.cc/doc/en/block-reference/shape.html#3-reporter-blocks" target="_blank">Reporter Block</a>, which is used with other blocks, and can't stand alone.</small>

<img src="mbuild-images/speaker-9-1.png">

**Example**

<img src="mbuild-images/speaker-9-2.png">

When the button of Halocode is pressed, speaker 1 will play number and letter sound "0" until it finishes.

---

## 10. English word (black)

The specified English word.

<small>Note: this is a <a href="http://www.mblock.cc/doc/en/block-reference/shape.html#3-reporter-blocks" target="_blank">Reporter Block</a>, which is used with other blocks, and can't stand alone.</small>

<img src="mbuild-images/speaker-10-1.png">

**Example**

<img src="mbuild-images/speaker-10-2.png">

When the button of Halocode is pressed, speaker 1 will play English word "black" until it finishes.

---

## 11. speaker (1) stops sound

The specified speaker stops playing sound.

<img src="mbuild-images/speaker-11-1.png">

**Example**

<img src="mbuild-images/speaker-11-2.png">

When the button of Halocode is pressed, speaker 1 will stop playing sound.

---

## 12. speaker (1) increases volume by (20)%

Changes the  volume of the specified speaker by the specified percent.

<img src="mbuild-images/speaker-12-1.png">

**Example**

<img src="mbuild-images/speaker-12-2.png">

When the button of Halocode is pressed, the volume of speaker 1 will increase by 20 percent.

---

## 13. speaker (1) sets volume to (100)%

Sets the volume of the specified speaker to the specified percent.

<img src="mbuild-images/speaker-13-1.png">

**Example**

<img src="mbuild-images/speaker-13-2.png">

When the button of Halocode is pressed, the volume of speaker 1 will be set to 100%.

---

## 14. speaker (1) volume (%)

Reports the volume of the specified speaker.

<img src="mbuild-images/speaker-14-1.png">

**Example**

<img src="mbuild-images/speaker-14-2.png">

When the button of Halocode is pressed, if the volume of speaker 1 is greater than 50%, the LED ring of Halocode will light up red.

---

## 15. speaker (1) is playing sound?

If the specified speaker is playing sound, the report condition is met.

<img src="mbuild-images/speaker-15-1.png">

**Example**

<img src="mbuild-images/speaker-15-2.png">

When the button of Halocode is pressed, if speaker 1 is playing sound, the LED ring of Halocode will light up red.

---