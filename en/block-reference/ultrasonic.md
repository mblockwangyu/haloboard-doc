# Ultrasonic Sensor

You can connect the <a href="http://docs.makeblock.com/diy-platform/en/mbuild/hardware/sensors/ultrasonic-sensor.html" target="_blank">[Ultrasonic Sensor]</a> block of [mbuild](http://docs.makeblock.com/diy-platform/en/mbuild.html) to Halocode, and make more creations.

## 1. ultrasonic sensor (1) distance to an object(cm)

Reports the distance of an object detected by the specified ultrasonic sensor, measured in centimeter.

<img src="mbuild-images/ultrasonic-1-1.png">

**Example**

<img src="mbuild-images/ultrasonic-1-2.png">

When the button of Halocode is pressed, the distance of obstacle detected by ultrasonic 1 will be displayed on blue LED matrix 1.

---


## 2. ultrasonic sensor (1) outside the distance range?

If an object is outside the detecting range of the specified ultrasonic sensor, the report condition is met.

<img src="mbuild-images/ultrasonic-2-1.png">

**Example**

<img src="mbuild-images/ultrasonic-2-2.png">

When the button of Halocode is pressed, if the obstacle is outside the range of ultrasonic sensor 1, the LED ring of Halocode will light up red.

---