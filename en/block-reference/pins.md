# Pins

## 1. pin () in high voltage

If the specified pin is in high voltage, the report condition is met.

![](images/pins-1-1.png)

**Example:**

![](images/pins-1-2.png)

When Halocode starts up, if pin0 is in high voltage, all the LEDs will light up green.

---

## 2. digital read pin ()

Read the digital value of the specified pin (0, 1, 2, 3). The value is 0 or 1.

![](images/pins-2-1.png)

**Example:**

![](images/pins-2-2.png)

When Halocode starts up, if the digital value of pin0 is 0, all the LEDs will light up green; otherwise, red.

---

## 3. analog read pin ()

Read the analog value of the specified pin (0, 1, 2, 3). The value range is 0-1023.

![](images/pins-3-1.png)

**Example:**

![](images/pins-3-2.png)

When Halocode starts up, if the analog value of pin2 is greater than 700, all the LEDs will light up green, otherwise, red.

---

## 4. digital write () to pin ()

Write the specified digital value (0 or 1) to the specified pin (0, 1, 2, 3).

![](images/pins-4-1.png)

**Example:**

![](images/pins-4-2.png)

When Halocode starts up, digital write 1 to pin0.

---

## 5. analog write () to pin ()

Write the specified analog value (0-1023) to the specified pin (0, 1, 2, 3).

![](images/pins-5-1.png)

**Example:**

![](images/pins-5-2.png)

When Halocode starts up, analog write 1023 to pin0.

---

## 6. servo pin () rotates to ()°

Rotates the servo of the specified pin (0, 1, 2, 3) to the specified angle.

![](images/pins-6-1.png)

**Example:**

![](images/pins-6-2.png)

When Halocode starts up, rotate the servo of pin0 to 90 degrees.

---
