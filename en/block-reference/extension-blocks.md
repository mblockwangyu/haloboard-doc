# Extension Blocks

* [Speaker](speaker.md)
* [Servo Driver](servo-driver.md)
* [PIR Sensor](pir-sensor.md)
* [Motor Driver](motor-driver.md)
* [Ranging Sensor](ranging-sensor.md)
* [8×16 Blue LED Matrix](8-16-led-matrix.md)
* [LED Driver](led-driver.md)
* [Slider](slider.md)
* [Humiture Sensor](humiture-sensor.md)
* [Dual Color Sensor](dual-rgb.md)
* [Ultrasonic Sensor](ultrasonic.md)