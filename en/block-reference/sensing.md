# Sensing

## 1. button is pressed?

If the button of Halocode is pressed, the report condition is met.

![](images/sensing-1-1.png)

**Example:**

![](images/sensing-1-2.png)

When Halocode starts up, if the button is pressed, the LED animation "rainbow" will be played.

---

## 2. microphone loudness

Report current loudness detected by Halocode's microphone. The value range is 0-100.

![](images/sensing-2-1.png)

**Example:**

![](images/sensing-2-2.png)

When Halocode starts up, if the detected loudness is greater than 50, the LED animation "rainbow" will be played.

---

## 3. touchpad () is touched?

If the specified touchpad (0, 1, 2, 3) is touched, the report condition is met.

![](images/sensing-3-1.png)

**Example:**

![](images/sensing-3-2.png)

When Halocode starts up, if touchpad0 is touched, the LED animation "rainbow" will be played.

---

## 4. touchpad () value

Report the strength by which the specified touchpad (0, 1, 2, 3) is touched. The value range is 0-100.

![](images/sensing-4-1.png)

**Example:**

![](images/sensing-4-2.png)

When Halocode starts up, if the touchpad0 is touched by strength greater than 50, the LED animation "rainbow" will be played.

---

## 5. Halocode is ()?

If Halocode is placed in the specified position, the report condition is met. There are six options: "left-tilted", "right-tilted", "arrow-up", "arrow-down", "LED-ring-up", and "LED-ring-down".

![](images/sensing-5-1.png)

**Example:**

![](images/sensing-5-2.png)

When Halocode starts up, if it is left-tilted, the LED animation "rainbow" will be played.

---

## 6. shaken?

If Halocode is being shaken, the report condition is met.

![](images/sensing-6-1.png)

**Example:**

![](images/sensing-6-2.png)

When Halocode starts up, if it is shaken, the LED animation "rainbow" will be played.

---

## 7. shaking strength

Report the strength by which Halocode is being shaken. The value range is 0-100.

![](images/sensing-7-1.png)

**Example:**

![](images/sensing-7-2.png)

When Halocode starts up, if the shaking strength is greater than 20, the LED animation "rainbow" will be played.

---

**Motion Sensor**

<img src="images/motion-sensor.png" style="width:600px;">

The picture above shows the three axes (x, y, z) of Halocode and corresponding directions of the roll and pitch.


## 8. motion sensor acceleration (m/s²) around ()

Report the acceleration (m/s²) detected by motion sensor around the x, y, or z axis.

![](images/sensing-8-1.png)

**Example:**

![](images/sensing-8-2.png)

When Halocode starts up, if the acceleration around the x-axis is greater than 10, all the LEDs will light up green.

---

## 9. motion sensor () angle°

Report the roll or pitch angle.

![](images/sensing-9-1.png)

**Example:**

![](images/sensing-9-2.png)

When Halocode starts up, if the pitch is greater than 150 degrees, all the LEDs will light up red.

---

## 10. motion sensor rotation angle around ()

Report the rotation angle around the x-axis, y-axis, z-axis, or all axes.

![](images/sensing-10-1.png)

**Example:**

![](images/sensing-10-2.png)

When Halocode starts up, if the rotation angle around the x-axis is greater than 720 degrees, all the LEDs will light up red.

---

## 11. reset rotation angle around ()

Reset rotation angle around the x-axis, y-axis, z-axis, or all axes.

![](images/sensing-11-1.png)

**Example:**

![](images/sensing-11-2.png)

When the button is pressed, rotation angle around all axes will be reset.

---

## 12. timer

Report Halocode's timer value (measured in second, rounded to the nearest tenth).

![](images/sensing-12-1.png)

**Example:**

![](images/sensing-12-2.png)

When Halocode starts up, if the timer is greater than 6000 seconds, all the LEDs will light up red.

---

## 13. reset timer

Reset Halocode's timer.

![](images/sensing-13-1.png)

**Example:**

![](images/sensing-13-2.png)

When the button is pressed, the timer will be reset.

---

