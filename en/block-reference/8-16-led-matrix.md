# 8×16 Blue LED Matrix

You can connect the <a href="http://docs.makeblock.com/diy-platform/en/mbuild/hardware/display/led-matrix.html" target="_blank">[8×16 Blue LED Matrix]</a> block of [mbuild](http://docs.makeblock.com/diy-platform/en/mbuild.html) to Halocode, and make more creations.

## 1. 8×16 blue LED matrix (1) shows image () (1) secs

Displays the specified image on the specified blue LED matrix for a specified period of time.

<img src="mbuild-images/led-matrix-1-1.png">

**Example**

<img src="mbuild-images/led-matrix-1-2.png">

When the button of Halocode is pressed, the specified image will be displayed on blue LED matrix 1 for 1 second.

---

## 2. 8×16 blue LED matrix (1) shows image ()

Displays the specified image on the specified blue LED matrix.

<img src="mbuild-images/led-matrix-2-1.png">

**Example**

<img src="mbuild-images/led-matrix-2-2.png">

When the button of Halocode is pressed, the specified image will be displayed on blue LED matrix 1.

---

## 3. 8×16 blue LED matrix (1) shows image () at x:(0) y:(0)

Displays the specified image in the specified position of the specified blue LED matrix.

<img src="mbuild-images/led-matrix-3-1.png">

**Example**

<img src="mbuild-images/led-matrix-3-2.png">

When the button of Halocode is pressed, the specified image will be displayed at (0,0) of blue LED matrix 1.

---

## 4. 8×16 blue LED matrix (1) shows (Hello)

Displays the specified text on the specified blue LED matrix.

<img src="mbuild-images/led-matrix-4-1.png">

**Example**

<img src="mbuild-images/led-matrix-4-2.png">

When the button of Halocode is pressed, "Hello" will be displayed on blue LED matrix 1.

---

## 5. 8×16 blue LED matrix (1) shows (Hello) until scroll done

Scrolls the specified text on the specified blue LED matrix until done.

<img src="mbuild-images/led-matrix-5-1.png">

**Example**

<img src="mbuild-images/led-matrix-5-2.png">

When the button of Halocode is pressed, "Hello" will be scrolled on blue LED matrix 1 until done.

---

## 6. 8×16 blue LED matrix (1) shows (Hello) with x:(0) y:(0) as starting point

Displays the specified text in the specified position of the specified blue LED matrix, with a specified position as the starting point.

<img src="mbuild-images/led-matrix-6-1.png">

**Example**

<img src="mbuild-images/led-matrix-6-2.png">

When the button of Halocode is pressed, "Hello" will be displayed on blue LED matrix 1, with (0,0) as the starting point.

---

## 7. 8×16 blue LED matrix (1) lights off

Lights off the specified LED matrix.

<img src="mbuild-images/led-matrix-7-1.png">

**Example**

<img src="mbuild-images/led-matrix-7-2.png">

When the button of Halocode is pressed, blue LED matrix 1 will go off.

---

## 8. 8×16 blue LED matrix (1) lights up at x:(0) y:(0)

Lights up the specified LED of the specified LED matrix.

<img src="mbuild-images/led-matrix-8-1.png">

**Example**

<img src="mbuild-images/led-matrix-8-2.png">

When the button of Halocode is pressed, the LED at (0,0) of blue LED matrix 1 will light up.

---

## 9. 8×16 blue LED matrix (1) lights off at x:(0) y:(0)

Lights off the specified LED of the specified blue LED matrix.

<img src="mbuild-images/led-matrix-9-1.png">

**Example**

<img src="mbuild-images/led-matrix-9-2.png">

When the button of Halocode is pressed, the LED at (0,0) of blue LED matrix 1 will go off.

---

## 10. 8×16 blue LED matrix (1) switches between on and off at x:() y:()

Switches the on-off status of the specified LED of the specified blue LED matrix.

<img src="mbuild-images/led-matrix-10-1.png">

**Example**

<img src="mbuild-images/led-matrix-10-2.png">

When the button of Halocode is pressed, the on-off status of the LED at (0,0) will be changed.

---

## 11. 8×16 blue LED matrix (1) x:() y:() is lighted?

If the specified LED of the specified blue LED matrix is lighted up, the report condition is met.

<img src="mbuild-images/led-matrix-11-1.png">

**Example**

<img src="mbuild-images/led-matrix-11-2.png">

When the button of Halocode is pressed, if the LED at (0,0) of blue LED matrix 1 is lighted up, the LED ring of Halocode will light up red.

---