# Dual RGB Sensor

You can connect the <a href="http://docs.makeblock.com/diy-platform/en/mbuild/hardware/sensors/dual-color-sensor.html" target="_blank">[Dual RGB Color Sensor]</a> block of [mbuild](http://docs.makeblock.com/diy-platform/en/mbuild.html) to Halocode, and make more creations.

## 1. dual color sensor (1) probe (1) detects color (white)?

If the specified probe of the specified dual color sensor detects the specified color, the report condition is met.

<img src="mbuild-images/dual-rgb-1-1.png">

**Example**

<img src="mbuild-images/dual-rgb-1-2.png">

When the button of Halocode is pressed, if probe 1 of dual color sensor 1 detects color red, the LED ring of Halocode will light up red.

---

## 2. dual color sensor (1) probe (1) detects ambient light intensity

Reports the ambient light intensity detected by the specified probe of the specified dual color sensor.

<img src="mbuild-images/dual-rgb-2-1.png">

**Example**

<img src="mbuild-images/dual-rgb-2-2.png">

When the button of Halocode is pressed, the ambient light intensity detected by probe 1 of dual color sensor 1 will be shown by the LED ring of Halocode.

---