# Operators

## 1. () + ()

Perform mathematical addition.

![](images/operators-1-1.png)

**Example:**

![](images/operators-1-2.png)

When Halocode starts up, the LED ring will light up in the specified color order, and after 3 (1+2) seconds, go off.

---

## 2. () &minus; ()

Perform mathematical subtraction.

![](images/operators-2-1.png)

**Example:**

![](images/operators-2-2.png)

When Halocode starts up, the LED ring will light up in the specified color order, and after 2 (3&minus;1) seconds, go off.

---

## 3. () * ()

Perform mathematical multiplication.

![](images/operators-3-1.png)

**Example:**

![](images/operators-3-2.png)

When Halocode starts up, the LED ring will light up in the specified color order, and after 6 (2&times;3) seconds, go off.

---

## 4. () / ()

Perform mathematical division.

![](images/operators-4-1.png)

**Example:**

![](images/operators-4-2.png)

When Halocode starts up, the LED ring will light up in the specified color order, and after 3 (6&divide;2) seconds, go off.

---

## 5. pick random () to ()

Pick a random number from the specified range.

![](images/operators-5-1.png)

**Example:**

![](images/operators-5-2.png)

When Halocode starts up, the LED ring will light up in the specified color order, and after a random number (1-5) of  seconds, go off.

---

## 6. () &gt; ()

If the value of the specified parameter is greater than the specified value, the report condition is met.

![](images/operators-6-1.png)

**Example:**

![](images/operators-6-2.png)

When Halocode starts up, if the loudness is greater than 50, all the LEDs will light up red.

---

## 7. () &lt; ()

If the value of the specified parameter is less than the specified value, the report condition is met.

![](images/operators-7-1.png)

**Example:**

![](images/operators-7-2.png)

When Halocode starts up, if the loudness is less than 50, all the LEDs will light up blue.

---

## 8. () = ()

If the value of the specified parameter equals the specified value, the report condition is met. 

![](images/operators-8-1.png)

**Example:**

![](images/operators-8-2.png)

When Halocode starts up, if the rotation angle around the x-axis equals 360 degrees, the rotation angle will be reset.

---

## 9. () and ()

If both the conditions are met, the report condition is met.

![](images/operators-9-1.png)

**Example:**

![](images/operators-9-2.png)

When Halocode starts up, if Wi-Fi is connected and Halocode is being shaken, the LED animation "rainbow" will be played.

---

## 10. () or ()

If either one of the two conditions is met, the report condition is met.

![](images/operators-10-1.png)

**Example:**

![](images/operators-10-2.png)

When Halocode starts up, if the button is pressed, or Halocode is being shaken, the LED animation "rainbow" will be played.

---

## 11. not ()

The report condition is met when the specified condition is not met.

![](images/operators-11-1.png)

**Example:**

![](images/operators-11-2.png)

When Halocode starts up, if the button is not pressed, the LED ring will light up in the specified color order.

---

## 12. join () ()

Join two specified character strings.

![](images/operators-12-1.png)

**Example:**

![](images/operators-12-2.png)

Report speech recognition result.

---

## 13. letter () of ()

Report the letter at specified position of a character string.

![](images/operators-13-1.png)

**Example:**

![](images/operators-13-2.png)

Report the first character of speech recognition result.

---

## 14. length of ()

Report the length of a specified character string.

![](images/operators-14-1.png)

**Example:**

![](images/operators-14-2.png)

Report the length of speech recognition result.

---

## 15. () contains ()?

If the specified character string contains the other specified character string, the report condition is met.

![](images/operators-15-1.png)

**Example:**

![](images/operators-15-2.png)

When Halocode starts up, if the speech recognition result includes "red", all the LEDs will light up red.

---

## 16. () mod ()

Calculate the remainder (modular) of two specified numbers.

![](images/operators-16-1.png)

**Example:**

![](images/operators-16-2.png)

Calculate the modular of 5 dividing 2.

---

## 17. round ()

Round the specified number to nearest integer.

![](images/operators-17-1.png)

**Example:**

![](images/operators-17-2.png)

Round the value of the timer.

---

## 18. () of ()

Perform specific mathematical operation on the specified number. Mathematical operations include: abs (absolute value), floor, ceiling, sqrt (square root), sin, sos, tan, asin, atan, acos, ln, log, e^, and 10^.

![](images/operators-18-1.png)

**Example:**

![](images/operators-18-2.png)

Ceil the value of the timer.

---
