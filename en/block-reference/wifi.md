# Wi-Fi

## 1. connect to Wi-Fi () password ()

Connect to the specified Wi-Fi network.

![](images/wifi-1-1.png)

**Example:**

![](images/wifi-1-2.png)

When Halocode starts up, connect to Wi-Fi "Maker-guest".

---

## 2. Wi-Fi is connected?

If the Wi-Fi is connected, the report condition is met.

![](images/wifi-2-1.png)

**Example:**

![](images/wifi-2-2.png)

When Halocode starts up, connect to Wi-Fi "Maker-guest". If the Wi-Fi is successfully connected, all the LEDs will light up green.

---

## 3. recognize () for () seconds

Recognize the specified language (Chinese or English) for the specified seconds.

![](images/wifi-3-1.png)

**Example:**

![](images/wifi-3-2.png)

When the button is pressed, Halocode will recognize Chinese for 3 seconds.

---

## 4. speech recognition result

Report the result of speech recognition.

![](images/wifi-4-1.png)

**Example:**

![](images/wifi-4-2.png)

When Halocode starts up, if the speech recognition result includes "red", all the LEDs will light up red.

---

## 5. broadcast user cloud message ()

Broadcast the specified user cloud message.

![](images/wifi-5-1.png)

**Example:**

![](images/wifi-5-2.png)

When the button is pressed, Halocode will broadcast user cloud message "message".

---

## 6. broadcast user cloud message () with value ()

Broadcast the specified user cloud message with the specified value.

![](images/wifi-6-1.png)

**Example:**

![](images/wifi-6-2.png)

When the button is pressed, Halocode will broadcast user cloud message "message" with value "1".

---

## 7. when receiving user cloud message ()

When receiving the specified user cloud message, run the script.

![](images/wifi-7-1.png)

**Example:**

![](images/wifi-7-2.png)

When Halocode receives the user cloud message "message", all the LEDs will light up green.

---

## 8. user cloud message () value received

Report the value received with the specified user cloud message.

![](images/wifi-8-1.png)

**Example:**

![](images/wifi-8-2.png)

When Halocode receives the user cloud message "message", if the value received is "1", all the LEDs will light up green.

---
