# Lighting

The LED ring of Halocode consists of 12 LEDs. The position and ID of each LED are as follows:

![](../python-api/led-id.png)

## 1. play LED animation () until done

Play the specified LED animation. There are four options: "rainbow", "spindrift", "meteor", and "firefly".

![](images/lighting-1-1.png)

**Example:**

![](images/lighting-1-2.png)

When Halocode starts up, the LED animation "rainbow" will be played.

---

## 2. show ()

Set the LEDs ring of Halocode to light up in the specified color order.

![](images/lighting-2-1.png)

**Example:**

![](images/lighting-2-2.png)

When Halocode starts up, the LED ring will light up in the specified color order.

---

## 3. show () after rotating () leds

Light up the LED ring in the color order rotated clockwise the specified number of leds.

![](images/lighting-3-1.png)

**Example:**

![](images/lighting-3-2.png)

When Halocode starts up, the LED ring will light up in the specified color order. One second later, the LED ring will light up in the color order rotated clockwise one LED.

---

## 4. all the LEDs light up ()

Light up all the LEDs the specified color.

![](images/lighting-4-1.png)

**Example:**

![](images/lighting-4-2.png)

When Halocode starts up, all the LEDs will light up green.

---

## 5. all the LEDs light up (), brightness ()%

Light up all the LEDs the specified color at the specified brightness.

![](images/lighting-5-1.png)

**Example:**

![](images/lighting-5-2.png)

When Halocode starts up, all the LEDs will light up green at 80% brightness.

---

## 6. light off all the LEDs

Turn off all the LEDs.

![](images/lighting-6-1.png)

**Example:**

![](images/lighting-6-2.png)

When Halocode starts up, all the LEDs will light up green and go off after one second.

---

## 7. light up all the LEDs with color R() G() B()

Light up all the LEDs a specified color, mixed by specified RGB color values.

![](images/lighting-7-1.png)

**Example:**

![](images/lighting-7-2.png)

When Halocode starts up, all the LEDs will light up red.

---

## 8. light up LED () with color R() G() B()

Light up the specified LED the specified color, mixed by specified RGB color values.

![](images/lighting-8-1.png)

**Example:**

![](images/lighting-8-2.png)

When Halocode starts up, LED 3 will light up red.

---

## 9. light off LED ()

Turn off the specified LED.

![](images/lighting-9-1.png)

**Example:**

![](images/lighting-9-2.png)

When Halocode starts up, all the LEDs will light up green. One second later, LED 1 will go off.

---

## 10. LED ring shows ()%

Display percentage with the LED ring.

![](images/lighting-10-1.png)

**Example:**

![](images/lighting-10-2.png)

When Halocode starts up, the LED ring will show current loudness by lighting up its LEDs.

---
