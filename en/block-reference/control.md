# Control

## 1. wait () seconds

Wait for a specified period of time to run the script.

![](images/control-1-1.png)

**Example:**

![](images/control-1-2.png)

When Halocode starts up, all the LEDs will light up green, and then go off after one second.

---

## 2. repeat ()

Run the script for specified number of times.

![](images/control-2-1.png)

**Example:**

![](images/control-2-2.png)

When Halocode starts up, all the LEDs will light up, and switch between green and red for 10 times. The time gap is 2 seconds.

---

## 3. forever

Run the script repeatedly.

![](images/control-3-1.png)

**Example:**

![](images/control-3-2.png)

When Halocode starts up, all the LEDs will light up, and switch between green and red. The time gap is 2 seconds.

---

## 4. if () then ()

If the report condition is met, run the script.

![](images/control-4-1.png)

**Example:**

![](images/control-4-2.png)

When Halocode starts up, if the button is pressed, the LED ring will light in the specified color order.

---

## 5. if () then () else ()

If the report condition is met, run script 1. If not, run script 2.

![](images/control-5-1.png)

**Example:**

![](images/control-5-2.png)

When Halocode starts up, if it is being shaken, all the LEDs will light up blue; otherwise, orange.

---

## 6. wait until ()

Wait until the report condition is met. Run the script.

![](images/control-6-1.png)

**Example:**

![](images/control-6-2.png)

When Halocode starts up, if the button is pressed, the LED animation "firefly" will be played.

---

## 7. repeat until ()

Run the script repeatedly until the report condition is met.

![](images/control-7-1.png)

**Example:**

![](images/control-7-2.png)

When Halocode starts up, all the LEDs will light up green until the button is pressed. Then all the LEDs will go off.

---

## 8. stop ()

Stop specified script or scripts. There are three options: all, this script, or other scripts in sprite.

![](images/control-8-1.png)

**Example:**

![](images/control-8-2.png)

When the button is pressed, all the scripts will be stopped.

---