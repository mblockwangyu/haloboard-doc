# Slider

You can connect the <a href="http://docs.makeblock.com/diy-platform/en/mbuild/hardware/interaction/slide-potentiometer.html" target="_blank">[Slider]</a> block of [mbuild](http://docs.makeblock.com/diy-platform/en/mbuild.html) to Halocode, and make more creations.

## slider (1) value

Reports the value of the specified slider.

<img src="mbuild-images/slider-1-1.png">

**Example**

<img src="mbuild-images/slider-1-2.png">

When the button of Halocode is pressed, the value of slider 1 will be displayed on blue LED matrix 1.

---