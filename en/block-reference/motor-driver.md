# Motor Driver

You can connect the <a href="http://docs.makeblock.com/diy-platform/en/mbuild/hardware/motion/motor-driver.html" target="_blank">[Motor Driver]</a> block of [mbuild](http://docs.makeblock.com/diy-platform/en/mbuild.html) to Halocode, and make more creations.

## 1. motor driver (1) outputs power (50)% for (1) secs

The specified motor driver outputs the specified power for a specified period of time.

<img src="mbuild-images/motor-driver-1-1.png">

**Example**

<img src="mbuild-images/motor-driver-1-2.png">

When the button of Halocode is pressed, motor driver 1 will output 50% power for 1 second.

---

## 2. motor driver (1) outputs power (50)%

The specified motor driver outputs the specified power.

<img src="mbuild-images/motor-driver-2-1.png">

**Example**

<img src="mbuild-images/motor-driver-2-2.png">

When the button of Halocode is pressed, motor driver 1 will output 50% power.

---

## 3. motor driver (1) increases power output by (20)%

Increases the power output of the specified motor driver by the specified percent.

<img src="mbuild-images/motor-driver-3-1.png">

**Example**

<img src="mbuild-images/motor-driver-3-2.png">

When the button of Halocode is pressed, the power output of motor driver 1 will increase by 20%.

---

## 4. motor driver (1) stops power output

The specified motor driver stops power output.

<img src="mbuild-images/motor-driver-4-1.png">

**Example**

<img src="mbuild-images/motor-driver-4-2.png">

When the button of Halocode is pressed, motor driver 1 will stop its power output.

---

## 5. all motor drivers stop power output

All motor drivers stop power output.

<img src="mbuild-images/motor-driver-5-1.png">

**Example**

<img src="mbuild-images/motor-driver-5-2.png">

When the button of Halocode is pressed, all connected motor drivers will stop power output.

---

## 6. motor driver (1) output level (%)

Reports the power output level of the specified motor driver.

<img src="mbuild-images/motor-driver-6-1.png">

**Example**

<img src="mbuild-images/motor-driver-6-2.png">

When the button of Halocode is pressed, if the power output of motor driver 1 is greater than 80%, the LED ring of Halocode will light up red.

---