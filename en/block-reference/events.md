# Events

## 1. when Halocode starts up

When Halocode starts up, run the script.

![](images/events-1-1.png)

**Example:**

![](images/events-1-2.png)

When Halocode starts up, the LED animation "spindrift" will be played.

---

## 2. when button is pressed

When the button is pressed, run the script.

![](images/events-2-1.png)

**Example:**

![](images/events-2-2.png)

When the button is pressed, all the LEDs will light up green.

---

## 3. when Halocode is shaking

When Halocode is being shaken, run the script.

![](images/events-3-1.png)

**Example:**

![](images/events-3-2.png)

When Halocode is being shaken, all the LEDs will light up green.

---

## 4. when Halocode is ()

When Halocode is placed in the specified position, run the script. There are four options: "arrow-up", "arrow-down", "left-tilted", and "right-tilted".

![](images/events-4-1.png)

**Example:**

![](images/events-4-2.png)

When Halocode is placed arrow-up, all the LEDs will light up green.

---

## 5. when touchpad () is touched

When the specified touchpad (0, 1, 2, 3) is touched, run the script.

![](images/events-5-1.png)

**Example:**

![](images/events-5-2.png)

When touchpad0 is touched, the LED animation "meteor" will be played.

---

## 6. when () &gt; ()

When the value of the specified parameter (loudness or timer) is greater than the specified value, run the script.

![](images/events-6-1.png)

**Example:**

![](images/events-6-2.png)

When the timer value is greater than 360 seconds, the timer will be reset.

---

## 7. when I receive ()

When the specified message is received, run the script.

![](images/events-7-1.png)

**Example:**

![](images/events-7-2.png)

When Halocode receives "message1", the LED animation "rainbow" will be played.

---

## 8. broadcast ()

Broadcast the specified message.

![](images/events-8-1.png)

**Example:**

![](images/events-8-2.png)

When the button is pressed, Halocode will broadcast "message1".

---