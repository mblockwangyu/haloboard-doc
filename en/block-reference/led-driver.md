# LED Driver

You can connect the <a href="http://docs.makeblock.com/diy-platform/en/mbuild/hardware/light/led-driver.html" target="_blank">[LED Driver]</a> block of [mbuild](http://docs.makeblock.com/diy-platform/en/mbuild.html) to Halocode, and make more creations.

## 1. LED driver (1) lights up ()

The LED ring or LED strip connected to the specified LED driver lights up in the specified color order.

<img src="mbuild-images/led-driver-1-1.png">

**Example**

<img src="mbuild-images/led-driver-1-2.png">

When the button of Halocode is pressed, the LED strip connected to LED driver 1 will light up in the specified color order.

---

## 2. LED driver (1) sets LED (1) to ()

Sets the color of the specified LED in the LED strip or LED ring connected to the specified LED driver.

<img src="mbuild-images/led-driver-2-1.png">

**Example**

<img src="mbuild-images/led-driver-2-2.png">

When the button of Halocode is pressed, the first LED in the LED strip connected to LED driver 1 will light up red. 

---

## 3. LED driver (1) sets LED (1) to R(255) G(0) B(0)

Sets the color of the specified LED in the LED strip connected to the specified LED driver, mixed by specified RGB values.

<img src="mbuild-images/led-driver-3-1.png">

**Example**

<img src="mbuild-images/led-driver-3-2.png">

When the button of Halocode is pressed, the first LED in the LED strip connected to LED driver 1 will light up red.

---

## 4. LED driver (1) LED (1) lights off

Lights off the specified LED in the LED strip or LED ring connected to the specified LED driver.

<img src="mbuild-images/led-driver-4-1.png">

**Example**

<img src="mbuild-images/led-driver-4-2.png">

When the button of Halocode is pressed, the first LED in the LED strip connected to LED driver 1 will go off.

---


## 5. LED driver (1) all lights off

Lights off the LED strip or LED ring connected to the specified LED driver.

<img src="mbuild-images/led-driver-5-1.png">

**Example**

<img src="mbuild-images/led-driver-5-2.png">

When the button of Halocode is pressed, the LED strip connected to LED driver 1 will go off.

---

## 6. LED driver (1) sets LED (1) to (red) with value (255)

Sets the specified color value of the specified LED in the LED strip or LED ring connected to the specified LED driver.

<img src="mbuild-images/led-driver-6-1.png">

**Example**

<img src="mbuild-images/led-driver-6-2.png">

When the button of Halocode is pressed, the first LED in the LED strip connected to LED driver 1 will light up, with the red value of 255.

---

## 7. LED driver (1) LED (1) increases (red) value by (20)

Increases the specified color value of the specified LED in the LED strip or LED ring connected to the specified LED driver, by the specified amount.

<img src="mbuild-images/led-driver-7-1.png">

**Example**

<img src="mbuild-images/led-driver-7-2.png">

When the button of Halocode is pressed, the red value of the first LED in the LED strip connected to LED driver 1 will increase by 20.

---

## 8. LED driver (1) sets light effect to (steady)

Sets the light effect of the LED strip or LED ring connected to the specified LED driver.

<img src="mbuild-images/led-driver-8-1.png">

**Example**

<img src="mbuild-images/led-driver-8-2.png">

When the button of Halocode is pressed, the LED strip connected to LED driver 1 will light up light effect "steady".

---