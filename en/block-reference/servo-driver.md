# Servo Driver

You can connect the <a href="http://docs.makeblock.com/diy-platform/en/mbuild/hardware/motion/servo-driver.html" target="_blank">[Servo Driver]</a> block of [mbuild](http://docs.makeblock.com/diy-platform/en/mbuild.html) to Halocode, and make more creations.

## 1. servo driver (1) sets angle to (90)°

Sets the rotation angle of the specified servo driver to the specified value.

<img src="mbuild-images/servo-driver-1-1.png">

**Example**

<img src="mbuild-images/servo-driver-1-2.png">

When the button of Halocode is pressed, the rotation angle of servo driver 1 will be set to 90°.

---

## 2. servo driver (1) increases angle by (20)°

Changes the rotation angle of the specified servo driver by the specified value.

<img src="mbuild-images/servo-driver-2-1.png">

**Example**

<img src="mbuild-images/servo-driver-2-2.png">

When the button of Halocode is pressed, the rotation angle of servo driver 1 will increase by 20°.

---

## 3. servo driver (1) back to zero position

Turns the specified servo driver back to zero position.

<img src="mbuild-images/servo-driver-3-1.png">

**Example**

<img src="mbuild-images/servo-driver-3-2.png">

When the button of Halocode is pressed, servo driver 1 will go back to zero position.

---

## 4. servo driver (1) current angle(°)

Reports the rotation angle of the specified servo driver.

<img src="mbuild-images/servo-driver-4-1.png">

**Example**

<img src="mbuild-images/servo-driver-4-2.png">

When the button of Halocode is pressed, if the rotation angle of servo driver 1 is greater than 90°, the LED ring of Halocode will light up red.

---