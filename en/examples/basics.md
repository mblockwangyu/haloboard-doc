# Basic Projects

This section introduces several basic projects that you can do with Halocode.

<table  cellpadding="15px;" cellspacing="12px;" style="font-size: 12px;text-align:center;">
<tr>
<td width="25%;"><a href="basics-1.html" target="_blank"><img src="../../zh/examples/image/led-ring.png" width="150px;"></a><br>
<p>Make a Smiling Face with the LED Ring</p></td>

<td width="25%;"><a href="basics-2.html" target="_blank"><img src="../../zh/examples/image/volume-detector.png" width="150px;"></a><br>
<p>Make a Volume Detector</p></td>

<td width="25%;"><a href="basics-3.html" target="_blank"><img src="../../zh/examples/image/press-button.png" width="150px;"></a><br>
<p>Press the Button to Play LED Animation "Meteor"</p></td>

<td width="25%;"><a href="rainbow-button.html" target="_blank"><img src="../../zh/examples/image/rainbow-button.png" width="150px;"></a><br>
<p>Rainbow Button</p></td>
</tr>

<tr>
<td><a href="color-mixer.html" target="_blank"><img src="../../zh/examples/image/color-mixer.png" width="150px;"></a><br>
<p>Color Mixer</p></td>
<td><a href="basics-5.html" target="_blank"><img src="../../zh/examples/image/compare-strength.png" width="150px;"></a><br>
<p>Compare Strength</p></td>
<td><a href="energy-ring.html" target="_blank"><img src="../../zh/examples/image/energy-ring.png" width="150px;"></a><br>
<p>Energy Ring</p></td>
<td><a href="smile.html" target="_blank"><img src="../../zh/examples/image/smile.png" width="150px;"></a><br>
<p>Raise Your Hand to Make Halocode Smile</p></td>
</tr>

<tr>
<td><a href="connect-wifi.html" target="_blank"><img src="../../zh/examples/image/wifi.png" width="150px;"></a><br>
<p>Connect Wi-Fi</p></td>
<td><a href="lan.html" target="_blank"><img src="../../zh/examples/image/lan.png" width="150px;"></a><br>
<p>Control Multiple Halocodes via LAN</p></td>
</tr>

</table>

<br>

<p><span style="background-color:#66CDAA;line-height:30px;padding:12px;font-size:16px;border-radius:3px;"><a href="code/code.rar" download style="color:white;font-weight:bold;">Download code</a></span></p>

---