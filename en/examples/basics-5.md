# Compare Strength

**Programming Result**

<img src="../../zh/examples/images/compare-strength-0.gif" width="600px;" style="padding:5px 5px 20px 5px;">

**Introduction to Halocode's motion sensor**

The motion sensor detects how Halocode moves, including shaking strength.

<img src="image/halo-motion.png" width="300px;" style="padding:5px 5px 20px 5px;">

## Set the range of shaking strength

1\. Drag an Operators block <span style="background-color:#59C059;color:white;padding:4px; font-size: 12px;border-radius:3px;">() &gt; ()</span> to the Scripts area, and change the value of the second parameter to "30". Add a Sensing block <span style="background-color:#F79226;color:white;padding:4px; font-size: 12px;border-radius:3px;">shaking strength</span> to the first parameter.

<img src="image/compare-strength-1.gif" width="800px;" style="padding:5px 5px 20px 5px;">

2\. Add a Control block <span style="background-color:#FFAB19;color:white;padding:4px; font-size: 12px;border-radius:3px;">if () then ()</span>.

<img src="image/compare-strength-2.gif" width="800px;" style="padding:5px 5px 20px 5px;">

## Set LED animation

3\. Add a Lighting block <span style="background-color:#8521D3;color:white;padding:4px; font-size: 12px;border-radius:3px;">all LEDs light up ()</span>, and a Control block <span style="background-color:#FFAB19;color:white;padding:4px; font-size: 12px;border-radius:3px;">wait () seconds</span>. Add another Lighting block <span style="background-color:#8521D3;color:white;padding:4px; font-size: 12px;border-radius:3px;">light off all LEDs</span> to light off all LEDs in 1 second.

<img src="image/compare-strength-3.gif" width="800px;" style="padding:5px 5px 20px 5px;">

4\. Likewise, for shaking strength greater than 60, we can duplicate the script. Change the value to 60 and LED color to red.

<img src="image/compare-strength-4.gif" width="800px;" style="padding:5px 5px 20px 5px;">

## Add event and control

5\. Add an Events block <span style="background-color:#FFBF00;color:white;padding:4px; font-size: 12px;border-radius:3px;">when Halocode is shaking</span> and a Control block <span style="background-color:#FFAB19;color:white;padding:4px; font-size: 12px;border-radius:3px;">forever</span>.

<img src="image/compare-strength-5.gif" width="800px;" style="padding:5px 5px 20px 5px;">

## Read shaking strength

6\. Check the box for Sensing block <span style="background-color:#F79226;color:white;padding:4px; font-size: 12px;border-radius:3px;">shaking strength</span>.

<img src="image/compare-strength-6.gif" width="800px;" style="padding:5px 5px 20px 5px;">

7\. Shake Halocode! See if you can make the LED ring light up red.

<p><span style="background-color:#66CDAA;line-height:30px;padding:12px;font-size:16px;border-radius:3px;"><a href="code/compare-strength.mblock" download style="color:white;font-weight:bold;">Download code</a></span></p>

---