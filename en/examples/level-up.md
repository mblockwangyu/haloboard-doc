# Level Up

<table  cellpadding="15px;" cellspacing="12px;" style="font-size: 12px;text-align:center;">

<tr>
<td colspan="2"><p><b>Virtual vs Reality</b></p></td>
<td colspan="2"><p><b>IoT</b></p></td>
</tr>

<tr>
<td width="25%;"><a href="level-up-1.html" target="_blank"><img src="../../zh/examples/image/steering-wheel.png" width="150px;"></a><br>
<p>Make Halocode the Steering Wheel of the Car on Stage</p></td>

<td width="25%;"><a href="level-up-5.html" target="_blank"><img src="../../zh/examples/image/global-variable.png" width="150px;"></a><br>
<p>Use Global Variable to Interact with Sprites</p></td>

<td width="25%;"><a href="remote-control.html" target="_blank"><img src="../../zh/examples/image/remote-control.png" width="150px;"></a><br>
<p>Remote Control</p></td>

<td width="25%;"><a href="level-up-2.html" target="_blank"><img src="../../zh/examples/image/control-deck.png" width="150px;"></a><br>
<p>Halocode's Remote Control Deck</p></td>
</tr>
</table>

<br>

<table  cellpadding="15px;" cellspacing="12px;" style="font-size: 12px;text-align:center;width:100%;">
<tr>
<td colspan="3"><p><b>Artificial Intelligence</b></p></td>
</tr>

<tr>
<td width=“33%;”><a href="level-up-3.html" target="_blank"><img src="image/voice-control.png" width="150px;"></a><br>
<p>Control Halocode's LEDs<br>via Voice Command</p></td>
<td width=“33%;”><a href="emotion-detector.html" target="_blank"><img src="image/emotion-detector.png" width="150px;"></a><br>
<p>Emotion Detector</p></td>
<td width=“33%;”><a href="level-up-6.html" target="_blank"><img src="image/deep-learning.png" width="150px;"></a><br>
<p>Deep Learning and<br>Facial Recognition</p></td>
</tr>
</table>

<br>