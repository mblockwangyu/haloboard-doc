# 4. Make LED Marquee with Halocode

**Programming result**

<img src="../../zh/examples/images/marquee-led-0.gif" width="600px;" style="padding:5px 5px 20px 5px;">

## Add a variable

1\. Click Variables category, and then click "Make a Variable". Name the variable "n".

<img src="images/marquee-led-1.gif" width="800px;" style="padding:5px 5px 20px 5px;">

## Set LEDs

2\. Add a Lighting block <span style="background-color:#8521D3;color:white;padding:4px; font-size: 12px;border-radius:3px;">show () after rotating () leds</span>, and change the colors as the following.

<img src="images/marquee-led-2.gif" width="800px;" style="padding:5px 5px 20px 5px;">

3\. Add two Variables blocks <span style="background-color:#FF8C1A;color:white;padding:4px; font-size: 12px;border-radius:3px;">change (n) by (1)</span> and <span style="background-color:#FF8C1A;color:white;padding:4px; font-size: 12px;border-radius:3px;">n</span>. Then drag a Control block <span style="background-color:#FFAB19;color:white;padding:4px; font-size: 12px;border-radius:3px;">wait () seconds</span>, and change the time to 0.1 second.

<img src="images/marquee-led-3.gif" width="800px;" style="padding:5px 5px 20px 5px;">

## Add event and control

4\. Add a Control block <span style="background-color:#FFAB19;color:white;padding:4px; font-size: 12px;border-radius:3px;">wait () seconds</span> and an Events block <span style="background-color:#FFBF00;color:white;padding:4px; font-size: 12px;border-radius:3px;">when Halocode starts up</span>.

<img src="images/marquee-led-4.gif" width="800px;" style="padding:5px 5px 20px 5px;">

5\. Upload the program and check the LED marquee.

<img src="images/marquee-led-5.gif" width="800px;" style="padding:5px 5px 20px 5px;">

<a href="marquee.mblock" download>Click to download code</a>

---