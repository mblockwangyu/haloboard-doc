# A Kitten with Blinking Eyes and a Waving Tail

<div style="background-color:#F0F8FF;padding:15px;border-radius:15px;line-height:28px"><b>Tools and materials:</b><br>
one Halocode, crocodile clip or wire, one battery box, a pair of scissors, one puncher, one ruler, some cardboard, some double-sided tapes, DuPont wire, 9g servo motor (3.3V).</div>

<br>

## Make a kitten

Draw the kitten's body and tail on the cardboard, and then cut them out. Use the puncher to make two holes as the eyes of kitten.

**<small>Note: the distance of the two eyes should equal to the greatest distance between two LEDs of Halocode.</small>**

<img src="../../zh/examples/images/cat-1.jpg" style="width:400px;padding:5px 5px 12px 0px;">

Connect the servo motor to Halocode (any of the four pins will work), as follows:

<div style="background-color:#F0F8FF;padding:15px;border-radius:15px;line-height:26px;margin:10px;"><ul>
<li>The red line of the servo means the positive pole, and is connected to the red line of the alligator clip, which is connected to the 3.3v pin of Halocode.</li>
<li>The black line of the servo means the negative pole, and is connected to the blue line of the alligator clip, which is connected to the GND pin of Halocode.</li>
<li>The white line of the servo is the communication line, and is connected to the yellow line of the alligator clip, which can be connected to any touchpad of Halocode. We'll use touchpad0 in this example.</li></ul>
</div>

<br>

<img src="../../zh/examples/images/cat-2.jpg" style="width:400px;padding:5px 5px 12px 0px;">
<img src="../../zh/examples/images/cat-3.jpg" style="width:400px;padding:5px 5px 12px 0px;">
<img src="../../zh/examples/images/cat-4.jpg" style="width:400px;padding:5px 5px 12px 0px;">

Stick the tail to the servo motor. Connect the body and tail of the kitten with Halocode, the servo motor, and the battery box.

<img src="../../zh/examples/images/cat-5.jpg" style="width:400px;padding:5px 5px 12px 0px;">
<img src="../../zh/examples/images/cat-6.jpg" style="width:400px;padding:5px 5px 12px 0px;">
<img src="../../zh/examples/images/cat-7.jpg" style="width:400px;padding:5px 5px 12px 0px;">

## Toggle on Upload mode

<img src="../tutorials/images/toggle-on-upload.gif" width="800px;" style="padding:5px 5px 20px 5px;">

## Script of Blinking Eyes

1\. Drag an Events block <span style="background-color:#FFBF00;color:white;padding:4px; font-size: 12px;border-radius:3px;">when Halocode starts up</span> and a Control block <span style="background-color:#FFAB19;color:white;padding:4px; font-size: 12px;border-radius:3px;">forever</span> to the Scripts area.

<img src="image/cat-1.gif" style="width:800px;padding:5px 5px 12px 0px;">

2\. Add a Lighting block <span style="background-color:#8521D3;color:white;padding:4px; font-size: 12px;border-radius:3px;">all the LEDs light up ()</span> and a Control block <span style="background-color:#FFAB19;color:white;padding:4px; font-size: 12px;border-radius:3px;">wait () seconds</span>. Set the color to white. We want the kitten to blink eyes randomly, so we need an Operators block <span style="background-color:#59C059;color:white;padding:4px; font-size: 12px;border-radius:3px;">pick random () to ()</span>, and input number 2 and 6. Add a Lighting block <span style="background-color:#8521D3;color:white;padding:4px; font-size: 12px;border-radius:3px;">light off all the LEDs</span> and another Control block <span style="background-color:#FFAB19;color:white;padding:4px; font-size: 12px;border-radius:3px;">wait () seconds</span>. Set the time to 0.2 second.

<img src="image/cat-2.gif" style="width:800px;padding:5px 5px 12px 0px;">

## Script of the Waving Tail

3\. Drag an Events block <span style="background-color:#FFBF00;color:white;padding:4px; font-size: 12px;border-radius:3px;">when Halocode starts up</span> and a Control block <span style="background-color:#FFAB19;color:white;padding:4px; font-size: 12px;border-radius:3px;">forever</span> to the Scripts area.

<img src="image/cat-3.gif" style="width:800px;padding:5px 5px 12px 0px;">

4\. Add a Pins block <span style="background-color:#AFCF1C;color:white;padding:3px;font-size:12px;">servo pin () rotates to ()</span> and a Control block <span style="background-color:#FFAB19;color:white;padding:4px; font-size: 12px;border-radius:3px;">wait () seconds</span>. Set the degree to 0 and keep the default time.

<img src="image/cat-4.gif" style="width:800px;padding:5px 5px 12px 0px;">

5\. Add another Pins block <span style="background-color:#AFCF1C;color:white;padding:3px;font-size:12px;">servo pin () rotates to ()</span> and Control block <span style="background-color:#FFAB19;color:white;padding:4px; font-size: 12px;border-radius:3px;">wait () seconds</span>. Both keep the default value.

<img src="image/cat-5.gif" style="width:800px;padding:5px 5px 12px 0px;">

6\. Click "Upload" to upload the program to Halocode.


7\. Check the kitten! Is it blinking eyes and waving its tail at the same time?

<img src="../../zh/examples/image/cat.gif" style="width:600px;padding:5px 5px 12px 0px;">

<p><span style="background-color:#66CDAA;line-height:30px;padding:12px;font-size:16px;border-radius:3px;"><a href="code/kitten.mblock" download style="color:white;font-weight:bold;">Download code</a></span></p>

---