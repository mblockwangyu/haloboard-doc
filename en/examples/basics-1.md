# Make a Smiling Face with the LED Ring

**Programming Result**

<img src="../../zh/examples/image/led-ring.png" width="400px;" style="padding:5px 5px 20px 5px;">

**LED ID of Halocode**

The 12 LEDs of Halocode share the same position as the twelve hours of a clock.

<img src="../../zh/examples/image/halo-led.png" width="360px;" style="padding:5px 5px 20px 5px;">

## Program the "eyes"

1\. Drag two Lighting blocks <span style="background-color:#8521D3;color:white;padding:4px; font-size: 12px;border-radius:3px;">light up LED () with color R() G() B()</span> to the Scripts area. Set the LED ID to "2", and "10".

<img src="image/led-ring-1.gif" width="800px;" style="padding:5px 5px 20px 5px;">

## Program the "mouth"

2\. Add 5 Lighting blocks <span style="background-color:#8521D3;color:white;padding:4px; font-size: 12px;border-radius:3px;">light up LED () with color R() G() B()</span> to the Scripts area. Set the LED ID to "4", "5", "6", "7", and "8" respectively.

<img src="image/led-ring-2.gif" width="800px;" style="padding:5px 5px 20px 5px;">

3\. Add an Events block <span style="background-color:#FFBF00;color:white;padding:4px; font-size: 12px;border-radius:3px;">when button is pressed</span>.

<img src="image/led-ring-3.gif" width="800px;" style="padding:5px 5px 20px 5px;">

4\. Press the button and check Halocode's smiling face.

<p><span style="background-color:#66CDAA;line-height:30px;padding:12px;font-size:16px;border-radius:3px;"><a href="code/mak-a-smiling-face-with-led-ring.mblock" download style="color:white;font-weight:bold;">Download code</a></span></p>

---