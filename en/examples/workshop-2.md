# Smart Home

With built-in Wi-Fi and User Cloud Message, Halocode can play its role in your smart home. Let's build a house. On the one hand, Halocode can be the light source of the room; on the other, by connecting to a servo, Halocode can control garage door.

<img src="image/smart-home.png" width="360px;" style="padding:5px 5px 20px 5px;">

## Set stage background

1\. Delete default sprite Panda

<img src="image/smart-home-1.gif" style="width:800px;padding:5px 5px 12px 0px;">

2\. Under "Background", click "Costumes", and then add "Bedroom2"

<img src="image/smart-home-2.gif" style="width:800px;padding:5px 5px 12px 0px;">

## Add a button

3\. Click "+" under "Sprites", and then add "Empty button1".

<img src="image/smart-home-3.gif" style="width:800px;padding:5px 5px 12px 0px;">

4\. Choose "Empty button1". Then click "Costume". Add text "Bright" to the button.

<img src="image/smart-home-4.gif" style="width:800px;padding:5px 5px 12px 0px;">

## Program the button

5\. Under "Sprites", click "+" in the Blocks area to add User Cloud Message blocks.

<img src="image/smart-home-5.gif" style="width:800px;padding:5px 5px 12px 0px;">

6\. Add an Events block <span style="background-color:#FFBF00;color:white;padding:4px; font-size: 12px;border-radius:3px;">when this sprite clicked</span> and a User Cloud Message block <span style="background-color:#4EAA83;color:white;padding:4px; font-size: 12px;border-radius:3px;">send user cloud message ()</span>. Name the message "bright".

<img src="image/smart-home-6.gif" style="width:800px;padding:5px 5px 12px 0px;">

7\. Add some special effects to the button when it is clicked. Drag two Looks block <span style="background-color:#9966FF;color:white;padding:4px; font-size: 12px;border-radius:3px;">change size by (10)</span> and <span style="background-color:#9966FF;color:white;padding:4px; font-size: 12px;border-radius:3px;">change (color) effect by (5)</span>. Then add a Control block <span style="background-color:#FFAB19;color:white;padding:4px; font-size: 12px;border-radius:3px;">wait () seconds</span>, and set the value to 0.1.

<img src="image/smart-home-7.gif" style="width:800px;padding:5px 5px 12px 0px;">

8\. Add another two Looks blocks <span style="background-color:#9966FF;color:white;padding:4px; font-size: 12px;border-radius:3px;">set size to (100)%</span> and <span style="background-color:#9966FF;color:white;padding:4px; font-size: 12px;border-radius:3px;">set (color) effect to (0)</span>.

<img src="image/smart-home-8.gif" style="width:800px;padding:5px 5px 12px 0px;">

## Add buttons: "Soft", "Night", and "Colorful"

9\. Duplicate "Empty button1" to make create another button for soft light, and edit the program accordingly. Likewise, we can create two more buttons for "night" and "colorful".

<img src="image/smart-home-9.gif" style="width:800px;padding:5px 5px 12px 0px;">

<img src="image/smart-home-10.png" style="padding:5px 5px 12px 0px;">

## Add garage button

10\. Under "Sprites", click "+" to add "Blue button45". Add text "garage lock" to the button.

<img src="image/smart-home-11.gif" style="width:800px;padding:5px 5px 12px 0px;">

11\. Choose "Blue button45", and then click "Costumes". Duplicate the costume. Edit the costume. Name the two costumes "lock", and "unlock"

<img src="image/smart-home-12.gif" style="width:800px;padding:5px 5px 12px 0px;">

## Program garage button

12\. Choose Variables blocks, and click "Make a Variable". Name the variable "lock state".

<img src="image/smart-home-13.gif" style="width:800px;padding:5px 5px 12px 0px;">

13\. Add an Events block <span style="background-color:#FFBF00;color:white;padding:4px; font-size: 12px;border-radius:3px;">when this sprite clicked</span>, a control block <span style="background-color:#FFAB19;color:white;padding:4px; font-size: 12px;border-radius:3px;">if () then () else ()</span>, an Operators block <span style="background-color:#59C059;color:white;padding:4px; font-size: 12px;border-radius:3px;">() = ()</span>, and a Variables block <span style="background-color:#FF8C1A;color:white;padding:4px; font-size: 12px;border-radius:3px;">lock state</span>. Input "unlocked" to the second slot of the Operators block

<img src="image/smart-home-14.gif" style="width:800px;padding:5px 5px 12px 0px;">

14\. When the door is open, we want to close the door. Add a User Cloud Message block <span style="background-color:#4EAA83;color:white;padding:4px; font-size: 12px;border-radius:3px;">send user cloud message ()</span>, and name the message "lock garage". Add a Variables block <span style="background-color:#FF8C1A;color:white;padding:4px; font-size: 12px;border-radius:3px;">set (lock state) to (locked)</span> and a Looks block <span style="background-color:#9966FF;color:white;padding:4px; font-size: 12px;border-radius:3px;">switch to costume(lock)</span>.

<img src="image/smart-home-15.gif" style="width:800px;padding:5px 5px 12px 0px;">

15\. Duplicate the script, to open garage lock. Change the variables accordingly.

<img src="image/smart-home-16.gif" style="width:800px;padding:5px 5px 12px 0px;">

16\. Add some special effects to the button when it is clicked. Add the following blocks: <span style="background-color:#9966FF;color:white;padding:4px; font-size: 12px;border-radius:3px;">change size by (10)</span>, <span style="background-color:#9966FF;color:white;padding:4px; font-size: 12px;border-radius:3px;">change (color) effect by (5)</span>, <span style="background-color:#9966FF;color:white;padding:4px; font-size: 12px;border-radius:3px;">set size to (100)%</span>, <span style="background-color:#FFAB19;color:white;padding:4px; font-size: 12px;border-radius:3px;">wait () seconds</span>, and <span style="background-color:#9966FF;color:white;padding:4px; font-size: 12px;border-radius:3px;">set (color) effect to (0)</span>.

<img src="image/smart-home-17.gif" style="width:800px;padding:5px 5px 12px 0px;">

## Connect to the internet

17\. To use User Cloud Message, we have to connect Halocode to the internet. Add two blocks: <span style="background-color:#FFBF00;color:white;padding:4px; font-size: 12px;border-radius:3px;">when Halocode starts up</span>, and <span style="background-color:#76CE14;;color:white;padding:4px; font-size: 12px;border-radius:3px;">connect to Wi-Fi () password ()</span>. 

<img src="image/smart-home-18.gif" style="width:800px;padding:5px 5px 12px 0px;">

18\. Add some special effects to Halocode when successfully connected to the internet

<img src="image/smart-home-19.gif" style="width:800px;padding:5px 5px 12px 0px;">

## Add light effect

19\. Add a Wi-Fi block <span style="background-color:#76CE14;color:white;padding:4px; font-size: 12px;border-radius:3px;">when receiving user cloud message ()</span>, and input "bright". Add a Lighting block <span style="background-color:#8521D3;color:white;padding:4px; font-size: 12px;border-radius:3px;">all LEDs light up (), brightness ()%</span>, to light up all LEDs white.

<img src="image/smart-home-20.gif" style="width:800px;padding:5px 5px 12px 0px;">

20\. Likewise, for User Cloud Message "soft", all LEDs will light up 40% white.

<img src="image/smart-home-21.gif" style="width:800px;padding:5px 5px 12px 0px;">

21\. For User Cloud Message "night", fewer LEDs will light up. Add a Wi-Fi block <span style="background-color:#76CE14;color:white;padding:4px; font-size: 12px;border-radius:3px;">when receiving user cloud message ()</span> and a Lighting block <span style="background-color:#8521D3;color:white;padding:4px; font-size: 12px;border-radius:3px;">show ()</span>.

<img src="image/smart-home-22.gif" style="width:800px;padding:5px 5px 12px 0px;">

22\. For User Cloud Message "colorful". the LED right will light up with multiple colors. We'll need these blocks: <span style="background-color:#8521D3;color:white;padding:4px; font-size: 12px;border-radius:3px;">show ()</span>, <span style="background-color:#FFAB19;color:white;padding:4px; font-size: 12px;border-radius:3px;">wait () seconds</span>, <span style="background-color:#8521D3;color:white;padding:4px; font-size: 12px;border-radius:3px;">light off all LEDs</span>, and <span style="background-color:#FFAB19;color:white;padding:4px; font-size: 12px;border-radius:3px;">repeat ()</span>.

<img src="image/smart-home-23.gif" style="width:800px;padding:5px 5px 12px 0px;">

## Connect second Halocode to the servo

23\. The red line of the servo means the positive pole, and is connected to the red line of the alligator clip, which is connected to the 3.3v pin of Halocode. The black line of the servo means the negative pole, and is connected to the blue line of the alligator clip, which is connected to the GND pin of Halocode. The white line of the servo is the communication line, and is connected to the yellow line of the alligator clip, which can be connected to any touchpad of Halocode. We'll use touchpad0 in this example.

<img src="../../zh/examples/images/smarthome-22.jpg" style="width:400px;padding:5px 5px 12px 0px;">

<img src="../../zh/examples/images/smarthome-23.jpg" style="width:400px;padding:5px 5px 12px 0px;">

<img src="../../zh/examples/images/smarthome-24.jpg" style="width:400px;padding:5px 5px 12px 0px;">

## Program the servo


24\. Add a Wi-Fi block <span style="background-color:#76CE14;color:white;padding:4px; font-size: 12px;border-radius:3px;">when receiving user cloud message ()</span>, and input message "lock garage". Add a Pins block <span style="background-color:#AFCF1C;color:white;padding:3px;font-size: 12px">servo pin () rotates to ()</span> (actual rotating degree depends on the angle of the servo), to close the door.

<img src="image/smart-home-24.gif" style="width:800px;padding:5px 5px 12px 0px;">

24\. Add another Wi-Fi block <span style="background-color:#76CE14;color:white;padding:4px; font-size: 12px;border-radius:3px;">when receiving user cloud message ()</span>, and input message "unlock grage". Add a Pins block <span style="background-color:#AFCF1C;color:white;padding:3px;font-size: 12px;">servo pin () rotates to ()</span> (actual rotating degree depends on the angle of the servo), to open the door.

<img src="image/smart-home-25.gif" style="width:800px;padding:5px 5px 12px 0px;">

## Programming result

26\. Upload program.

<p><span style="background-color:#66CDAA;line-height:30px;padding:12px;font-size:16px;border-radius:3px;"><a href="code/smart-home.mblock" download style="color:white;font-weight:bold;">Download code</a></span></p>

---