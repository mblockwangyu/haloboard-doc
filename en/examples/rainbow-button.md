# Rainbow Button

<img src="../../zh/examples/image/rainbow-button.png" width="400px;" style="padding:5px 5px 20px 5px;">

1\. Drag an Events block <span style="background-color:#FFBF00;color:white;padding:4px; font-size: 12px;border-radius:3px;">when button is pressed</span> to the Scripts area

<img src="image/rainbow-button-1.gif" width="800px;" style="padding:5px 5px 20px 5px;">

2\. Add a Lighting block <span style="background-color:#8521D3;color:white;padding:4px; font-size: 12px;border-radius:3px;">show ()</span>

<img src="image/rainbow-button-2.gif" width="800px;" style="padding:5px 5px 20px 5px;">

3\. Press the button of Halocode, and see what happens!

<p><span style="background-color:#66CDAA;line-height:30px;padding:12px;font-size:16px;border-radius:3px;"><a href="code/rainbow-button.mblock" download style="color:white;font-weight:bold;">Download code</a></span></p>

---