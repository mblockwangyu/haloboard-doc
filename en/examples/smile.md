# Raise Your Hand to Make Halocode Smile

When Halocode is placed "arrow-up", the LED ring will light up and make a smile.

<img src="../../zh/examples/image/smile.png" width="300px;" style="padding:5px 5px 20px 5px;">

## Toggle on Upload mode

Click to toggle on Upload mode

<img src="image/toggle-on-upload.gif" width="800px;" style="padding:5px 5px 20px 5px;">

## Program

1\. Drag an Events block <span style="background-color:#FFBF00;color:white;padding:4px; font-size: 12px;border-radius:3px;">when Halocode starts up</span> to the Scripts area, and then add a Control block <span style="background-color:#FFAB19;color:white;padding:4px; font-size: 12px;border-radius:3px;">forever</span>。

<img src="image/smile-1.gif" width="800px;" style="padding:5px 5px 20px 5px;">

2\. Add a Control block <span style="background-color:#FFAB19;color:white;padding:4px; font-size: 12px;border-radius:3px;">if () then () else</span>, and a Sensing block <span style="background-color:#F79226;color:white;padding:4px; font-size: 12px;border-radius:3px;">Halocode is ()?</span>. Choose "arrow-up"

<img src="image/smile-2.gif" width="800px;" style="padding:5px 5px 20px 5px;">

3\. Add a Lighting block <span style="background-color:#8521D3;color:white;padding:4px; font-size: 12px;border-radius:3px;">show ()</span>, and draw a smiling face

<img src="image/smile-3.gif" width="800px;" style="padding:5px 5px 20px 5px;">

3\. Add another Lighting block <span style="background-color:#8521D3;color:white;padding:4px; font-size: 12px;border-radius:3px;">show ()</span>

<img src="image/smile-4.gif" width="800px;" style="padding:5px 5px 20px 5px;">

4\. Upload the program

<p><span style="background-color:#66CDAA;line-height:30px;padding:12px;font-size:16px;border-radius:3px;"><a href="code/raise-your-hand-to-make-halocode-smile.mblock" download style="color:white;font-weight:bold;">Download code</a></span></p>

---