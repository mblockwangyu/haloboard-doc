# Make Halocode the Steering Wheel of the Car on Stage

**Programming Result**

When you tilt Halocode to the left, the car on stage will move leftward. When you tilt Halocode to the right, the car will move rightward. Halocode is just like the steering wheel of the car.

<img src="image/steering-wheel-0.gif" width="600px;" style="padding:5px 5px 20px 5px;">

## Use the motion sensor

The motion sensor of Halocode detects how Halocode moves. We'll use the motion sensor in this example. When Halocode is tilted to the left, a corresponding piece of message will be sent to the car on Scratch Stage to make it move leftward. Likewise, when Halocode is tilted to the right, a corresponding piece of message will tell the car to move rightward.

1\. Drag a Control block <span style="background-color:#FFAB19;color:white;padding:4px; font-size: 12px;border-radius:3px;">if () then ()</span> to the Scripts area. Add a Sensing block <span style="background-color:#F79226;color:white;padding:4px; font-size: 12px;border-radius:3px;">Halocode is ()?</span> to the condition.

<img src="image/steering-wheel-1.gif" width="800px;" style="padding:5px 5px 20px 5px;">

2\. Likewise, for the right side, duplicate the script, and select "right-tilted".

<img src="image/steering-wheel-2.gif" width="800px;" style="padding:5px 5px 20px 5px;">

## Message broadcast

3\. Add two Events blocks <span style="background-color:#FFBF00;color:white;padding:4px; font-size: 12px;border-radius:3px;">broadcast ()</span>. Create two pieces of message, namely "left" and "right".

<img src="image/steering-wheel-3.gif" width="800px;" style="padding:5px 5px 20px 5px;">

4\. Add an Events block <span style="background-color:#FFBF00;color:white;padding:4px; font-size: 12px;border-radius:3px;">when button is pressed</span> and a Control block <span style="background-color:#FFAB19;color:white;padding:4px; font-size: 12px;border-radius:3px;">forever</span>.

<img src="image/steering-wheel-4.gif" width="800px;" style="padding:5px 5px 20px 5px;">

## Add a sprite

5\. Under "Sprites", click "+". In the Sprites Library, choose "Car" from the Transportation category.

<img src="image/steering-wheel-5.gif" width="800px;" style="padding:5px 5px 20px 5px;">

6\. Delete the default sprite "Panda".

<img src="image/steering-wheel-6.gif" width="800px;" style="padding:5px 5px 20px 5px;">

## Program the car

7\. Add three Motion block: <span style="background-color:#4C97FF;color:white;padding:4px; font-size: 12px;border-radius:3px;">set rotation style (left-right)</span>, <span style="background-color:#4C97FF;color:white;padding:4px; font-size: 12px;border-radius:3px;">point in direction (-90)</span>, <span style="background-color:#4C97FF;color:white;padding:4px; font-size: 12px;border-radius:3px;">move (10) steps</span>.

<img src="image/steering-wheel-7.gif" width="800px;" style="padding:5px 5px 20px 5px;">

8\. Add an Events block <span style="background-color:#FFBF00;color:white;padding:4px; font-size: 12px;border-radius:3px;">when I receive ()</span>, and choose message "left". When the car receives message "left", it will move leftward for 10 steps.

<img src="image/steering-wheel-8.gif" width="800px;" style="padding:5px 5px 20px 5px;">

9\. Likewise, for the rightward part, duplicate the script. Choose message "right", and change the direction to "90".

**Note: the message received should be the same message that is broadcast.**

<img src="image/steering-wheel-9.gif" width="800px;" style="padding:5px 5px 20px 5px;">

10\. Press the button of Halocode and then tilt it!

<p><span style="background-color:#66CDAA;line-height:30px;padding:12px;font-size:16px;border-radius:3px;"><a href="code/steering-wheel.mblock" download style="color:white;font-weight:bold;">Download code</a></span></p>

---