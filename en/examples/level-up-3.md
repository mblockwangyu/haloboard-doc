# Control Halocode's LEDs via Voice Command

## Sign in to mBlock 5

Halocode needs to connect to the internet to use online speech recognition service. We need to sign in to mBlock 5 first.

<img src="image/login.gif" width="800px;" style="padding:5px 5px 20px 5px;">

## Connect to the internet

1\. Drag an Events block <span style="background-color:#FFBF00;color:white;padding:4px; font-size: 12px;border-radius:3px;">when Halocode starts up</span> and a Wi-Fi block <span style="background-color:#76CE14;;color:white;padding:4px; font-size: 12px;border-radius:3px;">connect to Wi-Fi () password ()</span>. Input the Wi-Fi name and password.

<img src="image/voice-control-1.gif" width="800px;" style="padding:5px 5px 20px 5px;">

2\. We want to know when the Wi-Fi is successfully connected. Add a Control block <span style="background-color:#FFAB19;color:white;padding:4px; font-size: 12px;border-radius:3px;">wait ()</span>, a Wi-Fi block <span style="background-color:#76CE14;color:white;padding:4px; font-size: 12px;border-radius:3px;">Wi-Fi is connected?</span>, and a Lighting block <span style="background-color:#8521D3;color:white;padding:4px; font-size: 12px;border-radius:3px;">show ()</span>.

<img src="image/voice-control-2.gif" width="800px;" style="padding:5px 5px 20px 5px;">

## Speech recognition

When the button is pressed, all LEDs will light up orange. The recognition process lasts for 2 seconds. When it's done, LED 1 will light up green.

3\. Add an Events blocks <span style="background-color:#FFBF00;color:white;padding:4px; font-size: 12px;border-radius:3px;">when button is pressed</span> and a Lighting block <span style="background-color:#8521D3;color:white;padding:4px; font-size: 12px;border-radius:3px;">all LEDs light up ()</span>. Change the color to orange. Then add a Control block <span style="background-color:#FFAB19;color:white;padding:4px; font-size: 12px;border-radius:3px;">wait (1) seconds</span> and a Lighting block <span style="background-color:#8521D3;color:white;padding:4px; font-size: 12px;border-radius:3px;">show ()</span>.

<img src="image/voice-control-3.gif" width="800px;" style="padding:5px 5px 20px 5px;">

4\. Add a Wi-Fi block <span style="background-color:#76CE14;color:white;padding:4px; font-size: 12px;border-radius:3px;">recognize (English) for (2) seconds</span>, and a Lighting block <span style="background-color:#8521D3;color:white;padding:4px; font-size: 12px;border-radius:3px;">light off all LEDs</span> to light off all LEDs after the speech recognition is done.

<img src="image/voice-control-4.gif" width="800px;" style="padding:5px 5px 20px 5px;">

## Recognize "blue"

5\. Add a Control block <span style="background-color:#FFAB19;color:white;padding:4px; font-size: 12px;border-radius:3px;">if () then () else</span> and an Operators block <span style="background-color:#59C059;color:white;padding:4px; font-size: 12px;border-radius:3px;">() contains ()?</span>. Add Wi-Fi block <span style="background-color:#76CE14;color:white;padding:4px; font-size: 12px;border-radius:3px;">speech recognition result</span> to the first box and input "blue" to the second box. If the speech recognition result contains "blue", all LEDs will light up blue. Add a Lighting block <span style="background-color:#8521D3;color:white;padding:4px; font-size: 12px;border-radius:3px;">all LEDs light up ()</span>

<img src="image/voice-control-5.gif" width="800px;" style="padding:5px 5px 20px 5px;">

## Recognize "red"

6\. Likewise, we can recognize "red". 

<img src="image/voice-control-6.gif" width="800px;" style="padding:5px 5px 20px 5px;">

7\. Upload the program. When Halocode connects to the internet successfully, try pressing the button and say "red" to the microphone. Then check the LEDs.

<p><span style="background-color:#66CDAA;line-height:30px;padding:12px;font-size:16px;border-radius:3px;"><a href="code/control-halocode's-leds-via-voice-command.mblock" download style="color:white;font-weight:bold;">Download code</a></span></p>

---
