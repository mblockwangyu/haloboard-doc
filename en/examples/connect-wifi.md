# Connect Wi-Fi

Connect Halocode to the internet.

<img src="../../zh/examples/image/wifi.png" width="300px;" style="padding:5px 5px 20px 5px;">

## Toggle on Upload mode

<img src="image/toggle-on-upload.gif" width="800px;" style="padding:5px 5px 20px 5px;">

## Connect Wi-Fi

1\. Add an Events block <span style="background-color:#FFBF00;color:white;padding:4px; font-size: 12px;border-radius:3px;">when Halocode starts up</span> and a Wi-Fi block <span style="background-color:#76CE14;color:white;padding:4px; font-size: 12px;border-radius:3px;">connect to Wi-Fi() password()</span>. Input the network name and password of the nearby Wi-Fi.

<img src="image/wifi-1.gif" width="800px;" style="padding:5px 5px 20px 5px;">

2\. We want to know when Halocode is successfully connected to the internet. Add a Control block <span style="background-color:#FFAB19;color:white;padding:4px; font-size: 12px;border-radius:3px;">wait ()</span>, a Wi-Fi block <span style="background-color:#76CE14;color:white;padding:4px; font-size: 12px;border-radius:3px;">Wi-Fi connected?</span>, and a Lighting block <span style="background-color:#8521D3;color:white;padding:4px; font-size: 12px;border-radius:3px;">show ()</span>. Light up 6 LEDs in color green.

<img src="image/wifi-2.gif" width="800px;" style="padding:5px 5px 20px 5px;">

<p><span style="background-color:#66CDAA;line-height:30px;padding:12px;font-size:16px;border-radius:3px;"><a href="code/connect-wifi.mblock" download style="color:white;font-weight:bold;">Download code</a></span></p>

---