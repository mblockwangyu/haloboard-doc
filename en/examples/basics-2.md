# Make a Volume Detector

Make a volume detector with Halocode. Use the microphone of Halocode to detect loudness and visualize the the value with the LED ring. The louder the sound is, the more LEDs will light up.

1\. Drag an Events block <span style="background-color:#FFBF00;color:white;padding:4px; font-size: 12px;border-radius:3px;">when button is pressed</span> to the Scripts area.

<img src="image/volume-detector-1.gif" width="800px;" style="padding:5px 5px 20px 5px;">

2\. Add a Control block <span style="background-color:#FFAB19;color:white;padding:4px; font-size: 12px;border-radius:3px;">forever</span>.

<img src="image/volume-detector-2.gif" width="800px;" style="padding:5px 5px 20px 5px;">

3\. Add a Lighting block <span style="background-color:#8521D3;color:white;padding:4px; font-size: 12px;border-radius:3px;">LED ring shows ()%</span> and a Sensing block <span style="background-color:#F79226;color:white;padding:4px; font-size: 12px;border-radius:3px;">microphone loudness</span>.

<img src="image/volume-detector-3.gif" width="800px;" style="padding:5px 5px 20px 5px;">

4\. Try hitting the table and check the LED ring of Halocode.

<img src="../../zh/examples/image/volume.gif" width="800px;" style="padding:5px 5px 20px 5px;">

<p><span style="background-color:#66CDAA;line-height:30px;padding:12px;font-size:16px;border-radius:3px;"><a href="code/make-a-volume-detector.mblock" download style="color:white;font-weight:bold;">Download code</a></span></p>

---