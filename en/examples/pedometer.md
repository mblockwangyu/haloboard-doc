# Pedometer

<img src="../../zh/examples/image/pedometer.png" width="360px;" style="padding:5px 5px 20px 5px;">

## Program Halocode

1\. Create 3 pieces of message: "start", "move", and "stop"

<img src="image/pedometer-1.gif" width="800px;" style="padding:5px 5px 20px 5px;">

2\. Add an Events block <span style="background-color:#FFBF00;color:white;padding:4px; font-size: 12px;border-radius:3px;">when button is pressed</span> , a Control block <span style="background-color:#FFAB19;color:white;padding:4px; font-size: 12px;border-radius:3px;">repeat</span>, another Events block <span style="background-color:#FFBF00;color:white;padding:4px; font-size: 12px;border-radius:3px;">broadcast (start)</span>, and a Sensing block <span style="background-color:#F79226;color:white;padding:4px; font-size: 12px;border-radius:3px;">reset timer</span>

<img src="image/pedometer-2.gif" width="800px;" style="padding:5px 5px 20px 5px;">

3\. Add a Control block <span style="background-color:#FFAB19;color:white;padding:4px; font-size: 12px;border-radius:3px;">if  () else ()</span>, an Operators block <span style="background-color:#59C059;color:white;padding:4px; font-size: 12px;border-radius:3px;">() > ()</span>, a Sensing block <span style="background-color:#F79226;color:white;padding:4px; font-size: 12px;border-radius:3px;">shaking strength</span>, and an Events block <span style="background-color:#FFBF00;color:white;padding:4px; font-size: 12px;border-radius:3px;">broadcast (move)</span>. Input "15" to the second box of the Operators block

<img src="image/pedometer-3.gif" width="800px;" style="padding:5px 5px 20px 5px;">

4\. Add a Control block <span style="background-color:#FFAB19;color:white;padding:4px; font-size: 12px;border-radius:3px;">if () then ()</span>, an Operators block <span style="background-color:#59C059;color:white;padding:4px; font-size: 12px;border-radius:3px;">() = ()</span>, a Sensing block <span style="background-color:#F79226;color:white;padding:4px; font-size: 12px;border-radius:3px;">shaking strength</span>, and an Events block <span style="background-color:#FFBF00;color:white;padding:4px; font-size: 12px;border-radius:3px;">broadcast (stop)</span>

<img src="image/pedometer-4.gif" width="800px;" style="padding:5px 5px 20px 5px;">

5\. Add a Control block <span style="background-color:#FFAB19;color:white;padding:4px; font-size: 12px;border-radius:3px;">repeat until ()</span>, an Operators block <span style="background-color:#59C059;color:white;padding:4px; font-size: 12px;border-radius:3px;">() > ()</span>, and a Sensing block <span style="background-color:#F79226;color:white;padding:4px; font-size: 12px;border-radius:3px;">timer</span>. Input "100" to the second box of the Operators block, and combine the two scripts

<img src="image/pedometer-5.gif" width="800px;" style="padding:5px 5px 20px 5px;">

## Add a sprite

6\. Delete default sprite Panda

<img src="image/pedometer-6.gif" width="800px;" style="padding:5px 5px 20px 5px;">

7\. Add sprite "Empty"

<img src="image/pedometer-7.gif" width="800px;" style="padding:5px 5px 20px 5px;">

8\. Choose "Empty", and then click "Costume". Draw a red dot

<img src="image/pedometer-8.gif" width="800px;" style="padding:5px 5px 20px 5px;">

9\. Duplicate "Empty", and then change the costume to make a blue dot

<img src="image/pedometer-9.gif" width="800px;" style="padding:5px 5px 20px 5px;">

10\. At the bottom of the Blocks Area, click "+ extension" to add Pen

<img src="image/pedometer-10.gif" width="800px;" style="padding:5px 5px 20px 5px;">

## Program the red dot

11\. Make sure the red dot is selected, and then click Variables blocks. Create a new variable "motion value"

<img src="image/pedometer-11.gif" width="800px;" style="padding:5px 5px 20px 5px;">

12\. Add an Events block <span style="background-color:#FFBF00;color:white;padding:4px; font-size: 12px;border-radius:3px;">when I receive (move)</span>, and a Control block <span style="background-color:#FFAB19;color:white;padding:4px; font-size: 12px;border-radius:3px;">repeat () times</span>. Input "4"

<img src="image/pedometer-12.gif" width="800px;" style="padding:5px 5px 20px 5px;">

13\. Add a Motion block <span style="background-color:#4C97FF;color:white;padding:4px; font-size: 12px; border-radius:3px;">turn &curarr; () degrees </span>, and a Pen block <span style="background-color:#0FBD8C;color:white;padding:4px; font-size: 12px; border-radius:3px;">stamp</span>, and a Variables block <span style="background-color:#FF8C1A;color:white;padding:4px; font-size: 12px;border-radius:3px;">change (Motion value) by (1)</span>

<img src="image/pedometer-13.gif" width="800px;" style="padding:5px 5px 20px 5px;">

14\. Add an Events block <span style="background-color:#FFBF00;color:white;padding:4px; font-size: 12px;border-radius:3px;">when I receive (start)</span>, and a Motion block <span style="background-color:#4C97FF;color:white;padding:4px; font-size: 12px; border-radius:3px;">point in direct (90)</span>

<img src="image/pedometer-14.gif" width="800px;" style="padding:5px 5px 20px 5px;">

## Program the blue dot

15\. Make sure the red dot is selected, and then click Variables blocks. Create a new variable "Slitting value"

<img src="image/pedometer-15.gif" width="800px;" style="padding:5px 5px 20px 5px;">

16\. Add an Events block <span style="background-color:#FFBF00;color:white;padding:4px; font-size: 12px;border-radius:3px;">when I receive (stop)</span>, and a Control block <span style="background-color:#FFAB19;color:white;padding:4px; font-size: 12px;border-radius:3px;">repeat () times</span>. Input "2"

<img src="image/pedometer-16.gif" width="800px;" style="padding:5px 5px 20px 5px;">

17\. Add a Motion block <span style="background-color:#4C97FF;color:white;padding:4px; font-size: 12px; border-radius:3px;">turn &curarr; () degrees </span>, and a Pen block <span style="background-color:#0FBD8C;color:white;padding:4px; font-size: 12px; border-radius:3px;">stamp</span>, and a Variables block <span style="background-color:#FF8C1A;color:white;padding:4px; font-size: 12px;border-radius:3px;">change (Slitting value) by (1)</span>

<img src="image/pedometer-17.gif" width="800px;" style="padding:5px 5px 20px 5px;">

18\. Add an Events block <span style="background-color:#FFBF00;color:white;padding:4px; font-size: 12px;border-radius:3px;">when I receive (start)</span>, and a Motion block <span style="background-color:#4C97FF;color:white;padding:4px; font-size: 12px; border-radius:3px;">point in direct (90)</span>

<img src="image/pedometer-18.gif" width="800px;" style="padding:5px 5px 20px 5px;">

<p><span style="background-color:#66CDAA;line-height:30px;padding:12px;font-size:16px;border-radius:3px;"><a href="code/pedometer.mblock" download style="color:white;font-weight:bold;">Download code</a></span></p>

---