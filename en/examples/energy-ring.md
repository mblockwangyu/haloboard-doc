# Energy Ring

Shake your hand to power Halocode. As Halocode is "charging", more LEDs will light up

<img src="../../zh/examples/image/energy-ring.png" width="300px;" style="padding:5px 5px 20px 5px;">


1\. Choose Variables block, and click "Make a Variable". Name the variable "n"

<img src="image/energy-ring-1.gif" width="800px;" style="padding:5px 5px 20px 5px;">

2\. Add an Events block <span style="background-color:#FFBF00;color:white;padding:4px; font-size: 12px;border-radius:3px;">when button is pressed</span>, a Lighting block <span style="background-color:#8521D3;color:white;padding:4px; font-size: 12px;border-radius:3px;">light off LED ()</span>, and a Variables block <span style="background-color:#FF8C1A;color:white;padding:4px; font-size: 12px;border-radius:3px;">set (n) to ()</span>. Input "1"

<img src="image/energy-ring-2.gif" width="800px;" style="padding:5px 5px 20px 5px;">

3\. Add a Control block <span style="background-color:#FFAB19;color:white;padding:4px; font-size: 12px;border-radius:3px;">if () then ()</span>, an Operators block <span style="background-color:#59C059;color:white;padding:4px; font-size: 12px;border-radius:3px;">() > ()</span>, and a Sensing block <span style="background-color:#F79226;color:white;padding:4px; font-size: 12px;border-radius:3px;">shaking strength</span>. Input "20"

<img src="image/energy-ring-3.gif" width="800px;" style="padding:5px 5px 20px 5px;">

4\. Add a Lighting block <span style="background-color:#8521D3;color:white;padding:4px; font-size: 12px;border-radius:3px;">light up LED () with color R()G()B()</span>, two Variables block <span style="background-color:#FF8C1A;color:white;padding:4px; font-size: 12px;border-radius:3px;">n</span> and <span style="background-color:#FF8C1A;color:white;padding:4px; font-size: 12px;border-radius:3px;">change (n) by (1)</span>, a Control block <span style="background-color:#FFAB19;color:white;padding:4px; font-size: 12px;border-radius:3px;">wait () secs</span>. Input "0.2"

<img src="image/energy-ring-4.gif" width="800px;" style="padding:5px 5px 20px 5px;">

5\. Add a Control block <span style="background-color:#FFAB19;color:white;padding:4px; font-size: 12px;border-radius:3px;">repeat until ()</span>, an Operators block <span style="background-color:#59C059;color:white;padding:4px; font-size: 12px;border-radius:3px;">() = ()</span>, a Variables block <span style="background-color:#FF8C1A;color:white;padding:4px; font-size: 12px;border-radius:3px;">n</span>. Input "12"

<img src="image/energy-ring-5.gif" width="800px;" style="padding:5px 5px 20px 5px;">

6\. Add a Lighting block <span style="background-color:#8521D3;color:white;padding:4px; font-size: 12px;border-radius:3px;">all LEDs light up ()</span>. Set the color to blue

<img src="image/energy-ring-6.gif" width="800px;" style="padding:5px 5px 20px 5px;">


7\. Toggle on Upload mode.

<p><span style="background-color:#66CDAA;line-height:30px;padding:12px;font-size:16px;border-radius:3px;"><a href="code/energy-ring.mblock" download style="color:white;font-weight:bold;">Download code</a></span></p>

---