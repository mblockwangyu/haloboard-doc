# Color Mixer

Change the color of the LED ring by RGB values. We'll use touchpad 0, 1, and 2 to control each of the values.

<img src="../../zh/examples/image/color-mixer.png" width="300px;" style="padding:5px 5px 20px 5px;">

## Introduction to Halocode's touchpads

Halocode has four touchpads.

<img src="image/halo-touchpad.png" width="400px;" style="padding:5px 5px 20px 5px;">

## Toggle on Upload mode

Click to toggle on Upload mode

<img src="image/toggle-on-upload.gif" width="800px;" style="padding:5px 5px 20px 5px;">

## Program touchpads

1\. Drag an Events block <span style="background-color:#FFBF00;color:white;padding:4px; font-size: 12px;border-radius:3px;">when Halocode starts up</span> to the Scripts area，and then add a Control block <span style="background-color:#FFAB19;color:white;padding:4px; font-size: 12px;border-radius:3px;">forever</span>。

<img src="image/color-mixer-1.gif" width="800px;" style="padding:5px 5px 20px 5px;">

2\. Add a Lighting block <span style="background-color:#8521D3;color:white;padding:4px; font-size: 12px;border-radius:3px;">light up all LEDs with color R()G()B()</span>and three Sensing blocks <span style="background-color:#F79226;color:white;padding:4px; font-size: 12px;border-radius:3px;">touchpad () value</span>. Choose touchpad 0, 1, and 2

<img src="image/color-mixer-2.gif" width="800px;" style="padding:5px 5px 20px 5px;">

3\. Upload the program

<p><span style="background-color:#66CDAA;line-height:30px;padding:12px;font-size:16px;border-radius:3px;"><a href="code/color-mixer.mblock" download style="color:white;font-weight:bold;">Download code</a></span></p>

---