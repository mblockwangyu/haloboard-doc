# Deep Learning and Facial Recognition

Use the Machine Learning extension to achieve facial recognition function. When "woman" is recognized, the message "laugh" will be broadcast, and Halocode will make a smiling face with its LED ring; otherwise, the message "angry" will be broadcast, and the LED ring of Halocode will light up red. You can apply facial recognition to your smart-home system. 

<img src="image/deep-learning.png" width="360px;" style="padding:5px 5px 20px 5px;">

## Training model

1\. Under "Sprites", click "+" in the Blocks area to add "Teachable Machine" extension.

<img src="image/deep-learning-1.gif" style="width:800px;padding:5px 5px 12px 0px;">

2\. Select Teachable Machine blocks, and click "Training model" to build a new model. In this example, we'll need 3 categories.

<img src="image/deep-learning-2.gif" style="width:800px;padding:5px 5px 12px 0px;">

3\. Name the first category "woman". Print a photo of a woman, and place the photo in front of the camera. Click and hold the button "Learn", so that enough samples will be collected. (Change the angle of the photo to collect different samples; more samples come with better recognition result.)

<img src="image/deep-learning-3.gif" style="width:800px;padding:5px 5px 12px 0px;">

4\. Likewise, we can collect samples for category "man" and "student".

<img src="image/deep-learning-4.gif" style="width:800px;padding:5px 5px 12px 0px;">

5\. When the sample collecting process is done, click "Use the model".

<img src="image/deep-learning-5.gif" style="width:800px;padding:5px 5px 12px 0px;">

## Add event and control

6\. Drag an Events block <span style="background-color:#FFBF00;color:white;padding:4px; font-size: 12px;border-radius:3px;">when green flag clicked</span> and a Control block <span style="background-color:#FFAB19;color:white;padding:4px; font-size: 12px;border-radius:3px;">if () then ()</span>. Then add a Teachable Machine (TM) block <span style="background-color:#0FBD8C;color:white;padding:4px; font-size: 12px;border-radius:3px;">recognition result is (woman)</span>.

<img src="image/deep-learning-6.gif" style="width:800px;padding:5px 5px 12px 0px;">


## Create broadcast messages

7\. Click Events blocks, and create two messages, "laugh" and "angry".

<img src="image/deep-learning-7.gif" style="width:800px;padding:5px 5px 12px 0px;">

8\. Add an Events block <span style="background-color:#FFBF00;color:white;padding:4px; font-size: 12px;border-radius:3px;">broadcast (laugh) and wait</span>. (Halocode will execute the script that is activated by the message "laugh", and then others.)

<img src="image/deep-learning-8.gif" style="width:800px;padding:5px 5px 12px 0px;">

9\. Likewise, for "man" and "student", add corresponding Events block <span style="background-color:#FFBF00;color:white;padding:4px; font-size: 12px;border-radius:3px;">broadcast () and wait</span>. Add a Control block <span style="background-color:#FFAB19;color:white;padding:4px; font-size: 12px;border-radius:3px;">forever</span> to keep facial recognition function on.

<img src="image/deep-learning-9.gif" style="width:800px;padding:5px 5px 12px 0px;">

## Program Halocode

10\. Under "Devices", choose "Halocode". Add an Events block <span style="background-color:#FFBF00;color:white;padding:4px; font-size: 12px;border-radius:3px;">when I receive()</span> and two Lighting blocks <span style="background-color:#8521D3;color:white;padding:4px; font-size: 12px;border-radius:3px;">light up LED () with color R() G() B()</span> to light up the second and tenth LED, as the "eyes" of the smiling face.

<img src="image/deep-learning-10.gif" style="width:800px;padding:5px 5px 12px 0px;">


11\. Add 5 more Lighting blocks <span style="background-color:#8521D3;color:white;padding:4px; font-size: 12px;border-radius:3px;">light up LED () with color R() G() B()</span> to light up the fourth to eighth LED as the "mouth". Adjust the RGB values to set the color to light pink.

<img src="image/deep-learning-11.gif" style="width:800px;padding:5px 5px 12px 0px;">

12\. Add a Control block <span style="background-color:#FFAB19;color:white;padding:4px; font-size: 12px;border-radius:3px;">wait (0.1) seconds</span> and a Lighting block <span style="background-color:#8521D3;color:white;padding:4px; font-size: 12px;border-radius:3px;">light off all LEDs</span>.

<img src="image/deep-learning-12.gif" style="width:800px;padding:5px 5px 12px 0px;">


13\. Add an Events block <span style="background-color:#FFBF00;color:white;padding:4px; font-size: 12px;border-radius:3px;">when I receive (angry)</span> and a Lighting block <span style="background-color:#8521D3;color:white;padding:4px; font-size: 12px;border-radius:3px;">all LEDs light up ()</span>. Set the color to red. 0.1 second later, light off all LEDs.

<img src="image/deep-learning-13.gif" style="width:800px;padding:5px 5px 12px 0px;">

## Programming result

<img src="image/deep-learning-14.gif" style="width:800px;padding:5px 5px 12px 0px;">

<p><span style="background-color:#66CDAA;line-height:30px;padding:12px;font-size:16px;border-radius:3px;"><a href="code/deep-learning.mblock" download style="color:white;font-weight:bold;">Download code</a></span></p>

---