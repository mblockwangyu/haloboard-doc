# Remote Control

With Wi-Fi and User Cloud Message, Halocode can interact with the stage. In this project, we can make a remote control to light up or light off Halocode's LED ring with stage bulb.

<img src="../../zh/examples/image/remote-control.png" width="360px;" style="padding:5px 5px 20px 5px;">


## Enable Upload mode

<img src="../tutorials/images/toggle-on-upload.gif" width="800px;" style="padding:5px 5px 20px 5px;">

## Set the background and sprite

1\. Delete default sprite Panda

<img src="image/remote-control-1.gif" width="800px;" style="padding:5px 5px 20px 5px;">

2\. Under "Background", click "Costumes", and then add background "Office3"

<img src="image/remote-control-2.gif" width="800px;" style="padding:5px 5px 20px 5px;">

3\. Add sprite "bulb", and set the costume to "bulb2"

<img src="image/remote-control-3.gif" width="800px;" style="padding:5px 5px 20px 5px;">

4\. Adjust the size of bulb (150% of the original size), and place on the table

<img src="image/remote-control-4.gif" width="800px;" style="padding:5px 5px 20px 5px;">

## Program bulb

5\. At the bottom of Blocks Area, click "+ extension", and then add User Cloud Message

<img src="image/remote-control-5.gif" width="800px;" style="padding:5px 5px 20px 5px;">

6\. Choose Variables block. Click "Make a Variable", and name it "frequency"

<img src="image/remote-control-6.gif" width="800px;" style="padding:5px 5px 20px 5px;">

7\. Drag an Events block <span style="background-color:#FFBF00;color:white;padding:4px; font-size: 12px;border-radius:3px;">当when green flag clicked</span> to the Scripts area, and then add a Variables block <span style="background-color:#FF8C1A;color:white;padding:4px; font-size: 12px;border-radius:3px;">set (frequency) to (0)</span>

<img src="image/remote-control-7.gif" width="800px;" style="padding:5px 5px 20px 5px;">

8\. Add an Events block <span style="background-color:#FFBF00;color:white;padding:4px; font-size: 12px;border-radius:3px;">when this sprite clicked</span>, a Variables block <span style="background-color:#FF8C1A;color:white;padding:4px; font-size: 12px;border-radius:3px;">change (frequency) by (1)</span>, and a User Cloud Message block <span style="background-color:#4EAA83;color:white;padding:4px; font-size: 12px;border-radius:3px;">send user cloud message () with value ()</span>. Name the message "switch" and add another Variables block <span style="background-color:#FF8C1A;color:white;padding:4px; font-size: 12px;border-radius:3px;">frequency</span>

<img src="image/remote-control-8.gif" width="800px;" style="padding:5px 5px 20px 5px;">

9\. Add a Control block <span style="background-color:#FFAB19;color:white;padding:4px; font-size: 12px;border-radius:3px;">if () then () else ()</span>, two Operators blocks <span style="background-color:#59C059;color:white;padding:4px; font-size: 12px;border-radius:3px;">() = ()</span> and <span style="background-color:#59C059;color:white;padding:4px; font-size: 12px;border-radius:3px;">() mod ()</span>, a Variables block <span style="background-color:#FF8C1A;color:white;padding:4px; font-size: 12px;border-radius:3px;">frequency</span>. Input "2" and "1"

<img src="image/remote-control-9.gif" width="800px;" style="padding:5px 5px 20px 5px;">

10\. Add two Looks blocks <span style="background-color:#9966FF;color:white;padding:4px; font-size: 12px;border-radius:3px;">switch costume to ()</span>, and choose "bulb-1" and "bulb-2" respectively

<img src="image/remote-control-10.gif" width="800px;" style="padding:5px 5px 20px 5px;">

## Halocode connects Wi-Fi

11\. Add an Events block <span style="background-color:#FFBF00;color:white;padding:4px; font-size: 12px;border-radius:3px;">when Halocode starts up</span>, a Wi-Fi block <span style="background-color:#76CE14;color:white;padding:4px; font-size: 12px;border-radius:3px;">connect to Wi-Fi () password ()</span>. Input the network name and password of your Wi-Fi

<img src="image/remote-control-11.gif" width="800px;" style="padding:5px 5px 20px 5px;">

12\. Add a Control block <span style="background-color:#FFAB19;color:white;padding:4px; font-size: 12px;border-radius:3px;">wait until ()</span>, a Wi-Fi block <span style="background-color:#76CE14;color:white;padding:4px; font-size: 12px;border-radius:3px;">Wi-Fi connected?</span>, a Lighting block <span style="background-color:#8521D3;color:white;padding:4px; font-size: 12px;border-radius:3px;">all LEDs light up ()</span>. Set the color to green. Add another Control block <span style="background-color:#FFAB19;color:white;padding43px; font-size: 12px;border-radius:3px;">wait (1) seconds</span>, and another Lighting block <span style="background-color:#8521D3;color:white;padding:4px; font-size: 12px;border-radius:3px;">light off all LEDs</span>

<img src="image/remote-control-12.gif" width="800px;" style="padding:5px 5px 20px 5px;">

## Halocode receives user cloud message

13\. Add a Wi-Fi block <span style="background-color:#76CE14;color:white;padding:4px; font-size: 12px;border-radius:3px;">when receiving user cloud message (switch)</span>, a Control block <span style="background-color:#FFAB19;color:white;padding:4px; font-size: 12px;border-radius:3px;">if () then () else ()</span>, two Operators blocks <span style="background-color:#59C059;color:white;padding:4px; font-size: 12px;border-radius:3px;">() = ()</span> and <span style="background-color:#59C059;color:white;padding:4px; font-size: 12px;border-radius:3px;">() mod ()</span>, a Wi-Fi block <span style="background-color:#76CE14;color:white;padding:4px; font-size: 12px;border-radius:3px;">user cloud message () value</span>. Input "2" and "1"

<img src="image/remote-control-13.gif" width="800px;" style="padding:5px 5px 20px 5px;">

14\. Add two Lighting blocks <span style="background-color:#8521D3;color:white;padding:4px; font-size: 12px;border-radius:3px;">light up all LEDs with color R() G() B()</span> and <span style="background-color:#8521D3;color:white;padding:4px; font-size: 12px;border-radius:3px;">light off all LEDs</span>

<img src="image/remote-control-14.gif" width="800px;" style="padding:5px 5px 20px 5px;">

15\. Upload the program

<img src="image/remote-control-15.gif" width="800px;" style="padding:5px 5px 20px 5px;">

<p><span style="background-color:#66CDAA;line-height:30px;padding:12px;font-size:16px;border-radius:3px;"><a href="code/remote-control.mblock" download style="color:white;font-weight:bold;">Download code</a></span></p>

---