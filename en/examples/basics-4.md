# 4. Use Touchpad to Control Halocode's LEDs

**Programming Result**

When touchpad0 or touchpad3 is touched, different LED animations will be played.

<img src="images/touch-light-0.gif" width="600px;" style="padding:5px 5px 20px 5px;">

**Introduction to Halocode's Touchpads**

<img src="images/halo-touchpad.png" width="400px;" style="padding:5px 5px 20px 5px;">

## Set LED animation

1\. Drag a Lighting block <span style="background-color:#8521D3;color:white;padding:4px; font-size: 12px;border-radius:3px;">play LED animation () until done</span> to the Scripts area. Use the default animation "Rainbow".

<img src="images/touch-light-1.gif" width="800px;" style="padding:5px 5px 20px 5px;">

## Add event and control

2\. Add an Events block <span style="background-color:#FFBF00;color:white;padding:4px; font-size: 12px;border-radius:3px;">when touchpad () is touched</span> and a Control block <span style="background-color:#FFAB19;color:white;padding:4px; font-size: 12px;border-radius:3px;">forever</span>.

<img src="images/touch-light-2.gif" width="800px;" style="padding:5px 5px 20px 5px;">

3\. Likewise, for touchpad3, duplicate the script. Set the touchpad to "3" and LED animation to "firefly".

<img src="images/touch-light-3.gif" width="800px;" style="padding:5px 5px 20px 5px;">

**Multi-threading interrupt**

4\. Add a Control block <span style="background-color:#FFAB19;color:white;padding:4px; font-size: 12px;border-radius:3px;">stop ()</span> to each script.

<img src="images/touch-light-4.gif" width="800px;" style="padding:5px 5px 20px 5px;">

5\. Upload the program. Try touching touchpad0 and touchpad3!

<img src="images/touch-light-5.gif" width="800px;" style="padding:5px 5px 20px 5px;">

<a href="touch-light.mblock" download>Click to download code</a>

---

