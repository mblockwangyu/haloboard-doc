# Halocode's Remote Control Deck

## Sign in to mBlock 5

We can use "User Cloud Message" to control Halocode remotely. We need to sign in to mBlock 5 first.

<img src="image/login.gif" width="800px;" style="padding:5px 5px 20px 5px;">

## Toggle on Upload mode

<img src="../tutorials/images/toggle-on-upload.gif" width="800px;" style="padding:5px 5px 20px 5px;">

## Connect to the internet

1\. Drag an Events block <span style="background-color:#FFBF00;color:white;padding:4px; font-size: 12px;border-radius:3px;">when Halocode starts up</span> and a Wi-Fi block <span style="background-color:#76CE14;;color:white;padding:4px; font-size: 12px;border-radius:3px;">connect to Wi-Fi () password ()</span>. Input the Wi-Fi name and password.

<img src="../tutorials/images/connect-wifi-1.gif" width="800px;" style="padding:5px 5px 20px 5px;">

2\. We want to know when the Wi-Fi is successfully connected. Add a Control block <span style="background-color:#FFAB19;color:white;padding:4px; font-size: 12px;border-radius:3px;">wait ()</span>, a Wi-Fi block <span style="background-color:#76CE14;color:white;padding:4px; font-size: 12px;border-radius:3px;">Wi-Fi is connected?</span>, and a Lighting block <span style="background-color:#8521D3;color:white;padding:4px; font-size: 12px;border-radius:3px;">all LEDs light up ()</span>.

<img src="../tutorials/images/connect-wifi-2.gif" width="800px;" style="padding:5px 5px 20px 5px;">

## Add a sprite

3\. Under "Sprites", click "+". From the pop-up Sprite Library window, choose "Balloon1" from the Props category.

<img src="image/control-deck-1.gif" width="800px;" style="padding:5px 5px 20px 5px;">

4\. Right click "Balloon1" to make a duplicate. Change the costume to "c", namely color purple.

<img src="image/control-deck-2.gif" width="800px;" style="padding:5px 5px 20px 5px;">

5\. Delete default sprite "Panda".

<img src="image/control-deck-3.gif" width="800px;" style="padding:5px 5px 20px 5px;">

## Program the sprite

6\. Click "+" in the Blocks area to add "User Cloud Message".

<img src="image/control-deck-4.gif" width="800px;" style="padding:5px 5px 20px 5px;">

7\. Program the blue balloon. Add an Events block <span style="background-color:#FFBF00;color:white;padding:4px; font-size: 12px;border-radius:3px;">when this sprite clicked</span> and a User Cloud Message block <span style="background-color:#4EAA83;color:white;padding:4px; font-size: 12px;border-radius:3px;">send user cloud message ()</span>. Name the message "blue".

<img src="image/control-deck-5.gif" width="800px;" style="padding:5px 5px 20px 5px;">

## Add some special effects to the balloon

8\. When the balloon is clicked, we want it to appear like being clicked. When clicked, the balloon gets brighter and bigger, and goes back to original color and size in 0.3 second. Add the following blocks: <span style="background-color:#9966FF;color:white;padding:4px; font-size: 12px;border-radius:3px;">change size by ()</span>, <span style="background-color:#9966FF;color:white;padding:4px; font-size: 12px;border-radius:3px;">change (color) effect by ()</span>, <span style="background-color:#FFAB19;color:white;padding:4px; font-size: 12px;border-radius:3px;">wait () seconds</span>, <span style="background-color:#9966FF;color:white;padding:4px; font-size: 12px;border-radius:3px;">set size to (100)%</span>,  <span style="background-color:#9966FF;color:white;padding:4px; font-size: 12px;border-radius:3px;">set (color) effect to (0)</span>.

<img src="image/control-deck-6.gif" width="800px;" style="padding:5px 5px 20px 5px;">

9\. Likewise, we can program the purple balloon. Add an Events block <span style="background-color:#FFBF00;color:white;padding:4px; font-size: 12px;border-radius:3px;">when this sprite clicked</span> and a User Cloud Message block <span style="background-color:#4EAA83;color:white;padding:4px; font-size: 12px;border-radius:3px;">send user cloud message ()</span>. Name the message "purple". Apply the same special effects.

<img src="image/control-deck-6.png" style="padding:5px 5px 20px 5px;">

## Halocode receives user cloud message

10\. Under "Devices", select "Halocode".

11\. Add a Wi-Fi block <span style="background-color:#76CE14;color:white;padding:4px; font-size: 12px;border-radius:3px;">when receiving user cloud message ()</span> and input message "blue".

<img src="image/control-deck-7.gif" width="800px;" style="padding:5px 5px 20px 5px;">

## Set Halocode's LEDs

12\. Add a Lighting block <span style="background-color:#8521D3;color:white;padding:4px; font-size: 12px;border-radius:3px;">all LEDs light up ()</span>, change the color to blue. Add a Control block <span style="background-color:#FFAB19;color:white;padding:4px; font-size: 12px;border-radius:3px;">wait () seconds</span> and another Lighting block <span style="background-color:#8521D3;color:white;padding:4px; font-size: 12px;border-radius:3px;">light off all LEDs</span>.

<img src="image/control-deck-8.gif" width="800px;" style="padding:5px 5px 20px 5px;">

13\. Likewise, for receiving the "purple" message, duplicate the script, and change LED color to purple.

<img src="image/control-deck-9.gif" width="800px;" style="padding:5px 5px 20px 5px;">

## Programming result

14\. Upload the program to Halocode. Disconnect Halocode from the PC, and use a battery box to power Halocode. Click the balloons on stage, and check the LEDs of Halocode.

<img src="image/control-deck-10.gif" width="800px;" style="padding:5px 5px 20px 5px;">

<img src="image/control-deck-11.gif" width="800px;" style="padding:5px 5px 20px 5px;">

<p><span style="background-color:#66CDAA;line-height:30px;padding:12px;font-size:16px;border-radius:3px;"><a href="code/halocode's-remote-control-deck.mblock" download style="color:white;font-weight:bold;">Download code</a></span></p>

---
