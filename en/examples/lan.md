# Control Multiple HaloCodes via LAN

Multiple HaloCodes can join the same LAN (Local Area Network), and communicate with each other. We can use Halocode to control others.

<img src="../../zh/examples/image/lan.png" width="500px;" style="padding:5px 5px 20px 5px;">

## Control Halocode B with Halocode A

### Halocode A sets up LAN

1\. Connect Halocode A

<img src="image/mesh-1.gif" width="800px;" style="padding:5px 5px 20px 5px;">

2\. Enable Upload mode

<img src="image/mesh-01.gif" width="800px;" style="padding:5px 5px 20px 5px;">

3\. Add an Events block <span style="background-color:#FFBF00;color:white;padding:4px; font-size: 12px;border-radius:3px;">when Halocode starts up</span>, and a LAN block <span style="background-color:#35D2B2;color:white;padding:4px; font-size: 12px;border-radius:3px;">set up LAN named (mesh1)</span>。

<img src="image/mesh-2.gif" width="800px;" style="padding:5px 5px 20px 5px;">

### Halocode A broadcasts on LAN

4\. Add an Events block <span style="background-color:#FFBF00;color:white;padding:4px; font-size: 12px;border-radius:3px;">when button is pressed</span>, and a LAN block <span style="background-color:#35D2B2;color:white;padding:4px; font-size: 12px;border-radius:3px;">broadcast () on LAN</span>. Name the message "light"

<img src="image/mesh-3.gif" width="800px;" style="padding:5px 5px 20px 5px;">

5\. Upload the program to Halocode A
   
<img src="image/mesh-4.gif" width="800px;" style="padding:5px 5px 20px 5px;">

### Halocode B joins LAN

6\. Connect Halocode B
   
<img src="image/mesh-5.gif" width="800px;" style="padding:5px 5px 20px 5px;">

7\. Enable Upload mode

<img src="image/mesh-6.gif" width="800px;" style="padding:5px 5px 20px 5px;">

8\. Add an Events block <span style="background-color:#FFBF00;color:white;padding:4px; font-size: 12px;border-radius:3px;">when Halocode starts up</span>, and LAN block <span style="background-color:#35D2B2;color:white;padding:4px; font-size: 12px;border-radius:3px;">join LAN named (mesh1)</span>

<img src="image/mesh-7.gif" width="800px;" style="padding:5px 5px 20px 5px;">

### Halocode B receives LAN broadcast

9\. Add a LAN block <span style="background-color:#35D2B2;color:white;padding:4px; font-size: 12px;border-radius:3px;">when receiving LAN broadcast ()</span>, and input "light". Add a Lighting block <span style="background-color:#8521D3;color:white;padding:4px; font-size: 12px;border-radius:3px;">all LEDs light up ()</span>, a Control block <span style="background-color:#FFAB19;color:white;padding:4px; font-size: 12px;border-radius:3px;">wait () seconds</span>, and another Lighting block <span style="background-color:#8521D3;color:white;padding:4px; font-size: 12px;border-radius:3px;">light off all LEDs</span>
   
<img src="image/mesh-8.gif" width="800px;" style="padding:5px 5px 20px 5px;">

10\. Upload the program to Halocode B
   
<img src="image/mesh-9.gif" width="800px;" style="padding:5px 5px 20px 5px;">

### Programming result

11\. Press the button Halocode A
    
<img src="../../zh/examples/image/mesh-11.gif" width="800px;" style="padding:5px 5px 20px 5px;">

**Challenge**

Challenge yourself. Can you make a new project like following example?

<img src="../../zh/examples/image/mesh-10.gif" width="800px;" style="padding:5px 5px 20px 5px;">

<p><span style="background-color:#66CDAA;line-height:30px;padding:12px;font-size:16px;border-radius:3px;"><a href="code/control-multiple-halocode-via-lan.mblock" download style="color:white;font-weight:bold;">Download code</a></span></p>

---