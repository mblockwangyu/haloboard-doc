# Use Global Variable to Interact with Sprites

**Programming result**

When you rotate Halocode around the y-axis, the sun on stage will rotate as well. 

<img src="images/global-variable-0.gif" width="600px;" style="padding:5px 5px 20px 5px;">

**Introduction to Halocode's Axes**

The axes of Halocode are as follows:

<img src="../block-reference/images/motion-sensor.png" width="500px;" style="padding:5px 5px 20px 5px;">


## Create a new variable

1\. Click Variables category, and then click "Make a Variable". Name the variable "y".

<img src="image/global-variable-1.gif" width="800px;" style="padding:5px 5px 20px 5px;">

2\. Add a Variables block <span style="background-color:#FF8C1A;color:white;padding:4px; font-size: 12px;border-radius:3px;">set (y) to ()</span> and a Sensing block <span style="background-color:#F79226;color:white;padding:4px; font-size: 12px;border-radius:3px;">rotation angle around ()</span>. Choose "Y" axis.

<img src="image/global-variable-2.gif" width="800px;" style="padding:5px 5px 20px 5px;">

## Add event and control

3\. Add an Events block <span style="background-color:#FFBF00;color:white;padding:4px; font-size: 12px;border-radius:3px;">when button is pressed</span> and a Control block <span style="background-color:#FFAB19;color:white;padding:4px; font-size: 12px;border-radius:3px;">forever</span>.

<img src="image/global-variable-3.gif" width="800px;" style="padding:5px 5px 20px 5px;">

## Add a sprite

4\. Under "Sprites", click "+". Choose "Sun1".

<img src="image/global-variable-4.gif" width="800px;" style="padding:5px 5px 20px 5px;">

5\. Delete default sprite "Panda".

<img src="image/global-variable-5.gif" width="800px;" style="padding:5px 5px 20px 5px;">

## Program the sun

6\. Add a Motion block <span style="background-color:#4C97FF;color:white;padding:4px; font-size: 12px;border-radius:3px;">turn &#8631; () degrees</span> and a Variables block <span style="background-color:#FF8C1A;color:white;padding:4px; font-size: 12px;border-radius:3px;">y</span>.

<img src="image/global-variable-6.gif" width="800px;" style="padding:5px 5px 20px 5px;">

7\. Add an Events block <span style="background-color:#FFBF00;color:white;padding:4px; font-size: 12px;border-radius:3px;">when green flag clicked</span> and a Control block <span style="background-color:#FFAB19;color:white;padding:4px; font-size: 12px;border-radius:3px;">forever</span>.

<img src="image/global-variable-7.gif" width="800px;" style="padding:5px 5px 20px 5px;">

8\. Rotate Halocode and see what happens.

<p><span style="background-color:#66CDAA;line-height:30px;padding:12px;font-size:16px;border-radius:3px;"><a href="code/global-variable.mblock" download style="color:white;font-weight:bold;">Download code</a></span></p>

---



