# Press the Button to play LED Animation "Meteor"

The programmable button of Halocode has plenty of usage scenarios, such as activating a script. The four touchpads of Halocode can report whether it is being touched via the change in capacitance.

1\. Drag an Events block <span style="background-color:#FFBF00;color:white;padding:4px; font-size: 12px;border-radius:3px;">when button is pressed</span> and a Lighting block <span style="background-color:#8521D3;color:white;padding:4px; font-size: 12px;border-radius:3px;">play LED animation () until done</span> to the Scripts area. Choose LED animation "meteor". When we press the button of Halocode, the LED animation "meteor" will be played.

<img src="image/button-meteor-1.gif" width="800px;" style="padding:5px 5px 20px 5px;">

2\. Add a Control block <span style="background-color:#FFAB19;color:white;padding:4px; font-size: 12px;border-radius:3px;">repeat until ()</span> and a Sensing block <span style="background-color:#F79226;color:white;padding:4px; font-size: 12px;border-radius:3px;">touchpad () is touched?</span>. Keep the default touchpad0.

<img src="image/button-meteor-2.gif" width="800px;" style="padding:5px 5px 20px 5px;">

3\. Try pressing the button of Halocode and then touch touchpad0.

<p><span style="background-color:#66CDAA;line-height:30px;padding:12px;font-size:16px;border-radius:3px;"><a href="code/press-button-to-play-led-animation-meteor.mblock" download style="color:white;font-weight:bold;">Download code</a></span></p>

---