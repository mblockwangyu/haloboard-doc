# Emotion Detector

When a happy face is detected, the LED ring of Halocode will light up, and make a smiling face.

<img src="image/emotion-detector.png" width="360px;" style="padding:5px 5px 20px 5px;">

## Add a sprite

1\. Delete default sprite Panda

<img src="image/emotion-detector-1.gif" width="800px;" style="padding:5px 5px 20px 5px;">

2\. Add a new sprite, Empty button14

<img src="image/emotion-detector-2.gif" width="800px;" style="padding:5px 5px 20px 5px;">

3\. Change the color, and size of the button.

<img src="image/emotion-detector-3.gif" width="800px;" style="padding:5px 5px 20px 5px;">

4\. In the Blocks Area, click "+ extension" to add Cognitive Services (AI) blocks

<img src="image/emotion-detector-4.gif" width="800px;" style="padding:5px 5px 20px 5px;">

5\. Drag an Events block <span style="background-color:#FFBF00;color:white;padding:4px; font-size: 12px;border-radius:3px;">when this sprite clicked</span> to the Scripts Area. Add an AI block  <span style="background-color:#FF3580;color:white;padding:4px; font-size: 12px; border-radius:3px;">recognize emotion after (1) seconds</span>

<img src="image/emotion-detector-5.gif" width="800px;" style="padding:5px 5px 20px 5px;">

6\. Add a Control block <span style="background-color:#FFAB19;color:white;padding:4px; font-size: 12px;border-radius:3px;">ig () then ()</span>, an AI block <span style="background-color:#FF3580;color:white;padding:4px; font-size: 12px; border-radius:3px;">emotion is (happiness)?</span>, and an Events block <span style="background-color:#FFBF00;color:white;padding:4px; font-size: 12px;border-radius:3px;">broadcast ()</span>. Create a new piece of message named "laugh"

<img src="image/emotion-detector-6.gif" width="800px;" style="padding:5px 5px 20px 5px;">

7\. Check the box of AI block <span style="background-color:#FF3580;color:white;padding:4px; font-size: 12px; border-radius:3px;">happiness value ()</span> to display the value on stage

<img src="image/emotion-detector-7.gif" width="800px;" style="padding:5px 5px 20px 5px;">

## Halocode makes a smiling face

8\. Add an Events block <span style="background-color:#FFBF00;color:white;padding:4px; font-size: 12px;border-radius:3px;">当when I receive (laugh)</span>, and a Lighting block <span style="background-color:#8521D3;color:white;padding:4px; font-size: 12px;border-radius:3px;">show ()</span>. Draw a smiling face

<img src="image/emotion-detector-8.gif" width="800px;" style="padding:5px 5px 20px 5px;">

9\. Add a Control block <span style="background-color:#FFAB19;color:white;padding:4px; font-size: 12px;border-radius:3px;">wait (2) seconds</span>, and a Lighting block <span style="background-color:#8521D3;color:white;padding:4px; font-size: 12px;border-radius:3px;">light off all LEDs</span>

<img src="image/emotion-detector-9.gif" width="800px;" style="padding:5px 5px 20px 5px;">

<p><span style="background-color:#66CDAA;line-height:30px;padding:12px;font-size:16px;border-radius:3px;"><a href="code/emotion-detector.mblock" download style="color:white;font-weight:bold;">Download code</a></span></p>

---