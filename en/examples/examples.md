# Halocode Projects

These projects are intended to give you some inspiration to start your own creations.

### [Basic Projects](basics.md)

<table  cellpadding="15px;" cellspacing="12px;" style="font-size: 12px;text-align:center;">
<tr>
<td width="25%;"><a href="basics-1.html" target="_blank"><img src="../../zh/examples/image/led-ring.png" width="150px;"></a><br>
<p>Make a Smiling Face with the LED Ring</p></td>

<td width="25%;"><a href="basics-2.html" target="_blank"><img src="../../zh/examples/image/volume-detector.png" width="150px;"></a><br>
<p>Make a Volume Detector</p></td>

<td width="25%;"><a href="basics-3.html" target="_blank"><img src="../../zh/examples/image/press-button.png" width="150px;"></a><br>
<p>Press the Button to Play LED Animation "Meteor"</p></td>

<td width="25%;"><a href="rainbow-button.html" target="_blank"><img src="../../zh/examples/image/rainbow-button.png" width="150px;"></a><br>
<p>Rainbow Button</p></td>
</tr>

<tr>
<td><a href="color-mixer.html" target="_blank"><img src="../../zh/examples/image/color-mixer.png" width="150px;"></a><br>
<p>Color Mixer</p></td>
<td><a href="basics-5.html" target="_blank"><img src="../../zh/examples/image/compare-strength.png" width="150px;"></a><br>
<p>Compare Strength</p></td>
<td><a href="energy-ring.html" target="_blank"><img src="../../zh/examples/image/energy-ring.png" width="150px;"></a><br>
<p>Energy Ring</p></td>
<td><a href="smile.html" target="_blank"><img src="../../zh/examples/image/smile.png" width="150px;"></a><br>
<p>Raise Your Hand to Make Halocode Smile</p></td>
</tr>

<tr>
<td><a href="connect-wifi.html" target="_blank"><img src="../../zh/examples/image/wifi.png" width="150px;"></a><br>
<p>Connect Wi-Fi</p></td>
<td><a href="lan.html" target="_blank"><img src="../../zh/examples/image/lan.png" width="150px;"></a><br>
<p>Control Multiple Halocods via LAN</p></td>
</tr>

</table>

<br>

### [Level Up](level-up.md)

<table  cellpadding="15px;" cellspacing="12px;" style="font-size: 12px;text-align:center;">

<tr>
<td colspan="2"><p><b>Virtual vs Reality</b></p></td>
<td colspan="2"><p><b>IoT</b></p></td>
</tr>

<tr>
<td width="25%;"><a href="level-up-1.html" target="_blank"><img src="../../zh/examples/image/steering-wheel.png" width="150px;"></a><br>
<p>Make Halocode the Steering Wheel of the Car on Stage</p></td>

<td width="25%;"><a href="level-up-5.html" target="_blank"><img src="../../zh/examples/image/global-variable.png" width="150px;"></a><br>
<p>Use Global Variable to Interact with Sprites</p></td>

<td width="25%;"><a href="remote-control.html" target="_blank"><img src="../../zh/examples/image/remote-control.png" width="150px;"></a><br>
<p>Remote Control</p></td>

<td width="25%;"><a href="level-up-2.html" target="_blank"><img src="../../zh/examples/image/control-deck.png" width="150px;"></a><br>
<p>Halocode's Remote Control Deck</p></td>
</tr>
</table>

<br>

<table  cellpadding="15px;" cellspacing="12px;" style="font-size: 12px;text-align:center;width:100%;">
<tr>
<td colspan="3"><p><b>Artificial Intelligence</b></p></td>
</tr>

<tr>
<td width=“33%;”><a href="level-up-3.html" target="_blank"><img src="image/voice-control.png" width="150px;"></a><br>
<p>Control Halocode's LEDs<br>via Voice Command</p></td>
<td width=“33%;”><a href="emotion-detector.html" target="_blank"><img src="image/emotion-detector.png" width="150px;"></a><br>
<p>Emotion Detector</p></td>
<td width=“33%;”><a href="level-up-6.html" target="_blank"><img src="image/deep-learning.png" width="150px;"></a><br>
<p>Deep Learning and<br>Facial Recognition</p></td>
</tr>
</table>

<br>

### [Workshop](workshop.md)

<table  cellpadding="15px;" cellspacing="12px;" style="font-size:12px;text-align:center;width:100%;">
<tr>
<td width="33%;"><a href="pedometer.html" target="_blank"><img src="../../zh/examples/image/pedometer.png" width="150px;"></a><br>
<p>Pedometer</p></td>

<td width="33%;"><a href="workshop-1.html" target="_blank"><img src="../../zh/examples/image/cat-7.jpg" width="150px;"></a><br>
<p>A Kitten with Blinking Eyes and a Waving Tail</p></td>

<td width="33%;"><a href="workshop-2.html" target="_blank"><img src="image/smart-home.png" width="150px;"></a><br>
<p>Smart Home</p></td>

</tr>
</table>

<br>

<p><span style="background-color:#66CDAA;line-height:30px;padding:12px;font-size:16px;border-radius:3px;"><a href="code/code.rar" download style="color:white;font-weight:bold;">Download code</a></span></p>

---