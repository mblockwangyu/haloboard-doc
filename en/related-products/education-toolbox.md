# AIoT Education Toolbox
## Modules
<table style="margin-left: auto; margin-right: auto" width="800px"  align="center">
        <tr>
            <td>


<strong>Sensors：</strong><br> 
<a href=../mbuild/hardware/sensors/pir-sensor.md> PIR Sensor</a><br>
<a href=../mbuild/hardware/sensors/ranging-sensor.md> Ranging Sensor</a><br>
<a href=../mbuild/hardware/sensors/light-sensor.md> Light Sensor</a><br>
<a href=../mbuild/hardware/sensors/ultrasonic-sensor.md> Ultrasonic Sensor</a><br>
<a href=../mbuild/hardware/sensors/sound-sensor.md> Sound Sensor</a><br>
<a href=../mbuild/hardware/sensors/temperature-sensor.md> Temp Sensor</a><br>
<a href=../mbuild/hardware/sensors/dual-color-sensor.md> Dual RGB Color Sensor</a><br>
<a href=../mbuild/hardware/sensors/motion-sensor.md> Motion Sensor</a><br>
<a href=../mbuild/hardware/sensors/gas-sensor.md> Gas Sensor</a><br>
<a href=../mbuild/hardware/sensors/humiture-sensor.md> Humiture Sensor</a><br>
<a href=../mbuild/hardware/sensors/magnetic-sensor.md> Magnetic Sensor</a><br>
<a href=../mbuild/hardware/sensors/flame-sensor.md> Flame Sensor</a><br>
<a href=../mbuild/hardware/sensors/soil-moisture-sensor.md> Soil Moisture</a><br>
<a href=../mbuild/hardware/sensors/smart-camera.md > Smart Camera


</a><br>
</td>

<td>

<strong>Interaction：</strong><br> 
<a href=../mbuild/hardware/interaction/angle-sensor.md> Angle Sensor</a><br>
<a href=../block-reference/slider.md> Slider</a><br>
<a href=../mbuild/hardware/interaction/button.md>Button</a><br>
<a href=../mbuild/hardware/interaction/multi-touch.md> Multi Touch</a><br>
<a href=../mbuild/hardware/interaction/joystick.md> Joystick</a><br>

<strong>Output：</strong><br> 
<a href=../mbuild/hardware/output-modules/speaker.md> Speaker</a><br>
<a href=../mbuild/hardware/motion/motor-driver.md> Motor Driver</a><br>
<a href=../mbuild/hardware/motion/servo-driver.md> Servo Driver</a><br>
<a href=../mbuild/hardware/display/led-matrix.md> 8x16 LED Matrix</a><br>
<a href=../mbuild/hardware/light/led-driver.md> LED Driver</a><br>

<strong>Else：</strong><br> 
<a href=../mbuild/hardware/communication/ir.md> IR Transceiver</a><br>
<a href=../mbuild/hardware/power/power.md> Power</a><br>
<a href=../mbuild/hardware/accessories/extend-modules.md> Extend Block</a><br>
<a href=../mbuild/hardware/light/rgb-led.md> RGB LED</a><br>
<a href=../mbuild/hardware/communication/bluetooth.md> Bluetooth</a><br>


</td>
        </tr>
    </table>



---