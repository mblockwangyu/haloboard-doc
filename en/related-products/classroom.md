# AIoT Classroom Kit

## Modules

<table style="margin-left: auto; margin-right: auto" width="800px"  align="center">
        <tr>
            <td>
<strong>Mainboard：</strong><br>         
<a href=../README.md> Halocode</a><br>
<strong>Sensors：</strong><br> 
<a href=../mbuild/hardware/sensors/ranging-sensor.md> Ranging Sensor</a><br>
<a href=../mbuild/hardware/sensors/light-sensor.md> Light Sensor</a><br>
</td>

<td>


<strong>Output：</strong><br> 
<a href=../mbuild/hardware/output-modules/speaker.md> Speaker</a><br>
<a href=../mbuild/hardware/motion/motor-driver.md> Motor Driver</a><br>
<a href=../mbuild/hardware/display/led-matrix.md> 8x16 LED Matrix</a><br>

<strong>Else：</strong><br> 
<a href=../mbuild/hardware/power/power.md> Power</a><br>

</td>
        </tr>
    </table>


