# AI & IoT

* [Standard kit](standard-kit.md)
* [AIoT Classroom Kit](classroom.md)
* [AIoT Creator Add-on Pack](creator.md)
* [AIoT Scientist Add-on Pack](scientist.md)
* [AIoT Education Toolbox](education-toolbox.md)