# AIoT Scientist Add-on Pack
## Modules
<table style="margin-left: auto; margin-right: auto" width="800px"  align="center">
        <tr>
            <td>
<strong>Mainboard：</strong><br>         
<a href=../README.md> halocode</a><br>
<strong>Sensors：</strong><br> 
<a href=../mbuild/hardware/sensors/ranging-sensor.md> Ranging Sensor</a><br>
<a href=../mbuild/hardware/sensors/light-sensor.md> Light Sensor</a><br>
<a href=../mbuild/hardware/sensors/temperature-sensor.md> Temp Sensor</a><br>
<a href=../mbuild/hardware/sensors/gas-sensor.md> Gas Sensor</a><br>
<a href=../mbuild/hardware/sensors/humiture-sensor.md> Humiture Sensor</a><br>
<a href=../mbuild/hardware/sensors/magnetic-sensor.md> Magnetic Sensor</a><br>
<a href=../mbuild/hardware/sensors/flame-sensor.md> Flame Sensor</a><br>
<a href=../mbuild/hardware/sensors/soil-moisture-sensor.md> Soil Moisture</a><br>
</td>

<td>
<strong>Interaction：</strong><br> 
<a href=../block-reference/slider.md> Slider</a><br>
<a href=../mbuild/hardware/interaction/joystick.md> Joystick</a><br>

<strong>Output：</strong><br> 
<a href=../mbuild/hardware/output-modules/speaker.md> Speaker</a><br>
<a href=../mbuild/hardware/motion/motor-driver.md> Motor Driver</a><br>
<a href=../mbuild/hardware/motion/servo-driver.md> Servo Driver</a><br>
<a href=../mbuild/hardware/display/led-matrix.md> 8x16 LED Matrix</a><br>
<a href=../mbuild/hardware/light/led-driver.md> LED Driver</a><br>

<strong>Else：</strong><br> 
<a href=../mbuild/hardware/power/power.md> Power</a><br>
<a href=../mbuild/hardware/accessories/extend-modules.md> Extend Block</a><br>
</td>
        </tr>
    </table>





---