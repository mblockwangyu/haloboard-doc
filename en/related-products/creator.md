# AIoT Creator Add-on Pack

## Modules

<table style="margin-left: auto; margin-right: auto" width="800px"  align="center">
        <tr>
            <td>
<strong>Mainboard：</strong><br>         
<a href=../README.md> halocode</a><br>
<strong>Sensors：</strong><br> 
<a href=../mbuild/hardware/sensors/pir-sensor.md> PIR Sensor</a><br>
<a href=../mbuild/hardware/sensors/ranging-sensor.md> Ranging Sensor</a><br>
<a href=../mbuild/hardware/sensors/dual-color-sensor.md> Dual RGB Color Sensor</a><br>

</td>

<td>

<strong>Interaction：</strong><br> 
<a href=../mbuild/hardware/interaction/angle-sensor.md> Angle Sensor</a><br>

<strong>Output：</strong><br> 
<a href=../mbuild/hardware/output-modules/speaker.md> Speaker</a><br>
<a href=../mbuild/hardware/motion/motor-driver.md> Motor Driver</a><br>
<a href=../mbuild/hardware/motion/servo-driver.md> Servo Driver</a><br>
<a href=../mbuild/hardware/display/led-matrix.md> 8x16 LED Matrix</a><br>
<a href=../mbuild/hardware/light/led-driver.md> LED Driver</a><br>

<strong>Else：</strong><br> 
<a href=../mbuild/hardware/power/power.md> Power</a><br>
<a href=../mbuild/hardware/accessories/extend-modules.md> Extend Block</a><br>

</td>
        </tr>
    </table>
    


---