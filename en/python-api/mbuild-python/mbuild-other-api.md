# Other mBuild Modules

The API functions described in this section are applicable to other mBuild electronic modules.
**Note:** To use these functions, you need to purchase an add-on pack or kit that includes the corresponding mBuild electronic modules.

### Omitting "halocode" from the code
**Note:** The word **halocode** is omitted from all the API functions described in this section. For example, **led_driver.off()** is actually **halocode.led_driver.off()**. All the functions of the mBuild electronic modules are maintained in the halocode library, and therefore it is recommended to omit **halocode.** from the code to simplify it.

``` py
from halocode import *
```
<br>

### Common Parameter `index`
All API functions for mBuild electronic modules include the parameter **index**, which indicates the place of a module among the ones of the same type in the chain referred to by an API function. Generally, the default value is **1**. Therefore, if only one module of each type is used in the chain, you don't need to set this parameter.
When two or more modules of the same type are used in the chain, you need to set **index** to 2, 3, 4, or another number to specify the second, third, forth, or another module. For example, **motor_driver.set(100, index = 2)** indicates that the output power of the second motor driver is set to 100.

### IR Remote
`ir.send(message, index = 1)`
Sends an IR message.<br>Parameters:
- ***message***: character string with a maximum length of 30. Currently, only English characters, numbers, and punctuations are supported.

`ir.receive(index = 1)`
Obtains the received IR message.<br>
Returns a character string.

`ir.record(record_id, index = 1)`
Records an IR message. This API function blocks the current thread for three seconds to complete the recording of the IR signal.<br>Parameters:
- ***record_id***: numeric value, indicating the serial number of an IR message. The setting range is 1 to 2. The IR remote module can record a maximum of two IR signals.

`ir.send_record(record_id, index = 1)`
Sends the recorded IR message.<br>Parameters:
- ***record_id***: numeric value, indicating the serial number of an IR message. The setting range is 1 to 2. The IR remote module can record a maximum of two IR signals.

`ir.is_receive(message, index = 1)`
Determines whether the IR remote module receives the specified IR signal.<br>Parameters:
- ***message***: character string or character string variable. The following table describes the available variables and their definitions.<br>
<table>
<tr><td>Variable</td><td>Definition</td></tr>
<tr><td>IR_REMOTE.up </td><td> ↑</td></tr>
<tr><td>IR_REMOTE.down  </td><td> ↓
<tr><td>IR_REMOTE.left  </td><td> ←
<tr><td>IR_REMOTE.right </td><td> →
<tr><td>IR_REMOTE.set   </td><td>Setting
<tr><td>IR_REMOTE.zero   </td><td>0
<tr><td>IR_REMOTE.one    </td><td>1
<tr><td>IR_REMOTE.two    </td><td>2
<tr><td>IR_REMOTE.three  </td><td>3
<tr><td>IR_REMOTE.four   </td><td>4
<tr><td>IR_REMOTE.five   </td><td>5
<tr><td>IR_REMOTE.six    </td><td>6
<tr><td>IR_REMOTE.seven  </td><td>7
<tr><td>IR_REMOTE.eight  </td><td>8
<tr><td>IR_REMOTE.nine   </td><td>9
<tr><td>IR_REMOTE.A      </td><td>A
<tr><td>IR_REMOTE.B     </td><td> B
<tr><td>IR_REMOTE.C     </td><td> C
<tr><td>IR_REMOTE.D     </td><td> D
<tr><td>IR_REMOTE.E     </td><td> E
<tr><td>IR_REMOTE.F     </td><td> F
</table>


