# Python API for mBuild Electronic Modules

The application programming interface (API) functions described in this section are applicable to mBuild electronic modules, which allow you to expand the functions and application scenarios of Halocode.

**Note:** To use these functions, you need to purchase an add-on pack or kit that includes mBuild electronic modules.

<img src="images/mbuild-all.png" style="padding:3px 0px 12px 3px;width:500px;">

* [mBuild Input Modules](mbuild-input-api.md)
* [mBuild Output Modules](mbuild-output-api.md)
* [Other mBuild Modules](mbuild-other-api.md)