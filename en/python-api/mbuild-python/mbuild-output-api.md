# mBuild Output Modules
The API functions described in this section are applicable to mBuild electronic modules of the output type.

**Note:** To use these functions, you need to purchase an add-on pack or kit that includes mBuild electronic modules of the output type.

### Omitting "halocode" from the code
**Note:** The word **halocode** is omitted from all the API functions described in this section. For example, **led_driver.off()** is actually **halocode.led_driver.off()**. All the functions of the mBuild electronic modules are maintained in the halocode library, and therefore it is recommended to omit **halocode.** from the code to simplify it.

``` py
from halocode import *
```

### Common Parameter `index`
All API functions for mBuild electronic modules include the parameter **index**, which indicates the place of a module among the ones of the same type in the chain referred to by an API function. Generally, the default value is **1**. Therefore, if only one module of each type is used in the chain, you don't need to set this parameter.
When two or more modules of the same type are used in the chain, you need to set **index** to 2, 3, 4, or another number to specify the second, third, forth, or another module. For example, **motor_driver.set(100, index = 2)** indicates that the output power of the second motor driver is set to 100.

## Light
### LED Matrix
### `led_matrix.show(image = "hi", index = 1)`
Sets the image to be displayed on the LED matrix.<br>Parameters:
- ***image***: character string. The default value is **hello**. The following figure shows the image on the LED matrix when **image** is set to **hi**.

![](images/hi.png)

<div style='display: none'>
//哈哈我是注释，不会在浏览器中显示。
//hello 是动图不好显示，所以这里用hi。
</div>



To display an image that is not predefined, you need to set **image** to the hexadecimal number corresponding to the image. The hexadecimal numbers are obtained as follows:<br>
Take the on/off state of the eight LEDs on one column as an 8-digit binary number. Convert the 8-digit binary number into a 2-digit hexadecimal number. There is 16 columns, and therefore a 32-digit hexadecimal number can be used to indicate the on/off state of the LEDs. The following figure shows the LEDs on the LED matrix.

![](images/点阵对照表.png)

**Example program 1**
```py
from halocode import *
led_matrix.show(image = "hello", index = 1) #Enables the LED matrix to display the image "hello"
```
**Example program 2**
```py
from halocode import *
led_matrix.show(image = "00003c7e7e3c000000003c7e7e3c0000", index = 1) 
#Enables the LED matrix to display the image "hello"
```

### `led_matrix.print(message, index = 1)`
Displays the specified characters on the LED matrix in scrolling mode.<br>Parameters:
- ***message***: character string. The value can be a combination of any English characters, digits, and  punctuations. Chinese characters are not supported. When exceeding the display range of the LED matrix, a message is displayed in scrolling mode.

### `led_matrix.print_until_done(message, index = 1)`
Displays the specified characters on the LED matrix until the scrolling ends.<br>Parameters:
- ***message***: character string. The value can be a combination of any English characters, digits, and  punctuations. Chinese characters are not supported. When exceeding the display range of the LED matrix, a message is displayed in scrolling mode. The thread is blocked until the scrolling ends.

### `led_matrix.print_at(message, x, y, index = 1)`
Displays the specified characters with the specified start point (x,y) on the LED matrix.<br>Parameters:
- ***message***: character string. The value can be a combination of any English characters, digits, and punctuations. Chinese characters are not supported. When exceeding the display range of the LED matrix, a message is displayed in scrolling mode. The thread is blocked until the scrolling ends.
- ***x***: numeric value ranging from 0 to 15, indicating the start x-coordinate
- ***y***: numeric value ranging from 0 to 7, indicating the start y-coordinate

### `led_matrix.on(x, y, index = 1)`
Lights up the LED in the specified position (x,y) on the LED matrix.<br>Parameters:
- ***x***: numeric value ranging from 0 to 15, indicating the x-coordinate of the LED to be lit up
- ***y***: numeric value ranging from 0 to 7, indicating the y-coordinate of the LED to be lit up

### `led_matrix.off(x, y, index = 1)`
Turns off the LED in the specified position (x,y) on the LED matrix.<br>Parameters:
- ***x***: numeric value ranging from 0 to 15, indicating the x-coordinate of the LED to be turned off
- ***y***: numeric value ranging from 0 to 7, indicating the y-coordinate of the LED to be turned off

### `led_matrix.toggle(x, y, index = 1)`
Changes the state (on/off) of the LED in the specified position (x,y) on the LED matrix.<br>Parameters:
- ***x***: numeric value ranging from 0 to 15, indicating the x-coordinate of the LED of which the state is to be changed
- ***y***: numeric value ranging from 0 to 7, indicating the y-coordinate of the LED of which the state is to be changed

### `led_matrix.clear(index = 1)`
Turns off all the LEDs on the LED matrix.

### RGB LED
### `rgb_led.on(r, g, b, index = 1)`
Sets the color of the RGB LED.<br>Parameters:
- ***r***: numeric value of character string<br>
To set **r** to a numeric value,indicating the value of the red component. The setting range is 0 to 255. The value 0 indicates no red component, and 255 indicates the maximum red component.<br>
To set **r** to a character string, the value is a color name or abbreviation. The following shows the color names and their abbreviations:<br>

```
red r

orange o

yellow y

green g

cyan c

blue b

purple p

white w

black k
```

- ***g***: numeric value, indicating the value of the green component. The setting range is 0 to 255. The value 0 indicates no green component, and 255 indicates the maximum green component.<br>
- ***b***: numeric value, indicating the value of the blue component. The setting range is 0 to 255. The value 0 indicates no blue component, and 255 indicates the maximum blue component.<br>
- ***pct***: numeric value. The setting range is 0 to 100, indicating the brightness of the LED. The default value is 100. When it is set to 0, the LED is turned off; and when it is set to 100, the LED is lit up in the color with the set red, green, and blue components.<br>

### `rgb_led.off(index = 1)`
Turns off the RGB LED.

### `rgb_led.set_red(val, index = 1)`
Changes the R value of the RGB LED.<br>Parameters:
- ***val***: numeric value, indicating the R value of the RGB LED after the change. The setting range is 0 to 255.

### `rgb_led.set_green(val, index = 1)`
Changes the G value of the RGB LED.<br>Parameters:
- ***val***: numeric value, indicating the G value of the RGB LED after the change. The setting range is 0 to 255.

### `rgb_led.set_blue(val, index = 1)`
Changes the B value of the RGB LED.<br>Parameters:
- ***val***: numeric value, indicating the B value of the RGB LED after the change. The setting range is 0 to 255.

### `rgb_led.add_red(val, index = 1)`
Increases the R value of the RGB LED.<br>Parameters:
- ***val***: numeric value, indicating the amount by which the R value of the RGB LED is increased. The setting range is –255 to +255.

### `rgb_led.add_green(val, index = 1)`
Increases the G value of the RGB LED.<br>Parameters:
- ***val***: numeric value, indicating the amount by which the G value of the RGB LED is increased. The setting range is –255 to +255. 

### `rgb_led.add_blue(val, index = 1)`
Increases the B value of the RGB LED.<br>Parameters:
- ***val***: numeric value, indicating the amount by which the B value of the RGB LED is increased. The setting range is –255 to +255.

### `rgb_led.get_red(val, index = 1)`
Obtains the R value of the RGB LED.<br>
Returns a numeric value ranging from 0 to 255.

### `rgb_led.get_green(index = 1)`
Obtains the G value of the RGB LED.<br>
Returns a numeric value ranging from 0 to 255.

### `rgb_led.get_blue(index = 1)`
Obtains the B value of the RGB LED.<br>
Returns a numeric value ranging from 0 to 255.

### LED Driver
### `led_driver.on(r, g, b, id = "all", index = 1)`
Sets the color of the specified LED or all LEDs.<br>Parameters:
- ***r***: numeric value or character string.<br>
To set **r** to a numeric value,indicating the value of the red component. The setting range is 0 to 255. The value 0 indicates no red component, and 255 indicates the maximum red component.<br>
To set **r** to a character string, the value is a color name or abbreviation. The following shows the color names and their abbreviations:

```
red r

orange o

yellow y

green g

cyan c

blue b

purple p

white w

black k
```

- ***g***: numeric value, indicating the value of the green component. The setting range is 0 to 255. The value 0 indicates no green component, and 255 indicates the maximum green component.<br>
- ***b***: numeric value, indicating the value of the blue component. The setting range is 0 to 255. The value 0 indicates no blue component, and 255 indicates the maximum blue component.<br>
- ***id***: character string or numeric value. The default value is **all**.<br>To set **id** to a character string, the value must be **all**, indicating all LEDs.<br>To set **id** to a numeric value, the setting range is 1 to 96, indicating the serial number of an LED.

### `led_driver.show(color, index = 1)`
Sets color(s) for multiple LEDs.<br>Parameters:
- ***color***: character string, in the format of "color1 color2 color3 color4 color5 color6 color7 color8 color9 color10 color11 color12", where colorx can be red, green, blue, yellow, cyan, purple, white, orange, or black, or it can also be r, g, b, y, c, p, w, o, or k, the abbreviation of a color name. When more than 12 LEDs are specified, only the first 12 LEDs are set.

### `led_driver.off(led_id = "all", index = 1)`
Turns off the specified LED or all LEDs.<br>Parameters:
- ***id***: character string or numeric value. The default value is **all**.<br>
To set **id** to a character string, the value must be **all**, indicating all LEDs.<br>To set **id** to a numeric value, the setting range is 1 to 96, indicating the serial number of an LED.

### `led_driver.set_red(val, led_id = "all", index = 1)`
Changes the R value of the specified LED.<br>Parameters:
- ***val***: numeric value, indicating the R value of the specified LED after the change. The setting range is 0 to 255.
- ***id***: character string or numeric value. The default value is **all**.<br>
To set **id** to a character string, the value must be **all**, indicating all LEDs.<br>To set **id** to a numeric value, the setting range is 1 to 96, indicating the serial number of an LED.

### `led_driver.set_green(val, led_id = "all", index = 1)`
Changes the G value of the specified LED.<br>Parameters:
- ***val***: numeric value, indicating the G value of the specified LED after the change. The setting range is 0 to 255.
- ***id***: character string or numeric value. The default value is **all**.<br>
To set **id** to a character string, the value must be **all**, indicating all LEDs.<br>To set **id** to a numeric value, the setting range is 1 to 96, indicating the serial number of an LED.

### `led_driver.set_blue(val, led_id = "all", index = 1)`
Changes the B value of the specified LED.<br>Parameters:
- ***val***: numeric value, indicating the B value of the specified LED after the change. The setting range is 0 to 255.
- ***id***: character string or numeric value. The default value is **all**.<br>
To set **id** to a character string, the value must be **all**, indicating all LEDs.<br>To set **id** to a numeric value, the setting range is 1 to 96, indicating the serial number of an LED.

### `led_driver.add_red(val, led_id = "all", index = 1)`
Increases the R value of the specified LED.<br>Parameters:
- ***val***: numeric value, indicating the amount by which the R value of the specified LED is increased. The setting range is –255 to +255.
- ***id***: character string or numeric value. The default value is **all**.<br>
To set **id** to a character string, the value must be **all**, indicating all LEDs.<br>To set **id** to a numeric value, the setting range is 1 to 96, indicating the serial number of an LED.

### `led_driver.add_green(val, led_id = "all", index = 1)`
Increases the G value of the specified LED.<br>Parameters:
- ***val***: numeric value, indicating the amount by which the G value of the specified LED is increased. The setting range is –255 to +255.
- ***id***: character string or numeric value. The default value is **all**.<br>
To set **id** to a character string, the value must be **all**, indicating all LEDs.<br>To set **id** to a numeric value, the setting range is 1 to 96, indicating the serial number of an LED.

### `led_driver.add_blue(val, led_id = "all", index = 1)`
Increases the B value of the specified LED.<br>Parameters:
- ***val***: numeric value, indicating the amount by which the B value of the specified LED is increased. The setting range is –255 to +255.
- ***id***: character string or numeric value. The default value is **all**.<br>
To set **id** to a character string, the value must be **all**, indicating all LEDs.<br>To set **id** to a numeric value, the setting range is 1 to 96, indicating the serial number of an LED.

### `led_driver.set_mode(mode = "steady", index = 50)`
Sets the display mode of the LED driver.<br>Parameters:
- ***mode***: character string. The values are described as follows:<br>**steady**: static mode. In this mode, the LEDs are normally on.<br>**breath**: blinking mode. In this mode, the LEDs blink periodically.<br>**marquee**: marquee mode. In this mode, LEDs on the LED strip or LED ring are lit up in scrolling mode.


## Play

### Speaker
### `speaker.mute(index = 1)`
Stops the playing of the speaker.

### `speaker.play_tone(freq, index = 1)`
Sets the frequency of the speaker.<br>Parameters:
- ***freq***: numeric value. The setting range is 20 to 20,000. The speaker makes no sound if the frequency you set exceeds the setting range. In addition, to protect your ears, the upper limit of the frequency is set to 5,000 Hz. If the value you set ranges from 5,000 to 20,000, the speaker plays sounds at the frequency of 5,000 Hz. To play a melody by changing the frequency of the speaker, you need to know the mapping between frequency and notes. The following is a reference.

<table border="1"  align="center" cellpadding="2">

<tr><td></td><th width= "10%">do / C</th><th width= "10%">re / D</th><th width= "10%"> mi / E</th><th width= "10%">fa / F</th><th width= "10%"> sol / G</th><th width= "10%">la / A</th><th width= "10%"> si / B</th></tr>

<tr><th><b>2</th>
<td>65 Hz</td><td>73 Hz</td><td>82 Hz</td><td>87 Hz</td><td>98 Hz</td><td>110 Hz</td><td>123 Hz</td></tr>

<tr><th><b>3</th>
<td>131 Hz</td><td>147 Hz</td><td>165 Hz</td><td>175 Hz</td><td>196 Hz</td><td>220 Hz</td><td>247 Hz</td></tr>

<tr><th><b>4 (Standard Alto)</th>
<td>262 Hz</td><td>294 Hz</td><td>330 Hz</td><td>349 Hz</td><td>392 Hz</td><td>440 Hz</td><td>494 Hz</td></tr>

<tr><th><b>5</th>
<td>523 Hz</td><td>587 Hz</td><td>659 Hz</td><td>698 Hz</td><td>784 Hz</td><td>880 Hz</td><td>988 Hz</td></tr>

<tr><th><b>6</th>
<td>1047 Hz</td><td>1175 Hz</td><td>1319 Hz</td><td>1397 Hz</td><td>1568 Hz</td><td>1760 Hz</td><td>1976 Hz</td></tr>

<tr><th><b>7</th>
<td>2093 Hz</td><td>2349 Hz</td><td>2637 Hz</td><td>2794 Hz</td><td>3136 Hz</td><td>3520 Hz</td><td>3951 Hz</td></tr>

<tr><th><b>8</th>
<td>4186 Hz</td><td>4699 Hz</td><td></td><td></td><td></td><td></td><td></td></tr>
</table>


For example, the standard alto pitch (the first international pitch) is **A4 = 440Hz**.

<div style='display: none'>
哈哈我是注释，不会在浏览器中显示。

['C2','65'],   ['D2','73'],   ['E2','82'],   ['F2','87'],  ['G2','98'],   ['A2','110'],  ['B2','123'],

['C3','131'],  ['D3','147'],  ['E3','165'],  ['F3','175'], ['G3','196'],  ['A3','220'],  ['B3','247'],

['C4','262'],  ['D4','294'],  ['E4','330'],  ['F4','349'], ['G4','392'],  ['A4','440'],  ['B4','494'],

['C5','523'],  ['D5','587'],  ['E5','659'],  ['F5','698'], ['G5','784'],  ['A5','880'],  ['B5','988'],

['C6','1047'], ['D6','1175'], ['E6','1319'], ['F6','1397'],['G6','1568'], ['A6','1760'], ['B6','1976'],

['C7','2093'], ['D7','2349'], ['E7','2637'], ['F7','2794'],['G7','3136'], ['A7','3520'], ['B7','3951'], 
['C8','4186'], ['D8','4699'],
</div>

### `speaker.play_music(music, index = 1)`
Plays the specified preset or user-defined audio file by the speaker.<br>Parameters:
- ***music***: character string. The value can be a 4-byte combination of any English characters, numbers, and punctuations, indicating the name of an audio file stored on the speaker. The speaker supports only mp3 files. This API function does not block the current thread when being implemented.
A large number of short audio files are preset on the speaker. For the file names and descriptions, click <a href="images/List of Preset Sounds.xlsx" download>List of Preset Sounds</a> to download the list. For how to play a user-defined audio file, see [Speaker](http://docs.makeblock.com/halocode/en/mbuild/hardware/output-modules/speaker.html).

### `speaker.play_music_until_done(music, index = 1)`
Plays the specified preset or user-defined audio file by the speaker until the playing is complete.<br>Parameters:
- ***music***: character string. The value can be a 4-byte combination of any English characters, numbers, and punctuations, indicating the name of an audio file stored on the speaker. The speaker supports only mp3 files. This API function blocks the current thread until the playing is complete.

### `speaker.set_vol(val, index = 1)`
Sets the volume of the speaker.<br>Parameters:
- ***vol***: numeric value, indicating the volume value of the speaker. The setting range is 0 to 100.

### `speaker.add_vol(val, index = 1)`
Changes the volume of the speaker (not applicable to some sounds with special frequency).<br>Parameters:
- ***vol***: numeric value, indicating the amount by which the volume of the speaker is changed. The setting range is –100 to +100.

### `speaker.get_vol(index = 1)`
Obtains the volume of the speaker.<br>
Returns a numeric value ranging from 0 to 100. The value 100 indicates the maximum volume of the speaker.

### `speaker.is_play(index = 1)`
Determines whether the speaker is playing music.<br>
Returns a Boolean value:<br>**True**: playing music<br>**False**: not playing music

## Motion
### Motor Driver
### `motor_driver.set(power, index = 1)`
Sets the output power of the motor driver.<br>Parameters:
- ***power***: numeric value, indicating the output power of the motor driver. The setting range is –100 to +100, in percentage (`%`). The output power of 100% corresponds to the PWM waveform with a resolution of 1024. Generally, a positive value enables a motor to run anticlockwise, and a negative value enables a motor to run clockwise.

### `motor_driver.add(power, index = 1)`
Increases the output power of the motor driver.<br>Parameters:
- ***power***: numeric value, indicating the amount by which the output pwoer of the motor driver is increased, in percentage (`%`). The setting range is –200 to +200. The output power of 100% corresponds to the PWM waveform with a resolution of 1024.

### `motor_driver.get(index = 1)`
Obtains the output power of the motor driver.<br>
Returns a numeric value ranging from –100 to +100.

### `motor_driver.get_load(index = 1)`
Obtains the load value of the motor driver. The load value varies according to the operation of the connected motor. When the motor operates with a heavy load or is stalled (heavy load may also be caused by short circuit), a large load value is obtained.<br>
Returns a numeric value ranging from 0 to 1024.

### `motor_driver.stop(index = 1)`
Stops the power output of the motor driver.

### Servo Driver
### `servo_driver.set(angle, index = 1)`
Sets the angle the servo connected to the servo driver is located.<br>Parameters:
- ***angle***: numeric number, indicating the angle the connected servo is located. The setting range is 0 to 180, in degrees (`°`).

### `servo_driver.add(angle, index = 1)`
Sets the angle the connected servo rotates from the current position.<br>Parameters:
- ***angle***: numeric number, indicating the angle the servo rotates from the current position. The setting range is –180 to +180, in degrees (`°`).

### `servo_driver.get(index = 1)`
Obtains the angle the servo connected to the servo driver is located.<br>Returns a numeric value ranging from 0 to 180, in degrees (`°`).

### `servo_driver.get_load(index = 1)`
Obtains the load value of the servo driver. The load value varies according to the operation of the connected servo. When the servo operates with a heavy load or is stalled (heavy load may also be caused  by short circuit), a large load value is obtained.<br>
Returns a numeric value ranging from 0 to 1024.

### `servo_driver.release(index = 1)`
Releases the servo connected to the servo driver. After the servo is released, you can manually rotate it and the **servo_driver.get(index)** function can no longer obtains the correct angle of the servo.

### Smart Servo Motor (12 Kg)
### `smart_servo.turn(angle, speed, index = 1)`
Rotates the smart servo by the specified angle. This function blocks the current thread until the rotating is completed or suspended by another function.<br>Parameters:
- ***angle***: numeric value, indicating the angle the servo rotates from the current position. A positive value indicates clockwise rotation, and a negative value indicates anticlockwise rotation.<br>

### `smart_servo.turn_to(angle, speed, index = 1 )`
Rotates the smart servo to the specified angle. This function blocks the current thread until the rotating is completed or suspended by another function.<br>Parameters:
- ***angle***: numeric value, indicating the angle the servo is located after the rotation. A positive value indicates clockwise rotation, and a negative value indicates anticlockwise rotation.<br>

### `smart_servo.run(power, index = 1)`
Sets the speed at which the smart servo rotates.<br>Parameters:
- ***power***: numeric value. The setting range is 0 to 100.

### `smart_servo.stop(index = "all")`
Stops the motion of the smart servo.<br>Parameters:
- ***index***: character string or numeric value. The default value is **all**, indicating all smart servos connected in the chain.

### `smart_servo.get_angle(index = 1)`
Obtains the angle the smart servo is located.<br>
Returns a numeric value, indicating the angle the smart servo rotates from the initial position.

### `smart_servo.get_speed(index = 1)`
Obtains the rotating speed of the smart servo, either the automatic rotating or the rotating drived by external force.<br>
Returns a numeric value. indicating the rotating speed, in degrees per second (`°/s`).

### `smart_servo.release_angle(index = "all")`
Releases the smart servo. After the smart servo is released, it is no longer locked at the angle and you can rotate it manually. If you do not rotate it, it remains at the angle when it is released until any one of the following functions is executed:<br>smart_servo.turn(angle)<br>smart_servo.turn_to(angle)<br>smart_servo.drive(power, t = "forever")<br>smart_servo.lock_angle()

### `smart_servo.lock_angle(index = "all")`
Locks the smart servo at the current angle. After being locked, a smart servo cannot be rotated by external force.

### `smart_servo.reset(index = "all")`
Resets the smart servo. After being reset, the smart servo takes the current position as the zero point. The number of times a smart servo can be reset is limited, and therefore this function blocks the current thread for three seconds to prevent it from being executed again.