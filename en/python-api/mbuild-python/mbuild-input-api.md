# mBuild Input Modules
The API functions described in this section are applicable to mBuild electronic modules of the input type.

**Note:** To use these functions, you need to purchase an add-on pack or kit that includes mBuild electronic modules of the input type.

### Omitting "halocode" from the code
**Note:** The word **halocode** is omitted from all the API functions described in this section. For example, **led_driver.off()** is actually **halocode.led_driver.off()**. All the functions of the mBuild electronic modules are maintained in the halocode library, and therefore it is recommended to omit **halocode.** from the code to simplify it.

``` py
from halocode import *
```

### Common Parameter `index`
All API functions for mBuild electronic modules include the parameter **index**, which indicates the place of a module among the ones of the same type in the chain referred to by an API function. Generally, the default value is **1**. Therefore, if only one module of each type is used in the chain, you don't need to set this parameter.
When two or more modules of the same type are used in the chain, you need to set **index** to 2, 3, 4, or another number to specify the second, third, forth, or another module. For example, **motor_driver.set(100, index = 2)** indicates that the output power of the second motor driver is set to 100.


## Interaction Modules
### Button
### `button.is_press(index = 1)`
Detects whether the button is pressed.<br>Returns a Boolean value:<br>**True**: button pressed<br>**False**: button not pressed

### `button.get_count(index = 1)`
Obtains the number of times the button is pressed after it is powered on.<br>Returns a numeric value.

### `button.reset_count(index = 1)`
Resets the number of times the button is pressed to zero.

### Angle Sensor
### `angle_sensor.get(index = 1)`
Obtains the angle the angle sensor rotates (relative to the position it is located when being powered).<br>Returns a numeric value in degrees (`°`). The value is increased when the angle sensor rotates clockwise and is reduced when it rotates anticlockwise.

### `angle_sensor.reset(index = 1)`
Resets the angle the angle sensor rotates.

### `angle_sensor.get_speed(index = 1)`
Obtains the speed at which the angle sensor rotates.<br>Returns a numeric value in degrees per second (`°/s`). The value is positive when the angle sensor rotates clockwise and is negative when it rotates anticlockwise.

### `angle_sensor.is_clockwise(index = 1).`
Detects whether the angle sensor is rotating clockwise.<br>Returns a Boolean value:<br>**True**: rotating clockwise<br>**False**: not rotating clockwise

### `angle_sensor.is_anticlockwise(index = 1)`
Detects whether the angle sensor is rotating anticlockwise.<br>Returns a Boolean value:<br>**True**: rotating anticlockwise<br>**False**: not rotating anticlockwise

### Slider
## `slider.get(index = 1)`
Obtains the position of the slider.<br>Returns a numeric value ranging from 0 to 100, in percentage (%).<br>It indicates the position of the slider.

### Joystick
### `joystick.get_x(index = 1)`
Obtains the output value of the joystick at the x axis.<br>Returns a numeric value ranging from –100 to +100.

### `joystick.get_y(index = 1)`
Obtains the output of the joystick at the y axis.<br>Returns a numeric value ranging from –100 to +100.

### `joystick.is_up(index = 1)`
Determines whether the joystick pivots upwards.<br>Returns a Boolean value:<br>True: pivoting upward<br>False: not pivoting upward

### `joystick.is_down(index = 1)`
Determines whether the joystick pivots downward.<br>Returns a Boolean value:<br>**True**: pivoting downward<br>**False**: not pivoting downward

### `joystick.is_left(index = 1)`
Determines whether the joystick pivots towards the left.<br>Returns a Boolean value:<br>True: pivoting towards the left<br>False: not pivoting towards the left

### `joystick.is_right(index = 1)`
Determines whether the joystick pivots towards the right.<br>Returns a Boolean value:<br>**True**: pivoting towards the right<br>**False**: not pivoting towards the right

### Multi Touch
### `multi_touch.is_touch(ch = "any", index = 1)`
Detects whether the specified touch sensor of Multi Touch is touched. The Multi touch module detects objects through the capacitance change of touch sensors. Therefore, with proper threshold settings, it can detect the touching through paper, wooden boards, plastics, or other insulators. The approaching of a conductor, after all, changes the capacitance of a touch sensor.<br>Parameters:
- ***ch***: character string or numeric value. The default value is **any**. 
To set ***ch*** to a character string, it must be set to **any**, which indicates that the touching of any touch sensor can be detected.<br>To set ***ch*** to a numeric value, it can be set to a number ranging from 1 to 8, which indicates the serial number of the corresponding touch sensor.

Returns a Boolean value:<br>**True**: touched<br>**False**: not touched

### `multi_touch.reset(level = "middle", index = 1)`
Sets the threshold for Multi Touch.<br>Parameters:
- ***level***: character string. The values are described as follows:<br>**low**: low sensitivity<br>**middle**: moderate sensitivity<br>**high**: high sensitivity


## Sensors
### Light Sensor
### `light_sensor.get(index = 1)`
Obtains the output value of the light sensor.<br>Returns a numeric value ranging from 0 to 100.

### Dual RGB Color Sensor
### `dual_rgb_sensor.is_color(color = "white", ch, index = 1)`
Determines whether the color sensor detects the specified color.<br>Parameters:
- ***color***: character string. It indicates the color to be detected. The following shows the color names and their abbreviations:<br>

```
red r

yellow y

green g

cyan c

blue b

purple p

white w

black k
```

- ***ch***: numeric value, ranging from 1 to 2, where 1 indicates RGB1, and 2 indicates RGB2.

Returns a Boolean value:<br>**True**: detected<br>**False**: not detected

### `dual_rgb_sensor.get_red(ch = 1, index = 1)`
Obtains the R value of the color detected by the color sensor.<br>Parameters:
- ***ch***: numeric value ranging from 1 to 2, where 1 indicates RGB1 and 2 indicates RGB2. The default value is 1.

Returns a numeric value ranging from 0 to 255.

### `dual_rgb_sensor.get_green(ch = 1, index = 1)`
Obtains the G value of the color detected by the color sensor.<br>Parameters:
- ***ch***: numeric value ranging from 1 to 2, where 1 indicates RGB1 and 2 indicates RGB2. The default value is 1.
gitbb
Returns a numeric value ranging from 0 to 255.

### `dual_rgb_sensor.get_blue(ch = 1, index = 1)`
Obtains the B value of the color detected by the color sensor.<br>Parameters:
- ***ch***: numeric value ranging from 1 to 2, where 1 indicates RGB1 and 2 indicates RGB2. The default value is 1.

Returns a numeric value ranging from 0 to 255.

### `dual_rgb_sensor.get_light(ch = 1, index = 1)`
Obtains the ambient light intensity detected by the color sensor. The fill light provided by the color sensor is also part of the ambient light. Turn off the fill light before you use this API function, if necessary.<br>Parameters:
- ***ch***: numeric value ranging from 1 to 2, where 1 indicates RGB1 and 2 indicates RGB2. The default value is 1.

Returns a numeric value ranging from 0 to 100.

### `dual_rgb_sensor.get_gray_level(ch = 1, index = 1)`
Obtains the reflected light intensity detected by the color sensor. The value is the difference between the ambient light intensity detected when the fill light is turned on and that detected when the fill light is turned off.<br>Parameters:
- ***ch***: numeric value ranging from 1 to 2, where 1 indicates RGB1 and 2 indicates RGB2. The default value is 1.

Returns a numeric value ranging from 0 to 100.

### `dual_rgb_sensor.set_led(color = "white", index = 1)`
Sets the color of the fill light.<br>Parameters:
- ***color***: character string. It indicates the color the fill light is lit up. The following shows the color names and their abbreviations:<br>

```
red r

yellow y

green g

cyan c

blue b

purple p

white w

black k
```

- ***ch***: numeric value ranging from 1 to 2, where 1 indicates RGB1 and 2 indicates RGB2.<br>

### `dual_rgb_sensor.off_led(index = 1)`
Turns off the fill light. Note that the color sensor automatically sets the fill light to the required color when invoking the color detection API function.

### Sound Sensor
### `sound_sensor.get(index = 1)`
Obtains the output value of the sound sensor.<br>Returns a numeric value ranging from 0 to 100.

### PIR Sensor
### `pir.is_detect(index = 1)`
Determines whether the PIR sensor detects human movement. The indicator is lit up when the PIR sensor is triggered. It remains in the triggered state for three seconds. It exits from the triggered state and turns off the indicator if it doesn't detect human movement in the subsequent three seconds.<br>Returns a Boolean value:<br>**True**: human movement detected<br>**False**: no human movement detected

### `pir.get_count(index = 1)`
Obtains the number of times the PIR sensor is triggered after it is powered on.<br>Returns a positive integer.

### `pir.reset_count(index = 1)`
Resets the number of times the PIR sensor is triggered to zero.<br>

### Ultrasonic Sensor
### `ultrasonic.get(index = 1)`
Obtains the distance between the ultrasonic sensor and obstacle.<br>Returns a numeric value ranging from 3 to 300, in centimeters (cm), with a deviation of ±5%.

### Ranging Sensor
### `ranging_sensor.get(index = 1)`
Obtains the distance between the ranging sensor and obstacle.<br>Returns a numeric value ranging from 2 to 200, in centimeters (cm), with a deviation of ±5%.

### Motion Sensor
### `motion_sensor.is_shake(index = 1)`
Detects whether the motion sensor is shaken.<br>Returns a Boolean value:<br>**True**: shaken<br>**False**: not shaken

### `motion_sensor.get_shakeval(index = 1)`
Obtains the strength the motion sensor is shaken.<br>Returns a numeric value ranging from 0 to 100. A greater value indicates stronger shaking.

### `motion_sensor.get_accel(axis, index = 1)`
Obtains the acceleration at the x, y, or z axis.<br>Parameters:
- ***axis***: character string, which can be **x**, **y**, or **z**, indicating an axis defined for the motion sensor.

Returns a numeric value in meters per second (`m/s²`).

### `motion_sensor.is_tiltleft(index = 1)`
Detects whether the motion sensor is tilted towards the left. The threshold is 15 degrees.<br>Returns a Boolean value:<br>**True**: tilted towards the left<br>**False**: not tilted towards the left

### `motion_sensor.is_tiltright(index = 1)`
Detects whether the motion sensor is tilted towards the right. The threshold is 15 degrees.<br>Returns a Boolean value:<br>**True**: tilted towards the right<br>**False**: not tilted towards the right

### `motion_sensor.is_tiltup(index = 1)`
Detects whether the motion sensor is tilted upwards. The threshold is 15 degrees.<br>Returns a Boolean value:<br>**True**: tilted upwards<br>**False**: not tilted upwards

### `motion_sensor.is_tiltdown(index = 1)`
Detects whether the motion sensor is tilted downwards. The threshold is 15 degrees.<br>Returns a Boolean value:<br>**True**: tilted downwards<br>**False**: not tilted downwards

### `motion_sensor.is_faceup(index = 1)`
Detects whether the motion sensor is placed face up.<br>**True**: placed face up<br>**False**: not placed face up

### `motion_sensor.is_facedown(index = 1)`
Detects whether the motion sensor is placed face down.<br>**True**: placed face down<br>**False**: not placed face down

### `motion_sensor.get_roll(index = 1)`
Obtains the rolling angle of the motion sensor.<br>Returns a numeric value ranging from –90 to +90, in degrees (`°`).

### `motion_sensor.get_pitch(index = 1)`
Obtains the pitch angle of the motion sensor.<br>Returns a numeric value ranging from –180 to +180, in degrees (`°`).

### `motion_sensor.get_yaw(index = 1)`
Obtains the yaw angle of the motion sensor.<br>Returns a numeric value ranging from 0 to 360, in  degrees (`°`).<br>
**Note:** No electronic compass is used, and therefore the yaw angle is the integral of the angular speed of the motion sensor at the z axis. Accumulative error may occur. This API function can't be used to obtain the accurate yaw angle.

### `motion_sensor.get_gyro(axis, index = 1)`
Obtains the angular speed of the motion sensor at the x, y, or z axis.<br>Parameters:
- ***axis***: character string, which can be **x**, **y**, or **z**, indicating an axis defined for the motion sensor.

Returns a numeric value in degrees per second (`°/s²`).

### `motion_sensor.get_rotation(axis, index = 1)`
Obtains the angle the motion sensor rotates at the x, y, or z axis. The angle is positive when the motion sensor is rotated anticlockwise.<br>Parameters:
- ***axis***: character string, which can be **x**, **y**, or **z**, indicating an axis defined for the motion sensor.

Returns a numeric value in degrees(`°`).

### `motion_sensor.reset_rotation(axis= "all", index = 1)`
Initializes the current angle of the motion sensor at the x, y, and/or z axis as zero. After the initialization, **get_rotation()** calculates from zero.<br>Parameters:
- ***axis***: character string, which can be **x**, **y**, **z**, or **all**. **x**, **y**, and **z** are the axes defined for the motion sensor. The default value is **all**, indicating all the three axes.

### Soil Moisture Sensor
### `soil_sensor.get(index = 1)`
Obtains the output value of the soil moisture sensor.<br>Returns a numeric value ranging from 0 to 100.

### Temperature Sensor
### `temp_sensor.get(index = 1)`
Obtains the output value of the temperature sensor.<br>Returns a numeric value ranging from –55 to +125, in degrees centigrade (℃).

### Humiture Sensor
### `humiture.get_humidity(index = 1)`
Obtains the output humidity value of the humiture sensor.<br>Returns a numeric value ranging from 0 to 100, in percentage (%).

### `humiture.get_temp(index = 1)`
Obtains the output temperature value of the humiture sensor.<br>Returns a numeric value ranging from –40 to +125, in degrees centigrade (℃).

### MQ2 Gas Sensor
### `mq2.is_detect(level = “high”, index = 1)`
Determines whether the density of the detected combustible gas exceeds the threshold.<br>Parameters:
- ***level***: character string. The values are described as follows:<br>**high**: high sensitivity, with which the sensor is triggered more easily<br>**low**: low sensitivity, with which the sensor isn't triggered easily

### `mq2.get(index = 1)`
Obtains the output value of the MQ2 gas sensor.<br>Returns a numeric value ranging from 0 to 100. A greater value indicates a higher density of combustible gas.

### Flame Sensor
### `flame_sensor.is_detect(index = 1)`
Determines whether a flame is detected.<br>Returns a Boolean value:<br>**True**: detected<br>**False**: not detected

### `flame_sensor.get(index = 1)`
Obtains the output value of the flame sensor.<br>Returns a numeric value ranging from 0 to 100.

### Magnetic Sensor
### `magnetic_sensor.is_detect(index = 1)`
Determines whether a magnet is detected around the magnetic sensor.<br>Returns a Boolean value:<br>**True**: detected<br>**False**: not detected

### `magnetic_sensor.get_count(index = 1)`
Obtains the number of times the magnetic sensor is triggered after it is powered on.<br>Returns a numeric value.

### `magnetic_sensor.reset_count(index = 1)`
Resets the number of times the magnetic sensor is triggered to zero.

### Smart Camera
### `smart_camera.set_mode(mode = "color", index = 1)`
Sets the detection mode of the smart camera.<br>Parameters:
- ***mode***: character string.<br>**color**: color block detection mode, in which the smart camera can detect and track color blocks<br>**line**: line tracking mode, in which the smart camera can track lines and barcodes.

### `smart_camera.learn(sign = 1, t = "until_button", index = 1)`
Sets the number of the color block to be learned.<br>Parameters:
- ***sign***: numeric value ranging from 1 to 7, indicating the number of a color block.<br>
- ***t***: numeric value or character string. The default value is **until_button**.<br>To set ***t*** to a character string, it must be set to **until_button**, which indicates that the learning ends when the on-board button is pressed.<br>When ***t*** is set to a numeric value, it indicates the waiting time before the color block is learned and recorded. The thread is blocked until the learning and recording ends.

### `smart_camera.detect_sign(sign, index = 1)`
Determines whether the specified color block is detected.<br>Parameters:
- ***sign***: numeric value ranging from 1 to 7, indicating the serial number of a color block.

Returns a Boolean value:<br>**True**: detected<br>**False**: not detected

### `smart_camera.detect_sign_location(sign, location, index = 1)`
Determines whether the specified color block is detected in the specified area of the captured image.<br>Parameters:
- ***sign***: numeric value ranging from 1 to 7, indicating the serial number of a color block.<br>
- ***location***: character string. The values are described as follows:<br>**up**: upper area of the captured image<br>**down**: lower area of the captured image<br>**left**: left side of the captured image<br>**right**: right side of the captured image<br>**middle**: center of the captured image<br>
The following figure shows the areas of a captured image.<br>![](images/视觉模块识别区.png)

Returns a Boolean value:<br>**True**: detected<br>**False**: not detected

### `smart_camera.get_sign_x(sign, index = 1)`
Obtains the x-coordinate of the specified color block in the captured image.<br>Parameters:
- ***sign***: numeric value ranging from 1 to 7, indicating the serial number of a color block.

Returns a numeric value ranging from 0 to 319. If the specified color block is not detected, the value 0 is returned.

### `smart_camera.get_sign_y(sign, index = 1)`
Obtains the y-coordinate of the specified color block in the captured image.<br>Parameters:
- ***sign***: numeric value ranging from 1 to 7, indicating the serial number of a color block.

Returns a numeric value ranging from 0 to 239. If the specified color block is not detected, the value 0 is returned.

### `smart_camera.get_sign_wide(sign, index = 1)`
Obtains the width of the specified color block in the captured image.<br>Parameters:
- ***sign***: numeric value ranging from 1 to 7, indicating the serial number of a color block.

Returns a numeric value ranging from 0 to 319. If the specified color block is not detected, the value 0 is returned.

### `smart_camera.get_sign_hight(sign, index = 1)`
Obtains the height of the specified color block in the captured image.<br>Parameters:
- ***sign***: numeric value ranging from 1 to 7, indicating the serial number of a color block.

Returns a numeric value ranging from 0 to 239. If the specified color block is not detected, the value 0 is returned.

### `smart_camera.open_light(index = 1)`
Turns on the two LED fill lights on the smart camera.

### `smart_camera.close_light(index = 1)`
Turns off the two LED fill lights on the smart camera.

### `smart_camera.reset(index = 1)`
Resets the white balance for the smart camera. This API function blocks the process being executed for five seconds.

### `smart_camera.detect_label(label, index = 1)`
Determines whether the specified barcode is detected.<br>Parameters:
- ***label***: numeric value ranging from 1 to 15, indicating the serial number of a barcode.

Returns a Boolean value:<br>**True**: detected<br>**False**: not detected

### `smart_camera.get_label_x(label, index = 1)`
Obtains the x-coordinate of the specified barcode in the captured image.<br>Parameters:
- ***label***: numeric value ranging from 1 to 15, indicating the serial number of a barcode.

Returns a numeric value ranging from 0 to 319. If the specified barcode is not detected, the value 0 is returned.

### `smart_camera.get_label_y(sign, index = 1)`
Obtains the y-coordinate of the specified barcode in the captured image.<br>Parameters:
- ***label***: numeric value ranging from 1 to 15, indicating the serial number of a barcode.

Returns a numeric value ranging from 0 to 239. If the specified barcode is not detected, the value 0 is returned.

### `smart_camera.detect_cross(index = 1)`
Determines whether an intersection is detected in the captured image.<br>

### `smart_camera.get_cross_x(index = 1)`
Obtains the x-coordinate of the detected intersection.<br>Returns a numeric value ranging from 0 to 319. If no intersection is detected, the value 0 is returned.

### `smart_camera.get_cross_y(index = 1)`
Obtains the y-coordinate of the detected intersection.<br>Returns a numeric value ranging from 0 to 239. If no intersection is detected, the value 0 is returned.

### `smart_camera.get_cross_road(index = 1)`
Obtains the number of detected crossroads.<br>Returns a numeric value.

### `smart_camera.get_cross_angle(sn = 1, index = 1)`
Obtains the angle between the specified crossroad and vector, which is the line where the smart camera is located.<br>Parameters:
- ***sn***: numeric value, indicating the space of a crossroad among the ones detected in the captured image. The number 1 indicates the first crossroad, 2 indicates the second one, and so on.

### `smart_camera.set_line(mode = "black, index = 1)`
Sets the color preference for the line tracking mode.<br>Parameters:
- ***mode***: character string. The values are described as follows:<br>**black**: detecting dark lines in light backgrounds<br>**white**: detecting light lines in dark backgrounds

### `smart_camera.get_vector_start_x(index = 1)`
Obtains the start x-coordinate of the vector where the smart camera is located.<br>Returns a numeric value ranging from 0 to 319.

### `smart_camera.get_vector_start_y(index = 1)`
Obtains the start y-coordinate of the vector where the smart camera is located.<br>Returns a numeric value ranging from 0 to 239.

### `smart_camera.get_vector_end_x(index = 1)`
Obtains the end x-coordinate of the vector where the smart camera is located.<br>Returns a numeric value ranging from 0 to 319.

### `smart_camera.get_vector_end_y(index = 1)`
Obtains the end y-coordinate of the vector where the smart camera is located.<br>Returns a numeric value ranging from 0 to 239.

### `smart_camera.set_vector_angle(angle, index = 1)`
Sets the angle preference for the vector.<br>Parameters:
- ***angle***: numeric value, indicating the angle preference for the vector. After the setting, the smart camera takes the crossroad whose angle is closest to the set value as the next vector.

### `smart_camera.get_vector_angle(index = 1)`
Obtains the set angle preference.<br>Returns a numeric value ranging from –180 to +180, in degrees (`°`).

### `smart_camera.set_kp(kp, index = 1)`
Sets the coefficient Kp for the smart camera to calculate the motor differential speed.<br>Parameters:
- ***kp***: numeric value.

### `smart_camera.get_sign_diff_speed(sign, axis, axis_val, index = 1)`
Calculates the motor differential speed required for keeping the specified color block in the specified x- or y-coordinate of the captured image. The motor differential speed is positively related to Kp. A greater Kp value indicates a greater motor differential speed.<br>Parameters:
- ***sign***: numeric value ranging from 1 to 7, indicating the serial number of a color block.<br>
- ***axis***: character string. The values are described as follows:<br>**x**: specifies the x-axis<br>**y**: specifies the y-axis
- ***axis_val***: numeric value. If **axis** is set to **x**, the setting range is 0 to 319; and if **axis** is set to **y**, the setting range is 0 to 239.

Returns a numeric value ranging from 0 to 100.

### `smart_camera.get_label_diff_speed(label, axis, axis_val, index = 1)`
Calculates the motor differential speed required for keeping the specified barcode in the specified x- or y-coordinate of the captured image. The motor differential speed is positively related to Kp. A greater Kp value indicates a greater motor differential speed.<br>Parameters:
- ***label***: numeric value ranging from 1 to 15, indicating the serial number of a barcode.<br>
- ***axis***: character string. The values are described as follows:<br>**x**: specifies the x-axis<br>**y**: specifies the y-axis
- ***axis_val***: numeric value. If **axis** is set to **x**, the setting range is 0 to 319; and if **axis** is set to **y**, the setting range is 0 to 239.

Returns a numeric value ranging from 0 to 100.

### `smart_camera.get_follow_vector_diff_speed(index = 1)`
Calculates the motor differential speed required for keeping the vector in the center of the captured image. The motor differential speed is positively related to Kp. A greater Kp value indicates a greater motor differential speed.<br>Returns a numeric value ranging from 0 to 100.

### `smart_camera.is_lock_sign(sign, axis, axis_val, index = 1)`
Determines whether the specified color block is near the specified x- or y-coordinate of the captured image.<br>Parameters:
- ***sign***: numeric value ranging from 1 to 7, indicating the serial number of a color block.<br>
- ***axis***: character string. The values are described as follows:<br>**x**: specifies the x-axis<br>**y**: specifies the y-axis
- ***axis_val***: numeric value. If **axis** is set to **x**, the setting range is 0 to 319; and if **axis** is set to **y**, the setting range is 0 to 239.

Returns a Boolean value:<br>**True**: near the specified point<br>**False**: not near the specified point

### `smart_camera.is_lock_label(sign, axis, axis_val index = 1)`
Determines whether the specified barcode is near the specified x- or y-coordinate of the captured image.<br>Parameters:
- ***label***: numeric value ranging from 1 to 15, indicating the serial number of a barcode.<br>
- ***axis***: character string. The values are described as follows:<br>**x**: specifies the x-axis<br>**y**: specifies the y-axis
- ***axis_val***: numeric value. If **axis** is set to **x**, the setting range is 0 to 319; and if **axis** is set to **y**, the setting range is 0 to 239.

Returns a Boolean value:<br>**True**: near the specified point<br>**False**: not near the specified point