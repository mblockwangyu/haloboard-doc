# Python API

## `button` &mdash; Onboard Button

**Function**

`button.is_pressed()`<br>
Get the current state of Halocode's button. The returned result is `True`: the button is pressed; `False`: the button is not pressed.

**Sample Code**

```py
import halocode

def loop():
    while True:
        if halocode.button.is_pressed():
            print("button is pressed")
loop()
```

## `touchpad0` &mdash; Touchpad 0

**Function**

`touchpad0.is_touched()`<br>
Get the current state of touchpad0. The returned result is `True`: touchpad0 is touched; `False`: touchpad0 is not touched.

`touchpad0.get_value()`<br>
If touchpad0 is touched, this function can obtain the strength of the touching. The value range is `0-10000`.

`touchpad0.set_touch_threshold(val)`<br>
Set the threshold of touchpad0, parameter:
- *val* Percentage change of the touching; value greater than the threshold means being touched; the value range is `0-1`.

**Sample Code**

```py
import halocode
import event

@event.start
def on_start():
    global results
    if halocode.touchpad0.is_touched():
        halocode.led.show_all(126, 211, 33)
```

## `touchpad1` &mdash; Touchpad 1

**Function**

`touchpad1.is_touched()`<br>
Get the current state of touchpad1. The returned result is `True`: touchpad1 is touched; `False`: touchpad1 is not touched.

`touchpad1.get_value()`<br>
If touchpad1 is touched, this function can obtain the strength of the touching. The value range is `0-10000`.

`touchpad1.set_touch_threshold(val)`<br>
Set the threshold of touchpad1, parameter:
- *val* Percentage change of the touching; value greater than the threshold means being touched; the value range is `0-1`.

**Sample Code**

```py
import halocode
import event

@event.start
def on_start():
    global results
    if halocode.touchpad1.is_touched():
        halocode.led.show_all(126, 211, 33)
```

## `touchpad2` &mdash; Touchpad 2

**Function**

`touchpad2.is_touched()`<br>
Get the current state of touchpad2. The returned result is `True`: touchpad2 is touched; `False`: touchpad2 is not touched.

`touchpad2.get_value()`<br>
If touchpad2 is touched, this function can obtain the strength of the touching. The value range is `0-10000`.

`touchpad2.set_touch_threshold(val)`<br>
Set the threshold of touchpad2, parameter:
- *val* Percentage change of the touching; value greater than the threshold means being touched; the value range is `0-1`.

**Sample Code**

```py
import halocode
import event

@event.start
def on_start():
    global results
    if halocode.touchpad2.is_touched():
        halocode.led.show_all(126, 211, 33)
```

## `touchpad3` &mdash; Touchpad 3

**Function**

`touchpad3.is_touched()`<br>
Get the current state of touchpad3. The returned result is `True`: touchpad3 is touched; `False`: touchpad3 is not touched.

`touchpad3.get_value()`<br>
If touchpad3 is touched, this function can obtain the strength of the touching. The value range is `0-10000`.

`touchpad3.set_touch_threshold(val)`<br>
Set the threshold of touchpad3, parameter:
- *val* Percentage change of the touching; value greater than the threshold means being touched; the value range is `0-1`.

**Sample Code**

```py
import halocode
import event

@event.start
def on_start():
    global results
    if halocode.touchpad3.is_touched():
        halocode.led.show_all(126, 211, 33)
```

## `pin0` &mdash; Pin0

**Function**

`pin0.is_touched()`<br>
Get the current state of the pin. The returned result is `True`: the pin is touched; `False`: the pin is not touched.

`pin0.get_touchpad_value()`<br>
Obtain the strength of the touching. The value range is `0-10000`.

`pin0.set_touchpad_threshold(val)`<br>
Set the threshold of the pin, parameter:
- *val* Percentage change of the touching; value greater than the threshold means being touched; the value range is `0.0-1`.

`pin0.read_digital()`<br>
Read digital input of the pin. The value is `0` or `1`.

`pin0.write_digital(val)`<br>
Set digital output of the pin, parameter:
- *val* The value is `0` or `1`.

`pin0.write_analog(val)`<br>
Set PWM (Pulse-Width Modulation) output, parameter:
- *val* The value range is `0-1023`.

`pin0.read_analog()`<br>
Read PWM input. The value range is `0-3300`, measured in `mv`.

`pin0.servo_write(val)`<br>
Set the angle by which the servo motor turns, or the length of the pulse, parameter:
- *val* The angle of turning, the value range is `0-19999`.
    * When the value is less than 544, the result is to set the angle: if the input value is less than 0, the returned result is 0; if the the value is greater than 180, the returned result is 180.
    * When the value is greater than or equals 544, the result is to set the length of the pulse, measured in `us`; the maximum value is 19999 us, nearly 20ms.

**Sample Code**

```py
import halocode
import event

@event.start
def on_start():
    global results
    if halocode.pin0.is_touched():
        halocode.led.show_all(126, 211, 33)
```

## `pin1` &mdash; Pin1

**Function**

`pin1.is_touched()`<br>
Get the current state of the pin. The returned result is `True`: the pin is touched; `False`: the pin is not touched.

`pin1.get_touchpad_value()`<br>
Obtain the strength of the touching. The value range is `0-10000`.

`pin1.set_touchpad_threshold(val)`<br>
Set the threshold of the pin, parameter:
- *val* Percentage change of the touching; value greater than the threshold means being touched; the value range is `0.0-1`.

`pin1.read_digital()`<br>
Read digital input of the pin. The value is `0` or `1`.

`pin1.write_digital(val)`<br>
Set digital output of the pin, parameter:
- *val* The value is `0` or `1`.

`pin1.write_analog(val)`<br>
Set PWM (Pulse-Width Modulation) output, parameter:
- *val* The value range is `0-1023`.

`pin1.read_analog()`<br>
Read PWM input. The value range is `0-3300`, measured in `mv`.

`pin1.servo_write(val)`<br>
Set the angle by which the servo motor turns, or the length of the pulse, parameter:
- *val* The angle of turning, the value range is `0-19999`.
    * When the value is less than 544, the result is to set the angle: if the input value is less than 0, the returned result is 0; if the the value is greater than 180, the returned result is 180.
    * When the value is greater than or equals 544, the result is to set the length of the pulse, measured in `us`; the maximum value is 19999 us, nearly 20ms.

**Sample Code**

```py
import halocode
import event

@event.start
def on_start():
    global results
    if halocode.pin1.is_touched():
        halocode.led.show_all(126, 211, 33)
```

## `pin2` &mdash; Pin2

**Function**

`pin2.is_touched()`<br>
Get the current state of the pin. The returned result is `True`: the pin is touched; `False`: the pin is not touched.

`pin2.get_touchpad_value()`<br>
Obtain the strength of the touching. The value range is `0-10000`.

`pin2.set_touchpad_threshold(val)`<br>
Set the threshold of the pin, parameter:
- *val* Percentage change of the touching; value greater than the threshold means being touched; the value range is `0.0-1`.

`pin2.read_digital()`<br>
Read digital input of the pin. The value is `0` or `1`.

`pin2.write_digital(val)`<br>
Set digital output of the pin, parameter:
- *val* The value is `0` or `1`.

`pin2.write_analog(val)`<br>
Set PWM (Pulse-Width Modulation) output, parameter:
- *val* The value range is `0-1023`.

`pin2.read_analog()`<br>
Read PWM input. The value range is `0-3300`, measured in `mv`.

`pin2.servo_write(val)`<br>
Set the angle by which the servo motor turns, or the length of the pulse, parameter:
- *val* The angle of turning, the value range is `0-19999`.
    * When the value is less than 544, the result is to set the angle: if the input value is less than 0, the returned result is 0; if the the value is greater than 180, the returned result is 180.
    * When the value is greater than or equals 544, the result is to set the length of the pulse, measured in `us`; the maximum value is 19999 us, nearly 20ms.

**Sample Code**

```py
import halocode
import event

@event.start
def on_start():
    global results
    if halocode.pin2.is_touched():
        halocode.led.show_all(126, 211, 33)
```

## `pin3` &mdash; Pin3

**Function**

`pin3.is_touched()`<br>
Get the current state of the pin. The returned result is `True`: the pin is touched; `False`: the pin is not touched.

`pin3.get_touchpad_value()`<br>
Obtain the strength of the touching. The value range is `0-10000`.

`pin3.set_touchpad_threshold(val)`<br>
Set the threshold of the pin, parameter:
- *val* Percentage change of the touching; value greater than the threshold means being touched; the value range is `0-1`.

`pin3.read_digital()`<br>
Read digital input of the pin. The value is `0` or `1`.

`pin3.write_digital(val)`<br>
Set digital output of the pin, parameter:
- *val* The value is `0.0` or `1`.

`pin3.write_analog(val)`<br>
Set PWM (Pulse-Width Modulation) output, parameter:
- *val* The value range is `0-1023`.

`pin3.read_analog()`<br>
Read PWM input. The value range is `0-3300`, measured in `mv`.

`pin3.servo_write(val)`<br>
Set the angle by which the servo motor turns, or the length of the pulse, parameter:
- *val* The angle of turning, the value range is `0-19999`.
    * When the value is less than 544, the result is to set the angle: if the input value is less than 0, the returned result is 0; if the the value is greater than 180, the returned result is 180.
    * When the value is greater than or equals 544, the result is to set the length of the pulse, measured in `us`; the maximum value is 19999 us, nearly 20ms.

**Sample Code**

```py
import halocode
import event

@event.start
def on_start():
    global results
    if halocode.pin3.is_touched():
        halocode.led.show_all(126, 211, 33)
```

## `motion_sensor` &mdash; Onboard Motion Sensor

**Motion Sensor Introduction**

<img src="motion-sensor.png" style="width:600px;">

As shown in the picture above, the direction of the roll and pitch are based on the right-handed screw rule.

Both roll and pitch are 0° when Halocode is horizontally placed.
- Roll range: -90° ~ 90°
- Pitch range: -180° ~ 180°

**Function**

`motion_sensor.get_roll()`<br>
Get the roll of the Euler angle, the returned data range is `-90 ~ 90`.

`motion_sensor.get_pitch()`<br>
Get the pitch of the Euler angle, the returned data range is `-180 ~ 180`.

`motion_sensor.get_yaw()`<br>
Get the yaw of the Euler angle, The returned data range is `0 ~ 360`. Since Halocode's onboard sensor does not have electronic compass. So in fact the yaw angle is just the integral of the Z-axis angular velocity. It has accumulated errors. If you want to get a true yaw angle, this API is not suitable for use.

`motion_sensor.get_acceleration(axis)`<br>
Get the acceleration values of the three axes in `m/s^2`, Parameters：
- *axis* String type, with `x`, `y`, `z` representing the axis defined by Halocode.

`motion_sensor.get_gyroscope(axis)`<br>
Get the angular velocity values of the three axes in `°/sec`, Parameters：
- *axis* String type, with `x`, `y`, `z` representing the axis defined by Halocode.

`motion_sensor.get_rotation(axis)`<br>
Get the angle at which Halocode rotates on the three axes, and the counterclockwise direction is the positive direction, parameter：
- *axis* String type, with `x`, `y`, `z` representing the axis defined by Halocode.

`motion_sensor.reset_rotation(axis = "all")`<br>
The current angle of initial rotation around the three axes is 0, and the `get_rotation()` will start at 0, parameter:
- *axis* string type, with `x`, `y`, `z` representing the axis defined by Halocode, and `all` representing all three axes. This is also the default value for this function.

`motion_sensor.is_tilted_left()`<br>
Check if Halocode is tilted to the left (threshold value is 15°), and the return value is a Boolean value, where `True` means that Halocode is tilted to the left, and `False` means that Halocode is not tilted to the left.

`motion_sensor.is_tilted_right()`<br>
Check if Halocode is tilted to the right (threshold value is 15°), and the return value is a Boolean value, where `True` means that Halocode is tilted to the right, and `False` means that Halocode is not tilted to the right.

`motion_sensor.is_arrow_up()`<br>
Check if Halocode is positioned arrow-up, and the return value is a Boolean value, where `True` means that Halocode is positioned arrow-up, and `False` means that the Halocode is not positioned arrow-up.

`motion_sensor.is_arrow_down()`<br>
Check if Halocode is positioned arrow-down, and the return value is a Boolean value, where `True` means that Halocode is positioned arrow-down, and `False` means that the Halocode is not positioned arrow-down.

`motion_sensor.is_shaked()`<br>
Check if Halocode is shaken, and the return value is a Boolean value, where `True` means that Halocode is shaken, and `False` means that Halocode is not shaken.

`motion_sensor.get_shake_strength()`<br>
If Halocode is shaken, this function can obtain the intensity of the shaking. The value of the return value range is `0 ~ 100`. The larger the value, the greater the intensity of the shaking.

**Sample Code 1:**

```py
import halocode
import time

while True:
    roll = halocode.motion_sensor.get_roll()
    pitch = halocode.motion_sensor.get_pitch()
    yaw = halocode.motion_sensor.get_yaw()
    print("roll:", end = "")
    print(roll, end = "")
    print("   ,pitch:", end = "")
    print(pitch, end = "")
    print("   ,yaw:", end = "")
    print(yaw)
    time.sleep(0.05)
```

**Sample Code 2:**

```py
import halocode

while True:
    if halocode.motion_sensor.is_shaked():
        print("shake_strength:", end = "")
        print(halocode.motion_sensor.get_shake_strength())
```

**Sample Code 3:**

```py
import halocode

while True:
    if halocode.motion_sensor.is_tilted_left():
        print("tilted_left")
    if halocode.motion_sensor.is_tilted_right():
        print("tilted_right")
    if halocode.motion_sensor.is_arrow_up():
        print("arrow_up")
    if halocode.motion_sensor.is_arrow_down():
        print("arrow_down")
```

## `led` &mdash; Onboard LED

**Function**

`led.show_single(led_id, r, g, b)`<br>
Set the displayed color one single LED, parameter:
- *led_id* The id of one single LED, the value range is `1-12`, and the position of each LED is shown in the following image:<br>
<img style="left;" src="led-id.png">
- *r* refers to the value of red component, parameter range is `0 ~ 255`，0 with no red component and 255 the highest red component.
- *g* refers to the value of green component, parameter range is `0 ~ 255`，0 with no green component and 255 the highest green component.
- *b* refers to the value of blue component, parameter range is `0 ~ 255`，0 with no blue component and 255 the highest blue component.<br>
    Color &ndash; RGB Value Table：
    ```
    red(255 , 0 , 0)

    green(0 , 255 , 0)

    blue(0 , 0 , 255)

    yellow(255 , 255 , 0)

    cyan(0 , 255 , 255)

    purple(255 , 0 , 255)

    white(150 , 150 , 150)

    orange(255 , 50 , 0)

    black(0 , 0 , 0)

    gray(0 , 0 , 0)
    ```

`led.show_all(r, g, b)`<br>
Set the color of all the LEDs, parameter:
- *r* refers to the value of red component, parameter range is `0 ~ 255`，0 with no red component and 255 the highest red component.
- *g* refers to the value of green component, parameter range is `0 ~ 255`，0 with no green component and 255 the highest green component.
- *b* refers to the value of blue component, parameter range is `0 ~ 255`，0 with no blue component and 255 the highest blue component.

`led.off_all()`<br>
Turn off all the LEDs.

`led.off_single(led_id)`<br>
Turn off one single LED, parameter:
- *led_id* The id of one single LEd, the value range is `1-12`.

`led.show.ring(color_str, offset=0)`<br>
Set the color of each LED at the same time, parameter:
- *color_str* String type, the format is "color1 color2 color3 color4"; "colorx" means "red"/"green"/"blue"/"yellow"/"cyan"/"purple"/"white"/"orange"/"black"/"gray"; over 12 colors will automatically be cut to 12.
- *offset* The value range is `0-12`.

`led.ring_graph(percentage)`<br>
Use the status of the LED ring to display percentage, parameter:
- *percentage* The value range is `0-100`.

`led.show.animation(name)`<br>
Show the default LED animation, parameter:
- *name* The four default LED animations: `spoondrift`, `meteor`, `rainbow`, and `firefly`.

`led.is_led_ring_up()`<br>
Check if the LED ring is positioned face-up. The return value is `True`: the LED ring is positioned face-up; `False`: the LED ring is not positioned face-up.

`led.is_led_ring_down()`<br>
Check if the LED ring is positioned face-down. The return value is `True`: the LED ring is positioned face-down; `False`: the LED ring is not positioned face-down.

**Sample Code 1:**

```py
import halocode
import time

halocode.led.show_all(255,0,0)
time.sleep(2)
halocode.led.off_all()
time.sleep(2)
halocode.led.show_all(0,0,255)
time.sleep(2)
halocode.led.off_all()
time.sleep(2)
halocode.led.show_all(0,255,0)
time.sleep(2)
halocode.led.off_all()
time.sleep(2)
```

**Sample Code 2:**

```py
import halocode 
import event

@event.button_pressed
def on_button_pressed():
    while True:
        halocode.led.show_ring('red orange yellow green cyan blue purple white white white white white')
        time.sleep(1)
        halocode.led.off_all()
        halocode.led.show_ring('red orange yellow green cyan blue purple white white white white white', 1)
        time.sleep(1)
        halocode.led.off_all()
        halocode.led.show_ring('red orange yellow green cyan blue purple white white white white white', 2)
        time.sleep(1)
        halocode.led.off_all()
        halocode.led.show_ring('red orange yellow green cyan blue purple white white white white white', 3)
        time.sleep(1)
        halocode.led.off_all()
        halocode.led.show_ring('red orange yellow green cyan blue purple white white white white white', 4)
        time.sleep(1)
        halocode.led.off_all()
        halocode.led.show_ring('red orange yellow green cyan blue purple white white white white white', 5)
        time.sleep(1)
        halocode.led.off_all()
        halocode.led.show_ring('red orange yellow green cyan blue purple white white white white white', 6)
        time.sleep(1)
        halocode.led.off_all()
        halocode.led.show_ring('red orange yellow green cyan blue purple white white white white white', 7)
        time.sleep(1)
        halocode.led.off_all()
        halocode.led.show_ring('red orange yellow green cyan blue purple white white white white white', 8)
        time.sleep(1)
        halocode.led.off_all()
        halocode.led.show_ring('red orange yellow green cyan blue purple white white white white white', 9)
        time.sleep(1)
        halocode.led.off_all()
        halocode.led.show_ring('red orange yellow green cyan blue purple white white white white white', 10)
        time.sleep(1)
        halocode.led.off_all()
        halocode.led.show_ring('red orange yellow green cyan blue purple white white white white white', 11)
        time.sleep(1)
        halocode.led.off_all()
        halocode.led.show_ring('red orange yellow green cyan blue purple white white white white white', 12)
        time.sleep(1)
        halocode.led.off_all()
```


## `microphone` &mdash; Onboard Microphone

**Function**

`microphone.get_loudness(type)`<br>
Get the loudness, parameter:
- *type* String type: `average`, to get the average loudness over a certain period of time; `maximum`, to get the maximum loudness over a certain period of time; the value rang is `0-100`.

**Sample Code:**

```py
import halocode

halocode.microphone.get_loudness("average")
```

## `speech_recognition` &mdash; Speech Recognition

**Function**

`speech_recognition.start(server, language, time)`<br>
Start speech recognition, parameter:
- *server* Speech recognition server, the default server SERVER_MICROSOFT, powered by Microsoft.
- *language* The language of recognition: `LAN_CHINESE`, Chinese; `LAN_ENGLISH`, English.
- *time* The duration of speech recognition, the default value is 3 seconds.

`speech_recognition.get_error_code()`<br>
Get the error code of speech recognition results, as follows:<br>
```
0: correct

3300: wrong parameter

3301: ambiguous speech

3302: authentication failure

3303: original audio file or server problem

3304: user request override (QPS)

3305: user request override (PV)

3307: server problem

3308：audio content too long

3309: abnormal audio

3310：audio file too large

3311：wrong sample rate

3312: wrong audio file format

3333：unknown error

3334: response timeout
```

`speech_recognition.get_error_message()`<br>
Report specific error message, string type.

`speech_recognition.get_result_code()`<br>
Report recognition results, string type; return empty string when there is an error or a timeout condition.

`speech_recognition.get_sn_code()`<br>
Report the single identification of voice data, generated by the server.

`speech_recognition.get_all_respond()`<br>
Report the results of speech recognition, including all returned messages, like error message.

**Sample Code:**

```py
import halocode, event
halocode.speech_recognition.set_recognition_url(halocode.speech_recognition.SERVER_MICROSOFT, "http://msapi.passport3.makeblock.com/ms/bing_speech/interactive")
halocode.speech_recognition.set_token(halocode.speech_recognition.SERVER_MICROSOFT, "ed8xubrmidv")

@event.start
def on_start():
    halocode.wifi.start(ssid = 'Maker-guest', password = 'makeblock', mode = halocode.wifi.WLAN_MODE_STA)
    while not halocode.wifi.is_connected():
        pass

    halocode.speech_recognition.start(halocode.speech_recognition.SERVER_MICROSOFT, halocode.speech_recognition.LAN_CHINESE, 3)
```

## `wifi` &mdash; Onboard Wi-Fi

**Function**

`wifi.start(ssid = "wifi_ssid", password = "password", mode = halocode.wifi.STA)`<br>
Start wifi connection, the API will not block process, API exit does not mean that wifi is connected, you need to call `wifi.is_connected()` to judge, Parameter：
- *ssid* string type, Wi-Fi account.
- *password* string type, Wi-Fi password.
- *mode* starts the Wi-Fi mode (currently only `WLAN_MODE_STA`)

`wifi.is_connected()`<br>
Check if Wi-Fi is connected, the return value is Boolean, where `True` means that Wi-Fi has established a connection, `False` means that Wi-Fi has not yet established a connection.

**Sample Code:**

```py
import halocode
import event

@event.start
def on_start():
    halocode.wifi.start(ssid = 'Maker-guest', password = 'makeblock', mode = halocode.wifi.WLAN_MODE_STA)
    while not halocode.wifi.is_connected():
        pass

    halocode.led.show_all(126, 211, 33)
    time.sleep(2)
    halocode.led.off_all()
```

## `cloud_message` &mdash; Cloud Message

**Function**

`cloud_message.start(topic_head, client_id=None, server="mq.makeblock.com", port=1883, user=None, password=None, keepalive=60, ssl=False)`<br>
Start sending cloud message, effective when Wi-Fi is connected, parameter:
- *topic_head* Prefix directory of cloud message topic.
- *client_id* The single client ID when connecting to proxy server; a random id will be generated when the string is empty or left unset, where the parameter `clean_session` of `connect` should be `True`.
- *server* Server name or IP address of remote server.
- _\*port\*_ (optional) The port of the server to connect to; the default port is `1883`.
- \*user\*_ (optional) The registered user name on the server.
- _\*password\*_ (optional) The registered password on the server.
- _\*keepalive\*_ (optional) Client's keep-alive value, default 60 seconds.
_\*ssl\*_ (optional) Choose to support SSL/TLS.

`cloud_message.get_info(message)`<br>
Report the additional parameters of cloud message, string or numerical type, parameter:
- *message* string type, the name of cloud message

**Sample Code:**

```py
import halocode
import event
halocode.cloud_message.start('/USER/1014148/MESSAGE')

@event.start
def on_start():
    halocode.wifi.start(ssid = 'Maker-guest', password = 'makeblock', mode = halocode.wifi.WLAN_MODE_STA)
    while not halocode.wifi.is_connected():
        pass

    halocode.led.show_all(126, 211, 33)
    time.sleep(2)
    halocode.led.off_all()
    halocode.cloud_message.broadcast('hello', '')
```

## `event` &mdash; Events Function

**Two Forms of Writing**

One: registration form, as follows:

`event.start(test_callback)`<br>
`event.received(callback, 'hello')`

Two: decorator form, as follows:

```py
@event.start

def start_callback():

    print(123)

@event.received('hello')

def received_callback():

    print(123)
```

**Note: parameters other than `callback` need to be included.**

**Function**

`event.start(callback)`<br>
Startup event.

`event.shaked(callback)`<br>
Halocode was shaken event.

`event.button_pressed(callback)`<br>
Button pressed event.

`event.tilted_left(callback)`<br>
Halocode left-tilted event.

`event.tilted_right(callback)`<br>
Halocode right-tilted event.

`event.arrow_up(callback)`<br>
Halocode arrow-up event.

`event.arrow_down(callback)`<br>
Halocode arrow-down event.

`event.received(callback, message_str)`<br>
Broadcast reception detection event. In addition to the callback parameter, the parameter：
- *msgstr* string type, the string to be matched. The event will be triggered when the received string matches the matching string.

`event.cloud_message(message)`<br>
Cloud message event, parameter:
- *message* String type, name of the cloud message.

`event.mesh_message(message)`<br>
Mesh message event, parameter:
- *message* String type, name of the mesh message.

`event.greater_than(callback, threshold, type_str)`<br>
The threshold comparison event, which will be triggered when the threshold is exceeded. In addition to the callback parameter, the parameter：
- *threshold* value data, set the threshold for triggering.
- *type_str* string data, currently only supports `sound_sensor`: volume sensor, `timer`: timer.

`event.less_than(callback, threshold, type_str)`<br>
Threshold comparison event, triggered below the threshold, in addition to the callback parameter, the parameter：
- *threshold* value data, set the threshold for triggering
- *type_str* string data, currently only supports `light_sensor`: light sensor.

`event.touchpad0_active(callback)`<br>
Touchpad0 event.

`event.touchpad1_active(callback)`<br>
Touchpad1 event.

`event.touchpad2_active(callback)`<br>
Touchpad2 event.

`event.touchpad3_active(callback)`<br>
Touchpad3 event.

## `timer` &mdash; Timer

**Function**

`get_timer()`<br>
Get current value of the timer, measured in second.

`reset_timer()`<br>
Reset timer.

## `variable` &mdash; Variables

**Function**

`set_variable(name, value)`<br>
Set variable, parameter:
- *name* The name of the variable.
- *value* The value of the variable.

`get_variable(name)`<br>
Get information of the variable, parameter:
- *name* The name of the variable.

## `broadcast` &mdash; Broadcast

**Function**

`broadcast(message_str)`<br>
Broadcast a certain piece of message, parameter:
- *message_str* String type, the message to broadcast.




