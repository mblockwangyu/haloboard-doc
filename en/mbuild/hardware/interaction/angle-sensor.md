# Angle Sensor

The Angle Sensor Block is made of a magnetic encoder, which detects the position of rotation with accuracy. Unlike the knob, the angle sensor rotates continuously, detecting the degree of rotation, and the angular velocity in real time. What's more, the Angle Sensor comes with a variety of plates to connect to various structural components, so as to be applied in different scenarios.

<img src="../../../../zh/mbuild/hardware/interaction/images/angle-sensor.png" style="padding:3px 0px 12px 3px;width:200px;">

- Connect to Makeblock mechanical parts
- Connect to LEGO cross stitches

### Principle

In Electromagnetism, if we apply a voltage to rectangular conductor, current will be generated in one direction. At this point, if a magnetic field perpendicular to the conductor plane is applied to the energized conductor as shown in the following figure:

<img src="../../../../zh/mbuild/hardware/interaction/images/angle-3.jpg" style="padding:3px 0px 12px 3px;width:600px;">

The magnetic field will induce a Lorentz force, and the charge flowing on the conductor will have a path offset.

According to Fleming's left-hand rule, the direction of the offset can be obtained. And the direction of the offset is opposite for the positive and negative charges. Then, as shown in the above figure, the positive and negative charges pass through the middle conductor along two different paths on the left and right, respectively.


At this time, a potential difference is generated between the sides of the conductor, that is, in a direction perpendicular to the current flowing therethrough. This is the Hall Effect.

From the Hall effect, we can know that the change of the magnetic field will affect the magnitude of the current. Therefore, we can obtain the change of the magnetic field , and even the change of the position of the magnet from the change of the current. Such a feature can be used in detecting the rotating shaft speed of magnet.


### Real-Life Examples

- The magnetic encoder used in some servo to detect angles<br>
<img src="../../../../zh/mbuild/hardware/interaction/images/angle-1.jpg" style="padding:10px 0px 12px 3px;width:300px;">
- The Digital Crown of Apple Watch can be used to control sound volume. The intricate system shows itself in miniature size.<br>
<img src="../../../../zh/mbuild/hardware/interaction/images/angle-2.jpg" style="padding:10px 0px 12px 3px;width:300px;">

### Parameters

- Size: 24×36mm
- Precision: ±1°
- Operating current: 22mA