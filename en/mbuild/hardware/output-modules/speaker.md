# Speaker

The speaker module can play various preset sounds. You can easily store your audio files in the module and play them by invoking corresponding blocks.

<img src="images/speaker.png" style="padding:3px 0px 12px 3px;width:200px;">

### Store user-define audio files

Store your audio files in the speaker module as follows:

**1\. Connect the module to a PC.**

Connect the speaker module to a PC by using a Micro USB cable. Use the Micro USB port of the module and a USB port of the PC.<br>After the successful connection, the PC displays the disk of the module, and you can open the disk to view the files stored in it.

**2\. Store an audio file.**

Drag the audio file that you want to play by the speaker to the disk.

<small>Note: The disk space of the module is limited. It may fail to store a large-size file. It is recommended that you compass a large-size file before storing it in the disk.</small>

**3\. Change the file name (important step).**

Due to hardware design, the speaker module can identify an audio file only by the first four characters (English characters, numbers, or punctuations) of its file name. To play an audio file, you need to change the file name to the special format, that is, a 4-digit combination of English characters, numbers, or punctuations.

The following describes some examples to help you change a file name. The names on the left side are the original ones, and those on the right side are the ones after the change.
- 龙的传人.mp3  ------------------>  ldcr.mp3  
- 改革春风吹满地.mp3  --------->  ggcf.mp3
- happy birthday.mp3  ---------->  M001.mp3  

After changing the names of audio files, you can play the audio files by using the corresponding blocks on mBlock 5.

### Play user-defined audio files

mBlock 5 provides the following two blocks for playing audio files:

<img src="images/speaker-1.png" style="padding:3px 0px 12px 3px;">
<br>
<img src="images/speaker-2.png" style="padding:3px 0px 12px 3px;">

Example:

<img src="images/speaker-3.png" style="padding:3px 0px 12px 3px;">

This block is used to play the audio file named "ldcr" by the first speaker connected.

### Restore the default audio files of the speaker

Misoperations may damage the preset sound library, causing the failure to play the preset sounds. The following provides the latest compressed package of the default sound files. You can download and decompress it on the disk of the speaker to replace the damaged files.

<a href="images/Preset audio files of the speaker.rar" download>Preset audio files of the speaker</a>

### Parameters

- Dimensions: 24 mm × 36 mm
- Total memory size: 16M
- Supported audio format: mp3
- Connector type: Micro-USB
- Rated operation current: 400 mA