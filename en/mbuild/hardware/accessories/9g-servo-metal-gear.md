# 9g Micro Servo (Metal Gear)

Makeblock 9g Micro Servo (metal gear) is a high-quality servo customized by Makeblock. It may be delivered assembled or unassembled in a pack, including the following four parts: 9g micro servo (metal gear), servo horn, screw, and servo holder.


<img src="images/9g-servo-metal-gear-1.png" style="padding:3px 0px 12px 3px;width:500
px;">

## Dimensions

The unit is millimeter.

<img src="images/9g-servo-metal-gear-2.png" style="padding:3px 0px 12px 3px;width:300px;">


## Specifications

### Electrical specifications

<table  cellpadding="10px;" cellspacing="15px;" style="text-align:left;;margin-bottom:20px;">
<tr>
<th width="40%;" style="border: 1px solid black;">Item</th>
<th width="30%;" style="border: 1px solid black;">4.8 V</th>
<th width="30%;" style="border: 1px solid black;">6.0 V</th>
</tr>

</td>
<td style="border: 1px solid black;">Operating speed (without load)</td>
<td style="border: 1px solid black;">0.15±0.015 sec/60°</td>
<td style="border: 1px solid black;">0.13±0.015 sec/60°</td>
</tr>

<tr>
<td style="border: 1px solid black;">Operating current (without load)</td>
<td style="border: 1px solid black;">80±35 mA</td>
<td style="border: 1px solid black;">90±35 mA</td>
</tr>

<tr>
<td style="border: 1px solid black;">Max. load in operation</td>
<td style="border: 1px solid black;">1.0±0.05kg·cm</td>
<td style="border: 1px solid black;">1.1±0.1kg·cm</td>
</tr>

<tr>
<td style="border: 1px solid black;">Stall torque (locked)</td>
<td style="border: 1px solid black;">1.5±0.05kg·cm</td>
<td style="border: 1px solid black;">1.8±0.1kg·cm</td>
</tr>

<tr>
<td style="border: 1px solid black;">Stall current (locked)</td>
<td style="border: 1px solid black;">680±40 mA</td>
<td style="border: 1px solid black;">750±50 mA</td>
</tr>

<tr>
<td style="border: 1px solid black;">Idle current (stopped)</td>
<td style="border: 1px solid black;">6±1 mA</td>
<td style="border: 1px solid black;">6±1 mA</td>
</tr>

<tr>
<td style="border: 1px solid black;">Temperature drift (at 25°C)</td>
<td style="border: 1px solid black;">≤5°</td>
<td style="border: 1px solid black;">≤7°</td>
</tr>

</table>


### Mechanical specifications

<table  cellpadding="10px;" cellspacing="15px;" style="text-align:left;;margin-bottom:20px;">
<tr>
<th width="40%;" style="border: 1px solid black;">Item</th>
<th width="60;" style="border: 1px solid black;">Specification</th>
</tr>

</td>
<td style="border: 1px solid black;">Overall dimensions</td>
<td style="border: 1px solid black;">See "Dimensions."</td>
</tr>

<tr>
<td style="border: 1px solid black;">Limit angle</td>
<td style="border: 1px solid black;">180°±10°</td>
</tr>

<tr>
<td style="border: 1px solid black;">Weight</td>
<td style="border: 1px solid black;">13.5±0.5g</td>
</tr>

<tr>
<td style="border: 1px solid black;">Connector wire gauge</td>
<td style="border: 1px solid black;">#28 PVC, black-red-white, plug: 2510-3P, white</td>
</tr>

<tr>
<td style="border: 1px solid black;">Connector wire length</td>
<td style="border: 1px solid black;">260±5 mm</td>
</tr>

<tr>
<td style="border: 1px solid black;">Horn gear spline</td>
<td style="border: 1px solid black;">40T</td>
</tr>

<tr>
<td style="border: 1px solid black;">Horn type</td>
<td style="border: 1px solid black;">Double-arm</td>
</tr>

<tr>
<td style="border: 1px solid black;">Reducation ratio</td>
<td style="border: 1px solid black;">1/324</td>
</tr>

<tr>
<td style="border: 1px solid black;">Excessive play</td>
<td style="border: 1px solid black;">≤1°</td>
</tr>

<tr>
<td style="border: 1px solid black;">Screw</td>
<td style="border: 1px solid black;">PM2 × 5 mm</td>
</tr>

</table>

### Control specifications

<table  cellpadding="10px;" cellspacing="15px;" style="text-align:left;;margin-bottom:20px;">
<tr>
<th width="40%;" style="border: 1px solid black;">Item</th>
<th width="60;" style="border: 1px solid black;">Specification</th>
</tr>

</td>
<td style="border: 1px solid black;">Control system</td>
<td style="border: 1px solid black;">Changing the pulse width</td>
</tr>

<tr>
<td style="border: 1px solid black;">Amplifier type</td>
<td style="border: 1px solid black;">Analog controller
</td>
</tr>

<tr>
<td style="border: 1px solid black;">Operating travel</td>
<td style="border: 1px solid black;">180°±10° (600 to 2400 µ sec)</td>
</tr>

<tr>
<td style="border: 1px solid black;">Left-right travelling angle deviation</td>
<td style="border: 1px solid black;">≤7°</td>
</tr>

<tr>
<td style="border: 1px solid black;">Centering deviation</td>
<td style="border: 1px solid black;">≤ 1°</td>
</tr>

<tr>
<td style="border: 1px solid black;">Neutral position</td>
<td style="border: 1px solid black;">1500  µ sec</td>
</tr>

<tr>
<td style="border: 1px solid black;">Dead band width</td>
<td style="border: 1px solid black;">≤7  µ sec</td>
</tr>

<tr>
<td style="border: 1px solid black;">Rotating direction</td>
<td style="border: 1px solid black;">Counterclockwise (1500 to 2000 µ sec)</td>
</tr>

<tr>
<td style="border: 1px solid black;">Pulse width range</td>
<td style="border: 1px solid black;">500 to 2500 µ sec</td>
</tr>

<tr>
<td style="border: 1px solid black;">Maximum travel</td>
<td style="border: 1px solid black;">About 190° (500 to 2500 µ sec)</td>
</tr>

</table>