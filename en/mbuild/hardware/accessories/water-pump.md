# Water Pump

With the specified connection cables, the water pump can work together with Pocket Shield, mBot2, and DC motor drivers.

Driven by Pocket Shield, mBot2, or a DC motor driver, the water pump draws water or blows air to control the flow of water.

<div style="background-color:#EAF6FD;padding:10px;border-left:5px solid #BAE3F9;margin-bottom:30px;border-radius:5px;line-height:28px;font-size:14px;"><strong>Note:</strong><br> 
The water pump must be used in combination with a tube that supports it.<br>
The water pump can't be used in water.
</div>

<img src="images/water-pump-1.png" style="padding:3px 0px 12px 3px;width:500px;">

## Compatibility

<table  cellpadding="10px;" cellspacing="15px;" style="text-align:left;;margin-bottom:20px;">
<tr>
<th width="33%;" style="border: 1px solid black;">Driven by</th>
<th width="33%;" style="border: 1px solid black;">With</th>
</tr>

</td>
<td style="border: 1px solid black;">DC motor port of Pocket Shield</td>
<td style="border: 1px solid black;"><img src="images/water-pump-2.png" style="padding:3px 0px 12px 3px;width:200px;"></td>
</tr>

<tr>
<td style="border: 1px solid black;">DC motor port of mBot2 Shield</td>
<td style="border: 1px solid black;"><img src="images/water-pump-2.png" style="padding:3px 0px 12px 3px;width:200px;"></td>
</tr>

<tr>
<td style="border: 1px solid black;">mBuild DC motor driver</td>
<td style="border: 1px solid black;"><img src="images/water-pump-3.png" style="padding:3px 0px 12px 3px;width:200px;"></td>
</tr>

<tr>
<td style="border: 1px solid black;">DC motor driver port that supports the voltage output of 5 V and peak current output of higher than 1 A</td>
<td style="border: 1px solid black;">Connect them through soldering or use a connection cable that supports the driver</td>
</tr>

</table>
