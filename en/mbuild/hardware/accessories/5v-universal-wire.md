# 5V Universal Wire

The 5V Universal Wire is used to connect different mbuild modules, or connecting mbuild modules to [Halocode](http://docs.makeblock.com/halocode/en/tutorials/introduction.html).

<img src="../../../../zh/mbuild/hardware/accessories/images/wire.png" style="padding:3px 0px 12px 3px;width:300px;">

If you need other lengths of cable, you can combine the existing cable with the extension module.