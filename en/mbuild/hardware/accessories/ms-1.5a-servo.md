# MS-1.5A Servo

The MS-1.5A servo is developed independently by Makeblock. Its dimensions are similar to common 9g servos.


<img src="images/ms-1.5a_servo-1.png" style="padding:3px 0px 12px 3px;width:300px;">

## Function comparison

<table  cellpadding="10px;" cellspacing="15px;" style="text-align:left;;margin-bottom:20px;">
<tr>
<th width="33%;" style="border: 1px solid black;"> </th>
<th width="33%;" style="border: 1px solid black;">MS-1.5A servo</th>
<th width="34%;" style="border: 1px solid black;">Earlier 9g servos</th>
</tr>

</td>
<td style="border: 1px solid black;">Servo hub mounted before delivery</td>
<td style="border: 1px solid black;">Yes</td>
<td style="border: 1px solid black;">No</td>
</tr>

<tr>
<td style="border: 1px solid black;">Supporting quick fitting</td>
<td style="border: 1px solid black;">Yes</td>
<td style="border: 1px solid black;">No</td>
</tr>

<tr>
<td style="border: 1px solid black;">Compatible with Makeblock metal parts and Lego blocks</td>
<td style="border: 1px solid black;">Yes</td>
<td style="border: 1px solid black;">	
Yes, but adapter brackets required</td>
</tr>

<tr>
<td style="border: 1px solid black;">Physical limit</td>
<td style="border: 1px solid black;">No (The circuits are redesigned to better protect the servo and thus ensuring longer service life.)</td>
<td style="border: 1px solid black;">Yes</td>
</tr>

</table>

## Connection example

<img src="images/ms-1.5a_servo-2.png" style="padding:3px 0px 12px 3px;width:250px;">

## Dimensions
The unit is millimeter.

<img src="images/ms-1.5a_servo-3.png" style="padding:3px 0px 12px 3px;width:250px;">

## Specifications

With the latest technology in the servo industry, the MS-1.5 servo provides significantly improved performance. The following describes its specifications.

### Electrical specifications (performance parameters)

<table  cellpadding="10px;" cellspacing="15px;" style="text-align:left;;margin-bottom:20px;">
<tr>
<th width="40%;" style="border: 1px solid black;">Item</th>
<th width="30%;" style="border: 1px solid black;">4.6 V</th>
<th width="30%;" style="border: 1px solid black;">6.0 V</th>
</tr>

</td>
<td style="border: 1px solid black;">Operating speed (without load)</td>
<td style="border: 1px solid black;">0.11 ± 0.01 sec/60°</td>
<td style="border: 1px solid black;">0.10 ± 0.01 sec/60°</td>
</tr>

<tr>
<td style="border: 1px solid black;">Operating current (without load)</td>
<td style="border: 1px solid black;">270 mA ± 50 mA</td>
<td style="border: 1px solid black;">380 mA ± 50 mA</td>
</tr>

<tr>
<td style="border: 1px solid black;">Stall torque (when locked)</td>
<td style="border: 1px solid black;">1.5 kg ± 0.30 kg·cm</td>
<td style="border: 1px solid black;">1.8 kg ± 0.30 kg·cm</td>
</tr>

<tr>
<td style="border: 1px solid black;">Stall current (when locked)</td>
<td style="border: 1px solid black;">1200 mA ± 50 mA</td>
<td style="border: 1px solid black;">1500 mA ± 70 mA</td>
</tr>

<tr>
<td style="border: 1px solid black;">Idle current (when stopped)</td>
<td style="border: 1px solid black;">4 mA ± 1 mA</td>
<td style="border: 1px solid black;">4 mA ± 1 mA</td>
</tr>

<tr>
<td style="border: 1px solid black;">Service life (without load)</td>
<td style="border: 1px solid black;">> 180 thousand times</td>
<td style="border: 1px solid black;">> 150 thousand times</td>
</tr>

</table>

<div style="background-color:#EAF6FD;padding:10px;border-left:5px solid #BAE3F9;margin-bottom:30px;border-radius:5px;line-height:28px;font-size:14px;"><strong>Note:</strong><br> 
The servo operates without load when the average value of item 2 is defined.
</div>

### Mechanical specifications

<table  cellpadding="10px;" cellspacing="15px;" style="text-align:left;;margin-bottom:20px;">
<tr>
<th width="40%;" style="border: 1px solid black;">Item</th>
<th width="60;" style="border: 1px solid black;">Specification</th>
</tr>

</td>
<td style="border: 1px solid black;">Overall dimensions</td>
<td style="border: 1px solid black;">See "Dimensions."</td>
</tr>

<tr>
<td style="border: 1px solid black;">Limit angle</td>
<td style="border: 1px solid black;">No</td>
</tr>

<tr>
<td style="border: 1px solid black;">Weight</td>
<td style="border: 1px solid black;">Subject to the actual weight</td>
</tr>

<tr>
<td style="border: 1px solid black;">Wire specification</td>
<td style="border: 1px solid black;">JST 2.54mm #30 PVC black-white-red</td>
</tr>

<tr>
<td style="border: 1px solid black;">Wire length</td>
<td style="border: 1px solid black;">20T</td>
</tr>

<tr>
<td style="border: 1px solid black;">Horn type</td>
<td style="border: 1px solid black;">N/A</td>
</tr>

<tr>
<td style="border: 1px solid black;">Excessive play</td>
<td style="border: 1px solid black;">≤ 1°</td>
</tr>

</table>

### Control specifications

<table  cellpadding="10px;" cellspacing="15px;" style="text-align:left;;margin-bottom:20px;">
<tr>
<th width="40%;" style="border: 1px solid black;">Item</th>
<th width="60;" style="border: 1px solid black;">Specification</th>
</tr>

</td>
<td style="border: 1px solid black;">Control system</td>
<td style="border: 1px solid black;">N/A</td>
</tr>

<tr>
<td style="border: 1px solid black;">Amplifier type</td>
<td style="border: 1px solid black;">N/A</td>
</tr>

<tr>
<td style="border: 1px solid black;">Operating travel</td>
<td style="border: 1px solid black;">180°± 5° (500 to 2500 µ sec)</td>
</tr>

<tr>
<td style="border: 1px solid black;">Left-right travelling angle deviation</td>
<td style="border: 1px solid black;">≤ 5°</td>
</tr>

<tr>
<td style="border: 1px solid black;">Centering deviation</td>
<td style="border: 1px solid black;">≤ 1°</td>
</tr>

<tr>
<td style="border: 1px solid black;">Neutral position</td>
<td style="border: 1px solid black;">N/A</td>
</tr>

<tr>
<td style="border: 1px solid black;">Dead band width</td>
<td style="border: 1px solid black;">N/A</td>
</tr>

<tr>
<td style="border: 1px solid black;">Rotating direction</td>
<td style="border: 1px solid black;">Counterclockwise (500 to 2500 µ sec)</td>
</tr>

<tr>
<td style="border: 1px solid black;">Pulse width range</td>
<td style="border: 1px solid black;">500 to 2500 µ sec</td>
</tr>

<tr>
<td style="border: 1px solid black;">Maximum travel</td>
<td style="border: 1px solid black;">About 180° (500 to 2500 µ sec)</td>
</tr>

</table>