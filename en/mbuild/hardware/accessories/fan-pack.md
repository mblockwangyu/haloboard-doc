# Fan Pack

With the specified connection cables, the fan can work together with Pocket Shield, mBot2, and DC motor drivers.

Driven by Pocket Shield, mBot2, or a DC motor driver, the fan can run clockwise or counterclockwise to control the wind power and direction.


<img src="images/fan-pack-1.png" style="padding:3px 0px 12px 3px;width:300px;">

## Compatibility

<table  cellpadding="10px;" cellspacing="15px;" style="text-align:left;;margin-bottom:20px;">
<tr>
<th width="33%;" style="border: 1px solid black;">Driven by</th>
<th width="33%;" style="border: 1px solid black;">With</th>
</tr>

</td>
<td style="border: 1px solid black;">DC motor port of Pocket Shield</td>
<td style="border: 1px solid black;"><img src="images/water-pump-2.png" style="padding:3px 0px 12px 3px;width:200px;"></td>
</tr>

<tr>
<td style="border: 1px solid black;">DC motor port of mBot2 Shield</td>
<td style="border: 1px solid black;"><img src="images/water-pump-2.png" style="padding:3px 0px 12px 3px;width:200px;"></td>
</tr>

<tr>
<td style="border: 1px solid black;">mBuild DC motor driver</td>
<td style="border: 1px solid black;"><img src="images/water-pump-3.png" style="padding:3px 0px 12px 3px;width:200px;"></td>
</tr>

<tr>
<td style="border: 1px solid black;">DC motor driver port that supports the voltage output of 5 V and peak current output of higher than 1 A</td>
<td style="border: 1px solid black;">Connect them through welding or use a connection cable that supports the driver</td>
</tr>

</table>
