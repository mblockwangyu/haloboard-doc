# Power

The Power Module powers other mbuild modules.

<img src="../../../../zh/mbuild/hardware/power/images/power.png" style="padding:3px 0px 12px 3px;width:300px;">


### Light Effect and Meaning

<table  cellpadding="10px;" cellspacing="15px;" style="text-align:left;;">
<tr>
<th style="border: 1px solid black;width:33%;"></th>
<th style="border: 1px solid black;width:33%;">on</th>
<th style="border: 1px solid black;width:33%;">off</th>
</tr>

</td>
<td style="border: 1px solid black;">Green</td>
<td style="border: 1px solid black;">Power On</td>
<td style="border: 1px solid black;">Power Off</td>
</tr>

<tr>
<td style="border: 1px solid black;">Red</td>
<td style="border: 1px solid black;">Low Battery or Charging</td>
<td style="border: 1px solid black;">High Battery or Charging Finished</td>
</tr>
</table>

<br>

### Parameters

- Size：48×48mm
- Battery Capacity：950mAh
- Battery Voltage：3.7V
- Discharging Rate：3CC
- Endurance：3 hours
- Charging time：1.25 hours
- Output Voltage：DC 5V
- Discharging Current：5V 1.5A，Peak Value 5V 2A
- Protection current：5V 3A
- Input Voltage：DC 5V
- Input Current：＜2A
- Operation Temp：0℃~45℃
- Service life：Cycles≥300

### Warning

- Please read the instructions carefully before using the battery;
- During use, keep away from heat sources, high voltage places, and don't hit the battery;
- Dispose of the battery module properly and safely. Don't put it in fire or water;
- During use or storage, if the battery is found to have high temperature, liquid leakage, odor, deformation, and other abnormalities, stop using it immediately;