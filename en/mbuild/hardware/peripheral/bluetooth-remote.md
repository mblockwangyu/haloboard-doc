# Bluetooth Remote

The Bluetooth Remote works with all kinds of robots, main-control boards, or bluetooth modules of Makeblock. 

<img src="../../../../zh/mbuild/hardware/peripheral/images/bluetooth-remote.png" style="padding:6px 0px 12px 3px;width:300px;">


<font size="2">The remote controller is mainly used for robot control in manual sessions in MakeX.</font><br /> 

### Parameters

- Material: ABS
- Bluetooth version: 4.0
- Transmission distance: 20m
- Anti-interference ability: supports bluetooth remotes working at the same time
- Powering method: 2 pieces of 5AA battery