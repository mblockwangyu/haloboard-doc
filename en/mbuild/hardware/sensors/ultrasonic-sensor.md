# Ultrasonic Sensor

The ultrasonic sensor block can be used to measure the distance between the block and an obstacle. The left probe of the ultrasonic module is responsible for transmitting ultrasonic waves, while the right probe is responsible for receiving ultrasonic waves.

<img src="../../../../zh/mbuild/hardware/sensors/images/ultrasonic-01.png" style="padding:3px 0px 12px 3px;width:250px;">

<img src="../../../../zh/mbuild/hardware/sensors/images/ultrasonic.png" style="padding:3px 0px 12px 3px;width:250px;">

### Principle

The frequency of sound waves that can be heard in the human ear is 20 to 20,000 Hz. Sound waves above 20,000 Hz are called ultrasonic waves. The sound waves are bounced when they encounter obstacles and are received by the probe of the ultrasonic sensor. Using the time difference from the transmission time to the reception time, we were able to calculate the distance between the ultrasonic probe and the obstacle.


### Real-Life Examples

- Bats use ultrasonic wave to locate an object<br>
<img src="../../../../zh/mbuild/hardware/sensors/images/ultrasonic-2.png" style="padding:15px 0px 12px 3px;width:300px;">

### Parameters

- Measuring range: 5~300cm
- Precision: ±5%
- Operating current: 26mA