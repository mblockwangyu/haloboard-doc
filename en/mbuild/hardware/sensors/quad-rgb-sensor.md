# Quad RGB Sensor

<img src="images/quad-rgb-sensor-1.png" style="padding:3px 0px 12px 3px;width:300px;">

## Overview

The quad RGB sensor uses visible light as fill lights, which significantly reduce the interference of ambient light. In addition, it provides the function for recognizing colors. The new ambient light calibration function also reduces the interference of the ambient light in line following. With four light sensors, it can support more programming scenarios.

<img src="images/quad-rgb-sensor-2.png" style="padding:3px 0px 12px 3px;width:300px;">



<table  cellpadding="10px;" cellspacing="15px;" style="text-align:left;;margin-bottom:20px;">
<tr>
<th width="33%;" style="border: 1px solid black;">   </th>
<th width="33%;" style="border: 1px solid black;">Quad RGB sensor</th>
<th width="34%;" style="border: 1px solid black;">Dual RGB color sensor</th>
</tr>

</td>
<td style="border: 1px solid black;">Plastic casing to improve the durability and quality</td>
<td style="border: 1px solid black;">Yes</td>
<td style="border: 1px solid black;">No</td>
</tr>

<tr>
<td style="border: 1px solid black;">Line-following sensor</td>
<td style="border: 1px solid black;">4</td>
<td style="border: 1px solid black;">2</td>
</tr>

<tr>
<td style="border: 1px solid black;">Color sensor</td>
<td style="border: 1px solid black;">4<br>(also serve as line-following sensors)</td>
<td style="border: 1px solid black;">No</td>
</tr>

<tr>
<td style="border: 1px solid black;">Light sensor</td>
<td style="border: 1px solid black;">4<br>(also serve as line-following sensors)</td>
<td style="border: 1px solid black;">No</td>
</tr>

<tr>
<td style="border: 1px solid black;">Fill light</td>
<td style="border: 1px solid black;">Visible light</td>
<td style="border: 1px solid black;">IR light</td>
</tr>

<tr>
<td style="border: 1px solid black;">Ambient light calibration to significantly reduce the interference of ambient light</td>
<td style="border: 1px solid black;">Yes</td>
<td style="border: 1px solid black;">No</td>
</tr>

</table>


## Using the button

* Double-press: When the button is double-pressed, the quad RGB sensor starts to learn the background and line for line following.<br>
Place the light sensors on the background of the line-following track map and double-press the button. When you see the LEDs indicating the line-following state blink quickly, sway the sensors from side to side above the background and line until the LEDs stop blinking. It takes about 2.5 seconds. The parameter values obtained are automatically stored. If the learning fails, the LEDs blink slowly, and you need to start the learning again.

* Long-press: When the button is long-pressed, the quad RGB sensor switches the color of the fill lights. Generally, you don't need to change the color. The color is set automatically after the learning is complete.