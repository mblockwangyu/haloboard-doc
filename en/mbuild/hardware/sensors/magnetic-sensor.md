# Magnetic

The magnetic sensor detects if there is a magnet around the module.

<img src="../../../../zh/mbuild/hardware/sensors/images/magnetic-sensor.png" style="padding:3px 0px 12px 3px;width:200px;">

### Real-Life Examples

The virtual wall of the sweeping robot uses magnetic field detection to determine if it can go through.

<img src="../../../../zh/mbuild/hardware/sensors/images/magnetic-sensor-2.png" style="padding:3px 0px 12px 3px;width:400px;">

### Parameters

- Size：24×20mm
- Detecting Range：<1cm
- Operating current:15mA