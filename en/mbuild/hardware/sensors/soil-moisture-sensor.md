# Soil Moisture Sensor

The Soil Moisture Sensor detects the level of moisture in the soil.

<img src="../../../../zh/mbuild/hardware/sensors/images/soil-moisture.png" style="padding:3px 0px 12px 3px;width:200px;">

### Principle

There are two typical types of soil moisture sensors, the capacitive one and resistive one. The capacitive type measured the change in capacitance caused by soil moisture to estimate the humidity of the soil . The resistive type is based on measuring the magnitude of the resistance between the two contacts to estimate the soil moisture. 

The strict definition of soil moisture is the percentage of the mass of water in the soil. Therefore, neither the resistive nor the capacitive type can accurately predict the water content in the soil, but only the reading value, which is positively correlated to the water content.

The soil moisture sensor in mBuild is resistive types. When the soil moisture is increased, the water can dissolve the ions contained in part of the soil, so that the resistance of the soil becomes smaller, and therefore the reading value of the soil moisture sensor will become larger. When the water content in the soil is further increased (in the case of extremes, it is imagined to throw some soil into the water), the ion concentration in the soil will be diluted by excess water, resulting in a decrease in electrical conductivity and a high resistance value. Therefore, you will find that the soil moisture sensor will read less in pure water than in wet soil (although the former has a significantly higher water content) because the ion concentration in the moist soil is higher and the resistance is higher. small.


### Parameters

- Size: 24×72mm
- Range value: 0~100
- Precision: ±5%
- Operating current: 14mA
