# Science Sensor

<img src="images/science-sensor-1.png" style="padding:3px 0px 12px 3px;width:100px;">

## Overview

The science sensor is a multi-in-one sensor developed independently by Makeblock. Integrating the MQ2 gas, magnetic field, flame, atmospheric pressure, temperature & humidity, PIR, touch, and soil moisture sensing components, the science sensor can be used in various data collectiong, science exploring, and Internet of Things (IoT) projects.

<img src="images/science sensor-2.png" style="padding:3px 0px 12px 3px;width:400px;">

## Functions and parameters

### Overall parameters

* Dimensions: 78.2 × 24 × 1.6 (mm)
* Operating voltage: 5.0 V
* Operating current: 250 mA

### Functions and parameters of each component

#### MQ2 gas sensor

* Function: Sensitively detects smoke,  liquefied natural gas (LNG), butane, propane, methane, alcohol, and hydrogen in the air.
* Gas concentration detection range: 300–10000 ppm (flammable gas)
* Impedance when heated: 33Ω
* Preheating energy consumption：< 950 mW

<div style="background-color:#EAF6FD;padding:10px;border-left:5px solid #BAE3F9;margin-bottom:30px;border-radius:5px;line-height:28px;font-size:14px;"><strong>Note:</strong><br> 
The deviation may be large when the sensor is just powered on. Preheat it for five minutes before you use it.
</div>

#### Magnetic field sensor

* Function: Measures the angle between the sensor and North Pole (90 degrees north latitude) after being calibrated.
* Degree detection range: –180° to +180°（The angle 0° refers to the magnetic north pole.）
* Magnetic flux value range:  ±30G (unit: gauss)

<div style="background-color:#EAF6FD;padding:10px;border-left:5px solid #BAE3F9;margin-bottom:30px;border-radius:5px;line-height:28px;font-size:14px;"><strong>Note:</strong><br> 
Place the sensor on a horizontal plane before you use it. Tilting it may cause a large detection deviation.<br>
Ensure that no powerful magnetic material, such as a magnet,  is placed nearby. Otherwise, detection errors may be caused.
</div>

#### Flame sensor

* Function: Detects a flame and its size through IR light detection.
* Detection wavelength range: 600–1000nm
* Flame size value range: 1–100

<div style="background-color:#EAF6FD;padding:10px;border-left:5px solid #BAE3F9;margin-bottom:30px;border-radius:5px;line-height:28px;font-size:14px;"><strong>Note:</strong><br> 
Don't use the sensor under the direct sunlight. The sunlight may cause severe interference.
</div>

#### Atmospheric pressure sensor

* Function: Estimates the altitude based on the detected atmospheric pressure.
* Atmospheric pressure detection range: 300–1100 hPa (unit: hundred pascals)
* Altitude value range: –500 m to +6000 m

<div style="background-color:#EAF6FD;padding:10px;border-left:5px solid #BAE3F9;margin-bottom:30px;border-radius:5px;line-height:28px;font-size:14px;"><strong>Note:</strong><br> 
Ensure that there are no particles, such as dust, on the surface of the sensor. Otherwise, the sensor may be damaged.
</div>

#### Humiture sensor

* Function: Detects the humidity and temperature of the air.
* Temperature value range: –40°C to +125°C
* Temperature precision: ±0.5°C (in the environment of 0°C to 50°C); ±1°C (in the environment of –20°C to +85°C)
* Humidity value range: 0–100%
* Humidity precision: ±3% (in the environment of 50% RH); ±5% (in the environment of 20% to 80% RH)

<div style="background-color:#EAF6FD;padding:10px;border-left:5px solid #BAE3F9;margin-bottom:30px;border-radius:5px;line-height:28px;font-size:14px;"><strong>Note:</strong><br> 
Ensure that there are no particles, such as dust, on the surface of the sensor. Otherwise, the detection deviation may be large or the sensor may not work properly.
</div>

#### PIR sensor

* Function: Detects whether a human being or homeothermic animal passes by.
* Detection range: < 2 m
* Detection angle at the x-axis: 80°

  <img src="images/science-sensor-3.png" style="padding:3px 0px 12px 3px;width:200px;">

* Detection angle at the y-axis: 55°

  <img src="images/science-sensor-4.png" style="padding:3px 0px 12px 3px;width:200px;">

* Detection duration after being triggered: 2 seconds

<div style="background-color:#EAF6FD;padding:10px;border-left:5px solid #BAE3F9;margin-bottom:30px;border-radius:5px;line-height:28px;font-size:14px;"><strong>Note:</strong><br> 
Don't remove the white Fresnel filter. Otherwise, a large detection deviation may be caused.
</div>

#### Touch sensor

* Function: Detects whether a hand touches the sensor.
* Touch area: area of the fingerprint pattern

  <img src="images/science-sensor-5.png" style="padding:3px 0px 12px 3px;width:200px;">

* Resistance value range: 0–102300000kΩ

<div style="background-color:#EAF6FD;padding:10px;border-left:5px solid #BAE3F9;margin-bottom:30px;border-radius:5px;line-height:28px;font-size:14px;"><strong>Note:</strong><br> 
Keep the area of the fingerprint pattern clear without any dust or waterdrop to prevent detection errors.
</div>

#### Soil moisture sensor

* Function: Detects soil moisture.
* Detection range: area of the ear patterns

  <img src="images/science-sensor-6.png" style="padding:3px 0px 12px 3px;width:200px;">

* Moisture value range: 0–100% (RH)
* Resistance value range: 0–102200kΩ

<div style="background-color:#EAF6FD;padding:10px;border-left:5px solid #BAE3F9;margin-bottom:30px;border-radius:5px;line-height:28px;font-size:14px;"><strong>Note:</strong><br> 
Ensure that the nameplate of the science sensor is kept above the soil when you insert the sensor into the soil. Otherwise, the other sensing components may be damaged.
</div>

## Example programs

<p><span style="background-color:#66CDAA;line-height:15px;padding:6px;font-size:12px;border-radius:3px;"><a href="images/science-sensor-example-program.mblock" download style="color:white;font-weight:bold;">Science sensor example programs</a></span></p>

### Example 1: PIR-sensitive lamp (using the PIR sensor)

You can control the LEDs on CyberPi based on the PIR sensor and view the detection result of the PIR sensor on the screen. For the working principle of the PIR sensor, see [PIR Sensor](pir-sensor.md).

<img src="images/science-sensor-7.png" style="padding:3px 0px 12px 3px;width:400px;">

## Example 2: Environment detector (using the humiture sensor and air pressure sensor)

The humiture sensor on the science sensor can be used to detect the ambient temperature and humidity, and the air pressure sensor can be used to detect the air pressure and estimate the altitude. You can program the screen of CyberPi to display all the output data of the sensors.

<img src="images/science-sensor-8.png" style="padding:3px 0px 12px 3px;width:600px;">

### Example 3: Fire alarm (using the MQ2 gas sensor and flame sensor)

The science sensor integrates an MQ2 gas sensor and flame sensor, which can be used to detect gas leaks and fires in families. 

With these two sensors, you can make a simple alarm that is to be triggered by a flammable gas or flame. When the alarm is triggered, CyberPi makes a warning sound.

<img src="images/science-sensor-9.png" style="padding:3px 0px 12px 3px;width:600px;">

For the working principle of the flame sensor, see the Working principle of the flame sensor. The flame sensor is triggered by infrared radiation, and the infrared radiated by a human body may also trigger it. Therefore, in pratice, you can set a threshold (strength of the flame detected) for triggering the flame sensor.

<img src="images/science-sensor-10.png" style="padding:3px 0px 12px 3px;width:200px;">

Example 4: Compass (using the magnetic field sensor)
The magnetic field sensor on the science sensor can be used to function as a compass. The magnetic declination varies according to region, and therefore you need to calibrate the magnetic field sensor before using it. 

<div style="background-color:#EAF6FD;padding:10px;border-left:5px solid #BAE3F9;margin-bottom:30px;border-radius:5px;line-height:28px;font-size:14px;"><strong>Note:</strong><br> 
You need to calibrate the magnetic field sensor in a non-magnetic environment. An outdoor open space is recommended.
</div>

Use the following block to enable or disable the calibration.

<img src="images/science-sensor-11.png" style="padding:3px 0px 12px 3px;width:200px;">

Output value range:  –180° to +180°（where 0° indicates that the science sensor points at 90°N, the North Pole）

<img src="images/science-sensor-12.png" style="padding:3px 0px 12px 3px;width:400px;">

Program the science sensor to function as a compass.

<img src="images/science-sensor-13.png" style="padding:3px 0px 12px 3px;width:600px;">

### Example 5: Soil moisture detector (using the soil moisture sensor)

The soil moisture sensor on the science sensor is of the resistive type. When the soil moisture increases, the water can dissolve the ions contained in part of the soil, so that the resistance of the soil decreases, and therefore the output value of the soil moisture sensor becomes greater. When the water content in the soil further increases (in the case of extremes, imagine that you throw some soil into water), the ion concentration in the soil is diluted by excess water, resulting in a decrease in electrical conductivity and an increase in resistance. That's why you may find that the soil moisture sensor outputs a smaller value in pure water than in moist soil (although the former has a significantly higher water content) because the ion concentration in the moist soil is higher and the resistance is smaller.

With the following program, you can view the change in soil moisture and resistance detected by the soil moisture sensor.

<img src="images/science-sensor-14.png" style="padding:3px 0px 12px 3px;width:600px;">

### Example 6: Touch control lamp (using the touch sensor）

The touch sensor on the science sensor can detect whether the touch point is touched and thus can implement touch-based control. 

With the following program, you can control the LEDs on CyberPi by touching the touch sensor.

<img src="images/science-sensor-15.png" style="padding:3px 0px 12px 3px;width:400px;">
