# PIR Sensor

The Passive Infrared (PIR) sensor detects motion from humans or warm-blooded animals.

<img src="../../../../zh/mbuild/hardware/sensors/images/pir-sensor.png" style="padding:3px 0px 12px 3px;width:200px;">

### Principle

The PIR sensor uses a human pyroelectric infrared sensor as a core component. The normal temperature of the human body is 37 ° C, corresponding to the infrared light of 10 μm wavelength. The PIR sensor detects the movement of the human body or the temperature heat source nearby by detecting the change of the infrared light of 10 μm wavelength in the environment.

In order to be as keenly aware of the 10 μm infrared light changes, the PIR sensor uses a Fresnel Filter to concentrate the infrared light so that the sensor can receive as much infrared light as possible in the environment.


### Real-Life Examples

- The automatic door of shopping malls uses PIR sensor in the opening or closing door<br>
<img src="../../../../zh/mbuild/hardware/sensors/images/pir-1.jpg" style="padding:8px 0px 12px 3px;width:360px;">

### Parameters

- Size: 24×20mm
- Detecting range: 3~5m
- Detecting angle of the X axis: 80°
- Detecting angle of the Y axis:55°
- Duration after triggering: 3s
- Operating current: 15mA