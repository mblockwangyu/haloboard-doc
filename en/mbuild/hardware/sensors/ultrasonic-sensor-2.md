# Ultrasonic Sensor 2

<img src="images/ultrasonic-sensor-2-1.png" style="padding:3px 0px 12px 3px;width:200px;">

## Overview

We have improved our ultrasonic sensor, using the plastic casing to protect it and adding the blue LEDs. The blue LEDs can increase the potential of mBot2 for emotion expression and interaction.

<img src="images/ultrasonic-sensor-2-2.png" style="padding:3px 0px 12px 3px;width:500px;">

### Ultrasonic sensor comparison

<table  cellpadding="10px;" cellspacing="15px;" style="text-align:left;;margin-bottom:20px;">
<tr>
<th width="33%;" style="border: 1px solid black;">   </th>
<th width="33%;" style="border: 1px solid black;">Ultrasonic sensor 2</th>
<th width="34%;" style="border: 1px solid black;">Ultrasonic sensor</th>
</tr>

</td>
<td style="border: 1px solid black;">Plastic casing to improve the durability and quality</td>
<td style="border: 1px solid black;">Yes</td>
<td style="border: 1px solid black;">No</td>
</tr>

<tr>
<td style="border: 1px solid black;">Built-in chip to improve the operation stability</td>
<td style="border: 1px solid black;">Yes</td>
<td style="border: 1px solid black;">No</td>
</tr>

<tr>
<td style="border: 1px solid black;">Blue LED<br>(additional function)</td>
<td style="border: 1px solid black;">8</td>
<td style="border: 1px solid black;">0</td>
</tr>

</table>