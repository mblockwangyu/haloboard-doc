# Smart Camera
The smart camera can learn and recognize brightly colored objects as well as detecting bar codes and lines, which allows it to be used in various applications, such as garbage sorting, intelligent transportation, object tracking, and intelligent line following.

<img src="images/vision-1.png" style="padding:3px 0px 12px 3px;width:800px;">

## Connect to mBot or Halocode

Based on the wiring mode, the smart camera can be used as an RJ25 electronic module or mBuild electronic. After connecting it to mBot or Halocode, you can control it through mBot or Halocode. 

### Connect to mBot
After connecting the smart camera to mBot, you can power it with a 3.7V lithium battery or the mBuild power module.

#### Method 1 Power through the mBuild power block (recommended) 

<img src="../../../../zh/mbuild/hardware/sensors/images/vision-5.png" style="padding:3px 0px 12px 3px;width:700px;">

#### Method 2 Power through a 3.7V lithium battery

<img src="../../../../zh/mbuild/hardware/sensors/images/vision-6.png" style="padding:3px 0px 12px 3px;width:700px;">

### Connect to Halocode

Note: It is recommended to power the smart camera through the mBuild power module. Power only through the USB port may affect the proper use of the block.

<img src="../../../../zh/mbuild/hardware/sensors/images/vision-7.png" style="padding:3px 0px 12px 3px;width:700px;">


## Features
### Color learning

The smart camera can learn brightly colored objects and identify the color blocks after learning, and then return their coordinates, length, and width. <br>

**Learn a colored object as follows:**

1. Long press the Learn button until the indicator turns red (orange, yellow, green, blue, or purple, different colors indicate learning different objects), and then release the button. 
2. Place the color block to be learned in front of the camera.
3. Observe the indicator on the front or back of the smart camera, and slowly move the object to be learned until the color of the indicator matches the object.<br>
4. Short press the Learn button to record the current learned object.
After the learning is successful, when the camera recognizes a learned object, the color of the indicator becomes the same as that of the learned object.

A maximum of seven objects can be learned and recorded in this mode.

<img src="images/vision-2.png" style="padding:3px 0px 12px 3px;width:800px;">

After the learning is complete, you can use the following program to track the color block (color block 1).

<img src="images/vision-3.png" style="padding:3px 0px 12px 3px;width:600px;">

**Note**

1\. The more brighter color of the learned object, the more accurate the recognition.<br>
Cautionary Example：learning a panda doll

<img src="../../../../zh/mbuild/hardware/sensors/images/vision-9.png" style="padding:3px 0px 12px 3px;width:150px;">

2\. The smart camera can learn and record seven objects, which should be easily distinguished in color.<br>
Cautionary Example：learning a yellow ball and a yellow hat

<img src="../../../../zh/mbuild/hardware/sensors/images/vision-10.png" style="padding:3px 0px 12px 3px;width:300px;">

Good example：learning an orange ball and a green ball

<img src="../../../../zh/mbuild/hardware/sensors/images/vision-11.png" style="padding:3px 0px 12px 3px;width:300px;">

3\. Switch the smart camera to the color block detection mode to detect the coordinates and size of the color block. <br>
4\. The smart camera can prevent light interference. However, changeable light can still affect recognition accuracy. The previous learning results may fail to be used in a new environment. Here are some solutions：
- Reset the white balance
- Turn on the LED before learning or recognizing
- Set the detail parameters in PixyMon
- Use a lampshade to cover the blocks

###  Bar codes and lines recognizing
The smart camera can detect bar codes, lines, and branch roads simultaneously without learning. <br>
Switch the smart camera to the line/label tracking mode to detect and return the coordinate information of bar codes, lines, and branch roads. <br>

**Bar codes recognizing**<br>
Bar code cards and stickers are included in the package. Click to <a href="images/barcodes.rar" download >download the bar code cards</a>.
<img src="../../../../zh/mbuild/hardware/sensors/images/vision-3.png" style="padding:3px 0px 12px 3px;width:800px;">

You can put the cards on the line-follower map.

<img src="../../../../zh/mbuild/hardware/sensors/images/vision-12.png" style="padding:3px 0px 12px 3px;width:400px;">

**Lines and branch roads recognizing**<br>
The smart camera can detect the lines and the number of branch roads, and return the coordinates, directions, and the number of branch roads.

<img src="../../../../zh/mbuild/hardware/sensors/images/vision-4.png" style="padding:3px 0px 12px 3px;width:600px;">

**Note**

1\. You can set the line tracking mode to **dark line on light** or **light line on dark** in the program.<br>
2\. The smart camera will filter out the narrow lines by default. Check PixyMon's user guide to change the default settings.

### PixyMon
The smart camera supports PixyMon software. You can view the camera screen through PixyMon, debug the smart camera function, and fine-tune some parameters to explore more complex features.

Click the following links to download the the software or firmware you need: 

[PixyMon for Windows](images/pixymon_v2_windows-3.0.24.rar)<br>
[PixyMon for Mac](images/pixymon_v2_mac-3.0.24.rar)<br>


**Note**<br>

Considering the ease of use of the module, we haven't developed a lot of functions on the blocks. If you are interested in controlling the smart camera through the blocks to implement more features, use the mBlock Extension Builder. 

[mBlock Extension Builder documentation](https://www.mblock.cc/doc/en/developer-documentation/developer-documentation.html?home)

## Parameters
* Dimensions: 48 × 48mm
* Resolution: 640 x 480
* Field of view: 65.0 degrees
* Effective focal length: 4.65±5% mm
* Recognition speed: 60fps
* Recognition distance: 0.25–1.2m
* Fall resistance: 1m
* Power supply: 3.7V lithium battery or 5V mBuild power module
* Power consumption range: 0.9–1.3W
* Operating temperature: -10℃–55℃