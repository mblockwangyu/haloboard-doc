# Motion Sensor

The motion sensor consists of a 3-axis gyroscope, and a 3-axis accelerometer, to detects the motion, acceleration, and vibration of the object.

<img src="../../../../zh/mbuild/hardware/sensors/images/motion-sensor.png" style="padding:3px 0px 12px 3px;width:200px;">

### The Axes and Angles

<table  cellpadding="10px;" cellspacing="15px;" style="text-align:left;;margin-bottom:20px;">
<tr>
<th style="border: 1px solid black;">Axis</th>
<th style="border: 1px solid black;">Angle</th>
<th style="border: 1px solid black;">Range</th>
</tr>

</td>
<td style="border: 1px solid black;">X</td>
<td style="border: 1px solid black;">Pitch</td>
<td style="border: 1px solid black;">-180~180°</td>
</tr>

<tr>
<td style="border: 1px solid black;">Y</td>
<td style="border: 1px solid black;">Roll</td>
<td style="border: 1px solid black;">-90~90°</td>
</tr>

<tr>
<td style="border: 1px solid black;">Z</td>
<td style="border: 1px solid black;">Yaw</td>
<td style="border: 1px solid black;">-180~180°</td>
</tr>
</table>

### Principle

If you search for gyroscopes on the Internet, you will most likely get the following picture:

<img src="../../../../zh/mbuild/hardware/sensors/images/gyro.gif" style="padding:3px 0px 12px 3px;width:200px;">


This is a theory based on the conservation of angular momentum, a mechanism used to sense and maintain direction. The gyroscope is mainly composed of a rotor that is located at the axis and rotatable. Due to the angular momentum of the rotor, once the gyroscope begins to rotate, there is a tendency to resist the change of direction. Such gyroscopes are used in early navigation systems.

In modern electronic products,  MEMS (Micro Electro Mechanical systems) gyroscope is widely used. Its body is small and can be directly soldered to the PCB, which is the main reason why we can control the size of the motion sensor at 24×20mm.

The principle of the two will be quite different, but you only need to know that they can more realistically reflect the posture and motion state of the object in space.


### Real-Life Examples

- Nintendo Switch has embedded gyro that enables to play somatosensory games.<br>
<img src="../../../../zh/mbuild/hardware/sensors/images/motion-1.jpg" style="padding:10px 0px 12px 3px;width:400px;">
- The level feature of iPhone also uses gyro<br>
<img src="../../../../zh/mbuild/hardware/sensors/images/motion-2.png" style="padding:10px 0px 12px 3px;width:300px;">

### Parameters

- Size: 24×20mm
- Precision: ±1°
- Acceleration range: ±8g
- Operating current: 18mA