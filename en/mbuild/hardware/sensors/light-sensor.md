# Light Sensor

The light sensor block detects ambient light intensity.

<img src="../../../../zh/mbuild/hardware/sensors/images/light-sensor.png" style="padding:3px 0px 12px 3px;width:200px;">

### Principle

The light sensor in mBuild uses a photoresistor as the core component. This component can change the resistance value according to the light intensity. The change of the resistance value can be detected by the MCU. So the MCU can obtain the light intensity in the environment.


### Real-Life Examples

- Light sensor helps in adjusting screen lightness automatically<br>
<img src="../../../../zh/mbuild/hardware/sensors/images/light-2.jpg" style="padding:8px 0px 12px 3px;width:400px;">
- Some smart bulbs with embedded light sensor to adjust lightness according to ambient light intensity<br>
<img src="../../../../zh/mbuild/hardware/sensors/images/light-1.jpg" style="padding:8px 0px 12px 3px;width:400px;">

### Parameters

- Size: 24×20mm
- Range value: 0~100%
- Operating current: 15mA