# MQ2 Gas Sensor

The MQ2 Gas Sensor can detect all kinds of gases in the air, such as smoke, liquefied gas, butane, propane, methane, alcohol, hydrogen, and others.

<img src="../../../../zh/mbuild/hardware/sensors/images/gas-sensor.png" style="padding:3px 0px 12px 3px;width:200px;">

### Principle


The MQ-2 type smoke sensor is made of a tin dioxide semiconductor gas sensing material. When at a temperature of 200 and 300 ° C, the tin dioxide adsorbs oxygen in the air to form an anion of oxygen, which reduces the density of electrons in the semiconductor, thereby increasing the resistance value. When in contact with smoke, if the barrier at the grain boundary is changed by the modulation of the smoke, it will cause a change in conductivity. By using this, information about the presence of this smoke can be obtained. 


### Real-Life Examples

- Smoke alarms detect smoke and other flammable gases in the environment.<br>
<img src="../../../../zh/mbuild/hardware/sensors/images/gas-sensor-1.jpg" style="padding:10px 0px 12px 3px;width:300px;">

### Parameters

- Size: 24×36mm
- Operating current: 160mA
- Heating impedance: 33Ω
- Pre-heat energy consumption: &lt; 800mW

### Notes

5-minute pre-heat for better performance.