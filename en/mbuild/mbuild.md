<img src="../../zh/mbuild/mbuild-all.png" style="padding:3px 0px 12px 3px;width:600px;">

# mBuild

As a new generation of electronic platform, Makeblock's mBuild consists of a big family of delicate yet highly intelligent electronic modules, which work with almost all mainstream open-source hardwares. The mBuild electronic modules are easy to use. Programming is not a necessity, but more complicated functions can be achieved through programming, for instance, using mBlock 5. The mBuild electronic platform comes in handy on all kinds of occasions, according to your needs, from creation, programming teaching, AI education, to robotic competition.

* [Hardware Guide](hardware.md)
    * [Power Modules](hardware/power.md)
    * [Communication](hardware/communication.md)
    * [Interaction Modules](hardware/interaction.md)
    * [Sensors](hardware/sensors.md)
    * [Display](hardware/display.md)
    * [Light](hardware/light.md)
    * [Play](hardware/sound.md)
    * [Motion](hardware/motion.md)
    * [Peripheral Modules](hardware/peripheral.md)
    * [Accessories](hardware/accessories.md)