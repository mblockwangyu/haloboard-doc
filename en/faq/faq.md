<img src='images/halo-front.png' width='240px;'>

# FAQ for Halocode

## 1. No device is detected when connecting Halocode?

When Halocode is connected to the USB port of PC via a USB cable and "Click" button clicked in mBlock 5, yet the Connect Device window says "No device can be detected to connect", as follows:

<img src="images/no-device-1.png" width="800px;" style="padding:5px 5px 20px 5px;">

A: there are two possible reasons: USB cable, or driver. Following are corresponding solutions:

**1\) USB cable**

Some USB cables are charge-only and cannot be used for data transmission. When a charge-only USB cable is used to connect Halocode, mBlock 5 PC might not detect Halocode properly.

If you have other USB cables, please try using another one.

**2\) The driver is not correctly installed**

Due to PC protection software (system built-in or third-party), the driver might not be correctly installed during the installation process. Thus, mBlock 5 PC is unable to detect Halocode.

Please disconnect Halocode, and then download and install the driver for Halocode:
- Windows: [http://www.wch.cn/download/CH341SER_EXE.html](http://www.wch.cn/download/CH341SER_EXE.html)
- macOS: [http://www.wch.cn/download/CH341SER_MAC_ZIP.html](http://www.wch.cn/download/CH341SER_MAC_ZIP.html)

After the installation is complete, connect Halocode to PC via a USB cable. Click "Connect", and check whether the connection port is detected (as follows):

<img src="images/no-device-3.png" width="600px;" style="padding:5px 5px 20px 5px;">

---

## 2. How to power Halocode?

The voltage range of Halocode's power supply is DC 3-5V. Voltage exceeding this range will cause malfunction. The following two methods are recommended:

1. Use external battery box connected via the battery interface
2. Use a Micro-USB cable to connect to your PC, mobile power, or power adapter (5V 1A recommended). Do not use fast-charging power adapter of mobile phones, for the voltage is usually greater than 5V.

---

## 3. Apart from mBlock 5 PC, can I use my phone or tablet to program with Halocode?

Currently, only mBlock 5 PC or Web is supported. We'll keep you updated if mBlock 5 Mobile supports Halocode.

---


## 4. Apart from mBlock 5 for PC, can I use other software to program with Halocode? 

Halocode currently only supports mBlock 5, which is an incredibly powerful software. Programming is easy as building blocks. You can also switch to Python coding to learn one of the hottest programming languages.

---

## 5. Apart from Scratch, can I use Python or Arduino to program with Halocode?

Halocode currently only supports mBlock 5, which provides both Scratch-3.0-based programming and micro-python textual programming.

---

## 6. How to use the Wi-Fi of Halocode?

You can use the <span style="background-color:#76CE14;;color:white;padding:4px; font-size: 12px;border-radius:3px;">connect to Wi-Fi () password ()</span> block from the Wi-Fi category to connect Halocode to the internet. Both the name and password of the Wi-Fi should be correctly input to ensure successful connection.

<img src="images/wifi-1.png" width="800px;" style="padding:5px 5px 20px 5px;">

<small><b>Note:</b><br>Some public Wi-Fi may not work, like those provided hotels, which usually require extra authentication via your phone or other methods.</small>

---

## 7. Why are some Wi-Fi blocks greyed out (disabled) in mBlock 5?

When using mBlock 5, if you do not sign in/up to mBlock 5, some Wi-Fi blocks will be greyed out (disabled) as follows:

<img src="images/wifi-2.png" width="800px;" style="padding:5px 5px 20px 5px;">

In this case, please find the sign in/up entry in the upper-right corner, and then sign in/up to mBlock 5. All Wi-Fi blocks should be available when you finish.

<img src="images/wifi-3.png" width="800px;" style="padding:5px 5px 20px 5px;">

---

## 8. How to use the Speech Recognition function?

You can use the <span style="background-color:#76CE14;color:white;padding:4px; font-size: 12px;border-radius:3px;">recognize () for () seconds</span> block from the Wi-Fi category. 

<img src="images/ai.png" width="800px;" style="padding:5px 5px 20px 5px;">

**Notes:** 
1. Before using the Speech Recognition block, you need to connect Halocode to the internet;  
2. The recognition process may take some time, depending on internet speed; the next block will not execute until Halocode receives the recognition results;  
3. So far, only two languages are supported, Chinese and English.

---

## 9. Unable to upload program to Halocode or run program?

Due to some incorrect operations or in some unexpected conditions, program is sometimes unable to be upload or fails to run. Please try the following steps to solve the program:

**1\) Is Halocode normally powered?**

Check the blue indicator in the bottom-left corner. If the indicator is off, Halocode is not powered.

<img src="../../zh/faq/images/power.png" width="400px;" style="padding:5px 5px 20px 5px;">

An incorrect power supply can also cause the malfunction of Halocode. There are two ways to power Halocode:
- Use external battery box connected via the battery interface
- Use a Micro-USB cable to connect to your PC, mobile power, or power adapter.

The voltage range of Halocode's power supply is DC 3-5V. Voltage exceeding this range will also cause malfunction. Therefore, some fast charging power adapter of mobile phones can not be used as power adapter of Halocode, for the voltage is usually greater than 5V.

**2\) Is the program bugged?**

Your program may be bugged. You can try uploading a simple program, such as lighting up the LED ring, to check if the problem lies in the program itself.

**3\) Is the USB cable used a data cable? Is the connection stable?**

Some USB cable is charge-only, thus can not be used for data transmission. If a charge-only cable is used, program won't be uploaded. Loose connection to the USB port of your PC is also one possible cause.

**4\) Does the firmware version match the block version?**

The programming blocks will keep updating. If there are new blocks, the firmware of supported device also need to be upgraded. If your Halocode uses the older-version firmware, but new-version blocks are used, the program can not be uploaded, or won't run properly even though program is uploaded. 

Usually, when there is new firmware available, there will be a corresponding prompt in the interface of mBlock 5. 

<small>Note: if you get an unofficial firmware version through some channels, these firmware versions will not be prompted for update in the interface of mBlock 5.</small>

When there is a new version, firmware update prompt will be displayed as follows:

<img src="images/upgrade-0.png" width="400px;" style="padding:5px 5px 20px 5px;">

**5\) Is the firmware damaged?**

In some rare situations, unstable power supply voltage will affect the FLASH of Halocode, thus damaging the firmware. Damaged firmware is not always detected by mBlock 5. Users thus are not prompted to fix the problem.

Follow the following steps to re-update and fix the firmware:

<img src="images/upgrade-1.png" width="400px;" style="padding:5px 5px 5px 5px;">
<td width="10%;">

<img src="images/upgrade-2.png" width="400px;" style="padding:5px 5px 5px 5px;">

<img src="images/upgrade-3.png" width="300px;" style="padding:5px 5px 20px 5px;">


---

## 10. Live mode doesn't work properly?

Due to technical limitations, user experience of Halocode under Live mode can be affected in these  particular areas:

**1\) Mode switching is slow**

Each time of mode switching accompanies restarting Halocode, approximately 5 seconds. During the restarting process, Halocode can not be controlled through programming (it is normal). When the restart is finished, the LED ring of Halocode will light up green and then go off. Uploading MESH related program requires restarting Halocode two times, which takes longer timer.

**2\) Limited speed in Live mode**

In Live mode, program runs on the host computer, so the speed is limited by the communication delay between the host and the hardware. Therefore, program runs more slowly than does in Upload mode. The speed difference is especially obvious when Halocode is connected through Bluetooth.

**3\) Limited multi-threading**

In Live mode, programs run on the host computer. Some multi-thread programs might not run properly.

<small>Note: Upload mode is recommended for multi-threaded programs.</small>

**4\) Wi-Fi MESH program conflicts with Bluetooth function**

If MESH related program is uploaded, Live mode will be disabled if Halocode is connected via Bluetooth. To restore Live mode, try uploading a piece of blank code.

---

FAQ will keep updating!


