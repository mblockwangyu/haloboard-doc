# Makeblock HaloCode

\* If you have any technical problems, please contact: <support@makeblock.com>

Thank you for your interest in Makeblock HaloCode!

The help documents include the following parts:

* [Tutorials](README.md)
    * [Introduction](tutorials/introduction.md)
    * [Get Started](tutorials/get-started.md)
    * [Upload Mode](tutorials/upload-mode.md)
    * [Speech Recognition](tutorials/voice-recognition.md)
    * [User Cloud Message](tutorials/cloud-messaging.md)
    * [Use Python](tutorials/use-python.md)
* [Related Product](related-products/related-products.md)
* [mbuild hardware](mbuild/mbuild.md)
* [Examples](examples/examples.md)
* [Block Reference](block-reference/block-reference.md)
* [Python API Reference](python-api/python-api.md)
* [Code with Mu: A Simple Python Editor ](tutorials/use-python-mu.md)
* [FAQ](faq/faq.md)

