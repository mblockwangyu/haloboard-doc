# Summary

## Docs

* [Kit estándar de HaloCode de Makeblock](README.md)
    * [Lista de piezas](parts.md)
    * [Guía de inicio rápido](quick-start.md)
    * [Introducción apariencia](appearance.md)
    * [Pila](battery.md)
    * [Crea con la correa de gancho y bucle ](strap.md)
    * [Crea con pinzas de cocodrilo](clip.md)
    * [Preguntas frecuentes](faq.md)
    * [Precaución](caution.md)