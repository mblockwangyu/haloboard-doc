# Pila

### Instalación de la pila

<img src="../fr/images/install-battery.png" width="600px" style="padding:5px 5px 5px 5px;">

### Advertencias

- No tires del cable de la batería directamente para desconectar la batería. Usa sus dedos para pellizcar el conector y retirarlo con cuidado.
- No mezcles diferentes tipos de pilas ni mezcles pilas nuevas y usadas.
- Inserta las pilas de forma correcta (con la polaridad correcta).

- Retira la batería usada de la caja de la batería.

- No coloques ningún objeto metálico en el zócalo de la batería de HaloCode, ya que puede provocar un cortocircuito y calentar la batería.
- No intentes cargar las pilas normales (no son recargables).
