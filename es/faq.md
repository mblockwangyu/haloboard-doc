# Preguntas frecuentes

### ¿Por qué el programa no funciona como se espera después de cargarlo en el HaloCode?

Puede ser que la versión del firmware no esté actualizada. Haga clic en **Actualizar** para actualizar el firmware.

<img src="images/update-halo.png" width="300px;" style="padding:5px 5px 20px 5px;"> <span style="font-size:30px;padding:5px;color:lightblue;">&rarr;</span><img src="images/update-halo-1.png" width="300px;" style="padding:5px 5px 20px 5px;">

### ¿Cómo alimentar el HaloCode?

Hay dos métodos para alimentar el HaloCode:

1. Lo conecta a una caja para pilas a través de la interfaz de pilas. 
2. Conectarlo a un ordenador o cargador portátil mediante un cable micro USB.

Requisito de tensión de alimentación: DC de 3 a 5 V
