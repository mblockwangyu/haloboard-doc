# Guía de inicio rápido

## 1. Descarga el software

Para descargar la última versión de mBlock 5, visita [http://www.mblock.cc](http://www.mblock.cc).

<img src="../fr/images/download.png" width="600px;" style="padding:5px 5px 20px 5px;">

## 2. Conecta el HaloCode a un ordenador

Conecta el HaloCode a tu ordenador por medio del cable micro USB.

<img src="../fr/images/connect.png" width="600px;" style="padding:5px 5px 20px 5px;">

## 3. Empieza a programar

<span style="color:white;font-size:18;border-radius:100%;background-color:lightblue;font-wieght:bold;">&nbsp; 1 &nbsp;</span> Abre el mBlock 5 y añade el HaloCode desde la biblioteca de dispositivos.

<table style="table-layout:fixed;width:100%;text-align:center;" cellpadding=20px; cellspacing=12px;>
<tr>
<td width="50%"><img src="images/connect-halo-1.png" height="120px;" style="padding:5px 5px 5px 5px;"></td>
<td width="10%;"><span style="font-size:30px;padding:5px;color:lightblue;">&rarr;</span></td>
<td width="40%"><img src="../fr/images/connect-halo-2.png" height="120px;" style="padding:5px 5px 5px 5px;"></td>
</tr>
</table>

<span style="color:white;font-size:18;border-radius:100%;background-color:lightblue;font-wieght:bold;">&nbsp; 2 &nbsp;</span> Conecta el dispositivo.

<table style="table-layout:fixed;width:100%;text-align:center;" cellpadding=20px; cellspacing=12px;>
<tr>
<td width="50%"><img src="images/connect-halo-3.png" height="120px;" style="padding:5px 5px 5px 5px;"></td>
<td width="10%;"><span style="font-size:30px;padding:5px;color:lightblue;">&rarr;</span></td>
<td width="40%"><img src="images/connect-halo-4.png" height="120px;" style="padding:5px 5px 5px 5px;"></td>
</tr>
</table>

<span style="color:white;font-size:18;border-radius:100%;background-color:lightblue;font-wieght:bold;">&nbsp; 3 &nbsp;</span> Ahora completemos una tarea de programación. Intenta hacer que las luces LED del HaloCode se pongan rojas.

<div style="margin:20px;">
<p>1) Agrega un bloque de evento como se muestra a continuación.</p>
<img src="images/program-1.png" width="600px;" style="padding:0px 5px 15px 5px;">
<p>2) Agrega los siguientes bloques de color púrpura del área del bloque de iluminación y cambie el color de la luz a rojo para completar el programa.</p>
<img src="images/program-2.png" width="600px;" style="padding:0px 5px 15px 5px;">
</div>

## 4. Ejecuta el programa

Puedes elegir si cargar el programa a la  Haloboard o ejecutar el programa directamente en el software de mBlock 5.

### Carga el programa al HaloCode: ejecuta el programa sin software.

Activa el **Modo de carga** y haga clic en **Cargar**. Después de que la carga haya sido correcta, presiona el botón azul en el HaloCode para comprobar si las luces LED se ponen de color rojo.

<img src="images/program-3.png" style="padding:5px 5px 5px 5px;">

### Ejecuta el programa en mBlock 5: No es necesaria la carga. Podrás ver los resultados en tiempo real.

Desactiva el modo de carga y cambia el color del programa de rojo a verde.Presiona el botón azul en el HaloCode para comprobar si las luces LED cambian de color.

<img src="images/program-4.png" style="padding:5px 5px 5px 5px;">
