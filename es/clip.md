# Crea con pinzas de cocodrilo

Las partes metálicas de los extremos de las pinzas de cocodrilo conducen electricidad. Con el sensor táctil y las cintas de cobre conductoras, puedes crear con facilidad proyectos atractivos, Por ejemplo una paleta de HaloCode. Lo que es aún mejor es que HaloCode también puede interactuar con la etapa 5 de mBlock.

Ahora hagamos un proyecto sencillo con HaloCode. En este proyecto, aprenderás a usar las pinzas de cocodrilo para crear un proyecto.

Con el cartóny las cintas de cobre  dentro del paquete , las luces LED pueden iluminar el color correspondiente cuando toques una pinza de cocodrilo específica. 

<img src="../fr/images/clip-1.png" width="300px;" style="padding:5px 5px 20px 5px;">

### Paso 1: Busca los materiales que necesites.

<img src="images/clip-2.png" width="800px;" style="padding:5px 5px 20px 5px;">

### Paso 2: Coloca las pinzas de cocodrilo.

<span style="color:white;font-size:18;border-radius:100%;background-color:lightblue;font-wieght:bold;">&nbsp; 1 &nbsp;</span> Coloca la pinza de cocodrilo roja en el sensor táctil 0.

<img src="../fr/images/clip-3.png" width="300px;" style="padding:5px 5px 20px 5px;">

<span style="color:white;font-size:18;border-radius:100%;background-color:lightblue;font-wieght:bold;">&nbsp; 2 &nbsp;</span> Coloca las pinzas de cocodrilo amarilla, azul y verde en los sensores táctiles 1, 2 y 3 respectivamente.

<img src="../fr/images/clip-4.png" width="300px;" style="padding:5px 5px 20px 5px;">

<div style="background-color:#FFFFCC;padding:10px;border-left:5px solid #FFEB3B;margin:20px;">NO uses la pinza de cocodrilo para conectar el pin de 3,3 V al pin GND directamente;  lo que puede causar un cortocircuito y dañar la placa del anillo óptico.
</div>

<br>

<span style="color:white;font-size:18;border-radius:100%;background-color:lightblue;font-wieght:bold;">&nbsp; 3 &nbsp;</span> Ajusta el cartón dentro del paquete y dóblalo como se muestra en la foto.

<img src="../fr/images/clip-5.png" width="800px;" style="padding:5px 5px 20px 5px;">

<span style="color:white;font-size:18;border-radius:100%;background-color:lightblue;font-wieght:bold;">&nbsp; 4 &nbsp;</span> Toma la cinta de lámina de cobre y péguela al cartón.

<img src="../fr/images/clip-6.png" width="300px;" style="padding:5px 5px 20px 5px;">

<div style="background-color:#FFFFCC;padding:10px;border-left:5px solid #FFEB3B;margin:20px;">*Las cintas de cobre son conductivas en ambos lados y puede extender y ampliar el área táctil con el clip de cocodrilo. 
</div>

<br>

<span style="color:white;font-size:18;border-radius:100%;background-color:lightblue;font-wieght:bold;">&nbsp; 5 &nbsp;</span> Retira las láminas protectoras de las cintas de cobre.

<img src="../fr/images/clip-7.png" width="300px;" style="padding:5px 5px 20px 5px;">

<div style="background-color:#FFFFCC;padding:10px;border-left:5px solid #FFEB3B;margin:20px;">*La lámina protectora se usa para proteger la cinta contra la oxidación.
</div>

<br>

<span style="color:white;font-size:18;border-radius:100%;background-color:lightblue;font-wieght:bold;">&nbsp; 6 &nbsp;</span> Coloca el otro extremo de las pinzas de cocodrilo en las cintas de cobre.

<img src="../fr/images/clip-8.png" width="500px;" style="padding:5px 5px 20px 5px;">

### Paso 3: Conecta el HaloCode a un ordenador con el cable micro USB.

<img src="../fr/images/clip-9.png" width="600px;" style="padding:5px 5px 20px 5px;">

### Paso 4: Empieza a codificar.

<img src="images/clip-11.png" width="600px;" style="padding:5px 5px 20px 5px;">

### Ya está completo.

Toca la parte metálica de la pinza de cocodrilo roja o la cinta de cobre para ver si las luces LED se ponen rojas.

<img src="../fr/images/clip-10.png" width="600px;" style="padding:5px 5px 20px 5px;">

Ahora ya sabes cómo usar las pinzas de cocodrilo. Usa tu imaginación para crear más proyectos divertidos.



