# Crea con la correa de gancho y bucle 

Con la correa de gancho y bucle, puedes sujetar el HaloCode a tu muñeca. Además, las luces LED junto con el sensor táctil del HaloCode te permiten crear un dispositivo portátil, como un podómetro.

Convirtamos el HaloCode en un dispositivo portátil. En este proyecto, aprenderás a crear un proyecto con la correa de gancho y bucle.

Lograrás lo siguiente: Usar el HaloCode en tu muñeca. Según lo programado, las luces LED iluminarán un color aleatorio cuando el sensor de movimiento de HaloCode detecte los movimientos de tu mano. 

<img src="../fr/images/strap-1.png" width="300px;" style="padding:5px 5px 20px 5px;">

### Paso 1: Busca los materiales que necesites.  

<img src="../fr/images/strap-2.png" width="800px;" style="padding:5px 5px 20px 5px;">

### Paso 2: Escribe el código y cárgalo en el HaloCode.

<img src="images/strap-3.png" width="800px;" style="padding:5px 5px 20px 5px;">

### Paso 3: Coloca las pilas en el compartimento para pilas. 

<img src="../fr/images/strap-4.png" width="800px;" style="padding:5px 5px 20px 5px;">

### Paso 4: Usa el HaloCode en tu muñeca como se muestra a continuación (la mano izquierda como ejemplo). 

<span style="color:white;font-size:18;border-radius:100%;background-color:lightblue;font-wieght:bold;">&nbsp; 1 &nbsp;</span> Conecta el compartimento para pilas al HaloCode.

<img src="images/strap-5.png" width="600px;" style="padding:5px 5px 20px 5px;">

<span style="color:white;font-size:18;border-radius:100%;background-color:lightblue;font-wieght:bold;">&nbsp; 2 &nbsp;</span> Coloca los cierres de gancho y bucle en la tapa deslizante del compartimento para pilas y en la parte posterior del HaloCode. 

<img src="images/strap-6.png" width="600px;" style="padding:5px 5px 20px 5px;">

<span style="color:white;font-size:18;border-radius:100%;background-color:lightblue;font-wieght:bold;">&nbsp; 3 &nbsp;</span>

<img src="images/strap-7.png" width="800px;" style="padding:5px 5px 20px 5px;">

<span style="color:white;font-size:18;border-radius:100%;background-color:lightblue;font-wieght:bold;">&nbsp; 4 &nbsp;</span>

<img src="images/strap-8.png" width="600px;" style="padding:5px 5px 20px 5px;">

<span style="color:white;font-size:18;border-radius:100%;background-color:lightblue;font-wieght:bold;">&nbsp; 5 &nbsp;</span>

<img src="images/strap-9.png" width="800px;" style="padding:5px 5px 20px 5px;">

<span style="color:white;font-size:18;border-radius:100%;background-color:lightblue;font-wieght:bold;">&nbsp; 6 &nbsp;</span>

<img src="images/strap-10.png" width="600px;" style="padding:5px 5px 20px 5px;">

### Y ya está completo.

Ahora enciende el interruptor de la pila. Sacude la mano para ver las luces LED encenderse.

<img src="images/strap-11.png" width="600px;" style="padding:5px 5px 20px 5px;">

Ahora sabes cómo usar la correa de gancho y bucle. Usa tu imaginación para crear más proyectos divertidos.  
