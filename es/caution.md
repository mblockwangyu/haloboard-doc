# Precaución

- Conecta el HaloCode solo a una fuente de alimentación de DC de 3V a 5V
- Evita manejar el HaloCode mientras esté conectado a una fuente de alimentación. El HaloCode debe estar apagado cuando se conecte a un circuito de E/S externo.
- La corriente máxima que se puede suministrar de forma segura a un circuito externo mediante la E/S es de 10 mA.

- La corriente máxima que se puede suministrar de forma segura a un circuito externo mediante la interfaz de 3,3 V es de 500 mA. Asegúrate de que no se exceda este límite.

- La corriente máxima suministrada de forma segura a un circuito externo mediante la interfaz de expansión de bloque electrónico es 1A: solo cuando la corriente de entrada de energía sea mayor que 3.5A, se alcanzará la corriente máxima. En este caso, se pueden conectar más bloques electrónicos al HaloCode.
- No uses el HaloCode en agua o con las manos mojadas.
- Opera el HaloCode en una habitación bien ventilada.
- No guardes ni uses el HaloCode en entornos extremadamente calientes o fríos.
- No coloques objetos metálicos a través de los circuitos impresos en la Haloboard, ya que esto puede provocar un cortocircuito y dañar el HaloCode. Esto puede provocar riesgos de daño permanente.

- Conectar tu HaloCode a cualquier periférico no aprobado podría dañar tu HaloCode.
- No dejes el HaloCode conectado a  cualquier  dispositivo sin supervisión.
- NO uses la pinza de cocodrilo para conectar el pin de 3,3 V al pin GND directamente;  lo que puede causar un cortocircuito y dañar la placa del anillo óptico.
