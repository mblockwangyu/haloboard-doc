# Kit estándar de HaloCode de Makeblock

* [Lista de piezas](parts.md)
* [Guía de inicio rápido](quick-start.md)
* [Introducción apariencia](appearance.md)
* [Pila](battery.md)
* [Crea con la correa de gancho y bucle ](strap.md)
* [Crea con pinzas de cocodrilo](clip.md)
* [Preguntas frecuentes](faq.md)
* [Precaución](caution.md)

### Ayuda y recursos

Nos importa cómo te sientes y cómo usas nuestro producto. No dudes en ponerte en contacto con nosotros si tienes algún problema o sugerencia.

Soporte técnico: <support@makeblock.com>

Makeblock Educación: [http://education.makeblock.com/](http://education.makeblock.com/)
