# 摇尾巴小猫

<div style="background-color:#F0F8FF;padding:15px;border-radius:15px;line-height:28px;margin-bottom:36px;"><b>工具与材料：</b><br>
光环板，鳄鱼夹或导线，电池盒，剪刀，打孔工具，刻度尺，卡纸，双面胶，杜邦线，9g小舵机（3.3V）等。</div>

## 制作小猫

在卡纸上画出小猫的身体和尾巴，将它们从卡纸上剪下，眼睛打孔。

**注意：小猫眼睛的间距要和光环板最远的两颗LED灯距离相等。**

<img src="images/cat-1.jpg" style="width:400px;padding:5px 5px 12px 0px;">

将舵机与光环板连接，四个引脚都可以控制舵机。

<div style="background-color:#F0F8FF;padding:15px;border-radius:15px;line-height:26px;margin-bottom:20px;"><ul>
<li>舵机的红线是代表电源正极，与鳄鱼夹红线相连接，红线鳄鱼夹与光环板3.3V引脚相连接;</li>
<li>舵机的黑线代表电源负极，与鳄鱼夹蓝线相连接，蓝线鳄鱼夹与光环板GND引脚相连；</li>
<li>舵机的白线是信号线，与鳄鱼夹黄线相连接，黄线鳄鱼夹可以与光环板任一个触摸传感器想连，在这里，我们选择与触摸传感器0相连。</li></ul>
</div>

<img src="images/cat-2.jpg" style="width:400px;padding:5px 5px 12px 0px;">
<img src="images/cat-3.jpg" style="width:400px;padding:5px 5px 12px 0px;">
<img src="images/cat-4.jpg" style="width:400px;padding:5px 5px 12px 0px;">

将尾巴粘在舵机上，将小猫的身体和尾巴与光环板、舵机、电池盒粘在一起。

<img src="images/cat-5.jpg" style="width:400px;padding:5px 5px 12px 0px;">
<img src="images/cat-6.jpg" style="width:400px;padding:5px 5px 12px 0px;">
<img src="images/cat-7.jpg" style="width:400px;padding:5px 5px 12px 0px;">

## 启用上传模式

点击启用上传模式。

<img src="../tutorials/images/toggle-on-upload.gif" width="800px;" style="padding:5px 5px 20px 5px;">

## 眨眼程序

1\. 从事件类积木拖取一个 <span style="background-color:#FFBF00;color:white;padding:4px; font-size: 12px;border-radius:3px;;border-radius:3px;">当光环板启动时</span> 积木，再从控制类积木拖取一个 <span style="background-color:#FFAB19;color:white;padding:4px; font-size: 12px;border-radius:3px;">重复执行</span> 积木。

<img src="image/cat-1.gif" style="width:800px;padding:5px 5px 12px 0px;">

2\. 从灯光类积木拖取一个 <span style="background-color:#8521D3;color:white;padding:4px; font-size: 12px;border-radius:3px;">全部LED显示（）色</span> 积木，再从控制类积木拖取一个 <span style="background-color:#FFAB19;color:white;padding:4px; font-size: 12px;border-radius:3px;">等待（）秒</span> 积木，并添加运算类积木 <span style="background-color:#59C059;color:white;padding:4px; font-size: 12px;border-radius:3px;">在（）和（）之间取随机数</span>，输入2和6，让LED显示时长随机，最后再加上一个灯光类积木 <span style="background-color:#8521D3;color:white;padding:4px; font-size: 12px;border-radius:3px;">熄灭所有灯光</span> 和控制类积木 <span style="background-color:#FFAB19;color:white;padding:4px; font-size: 12px;border-radius:3px;">等待（）秒</span>，输入0.2，实现眨眼的效果。

<img src="image/cat-2.gif" style="width:800px;padding:5px 5px 12px 0px;">

## 摇尾巴程序

3\. 从事件类积木拖取一个 <span style="background-color:#FFBF00;color:white;padding:4px; font-size: 12px;border-radius:3px;">当光环板启动时</span> 积木，再从控制类积木拖取一个 <span style="background-color:#FFAB19;color:white;padding:4px; font-size: 12px;border-radius:3px;">重复执行</span> 积木。

<img src="image/cat-3.gif" style="width:800px;padding:5px 5px 12px 0px;">

4\. 从引脚类积木拖取一个 <span style="background-color:#AFCF1C;color:white;padding:3px;font-size: 12px">引脚（）舵机转至（）度°</span> 积木，输入0度，再从控制类积木拖取一个 <span style="background-color:#FFAB19;color:white;padding:4px; font-size: 12px;border-radius:3px;">等待（）秒</span> 积木，保持默认1秒。

<img src="image/cat-4.gif" style="width:800px;padding:5px 5px 12px 0px;">

5\. 从引脚类积木拖取一个 <span style="background-color:#AFCF1C;color:white;padding:3px;font-size: 12px">引脚（）舵机转至（）度°</span> 积木，保留默认90度，再从控制类积木拖取一个 <span style="background-color:#FFAB19;color:white;padding:4px; font-size: 12px;border-radius:3px;">等待（）秒</span> 积木，保持默认1秒。

<img src="image/cat-5.gif" style="width:800px;padding:5px 5px 12px 0px;">

6\. 点击“上传到设备”将程序上传到光环板。


7\. 看看小猫，边眨眼边摇尾巴啦！

<img src="image/cat.gif" style="width:600px;padding:5px 5px 12px 0px;">

<p><span style="background-color:#66CDAA;line-height:30px;padding:12px;font-size:16px;border-radius:3px;"><a href="code/眨眼小猫.mblock" download style="color:white;font-weight:bold;">下载代码</a></span></p>

---