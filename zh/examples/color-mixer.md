# 使用触摸传感器组合颜色

使用RGB值控制灯环，通过触摸光环板的0、1、2号引脚，改变光环板上灯的颜色。

<img src="image/color-mixer.png" width="300px;" style="padding:5px 5px 20px 5px;">

## 认识光环板的触摸传感器

光环板有4个触摸传感器，光环板的触摸传感器可以通过电容的变化来检测是否被触摸。

<img src="image/touch-light.png" width="400px;" style="padding:5px 5px 20px 5px;">

## 启用上传模式

点击启用上传模式。

<img src="../tutorials/images/toggle-on-upload.gif" width="800px;" style="padding:5px 5px 20px 5px;">

## 编写程序

1\. 拖取一个事件类积木 <span style="background-color:#FFBF00;color:white;padding:4px; font-size: 12px;border-radius:3px;">当光环板启动时</span> 到脚本区，添加控制类积木 <span style="background-color:#FFAB19;color:white;padding:4px; font-size: 12px;border-radius:3px;">重复执行</span>。

<img src="image/color-mixer-1.gif" width="800px;" style="padding:5px 5px 20px 5px;">

2\. 添加一个灯光类积木 <span style="background-color:#8521D3;color:white;padding:4px; font-size: 12px;border-radius:3px;">所有LED显示颜色 R（）G（）B（）</span>，还有三个感知类积木 <span style="background-color:#F79226;color:white;padding:4px; font-size: 12px;border-radius:3px;">触摸传感器（）的触摸值</span>，分别选择0、1、2

<img src="image/color-mixer-2.gif" width="800px;" style="padding:5px 5px 20px 5px;">

3\. 上传程序，然后分别触摸传感器0、1、2看看灯环颜色变化吧！

<p><span style="background-color:#66CDAA;line-height:30px;padding:12px;font-size:16px;border-radius:3px;"><a href="code/组合颜色.mblock" download style="color:white;font-weight:bold;">下载代码</a></span></p>

---