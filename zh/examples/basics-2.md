# 使用麦克风检测音量

使用光环板制作一个音量检测计，通过光环板的麦克风检测音量大小，并通过可视化的形式呈现出来，音量越大，LED灯环亮起的灯就越多。

1\. 从事件类积木拖取一个 <span style="background-color:#FFBF00;color:white;padding:4px; font-size: 12px;border-radius:3px;">当按钮被按下时</span> 积木。

<img src="image/volume-detector-1.gif" width="800px;" style="padding:5px 5px 20px 5px;">

2\. 从控制类积木拖取一个 <span style="background-color:#FFAB19;color:white;padding:4px; font-size: 12px;border-radius:3px;">重复执行</span> 积木。

<img src="image/volume-detector-2.gif" width="800px;" style="padding:5px 5px 20px 5px;">

3\. 从灯光类积木拖取一个 <span style="background-color:#8521D3;color:white;padding:4px; font-size: 12px;border-radius:3px;">显示LED环形图（）%</span> 积木，再添加一个传感器类积木 <span style="background-color:#F79226;color:white;padding:4px; font-size: 12px;border-radius:3px;">麦克风 响度</span>。

<img src="image/volume-detector-3.gif" width="800px;" style="padding:5px 5px 20px 5px;">

4\. 试着拍下桌子，看光环板LED环形图的变化吧！

<img src="image/volume.gif" width="800px;" style="padding:5px 5px 20px 5px;">

<p><span style="background-color:#66CDAA;line-height:30px;padding:12px;font-size:16px;border-radius:3px;"><a href="code/检测音量.mblock" download style="color:white;font-weight:bold;">下载代码</a></span></p>

---