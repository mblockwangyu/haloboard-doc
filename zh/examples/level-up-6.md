# 深度学习与人脸识别

使用慧编程的机器学习功能可以实现人脸识别，当识别到“女士”，广播消息“笑”并等待，光环板接收到广播消息，露出笑脸，否则，广播消息“生气”并等待，光环板亮红灯。此功能可应用于智能家居系统，当识别到主人回家时，大门自动打开，当识别到陌生人时，开启警铃。

<img src="image/deep-learning.png" width="360px;" style="padding:5px 5px 20px 5px;">


## 训练模型

1\. 选择“角色”，点击积木区下方的“＋”，添加扩展“机器学习”。

<img src="image/deep-learning-1.gif" style="width:800px;padding:5px 5px 12px 0px;">

2\. 选中机器学习积木，点击“训练模型”，在训练模型界面点击“新建模型”，本次案例需要识别3个人，所以模型数量选择3。

<img src="image/deep-learning-2.gif" style="width:800px;padding:5px 5px 12px 0px;">

3\. 将第一个模型命名为“女士”。打印一张女士图片，将电脑摄像头对准女士图片，点击“学习”按钮不松开，电脑会一直采集女士图片的样本，可以调节女士图片角度，进行多角度采集，采集样本数量越多，识别结果准确度越高。

<img src="image/deep-learning-3.gif" style="width:800px;padding:5px 5px 12px 0px;">

4\. 同样，可以为第二个模型命名为“男士”，第三个模型命名为“男学生”，分别对这两个模型采集样本。

<img src="image/deep-learning-4.gif" style="width:800px;padding:5px 5px 12px 0px;">

5\. 样本采集完毕，点击“使用模型”。

<img src="image/deep-learning-5.gif" style="width:800px;padding:5px 5px 12px 0px;">

## 添加事件和控制

6\. 添加一个事件类积木 <span style="background-color:#FFBF00;color:white;padding:3px; font-size: 12px">当绿色旗帜被点击</span> 和控制类积木 <span style="background-color:#FFAB19;color:white;padding:3px; font-size: 12px">如果（）那么（）</span>，然后点击机器学习积木，把积木 <span style="background-color:#0FBD8C;color:white;padding:3px; font-size: 12px">识别结果为（）</span> 拖入控制类积木，保持默认“女士”。

<img src="image/deep-learning-6.gif" style="width:800px;padding:5px 5px 12px 0px;">


## 新建广播消息

7\. 选择事件类积木，新建两个广播消息，分别命名为“笑”、“生气”。

<img src="image/deep-learning-7.gif" style="width:800px;padding:5px 5px 12px 0px;">

8\. 添加事件类积木 <span style="background-color:#FFBF00;color:white;padding:3px; font-size: 12px">广播（）并等待</span>，选择“笑”，该广播积木意味着只有当光环板接收到消息并且执行完程序后，才会广播下一条消息。

<img src="image/deep-learning-8.gif" style="width:800px;padding:5px 5px 12px 0px;">

9\. 如果识别结果为“男士”或者“男学生”，则 <span style="background-color:#FFBF00;color:white;padding:3px; font-size: 12px">广播（生气）并等待</span>。为了让电脑可以一直保持识别状态，我们需要为程序添加一个控制 <span style="background-color:#FFAB19;color:white;padding:3px; font-size: 12px">重复执行</span>。

<img src="image/deep-learning-9.gif" style="width:800px;padding:5px 5px 12px 0px;">

## 对光环板编程

10\. 点击“设备”，选择“光环板”。添加事件类积木 <span style="background-color:#FFBF00;color:white;padding:3px; font-size: 12px">当接收到（笑）</span> 和两个灯光类积木 <span style="background-color:#8521D3;color:white;padding:3px; font-size: 12px">第（）颗LED显示颜色R（）G（）B（）</span>， 使光环板第2、10颗LED灯亮起，作为笑脸的“眼睛”。

<img src="image/deep-learning-10.gif" style="width:800px;padding:5px 5px 12px 0px;">


11\. 继续添加5个灯光类积木 <span style="background-color:#8521D3;color:white;padding:3px; font-size: 12px">第（）颗LED显示颜色R（）G（）B（）</span>，让第4、5、6、7、8颗LED灯作为笑脸的“嘴巴”，并调整RGB值，使其呈现浅粉色。

<img src="image/deep-learning-11.gif" style="width:800px;padding:5px 5px 12px 0px;">

12\. 添加控制类积木 <span style="background-color:#FFAB19;color:white;padding:3px; font-size: 12px">等待（0.1）秒</span> 和灯光类积木 <span style="background-color:#8521D3;color:white;padding:3px; font-size: 12px">熄灭所有灯光</span>。

<img src="image/deep-learning-12.gif" style="width:800px;padding:5px 5px 12px 0px;">


13\. 添加事件类积木 <span style="background-color:#FFBF00;color:white;padding:3px; font-size: 12px">当接收到（生气）</span> 和灯光类积木 <span style="background-color:#8521D3;color:white;padding:3px; font-size: 12px">全部LED显示（）色</span>，调整颜色为红色。0.1秒后，熄灭灯光。

<img src="image/deep-learning-13.gif" style="width:800px;padding:5px 5px 12px 0px;">

## 效果展示

<img src="images/deep-learning-16.gif" style="width:800px;padding:5px 5px 12px 0px;">

<p><span style="background-color:#66CDAA;line-height:30px;padding:12px;font-size:16px;border-radius:3px;"><a href="code/深度学习.mblock" download style="color:white;font-weight:bold;">下载代码</a></span></p>

---