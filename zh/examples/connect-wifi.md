# 连接Wi-Fi

光环板连接Wi-Fi。

<img src="image/wifi.png" width="300px;" style="padding:5px 5px 20px 5px;">

## 启用上传模式

点击启用上传模式。

<img src="../tutorials/images/toggle-on-upload.gif" width="800px;" style="padding:5px 5px 20px 5px;">

## 连接网络

1\. 添加事件类积木 <span style="background-color:#FFBF00;color:white;padding:4px; font-size: 12px;border-radius:3px;">当光环板启动时</span>，Wi-Fi类积木 <span style="background-color:#76CE14;color:white;padding:4px; font-size: 12px;border-radius:3px;">开始连接无线网络（）密码（）</span>，输入附近Wi-Fi的名称和密码。

<img src="image/wifi-1.gif" width="800px;" style="padding:5px 5px 20px 5px;">

2\. 网络连接成功后，我们需要一个信号来告诉我们网络连接成功。添加控制类积木 <span style="background-color:#FFAB19;color:white;padding:4px; font-size: 12px;border-radius:3px;">等待（）</span>，拖入Wi-Fi类积木 <span style="background-color:#76CE14;color:white;padding:4px; font-size: 12px;border-radius:3px;">网络已连接？</span>，添加灯光类积木 <span style="background-color:#8521D3;color:white;padding:4px; font-size: 12px;border-radius:3px;">显示（）</span>，让灯环亮起指定图案。

<img src="image/wifi-2.gif" width="800px;" style="padding:5px 5px 20px 5px;">

<p><span style="background-color:#66CDAA;line-height:30px;padding:12px;font-size:16px;border-radius:3px;"><a href="code/连接Wi-Fi.mblock" download style="color:white;font-weight:bold;">下载代码</a></span></p>

---