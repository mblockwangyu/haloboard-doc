# 设计灯环灯效

**效果展示**

<img src="image/led-ring.png" width="400px;" style="padding:5px 5px 20px 5px;">

**光环板的LED灯编号**

LED灯编号和时钟相同，箭头对应的灯编号为12。

<img src="image/halo-led.png" width="360px;" style="padding:5px 5px 20px 5px;">

## 编程“笑脸”的“眼睛”

1\. 拖取2个灯光类积木 <span style="background-color:#8521D3;color:white;padding:4px; font-size: 12px;border-radius:3px;">第（）颗LED显示颜色R（）G（）B（）</span> 到脚本区。分别修改为第2颗、第10颗。

<img src="image/led-ring-1.gif" width="800px;" style="padding:5px 5px 20px 5px;">

## 编程“笑脸”的“嘴巴”

2\. 再添加5个灯光类积木 <span style="background-color:#8521D3;color:white;padding:4px; font-size: 12px;border-radius:3px;">第（）颗LED显示颜色R（）G（）B（）</span>，分别修改为第4颗、第5颗、第6颗、第7颗和第8颗。

<img src="image/led-ring-2.gif" width="800px;" style="padding:5px 5px 20px 5px;">

3\. 添加事件类积木 <span style="background-color:#FFBF00;color:white;padding:4px; font-size: 12px;border-radius:3px;">当按钮被按下时</span>。

<img src="image/led-ring-3.gif" width="800px;" style="padding:5px 5px 20px 5px;">

4\. 按下光环板的按钮，看光环板呈现的“笑脸”。

<p><span style="background-color:#66CDAA;line-height:30px;padding:12px;font-size:16px;border-radius:3px;"><a href="code/设计灯环灯效.mblock" download style="color:white;font-weight:bold;">下载代码</a></span></p>

---