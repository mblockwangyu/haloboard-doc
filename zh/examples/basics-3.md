# 使用按钮触发流星灯效

光环板可编程按钮有丰富的应用，你可以用按下按钮来启动一个程序。光环板自带4个触摸传感器，可以通过电容的变化来检测是否被触摸。

1\. 从事件类积木拖取一个 <span style="background-color:#FFBF00;color:white;padding:4px; font-size: 12px;border-radius:3px;">当按钮被按下时</span> 积木，和一个灯光类积木 <span style="background-color:#8521D3;color:white;padding:4px; font-size: 12px;border-radius:3px;">播放LED动画（）直到结束</span>，并选择“流星”。这样，当光环板按钮被按下时，LED动画“流星”就会播放。

<img src="image/button-meteor-1.gif" width="800px;" style="padding:5px 5px 20px 5px;">

2\. 添加一个控制类积木 <span style="background-color:#FFAB19;color:white;padding:4px; font-size: 12px;border-radius:3px;">重复执行直到（）</span> 和一个传感器类积木 <span style="background-color:#F79226;color:white;padding:4px; font-size: 12px;border-radius:3px;">触摸传感器（）被触摸？</span>，保留默认的触摸传感器0。

<img src="image/button-meteor-2.gif" width="800px;" style="padding:5px 5px 20px 5px;">

3\. 试试按下光环板的按钮，再触摸一下触摸传感器0。

<p><span style="background-color:#66CDAA;line-height:30px;padding:12px;font-size:16px;border-radius:3px;"><a href="code/按钮触发流星灯效.mblock" download style="color:white;font-weight:bold;">下载代码</a></span></p>

---