# 基础案例

本章介绍了光环板的一些基础案例。

<table  cellpadding="15px;" cellspacing="12px;" style="font-size: 12px;text-align:center;">
<tr>
<td width="25%;"><a href="basics-1.html" target="_blank"><img src="image/led-ring.png" width="150px;"></a><br>
<p>设计灯环灯效</p></td>

<td width="25%;"><a href="basics-2.html" target="_blank"><img src="image/volume-detector.png" width="150px;"></a><br>
<p>使用麦克风检测音量</p></td>

<td width="25%;"><a href="basics-3.html" target="_blank"><img src="image/press-button.png" width="150px;"></a><br>
<p>使用按钮触发流星灯效</p></td>

<td width="25%;"><a href="rainbow-button.html" target="_blank"><img src="image/rainbow-button.png" width="150px;"></a><br>
<p>彩虹按钮</p></td>
</tr>

<tr>
<td><a href="color-mixer.html" target="_blank"><img src="image/color-mixer.png" width="150px;"></a><br>
<p>使用触摸传感器组合颜色</p></td>
<td><a href="basics-5.html" target="_blank"><img src="image/compare-strength.png" width="150px;"></a><br>
<p>比一比谁的力气大</p></td>
<td><a href="energy-ring.html" target="_blank"><img src="image/energy-ring.png" width="150px;"></a><br>
<p>能量手环</p></td>
<td><a href="smile.html" target="_blank"><img src="image/smile.png" width="150px;"></a><br>
<p>抬腕笑脸</p></td>
</tr>

<tr>
<td><a href="connect-wifi.html" target="_blank"><img src="image/wifi.png" width="150px;"></a><br>
<p>连接Wi-Fi</p></td>
<td><a href="lan.html" target="_blank"><img src="image/lan.png" width="150px;"></a><br>
<p>使用Wi-Fi进行局域网通信</p></td>
</tr>

</table>

<br>

<p><span style="background-color:#66CDAA;line-height:30px;padding:12px;font-size:16px;border-radius:3px;"><a href="code/code.rar" download style="color:white;font-weight:bold;">下载代码</a></span></p>

---