# 抬腕笑脸

当光环板箭头向上放置时，灯环会显示笑脸，案例使用了光环板的运动传感器。

<img src="image/smile.png" width="300px;" style="padding:5px 5px 20px 5px;">

## 启用上传模式

点击启用上传模式。

<img src="../tutorials/images/toggle-on-upload.gif" width="800px;" style="padding:5px 5px 20px 5px;">

## 编写程序

1\. 拖取一个事件类积木 <span style="background-color:#FFBF00;color:white;padding:4px; font-size: 12px;border-radius:3px;">当光环板启动时</span> 到脚本区，添加控制类积木 <span style="background-color:#FFAB19;color:white;padding:4px; font-size: 12px;border-radius:3px;">重复执行</span>。

<img src="image/smile-1.gif" width="800px;" style="padding:5px 5px 20px 5px;">

2\. 添加一个控制类积木 <span style="background-color:#FFAB19;color:white;padding:4px; font-size: 12px;border-radius:3px;">如果（）那么（）否则</span>，一个感知类积木 <span style="background-color:#F79226;color:white;padding:4px; font-size: 12px;border-radius:3px;">光环板（箭头向上）？</span>，选择“箭头向上”

<img src="image/smile-2.gif" width="800px;" style="padding:5px 5px 20px 5px;">

3\. 添加灯光类积木 <span style="background-color:#8521D3;color:white;padding:4px; font-size: 12px;border-radius:3px;">显示（）</span>，绘制一个笑脸

<img src="image/smile-3.gif" width="800px;" style="padding:5px 5px 20px 5px;">

3\. 继续添加灯光类积木 <span style="background-color:#8521D3;color:white;padding:4px; font-size: 12px;border-radius:3px;">显示（）</span>，设置灯环不亮

<img src="image/smile-4.gif" width="800px;" style="padding:5px 5px 20px 5px;">

4\. 上传程序，然后将光环板戴在手腕上看看吧！

<p><span style="background-color:#66CDAA;line-height:30px;padding:12px;font-size:16px;border-radius:3px;"><a href="code/抬腕笑脸.mblock" download style="color:white;font-weight:bold;">下载代码</a></span></p>

---