# 远程开关

利用光环板的Wi-Fi功能，可以实现账号云广播，云广播通过互联网将消息或数值传递给舞台角色，从而实现舞台角色与硬件的互动。本程序中，实现了通过点击舞台中的灯泡，远程控制光环板灯环亮灭的功能。

<img src="image/remote-control.png" width="360px;" style="padding:5px 5px 20px 5px;">


## 启用上传模式

点击启用上传模式。

<img src="../tutorials/images/toggle-on-upload.gif" width="800px;" style="padding:5px 5px 20px 5px;">

## 舞台背景与角色

1\. 删除默认角色Panda

<img src="image/remote-control-1.gif" width="800px;" style="padding:5px 5px 20px 5px;">

2\. 选择“背景”，点击“造型”，添加背景“Office3”

<img src="image/remote-control-2.gif" width="800px;" style="padding:5px 5px 20px 5px;">

3\. 选择“角色”，添加“bulb”，并设置造型为“bulb2”

<img src="image/remote-control-3.gif" width="800px;" style="padding:5px 5px 20px 5px;">

4\. 调整灯泡大小，设置为“150”，并移动到桌子中间

<img src="image/remote-control-4.gif" width="800px;" style="padding:5px 5px 20px 5px;">

## 为bulb编程

5\. 在积木区点击“+ 添加扩展”，添加“帐号云广播”

<img src="image/remote-control-5.gif" width="800px;" style="padding:5px 5px 20px 5px;">

6\. 选择变量类积木，点击“建立一个变量”，命名为“frequency”

<img src="image/remote-control-6.gif" width="800px;" style="padding:5px 5px 20px 5px;">

7\. 拖取一个事件类积木 <span style="background-color:#FFBF00;color:white;padding:4px; font-size: 12px;border-radius:3px;">当绿色旗帜被点击</span> 到脚本区，添加一个
变量类积木 <span style="background-color:#FF8C1A;color:white;padding:4px; font-size: 12px;border-radius:3px;">将（frequency）设为（0）</span>

<img src="image/remote-control-7.gif" width="800px;" style="padding:5px 5px 20px 5px;">

8\. 添加事件类积木 <span style="background-color:#FFBF00;color:white;padding:4px; font-size: 12px;border-radius:3px;">当角色被点击</span>，变量类积木 <span style="background-color:#FF8C1A;color:white;padding:4px; font-size: 12px;border-radius:3px;">将（frequency）增加（1）</span>，还有帐号云广播积木 <span style="background-color:#4EAA83;color:white;padding:4px; font-size: 12px;border-radius:3px;">发送帐号云广播（）并附加值（）</span>。将广播命名为“switch”，附加值为变量类积木 <span style="background-color:#FF8C1A;color:white;padding:4px; font-size: 12px;border-radius:3px;">frequency</span>

<img src="image/remote-control-8.gif" width="800px;" style="padding:5px 5px 20px 5px;">

9\. 添加一个控制类积木 <span style="background-color:#FFAB19;color:white;padding:4px; font-size: 12px;border-radius:3px;">如果（）那么（）否则</span>，运算类积木 <span style="background-color:#59C059;color:white;padding:4px; font-size: 12px;border-radius:3px;">（）=（）</span> 和 <span style="background-color:#59C059;color:white;padding:4px; font-size: 12px;border-radius:3px;">（）除以（）的余数</span>，再放入变量类积木 <span style="background-color:#FF8C1A;color:white;padding:4px; font-size: 12px;border-radius:3px;">frequency</span>，再输入“2”和“1”

<img src="image/remote-control-9.gif" width="800px;" style="padding:5px 5px 20px 5px;">

10\. 添加两个外观类积木 <span style="background-color:#9966FF;color:white;padding:4px; font-size: 12px;border-radius:3px;">换成（）造型</span>，分别选择“bulb-1”和“bulb-2”

<img src="image/remote-control-10.gif" width="800px;" style="padding:5px 5px 20px 5px;">

## 光环板连接Wi-Fi

11\. 添加事件类积木 <span style="background-color:#FFBF00;color:white;padding:4px; font-size: 12px;border-radius:3px;">当光环板启动时</span>。使用帐号云广播需要连接网络。添加Wi-Fi类积木 <span style="background-color:#76CE14;color:white;padding:4px; font-size: 12px;border-radius:3px;">开始连接无线网络（）密码（）</span>，输入附近Wi-Fi的名称和密码。

<img src="image/remote-control-11.gif" width="800px;" style="padding:5px 5px 20px 5px;">

12\. 网络连接成功后，我们需要一个信号来告诉我们网络连接成功。添加控制类积木 <span style="background-color:#FFAB19;color:white;padding:4px; font-size: 12px;border-radius:3px;">等待（）</span>，拖入Wi-Fi类积木 <span style="background-color:#76CE14;color:white;padding:4px; font-size: 12px;border-radius:3px;">网络已连接？</span>，添加灯光类积木 <span style="background-color:#8521D3;color:white;padding:4px; font-size: 12px;border-radius:3px;">全部LED显示（）色</span>，选择绿色。当光环板亮起绿色，表明网络连接成功。再添加控制类积木 <span style="background-color:#FFAB19;color:white;padding43px; font-size: 12px;border-radius:3px;">等待（1）秒</span> 和灯光类积木 <span style="background-color:#8521D3;color:white;padding:4px; font-size: 12px;border-radius:3px;">熄灭所有灯光</span>

<img src="image/remote-control-12.gif" width="800px;" style="padding:5px 5px 20px 5px;">

## 光环板接收帐号云广播

13\. 添加Wi-Fi类积木 <span style="background-color:#76CE14;color:white;padding:4px; font-size: 12px;border-radius:3px;">当接收帐号云广播（switch）</span>，控制类积木 <span style="background-color:#FFAB19;color:white;padding:4px; font-size: 12px;border-radius:3px;">如果（）那么（）否则</span>，运算类积木 <span style="background-color:#59C059;color:white;padding:4px; font-size: 12px;border-radius:3px;">（）=（）</span> 和 <span style="background-color:#59C059;color:white;padding:4px; font-size: 12px;border-radius:3px;">（）除以（）的余数</span>，放入Wi-Fi类积木 <span style="background-color:#76CE14;color:white;padding:4px; font-size: 12px;border-radius:3px;">帐号云广播（switch）收到的值</span>，输入“2”和“1”

<img src="image/remote-control-13.gif" width="800px;" style="padding:5px 5px 20px 5px;">

14\. 添加灯光类积木 <span style="background-color:#8521D3;color:white;padding:4px; font-size: 12px;border-radius:3px;">所有LED显示颜色R（）G（）B（）</span> 和 <span style="background-color:#8521D3;color:white;padding:4px; font-size: 12px;border-radius:3px;">熄灭所有灯光</span>

<img src="image/remote-control-14.gif" width="800px;" style="padding:5px 5px 20px 5px;">

15\. 上传程序。

<img src="image/remote-control-15.gif" width="800px;" style="padding:5px 5px 20px 5px;">

<p><span style="background-color:#66CDAA;line-height:30px;padding:12px;font-size:16px;border-radius:3px;"><a href="code/远程开关.mblock" download style="color:white;font-weight:bold;">下载代码</a></span></p>

---