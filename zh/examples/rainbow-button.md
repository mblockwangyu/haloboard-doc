# 彩虹按钮

按下按钮，灯环显示如下效果，行如彩虹。

<img src="image/rainbow-button.png" width="400px;" style="padding:5px 5px 20px 5px;">

1\. 添加事件类积木 <span style="background-color:#FFBF00;color:white;padding:4px; font-size: 12px;border-radius:3px;">当按钮被按下时</span>。

<img src="image/rainbow-button-1.gif" width="800px;" style="padding:5px 5px 20px 5px;">

2\. 添加灯光类积木 <span style="background-color:#8521D3;color:white;padding:4px; font-size: 12px;border-radius:3px;">显示（）</span>，让灯环亮起彩虹图案。

<img src="image/rainbow-button-2.gif" width="800px;" style="padding:5px 5px 20px 5px;">

3\. 按下光环板的按钮看看吧！

<p><span style="background-color:#66CDAA;line-height:30px;padding:12px;font-size:16px;border-radius:3px;"><a href="code/彩虹按钮.mblock" download style="color:white;font-weight:bold;">下载代码</a></span></p>

---