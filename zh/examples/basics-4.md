# 触摸控制光环板灯效

**效果展示**

程序编写完毕，上传至设备后，分别触摸触摸传感器0、触摸传感器3，观看LED变化。

<img src="images/touch-light-0.gif" width="600px;" style="padding:5px 5px 20px 5px;">

**认识光环板的触摸传感器**

光环板有4个触摸传感器，光环板的触摸传感器可以通过电容的变化来检测是否被触摸。

<img src="images/touch-light.png" width="400px;" style="padding:5px 5px 20px 5px;">

## 添加LED动画

1\. 添加灯光类积木 <span style="background-color:#8521D3;color:white;padding:3px; font-size: 12px">播放LED动画（）直到结束</span>，动画效果使用默认“彩虹”。

<img src="images/touch-light-1.gif" width="800px;" style="padding:5px 5px 20px 5px;">

## 添加事件和控制

2\. 添加事件类积木 <span style="background-color:#FFBF00;color:white;padding:3px; font-size: 12px">当触摸传感器（）被触摸时</span>，使用默认传感器0。添加控制类积木 <span style="background-color:#FFAB19;color:white;padding:3px; font-size: 12px">重复执行</span>，触摸传感器0被触摸时，LED一直重复播放“彩虹”灯效。

<img src="images/touch-light-2.gif" width="800px;" style="padding:5px 5px 20px 5px;">

3\. 同理可对触摸传感器3添加事件和控制，复制脚本，修改为传感器3，LED动画改为“萤火虫”。

<img src="images/touch-light-3.gif" width="800px;" style="padding:5px 5px 20px 5px;">

**多线程中断**

4\. 添加控制类积木 <span style="background-color:#FFAB19;color:white;padding:3px; font-size: 12px">停止（）</span>，选择“停止该角色的其它脚本”，使用该积木可以实现在不同线程之间切换的功能，并且有中断循环的作用。

<img src="images/touch-light-4.gif" width="800px;" style="padding:5px 5px 20px 5px;">

5\. 上传程序，试试触摸触摸传感器0和触摸传感器3，看光环板灯效变化。

<img src="images/touch-light-5.gif" width="800px;" style="padding:5px 5px 20px 5px;">

<a href="images/触摸控制光环板灯效.mblock" download>点击下载代码</a>

---