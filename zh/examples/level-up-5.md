# 利用全局变量实现光环板与舞台的互动

**效果展示**

程序编写完毕，按下光环板按钮，使光环板绕Y轴旋转，观察舞台角色“太阳”旋转角度是否是随着光环板角度变而变。

<img src="images/global-variable-0.gif" width="600px;" style="padding:5px 5px 20px 5px;">

**认识运动传感器**

以光环板所在平面为基础建立X轴、Y轴、Z轴，光环板箭头所指方向即为Y轴正向。

<img src="images/halo-axes.png" width="500px;" style="padding:5px 5px 20px 5px;">

## 新建变量

1\. 建立一个新变量，设置变量名称为“y”。

<img src="image/global-variable-1.gif" width="800px;" style="padding:5px 5px 20px 5px;">

2\. 添加变量积木 <span style="background-color:#FF8C1A;color:white;padding:4px; font-size: 12px;border-radius:3px;">将y设为（）</span>，并添加传感器类积木 <span style="background-color:#F79226;color:white;padding:4px; font-size: 12px;border-radius:3px;">绕（Y）轴转过的角度(°)</span>。

<img src="image/global-variable-2.gif" width="800px;" style="padding:5px 5px 20px 5px;">

## 添加事件和控制

3\. 添加事件类积木 <span style="background-color:#FFBF00;color:white;padding:4px; font-size: 12px;border-radius:3px;">当按钮被按下时</span> 和控制类积木 <span style="background-color:#FFAB19;color:white;padding:4px; font-size: 12px;border-radius:3px;">重复执行</span>。

<img src="image/global-variable-3.gif" width="800px;" style="padding:5px 5px 20px 5px;">

## 添加舞台角色

4\. 点击“角色”，添加舞台角色。点击“道具”，选择太阳作为舞台角色。

<img src="image/global-variable-4.gif" width="800px;" style="padding:5px 5px 20px 5px;">

5\. 删除原有舞台角色小熊猫。

<img src="image/global-variable-5.gif" width="800px;" style="padding:5px 5px 20px 5px;">

## 编程舞台角色

6\. 添加运动积木 <span style="background-color:#4C97FF;color:white;padding:4px; font-size: 12px;border-radius:3px;">右转（）度</span>，并添加变量类积木 <span style="background-color:#FF8C1A;color:white;padding:4px; font-size: 12px;border-radius:3px;">y</span>。太阳的旋转角度随着光环板绕Y轴角度变化而变化。

<img src="image/global-variable-6.gif" width="800px;" style="padding:5px 5px 20px 5px;">

7\. 添加事件类积木 <span style="background-color:#FFBF00;color:white;padding:4px; font-size: 12px;border-radius:3px;">当绿色旗帜被点击</span> 和控制类积木 <span style="background-color:#FFAB19;color:white;padding:4px; font-size: 12px;border-radius:3px;">重复执行</span>。

<img src="image/global-variable-7.gif" width="800px;" style="padding:5px 5px 20px 5px;">

8\. 旋转一下光环板试试看吧！

<p><span style="background-color:#66CDAA;line-height:30px;padding:12px;font-size:16px;border-radius:3px;"><a href="code/全局变量实现光环板与舞台互动.mblock" download style="color:white;font-weight:bold;">下载代码</a></span></p>

---