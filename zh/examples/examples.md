# 光环板案例

希望这些项目能够给大家带来启发和灵感，帮助大家创作自己的项目。


### [基础案例](basics.md)

<table  cellpadding="15px;" cellspacing="12px;" style="font-size: 12px;text-align:center;">
<tr>
<td width="25%;"><a href="basics-1.html" target="_blank"><img src="image/led-ring.png" width="150px;"></a><br>
<p>设计灯环灯效</p></td>

<td width="25%;"><a href="basics-2.html" target="_blank"><img src="image/volume-detector.png" width="150px;"></a><br>
<p>使用麦克风检测音量</p></td>

<td width="25%;"><a href="basics-3.html" target="_blank"><img src="image/press-button.png" width="150px;"></a><br>
<p>使用按钮触发流星灯效</p></td>

<td width="25%;"><a href="rainbow-button.html" target="_blank"><img src="image/rainbow-button.png" width="150px;"></a><br>
<p>彩虹按钮</p></td>
</tr>

<tr>
<td><a href="color-mixer.html" target="_blank"><img src="image/color-mixer.png" width="150px;"></a><br>
<p>使用触摸传感器组合颜色</p></td>
<td><a href="basics-5.html" target="_blank"><img src="image/compare-strength.png" width="150px;"></a><br>
<p>比一比谁的力气大</p></td>
<td><a href="energy-ring.html" target="_blank"><img src="image/energy-ring.png" width="150px;"></a><br>
<p>能量手环</p></td>
<td><a href="smile.html" target="_blank"><img src="image/smile.png" width="150px;"></a><br>
<p>抬腕笑脸</p></td>
</tr>

<tr>
<td><a href="connect-wifi.html" target="_blank"><img src="image/wifi.png" width="150px;"></a><br>
<p>连接Wi-Fi</p></td>
<td><a href="lan.html" target="_blank"><img src="image/lan.png" width="150px;"></a><br>
<p>使用Wi-Fi进行局域网通信</p></td>
</tr>

</table>

<br>

### [进阶案例](level-up.md)

<table  cellpadding="15px;" cellspacing="12px;" style="font-size: 12px;text-align:center;">

<tr>
<td colspan="2"><p><b>虚实结合</b></p></td>
<td colspan="2"><p><b>物联网</b></p></td>
</tr>

<tr>
<td width="25%;"><a href="level-up-1.html" target="_blank"><img src="image/steering-wheel.png" width="150px;"></a><br>
<p>光环板做舞台小车的方向盘</p></td>

<td width="25%;"><a href="level-up-5.html" target="_blank"><img src="image/global-variable.png" width="150px;"></a><br>
<p>全局变量实现光环板与舞台互动</p></td>

<td width="25%;"><a href="remote-control.html" target="_blank"><img src="image/remote-control.png" width="150px;"></a><br>
<p>远程开关</p></td>

<td width="25%;"><a href="level-up-2.html" target="_blank"><img src="image/control-deck.png" width="150px;"></a><br>
<p>远程控制台</p></td>
</tr>
</table>

<br>

<table  cellpadding="15px;" cellspacing="12px;" style="font-size: 12px;text-align:center;width:100%;">
<tr>
<td colspan="3"><p><b>人工智能</b></p></td>
</tr>

<tr>
<td width=“33%;”><a href="level-up-3.html" target="_blank"><img src="image/voice-control.png" width="150px;"></a><br>
<p>语音控制灯环颜色</p></td>
<td width=“33%;”><a href="emotion-detector.html" target="_blank"><img src="image/emotion-detector.png" width="150px;"></a><br>
<p>情绪识别</p></td>
<td width=“33%;”><a href="level-up-6.html" target="_blank"><img src="image/deep-learning.png" width="150px;"></a><br>
<p>深度学习与人脸识别</p></td>
</tr>
</table>

<br>

### [工作坊](workshop.md)

<table  cellpadding="15px;" cellspacing="12px;" style="font-size:12px;text-align:center;width:100%;">
<tr>
<td width="33%;"><a href="pedometer.html" target="_blank"><img src="image/pedometer.png" width="150px;"></a><br>
<p>计步器</p></td>

<td width="33%;"><a href="workshop-1.html" target="_blank"><img src="image/cat-7.jpg" width="150px;"></a><br>
<p>摇尾巴小猫</p></td>

<td width="33%;"><a href="workshop-2.html" target="_blank"><img src="image/smarthome.png" width="150px;"></a><br>
<p>智能家居</p></td>

</tr>
</table>

<br>

<p><span style="background-color:#66CDAA;line-height:30px;padding:12px;font-size:16px;border-radius:3px;"><a href="code/code.rar" download style="color:white;font-weight:bold;">下载代码</a></span></p>

---

