# 情绪识别

当摄像头识别到人物情绪为“笑”，光环板也会亮出笑脸。

知识小贴士：虽然不同国家的语言和文化有很大差异，但是表情代表的情绪含义是高度一致的。并且表情也难以伪装出来，所以我们可以从最容易的“笑”开始吧！

<img src="image/emotion-detector.png" width="360px;" style="padding:5px 5px 20px 5px;">

## 舞台角色

1\. 删除默认角色Panda

<img src="image/emotion-detector-1.gif" width="800px;" style="padding:5px 5px 20px 5px;">

2\. 添加角色“Empty button14”

<img src="image/emotion-detector-2.gif" width="800px;" style="padding:5px 5px 20px 5px;">

3\. 点击“造型”，将按钮颜色设置成绿色，并添加文字“Start”，调整大小，位置

<img src="image/emotion-detector-3.gif" width="800px;" style="padding:5px 5px 20px 5px;">

4\. 在积木区下方，点击“+ 添加扩展”，添加认知服务

<img src="image/emotion-detector-4.gif" width="800px;" style="padding:5px 5px 20px 5px;">

5\. 拖取一个事件类积木 <span style="background-color:#FFBF00;color:white;padding:4px; font-size: 12px;border-radius:3px;">当角色被点击</span> 到脚本区，添加一个认知类积木 <span style="background-color:#FF3580;color:white;padding:4px; font-size: 12px; border-radius:3px;">（1）秒后，检测人脸情绪</span>

<img src="image/emotion-detector-5.gif" width="800px;" style="padding:5px 5px 20px 5px;">

6\. 添加一个控制类积木 <span style="background-color:#FFAB19;color:white;padding:4px; font-size: 12px;border-radius:3px;">如果（）那么（）</span>，认知类积木 <span style="background-color:#FF3580;color:white;padding:4px; font-size: 12px; border-radius:3px;">情绪为（高兴）？</span>，还有事件类积木 <span style="background-color:#FFBF00;color:white;padding:4px; font-size: 12px;border-radius:3px;">广播（）</span>，新建消息“laugh”

<img src="image/emotion-detector-6.gif" width="800px;" style="padding:5px 5px 20px 5px;">

7\. 在认知类积木 <span style="background-color:#FF3580;color:white;padding:4px; font-size: 12px; border-radius:3px;">情绪（高兴）的程度</span> 前面打勾，让值显示在舞台上

<img src="image/emotion-detector-7.gif" width="800px;" style="padding:5px 5px 20px 5px;">

## 光环板展示笑脸

8\. 添加事件类积木 <span style="background-color:#FFBF00;color:white;padding:4px; font-size: 12px;border-radius:3px;">当接收到（laugh）时</span>，灯光类积木 <span style="background-color:#8521D3;color:white;padding:4px; font-size: 12px;border-radius:3px;">显示（）</span>，绘制一个笑脸图案

<img src="image/emotion-detector-8.gif" width="800px;" style="padding:5px 5px 20px 5px;">

9\. 添加控制类积木 <span style="background-color:#FFAB19;color:white;padding:4px; font-size: 12px;border-radius:3px;">等待（）秒</span>
和灯光类积木 <span style="background-color:#8521D3;color:white;padding:4px; font-size: 12px;border-radius:3px;">熄灭所有灯光</span>，时间设置为2秒

<img src="image/emotion-detector-9.gif" width="800px;" style="padding:5px 5px 20px 5px;">

<p><span style="background-color:#66CDAA;line-height:30px;padding:12px;font-size:16px;border-radius:3px;"><a href="code/情绪识别.mblock" download style="color:white;font-weight:bold;">下载代码</a></span></p>

---