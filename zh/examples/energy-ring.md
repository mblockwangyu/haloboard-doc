# 能量手环

通过摇晃手臂为手环注入能量，光环板的灯会随着摇晃逐渐亮起,能量蓄满后光环板 LED 全亮。

<img src="image/energy-ring.png" width="300px;" style="padding:5px 5px 20px 5px;">


1\. 选择变量类积木，点击“建立一个变量”，并命名为“n”

<img src="image/energy-ring-1.gif" width="800px;" style="padding:5px 5px 20px 5px;">

2\. 拖取一个事件类积木 <span style="background-color:#FFBF00;color:white;padding:4px; font-size: 12px;border-radius:3px;">当按钮被按下时</span> 到脚本区，添加一个灯光类积木 <span style="background-color:#8521D3;color:white;padding:4px; font-size: 12px;border-radius:3px;">熄灭第（1）颗灯</span>，还有一个变量类积木 <span style="background-color:#FF8C1A;color:white;padding:4px; font-size: 12px;border-radius:3px;">将（n）设为（）</span>，输入1

<img src="image/energy-ring-2.gif" width="800px;" style="padding:5px 5px 20px 5px;">

3\. 添加一个控制类积木 <span style="background-color:#FFAB19;color:white;padding:4px; font-size: 12px;border-radius:3px;">如果（）那么（）</span>，运算类积木 <span style="background-color:#59C059;color:white;padding:4px; font-size: 12px;border-radius:3px;">（）>（）</span>，还有感知类积木 <span style="background-color:#F79226;color:white;padding:4px; font-size: 12px;border-radius:3px;">光环板的摇晃强度</span>，并输入20

<img src="image/energy-ring-3.gif" width="800px;" style="padding:5px 5px 20px 5px;">

4\. 添加灯光类积木 <span style="background-color:#8521D3;color:white;padding:4px; font-size: 12px;border-radius:3px;">第（）LED显示颜色 R（）G（）B（）</span>，变量类积木 <span style="background-color:#FF8C1A;color:white;padding:4px; font-size: 12px;border-radius:3px;">n</span>，变量类积木 <span style="background-color:#FF8C1A;color:white;padding:4px; font-size: 12px;border-radius:3px;">将（n）增加（1）</span>，还有控制类积木 <span style="background-color:#FFAB19;color:white;padding:4px; font-size: 12px;border-radius:3px;">等待（）秒</span>，输入0.2

<img src="image/energy-ring-4.gif" width="800px;" style="padding:5px 5px 20px 5px;">

5\. 添加控制类积木 <span style="background-color:#FFAB19;color:white;padding:4px; font-size: 12px;border-radius:3px;">重复执行直到（）</span>，运算类积木 <span style="background-color:#59C059;color:white;padding:4px; font-size: 12px;border-radius:3px;">（）=（）</span>，变量类积木 <span style="background-color:#FF8C1A;color:white;padding:4px; font-size: 12px;border-radius:3px;">n</span>，输入12，让光环板12颗LED全部亮起红色

<img src="image/energy-ring-5.gif" width="800px;" style="padding:5px 5px 20px 5px;">

6\. 添加灯光类积木 <span style="background-color:#8521D3;color:white;padding:4px; font-size: 12px;border-radius:3px;">全部LED显示（）色</span>，设置为蓝色，当“能量充满时”，光环板LED灯环显示蓝色

<img src="image/energy-ring-6.gif" width="800px;" style="padding:5px 5px 20px 5px;">


7\. 启用上传模式，将程序上传。然后按下光环板的按钮，并摇晃它吧！

<p><span style="background-color:#66CDAA;line-height:30px;padding:12px;font-size:16px;border-radius:3px;"><a href="code/能量手环.mblock" download style="color:white;font-weight:bold;">下载代码</a></span></p>

---