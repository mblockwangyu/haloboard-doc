# 4. 为光环板设计跑马灯

**效果展示**

<img src="images/marquee-led-0.gif" width="600px;" style="padding:5px 5px 20px 5px;">

## 添加变量

1\. 使用变量，可以为光环板设计一个跑马灯。点击变量类积木，建立一个新变量，修改新变量名称为“n”。

<img src="images/marquee-led-1.gif" width="800px;" style="padding:5px 5px 20px 5px;">

## 添加灯效

2\. 添加灯光类积木 <span style="background-color:#8521D3;color:white;padding:3px; font-size: 12px">显示旋转（）格后的（）</span>，修改灯光颜色。

<img src="images/marquee-led-3.gif" width="800px;" style="padding:5px 5px 20px 5px;">

3\. 添加变量类积木 <span style="background-color:#FF8C1A;color:white;padding:3px; font-size: 12px">将n增加（）</span>，使用默认数值1，和另一变量类积木 <span style="background-color:#FF8C1A;color:white;padding:3px; font-size: 12px">n</span>。再添加控制类积木 <span style="background-color:#FFAB19;color:white;padding:3px; font-size: 12px">等待（）秒</span>，修改数值为“0.1”。

<img src="images/marquee-led-4.gif" width="800px;" style="padding:5px 5px 20px 5px;">

## 添加事件和控制

4\. 添加控制类积木 <span style="background-color:#FFAB19;color:white;padding:3px; font-size: 12px">重复执行</span> 和事件类积木 <span style="background-color:#FFBF00;color:white;padding:3px; font-size: 12px">当光环板启动时</span>。

<img src="images/marquee-led-5.gif" width="800px;" style="padding:5px 5px 20px 5px;">

5\. 上传程序，看一下自己设计的跑马灯效果如何。

<img src="images/marquee-led-6.gif" width="800px;" style="padding:5px 5px 20px 5px;">

<a href="images/跑马灯.mblock" download>点击下载代码</a>

---