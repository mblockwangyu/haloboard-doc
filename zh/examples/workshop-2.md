# 智能家居

光环板内置Wi-Fi及帐号云广播功能可以用于智能家居场景，轻松实现远程控制愿望。光环板作为智能家居的灯光来源，使用电脑可以对光环板远程控制， 即使远离家门也可以轻松控制房屋灯光。光环板连接舵机，可以当智能车库锁，坐在车里即可以轻松关闭车库门。

<img src="image/smarthome.png" width="360px;" style="padding:5px 5px 20px 5px;">


## 设置舞台背景

1\. 选择“角色”，删除默认角色Panda

<img src="image/smart-home-1.gif" style="width:800px;padding:5px 5px 12px 0px;">

2\. 选择“背景”，点击“造型”，添加“Bedroom2”

<img src="image/smart-home-2.gif" style="width:800px;padding:5px 5px 12px 0px;">

## 添加舞台按钮

3\. 选择“角色”，添加角色“Empty button1”。

<img src="image/smart-home-3.gif" style="width:800px;padding:5px 5px 12px 0px;">

4\. 选中舞台角色“按钮”，点击“造型”，为按钮添加文字“明亮”，调整文字大小和颜色。

<img src="image/smart-home-4.gif" style="width:800px;padding:5px 5px 12px 0px;">

## 为“明亮”按钮编程

5\. 在角色积木区点击“＋”号，添加扩展“帐号云广播”。

<img src="image/smart-home-5.gif" style="width:800px;padding:5px 5px 12px 0px;">

6\. 添加事件类积木 <span style="background-color:#FFBF00;color:white;padding:3px; font-size: 12px">当角色被点击</span> 和帐号云广播类积木 <span style="background-color:#4EAA83;color:white;padding:3px; font-size: 12px">发送帐号云广播（）</span>，将广播命名为“明亮”。

<img src="image/smart-home-6.gif" style="width:800px;padding:5px 5px 12px 0px;">

7\. 为方便我们直观看出按钮是否被点击，我们需要为它增加一些特效。添加外观类积木 <span style="background-color:#9966FF;color:white;padding:3px; font-size: 12px">将大小增加（）</span> 和 <span style="background-color:#9966FF;color:white;padding:3px; font-size: 12px">将（颜色）特效增加（）</span>，并修改值。再添加控制<span style="background-color:#FFAB19;color:white;padding:3px; font-size: 12px">等待（）秒</span>，修改时间为0.1秒。

<img src="image/smart-home-7.gif" style="width:800px;padding:5px 5px 12px 0px;">

8\. 0.1秒后，角色“按钮”大小和颜色恢复原状。添加两个外观类积木 <span style="background-color:#9966FF;color:white;padding:3px; font-size: 12px">将大小设为（100）</span> 和 <span style="background-color:#9966FF;color:white;padding:3px; font-size: 12px">将（颜色）特效设定为（0）</span>。

<img src="image/smart-home-8.gif" style="width:800px;padding:5px 5px 12px 0px;">

## “柔和”、“夜灯”、“炫彩”按钮

9\. 点击角色“按钮”，鼠标右键复制角色，点击“造型”，修改按钮名称为“柔和”，修改该按钮程序，发送帐号云广播 “柔和”。同理，可以编程其它两个按钮，分别命名为“夜灯”、“炫彩”，并修改对应的帐号云广播名称。

<img src="image/smart-home-9.gif" style="width:800px;padding:5px 5px 12px 0px;">

<img src="image/smart-home-10.png" style="width:800px;padding:5px 5px 12px 0px;">

## 添加车库按钮

10\. 继续点击“＋”号，添加角色，在“图标”分类中添加蓝色按钮，点击“造型”修改按钮大小，并为按钮添加字体“车库锁”。 

<img src="image/smart-home-11.gif" style="width:800px;padding:5px 5px 12px 0px;">

11\. 点击舞台角色“车库锁”，点击“造型”，鼠标右键复制一个“车库锁”。用橡皮擦擦掉锁的一部分，点击“填充”，取样车库锁颜色，用油漆桶填充擦掉的部分。分别为两个车库锁命名，分别是“车锁关闭”和“车锁打开”。

<div style="background-color:#FFFFCC;padding:10px;border-left:5px solid #FFEB3B;margin:10px;"><strong>注：</strong><br>
如角色上的字体在舞台界面未显示，点击角色“造型”，点击造型页面左下角“转成位图”
</div>

<br>

<img src="image/smart-home-12.gif" style="width:800px;padding:5px 5px 12px 0px;">

## 为角色“车库锁”编程

12\. 选择变量类积木，点击新建立一个变量，并命名为“车库锁的状态”。

<div style="background-color:#FFFFCC;padding:10px;border-left:5px solid #FFEB3B;margin:10px;"><strong>注：</strong><br>
如果不使用变量，亦可用两个车库锁角色替代，使用其中一个车库锁角色打开车库门，另一个车库锁角色关闭门！
</div>

<br>

<img src="image/smart-home-13.gif" style="width:800px;padding:5px 5px 12px 0px;">

13\. 添加事件类积木 <span style="background-color:#FFBF00;color:white;padding:3px; font-size: 12px">当角色被点击</span> 和控制类积木 <span style="background-color:#FFAB19;color:white;padding:3px; font-size: 12px">如果（）那么（）否则（）</span>，运算类积木 <span style="background-color:#59C059;color:white;padding:4px; font-size: 12px; border-radius:3px;">（）=（）</span>，还有变量类积木 <span style="background-color:#FF8C1A;color:white;padding:4px; font-size: 12px; border-radius:3px;">车库锁状态</span>，输入“开”

<img src="image/smart-home-14.gif" style="width:800px;padding:5px 5px 12px 0px;">

14\. 当角色“车库锁”被点击时，如果车库锁的状态是开，那么<span style="background-color:#4EAA83;color:white;padding:3px; font-size: 12px">发送帐号云广播（）</span>，输入“关闭车库门”，把车库锁的状态设为“关”，并且修改造型为“车锁关闭”的造型。

<img src="image/smart-home-15.gif" style="width:800px;padding:5px 5px 12px 0px;">

15\. 反之，如果车库锁状态是“关”，发送帐号云广播“打开车库门”，把车库锁状态设置为“开”，修改车库锁造型为“车锁打开”的造型。

<img src="image/smart-home-16.gif" style="width:800px;padding:5px 5px 12px 0px;">

16\. 同样的，我们需要为角色添加一些特效，方便我们可以直观看出角色是否被点击。

<img src="image/smart-home-17.gif" style="width:800px;padding:5px 5px 12px 0px;">

## 光环板连接网络

17\. 使用帐号云广播功能，需要登录慧编程，连接WiFi。

<img src="image/smart-home-18.gif" style="width:800px;padding:5px 5px 12px 0px;">

18\. 我们需要为光环板添加一些灯效，便于我们判断网络是否连接成功。

<img src="image/smart-home-19.gif" style="width:800px;padding:5px 5px 12px 0px;">

## 光环板灯效

19\. 添加WiFi类积木 <span style="background-color:#76CE14;color:white;padding:3px; font-size: 12px">当接收账号云广播（）</span>，输入“明亮”，全部LED亮起白色。

<img src="image/smart-home-20.gif" style="width:800px;padding:5px 5px 12px 0px;">

20\. 当接收到帐号云广播“柔和”，全部LED亮起白色，亮度降低为40%。

<img src="image/smart-home-21.gif" style="width:800px;padding:5px 5px 12px 0px;">

21\. 当接收到帐号云广播“夜灯”，修改灯效，使LED亮起颗数减少。

<img src="image/smart-home-22.gif" style="width:800px;padding:5px 5px 12px 0px;">

22\. 当接收帐号云广播“炫彩”，添加灯效，使LED灯亮起光彩夺人的灯光。

<img src="image/smart-home-23.gif" style="width:800px;padding:5px 5px 12px 0px;">

## 光环板与舵机相连

23\. 舵机的红线是代表电源正极，与鳄鱼夹红线相连接，红线鳄鱼夹与光环板3.3V引脚相连接；舵机的黑线代表电源负极，与鳄鱼夹蓝线相连接，蓝线鳄鱼夹与光环板GND引脚相连；舵机的白线是信号线，与鳄鱼夹黄线相连接，黄线鳄鱼夹可以与光环板任一个触摸传感器想连，在这里，我们选择与触摸传感器0相连。

<img src="images/smarthome-22.jpg" style="width:400px;padding:5px 5px 12px 0px;">

<img src="images/smarthome-23.jpg" style="width:400px;padding:5px 5px 12px 0px;">

<img src="images/smarthome-24.jpg" style="width:400px;padding:5px 5px 12px 0px;">

## 对舵机编程


24\. 当舞台角色“车库锁”发送帐号云广播“关闭车库门”，车库的光环板接收到消息，与引脚0相连的舵机转动至0度。（具体转动度数以舵机摆放角度为准！）

<img src="image/smart-home-24.gif" style="width:800px;padding:5px 5px 12px 0px;">

25\. 当接收到帐号云广播“打开车库门”，舵机再次转动到某一角度，用叶片把车库门打开。车库门打开时，也可以为光环板增加一些灯光。

<img src="image/smart-home-25.gif" style="width:800px;padding:5px 5px 12px 0px;">

26\. 将程序上传

<p><span style="background-color:#66CDAA;line-height:30px;padding:12px;font-size:16px;border-radius:3px;"><a href="code/智能家居.mblock" download style="color:white;font-weight:bold;">下载代码</a></span></p>

---