# 光环板做舞台小车的“方向盘”

**效果展示**

程序编写完毕，试试用光环板遥控小车，光环板向左倾斜，舞台小车向左移动，光环板向右倾斜，小车向右移动。

<img src="images/steering-wheel-0.gif" width="600px;" style="padding:5px 5px 20px 5px;">

## 添加运动传感器

光环板的运动传感器可以检测出光环板如何运动，包括倾斜。当光环板向左倾斜时，发送一条广播消息，舞台小车接收到消息，向左移动。当光环板向右倾斜时，发送出一条广播消息，舞台小车向右移动。

1\. 添加控制类积木 <span style="background-color:#FFAB19;color:white;padding:4px; font-size: 12px;border-radius:3px;">如果（）那么（）</span>，把传感器类积木 <span style="background-color:#F79226;color:white;padding:4px; font-size: 12px;border-radius:3px;">光环板（向左倾斜）？</span> 添加到条件中。

<img src="image/steering-wheel-1.gif" width="800px;" style="padding:5px 5px 20px 5px;">


2\. 同理，可编写向右倾斜的程序，复制脚本，修改参数即可。

<img src="image/steering-wheel-2.gif" width="800px;" style="padding:5px 5px 20px 5px;">

## 添加广播

3\. 添加事件类积木 <span style="background-color:#FFBF00;color:white;padding:4px; font-size: 12px;border-radius:3px;">广播（）</span>，新建广播消息“左”和“右”。当光环板向左倾斜时，广播消息“左”，向右倾斜时，广播消息“右”。

<img src="image/steering-wheel-3.gif" width="800px;" style="padding:5px 5px 20px 5px;">

4\. 添加事件类积木 <span style="background-color:#FFBF00;color:white;padding:4px; font-size: 12px;border-radius:3px;">当按钮被按下时</span> 和控制类积木 <span style="background-color:#FFAB19;color:white;padding:4px; font-size: 12px;border-radius:3px;">重复执行</span>。

<img src="image/steering-wheel-4.gif" width="800px;" style="padding:5px 5px 20px 5px;">

## 添加舞台角色

5\. 点击“角色”，在角色库中点击“交通”，选择一辆小车。

<img src="image/steering-wheel-5.gif" width="800px;" style="padding:5px 5px 20px 5px;">

6\. 删除默认角色Panda。

<img src="image/steering-wheel-6.gif" width="800px;" style="padding:5px 5px 20px 5px;">

## 对舞台角色编程

7\. 小车需要通过旋转改变方向，在运动类积木中添加 <span style="background-color:#4C97FF;color:white;padding:4px; font-size: 12px;border-radius:3px;">将旋转方式设为（左右翻转）</span>。添加运动类积木 <span style="background-color:#4C97FF;color:white;padding:4px; font-size: 12px;border-radius:3px;">面向（）方向</span> 改变小车方向。添加运动类积木 <span style="background-color:#4C97FF;color:white;padding:4px; font-size: 12px;border-radius:3px;">移动（10）步</span> 使小车移动。

<img src="image/steering-wheel-7.gif" width="800px;" style="padding:5px 5px 20px 5px;">

8\. 添加事件类积木 <span style="background-color:#FFBF00;color:white;padding:4px; font-size: 12px;border-radius:3px;">当接收到（）</span>，选择“左”。当舞台小车接收到广播消息“左”时，面向-90方向，移动10步。

<img src="image/steering-wheel-8.gif" width="800px;" style="padding:5px 5px 20px 5px;">

9\. 同理，当接收到广播消息“右”时，面向90方向移动10步。

<small>注：接收广播的消息内容必须与其对应的发送广播的消息内容一致！</small>

<img src="image/steering-wheel-9.gif" width="800px;" style="padding:5px 5px 20px 5px;">

10\. 按下光环板按钮，左右倾斜光环板，看舞台小车如何移动吧！

<p><span style="background-color:#66CDAA;line-height:30px;padding:12px;font-size:16px;border-radius:3px;"><a href="code/光环板做舞台小车方向盘.mblock" download style="color:white;font-weight:bold;">下载代码</a></span></p>

---