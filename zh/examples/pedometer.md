# 计步器

<img src="image/pedometer.png" width="360px;" style="padding:5px 5px 20px 5px;">

## 光环板程序

1\. 新建三个消息：“start”、“move”、“stop”

<img src="image/pedometer-1.gif" width="800px;" style="padding:5px 5px 20px 5px;">

2\. 拖取一个事件类积木 <span style="background-color:#FFBF00;color:white;padding:4px; font-size: 12px;border-radius:3px;">当按钮被按下</span> 到脚本区，添加一个
控制类积木 <span style="background-color:#FFAB19;color:white;padding:4px; font-size: 12px;border-radius:3px;">重复执行</span>，事件类积木 <span style="background-color:#FFBF00;color:white;padding:4px; font-size: 12px;border-radius:3px;">广播（start）</span>，感知类积木 <span style="background-color:#F79226;color:white;padding:4px; font-size: 12px;border-radius:3px;">重置计时器</span>

<img src="image/pedometer-2.gif" width="800px;" style="padding:5px 5px 20px 5px;">

3\. 添加控制类积木 <span style="background-color:#FFAB19;color:white;padding:4px; font-size: 12px;border-radius:3px;">如果（）那么（）</span>，运算类积木 <span style="background-color:#59C059;color:white;padding:4px; font-size: 12px;border-radius:3px;">（）>（）</span>，感知类积木 <span style="background-color:#F79226;color:white;padding:4px; font-size: 12px;border-radius:3px;">光环板摇晃强度</span>，输入“15”，再添加一个事件类积木 <span style="background-color:#FFBF00;color:white;padding:4px; font-size: 12px;border-radius:3px;">广播（move）</span>

<img src="image/pedometer-3.gif" width="800px;" style="padding:5px 5px 20px 5px;">

4\. 添加控制类积木 <span style="background-color:#FFAB19;color:white;padding:4px; font-size: 12px;border-radius:3px;">如果（）那么（）</span>，运算类积木 <span style="background-color:#59C059;color:white;padding:4px; font-size: 12px;border-radius:3px;">（）=（）</span>，感知类积木 <span style="background-color:#F79226;color:white;padding:4px; font-size: 12px;border-radius:3px;">光环板摇晃强度</span>，再添加一个事件类积木 <span style="background-color:#FFBF00;color:white;padding:4px; font-size: 12px;border-radius:3px;">广播（stop）</span>

<img src="image/pedometer-4.gif" width="800px;" style="padding:5px 5px 20px 5px;">

5\. 添加控制类积木 <span style="background-color:#FFAB19;color:white;padding:4px; font-size: 12px;border-radius:3px;">重复执行直到（）</span>，运算类积木 <span style="background-color:#59C059;color:white;padding:4px; font-size: 12px;border-radius:3px;">（）>（）</span>，感知类积木 <span style="background-color:#F79226;color:white;padding:4px; font-size: 12px;border-radius:3px;">计时器</span>，输入100，将脚本与另外的积木合并

<img src="image/pedometer-5.gif" width="800px;" style="padding:5px 5px 20px 5px;">

## 添加舞台角色

6\. 删除默认角色Panda

<img src="image/pedometer-6.gif" width="800px;" style="padding:5px 5px 20px 5px;">

7\. 添加空白角色“Empty”

<img src="image/pedometer-7.gif" width="800px;" style="padding:5px 5px 20px 5px;">

8\. 选中角色“Empty”，点击“造型”，绘制一个红色的圆点

<img src="image/pedometer-8.gif" width="800px;" style="padding:5px 5px 20px 5px;">

9\. 复制角色“Empty”，点击“造型”，修改为蓝色圆点

<img src="image/pedometer-9.gif" width="800px;" style="padding:5px 5px 20px 5px;">

10\. 为舞台添加画笔扩展

<img src="image/pedometer-10.gif" width="800px;" style="padding:5px 5px 20px 5px;">

## 编程红色圆点

11\. 选择变量类积木，点击“建立一个变量”，命名为“Motion value”

<img src="image/pedometer-11.gif" width="800px;" style="padding:5px 5px 20px 5px;">

12\. 拖取一个事件类积木 <span style="background-color:#FFBF00;color:white;padding:4px; font-size: 12px;border-radius:3px;">当接收到（move）</span> 到脚本区，添加一个
控制类积木 <span style="background-color:#FFAB19;color:white;padding:4px; font-size: 12px;border-radius:3px;">重复执行（）次</span>，输入4

<img src="image/pedometer-12.gif" width="800px;" style="padding:5px 5px 20px 5px;">

13\. 添加运动积木 <span style="background-color:#4C97FF;color:white;padding:4px; font-size: 12px; border-radius:3px;">右转 &curarr;（）度</span>，画笔类积木 <span style="background-color:#0FBD8C;color:white;padding:4px; font-size: 12px; border-radius:3px;">图章</span>，还有变量类积木 <span style="background-color:#FF8C1A;color:white;padding:4px; font-size: 12px;border-radius:3px;">将（Motion value）增加（1）</span>

<img src="image/pedometer-13.gif" width="800px;" style="padding:5px 5px 20px 5px;">

14\. 添加事件类积木 <span style="background-color:#FFBF00;color:white;padding:4px; font-size: 12px;border-radius:3px;">当接收到（start）</span> 和运动类积木 <span style="background-color:#4C97FF;color:white;padding:4px; font-size: 12px; border-radius:3px;">面向（90）方向</span>

<img src="image/pedometer-14.gif" width="800px;" style="padding:5px 5px 20px 5px;">

## 编程蓝色圆点

15\. 选择变量类积木，点击“建立一个变量”，命名为“Slitting value”

<img src="image/pedometer-15.gif" width="800px;" style="padding:5px 5px 20px 5px;">

16\. 拖取一个事件类积木 <span style="background-color:#FFBF00;color:white;padding:4px; font-size: 12px;border-radius:3px;">当接收到（stop）</span> 到脚本区，添加一个
控制类积木 <span style="background-color:#FFAB19;color:white;padding:4px; font-size: 12px;border-radius:3px;">重复执行（）次</span>，输入2

<img src="image/pedometer-16.gif" width="800px;" style="padding:5px 5px 20px 5px;">

17\. 添加运动类积木 <span style="background-color:#9966FF;color:white;padding:4px; font-size: 12px; border-radius:3px;">右转 &curarr;（）度</span>，画笔类积木 <span style="background-color:#0FBD8C;color:white;padding:4px; font-size: 12px; border-radius:3px;">图章</span>，还有变量类积木 <span style="background-color:#FF8C1A;color:white;padding:4px; font-size: 12px;border-radius:3px;">将（Splitting value）增加（1）</span>

<img src="image/pedometer-17.gif" width="800px;" style="padding:5px 5px 20px 5px;">

18\. 添加事件类积木 <span style="background-color:#FFBF00;color:white;padding:4px; font-size: 12px;border-radius:3px;">当接收到（start）</span> 和外观类积木 <span style="background-color:#4C97FF;color:white;padding:4px; font-size: 12px; border-radius:3px;">面向（90）方向</span>

<img src="image/pedometer-18.gif" width="800px;" style="padding:5px 5px 20px 5px;">

<p><span style="background-color:#66CDAA;line-height:30px;padding:12px;font-size:16px;border-radius:3px;"><a href="code/计步器.mblock" download style="color:white;font-weight:bold;">下载代码</a></span></p>

---