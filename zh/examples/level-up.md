# 进阶案例

<table  cellpadding="15px;" cellspacing="12px;" style="font-size: 12px;text-align:center;">

<tr>
<td colspan="2"><p><b>虚实结合</b></p></td>
<td colspan="2"><p><b>物联网</b></p></td>
</tr>

<tr>
<td width="25%;"><a href="level-up-1.html" target="_blank"><img src="image/steering-wheel.png" width="150px;"></a><br>
<p>光环板做舞台小车的方向盘</p></td>

<td width="25%;"><a href="level-up-5.html" target="_blank"><img src="image/global-variable.png" width="150px;"></a><br>
<p>全局变量实现光环板与舞台互动</p></td>

<td width="25%;"><a href="remote-control.html" target="_blank"><img src="image/remote-control.png" width="150px;"></a><br>
<p>远程开关</p></td>

<td width="25%;"><a href="level-up-2.html" target="_blank"><img src="image/control-deck.png" width="150px;"></a><br>
<p>远程控制台</p></td>
</tr>
</table>

<br>

<table  cellpadding="15px;" cellspacing="12px;" style="font-size: 12px;text-align:center;width:100%;">
<tr>
<td colspan="3"><p><b>人工智能</b></p></td>
</tr>

<tr>
<td width=“33%;”><a href="level-up-3.html" target="_blank"><img src="image/voice-control.png" width="150px;"></a><br>
<p>语音控制灯环颜色</p></td>
<td width=“33%;”><a href="emotion-detector.html" target="_blank"><img src="image/emotion-detector.png" width="150px;"></a><br>
<p>情绪识别</p></td>
<td width=“33%;”><a href="level-up-6.html" target="_blank"><img src="image/deep-learning.png" width="150px;"></a><br>
<p>深度学习与人脸识别</p></td>
</tr>
</table>

<br>

<p><span style="background-color:#66CDAA;line-height:30px;padding:12px;font-size:16px;border-radius:3px;"><a href="code/code.rar" download style="color:white;font-weight:bold;">下载代码</a></span></p>

---