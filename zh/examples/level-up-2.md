# 远程控制台

## 登陆慧编程帐号

使用帐号云广播可以实现电脑对光环板的远程控制功能。使用帐号云广播需要登陆慧编程帐号。

<img src="../tutorials/images/login.gif" width="600px;" style="padding:5px 5px 20px 5px;">

## 启用上传模式

点击启用上传模式。

<img src="../tutorials/images/toggle-on-upload.gif" width="800px;" style="padding:5px 5px 20px 5px;">

## 连接网络

1\. 添加事件类积木 <span style="background-color:#FFBF00;color:white;padding:4px; font-size: 12px;border-radius:3px;">当光环板启动时</span>。使用帐号云广播需要连接网络。添加Wi-Fi类积木 <span style="background-color:#76CE14;color:white;padding:4px; font-size: 12px;border-radius:3px;">开始连接无线网络（）密码（）</span>，输入附近Wi-Fi的名称和密码。

<img src="../tutorials/images/connect-wifi-1.gif" width="800px;" style="padding:5px 5px 20px 5px;">

2\. 网络连接成功后，我们需要一个信号来告诉我们网络连接成功。添加控制类积木 <span style="background-color:#FFAB19;color:white;padding:4px; font-size: 12px;border-radius:3px;">等待（）</span>，拖入Wi-Fi类积木 <span style="background-color:#76CE14;color:white;padding:4px; font-size: 12px;border-radius:3px;">网络已连接？</span>，添加灯光类积木 <span style="background-color:#8521D3;color:white;padding:4px; font-size: 12px;border-radius:3px;">全部LED显示（）色</span>，选择绿色。当光环板亮起绿色，表明网络连接成功。

<img src="../tutorials/images/connect-wifi-2.gif" width="800px;" style="padding:5px 5px 20px 5px;">

## 添加舞台角色

3\. 点击“角色”，添加角色，选择“道具”气球作为角色。

<img src="image/control-deck-1.gif" width="800px;" style="padding:5px 5px 20px 5px;">

4\. 点击右键复制角色，点击“造型”选择紫色气球作为造型。

<img src="image/control-deck-2.gif" width="800px;" style="padding:5px 5px 20px 5px;">

5\. 删除默认角色Panda。

<img src="image/control-deck-3.gif" width="800px;" style="padding:5px 5px 20px 5px;">

## 对舞台角色编程

6\. 点击“＋”添加扩展“帐号云广播”。

<img src="image/control-deck-4.gif" width="800px;" style="padding:5px 5px 20px 5px;">

7\. 对蓝色气球编程，添加事件类积木 <span style="background-color:#FFBF00;color:white;padding:4px; font-size: 12px;border-radius:3px;">当角色被点击</span> 和云广播 <span style="background-color:#4EAA83;color:white;padding:4px; font-size: 12px;border-radius:3px;">发送帐号云广播（）</span>，命名为“蓝色”。

<img src="image/control-deck-5.gif" width="800px;" style="padding:5px 5px 20px 5px;">

## 为角色增加特效

8\. 我们需要给气球加一些特效，便于我们判断气球是否被点击。当气球被点击时，气球变大，颜色变亮，0.3秒后，气球恢复原样。依次添加积木： <span style="background-color:#9966FF;color:white;padding:4px; font-size: 12px;border-radius:3px;">将大小增加（）</span>， <span style="background-color:#9966FF;color:white;padding:4px; font-size: 12px;border-radius:3px;">将（）特效增加（）</span>，<span style="background-color:#FFAB19;color:white;padding:4px; font-size: 12px;border-radius:3px;">等待（）秒</span>，<span style="background-color:#9966FF;color:white;padding:4px; font-size: 12px;border-radius:3px;">将大小设为（）</span>， <span style="background-color:#9966FF;color:white;padding:4px; font-size: 12px;border-radius:3px;">将（）特效设为（）</span>。

<img src="image/control-deck-6.gif" width="800px;" style="padding:5px 5px 20px 5px;">

9\. 同理，可对紫色气球编程。添加事件类积木 <span style="background-color:#FFBF00;color:white;padding:4px; font-size: 12px;border-radius:3px;">当角色被点击</span> 和云广播 <span style="background-color:#4EAA83;color:white;padding:4px; font-size: 12px;border-radius:3px;">发送帐号云广播（）</span>，命名为“紫色”。

<img src="image/control-deck-7.gif" width="800px;" style="padding:5px 5px 20px 5px;">

## 接收帐号云广播

10\. 点击“设备”对光环板编程。

11\. 添加Wi-Fi类积木 <span style="background-color:#76CE14;color:white;padding:4px; font-size: 12px;border-radius:3px;">当接收账号云广播（）</span>，修改括号内容为“蓝色”。（注意：一定要确保发送的云广播名称与其对应的接收云广播名称保持一致！）

<img src="image/control-deck-8.gif" width="800px;" style="padding:5px 5px 20px 5px;">

## 添加灯效

12\. 添加灯光类积木 <span style="background-color:#8521D3;color:white;padding:4px; font-size: 12px;border-radius:3px;">全部LED显示（）色</span>，修改LED颜色为蓝色。添加控制类积木 <span style="background-color:#FFAB19;color:white;padding:4px; font-size: 12px;border-radius:3px;">等待（）秒</span> 和灯光类积木 <span style="background-color:#8521D3;color:white;padding:4px; font-size: 12px;border-radius:3px;">熄灭所有灯光</span>。

<img src="image/control-deck-9.gif" width="800px;" style="padding:5px 5px 20px 5px;">

13\. 同理，复制脚本，当接收到云广播“紫色”时，修改LED灯颜色为紫色。

<img src="image/control-deck-10.gif" width="800px;" style="padding:5px 5px 20px 5px;">

## 效果展示

14\. 上传程序至设备。拔下USB数据线，用电池连接光环板，使光环板与电脑远隔两地，分别点击两个气球，看光环板颜色变化。

<img src="image/control-deck-12.gif" width="800px;" style="padding:5px 5px 20px 5px;">

也可以尝试在舞台添加“按钮”的角色，制作一个IOT（物联网）控制面板。只要在同一帐号下编程并且接入互联网，即使电脑和光环板相隔两地，也可以远程控制。

<p><span style="background-color:#66CDAA;line-height:30px;padding:12px;font-size:16px;border-radius:3px;"><a href="code/远程控制台.mblock" download style="color:white;font-weight:bold;">下载代码</a></span></p>

---