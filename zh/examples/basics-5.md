# 比一比谁的力气大

**效果展示**

<img src="image/compare-strength-0.gif" width="600px;" style="padding:5px 5px 20px 5px;">

**认识光环板的运动传感器**

光环板的运动传感器可以检测光环板如何运动，包括摇晃、旋转角度、倾斜等。为光环板编程，使得LED灯颜色随着摇晃强度的变化而变化。

<img src="images/halo-motion.png" width="300px;" style="padding:5px 5px 20px 5px;">

## 设置摇晃强度范围值

1\. 添加运算类积木 <span style="background-color:#59C059;color:white;padding:4px; font-size: 12px;border-radius:3px;;">（）&gt;（50）</span>，修改数值为30，在（）中添加传感器类积木 <span style="background-color:#F79226;color:white;padding:4px; font-size: 12px;border-radius:3px;">光环板的摇晃强度</span>。

<img src="image/compare-strength-1.gif" width="800px;" style="padding:5px 5px 20px 5px;">

2\. 添加控制类积木 <span style="background-color:#FFAB19;color:white;padding:4px; font-size: 12px;border-radius:3px;">如果（）那么（）</span>。

<img src="image/compare-strength-2.gif" width="800px;" style="padding:5px 5px 20px 5px;">

## 编程LED灯效

3\. 添加灯光类积木 <span style="background-color:#8521D3;color:white;padding:4px; font-size: 12px;border-radius:3px;">全部LED显示（）色</span>，添加控制类积木 <span style="background-color:#FFAB19;color:white;padding:4px; font-size: 12px;border-radius:3px;">等待（）秒</span>，使用积木默认数值1，以及灯光类积木 <span style="background-color:#8521D3;color:white;padding:4px; font-size: 12px;border-radius:3px;">熄灭所有灯光</span>，则等待1秒，熄灭所有灯光。

<img src="image/compare-strength-3.gif" width="800px;" style="padding:5px 5px 20px 5px;">

4\. 同理，可以编程当光环板摇晃强度大于60时，全部LED显示红色，等待1秒，灯光熄灭。复制脚本，修改参数即可。

<img src="image/compare-strength-4.gif" width="800px;" style="padding:5px 5px 20px 5px;">

## 添加事件和控制

5\. 添加事件类积木 <span style="background-color:#FFBF00;color:white;padding:4px; font-size: 12px;border-radius:3px;">当光环板摇晃时</span>，添加控制类积木 <span style="background-color:#FFAB19;color:white;padding:4px; font-size: 12px;border-radius:3px;">重复执行</span>。

<img src="image/compare-strength-5.gif" width="800px;" style="padding:5px 5px 20px 5px;">

## 读取传感器的值

6\. 在传感器类积木 <span style="background-color:#F79226;color:white;padding:4px; font-size: 12px;border-radius:3px;">光环板的摇晃强度</span> 面前打钩，使其在舞台上呈现，摇晃光环板观察数值的变化。

<img src="image/compare-strength-6.gif" width="800px;" style="padding:5px 5px 20px 5px;">

7\. 用力摇晃光环板，谁能把光环板的颜色摇晃为红色，谁的力气最大！

<p><span style="background-color:#66CDAA;line-height:30px;padding:12px;font-size:16px;border-radius:3px;"><a href="code/比一比力气.mblock" download style="color:white;font-weight:bold;">下载代码</a></span></p>

---