# 语音控制灯环颜色

## 登陆慧编程帐号

<img src="../tutorials/images/login.gif" width="600px;" style="padding:5px 5px 20px 5px;">

## 启用上传模式

点击启用上传模式。

<img src="../tutorials/images/toggle-on-upload.gif" width="800px;" style="padding:5px 5px 20px 5px;">

## 连接网络

1\. 添加事件类积木 <span style="background-color:#FFBF00;color:white;padding:4px; font-size: 12px;border-radius:3px;">当光环板启动时</span>。使用帐号云广播需要连接网络。添加Wi-Fi类积木 <span style="background-color:#76CE14;color:white;padding:4px; font-size: 12px;border-radius:3px;">开始连接无线网络（）密码（）</span>，输入附近Wi-Fi的名称和密码。

<img src="image/voice-control-1.gif" width="800px;" style="padding:5px 5px 20px 5px;">

2\. 网络连接成功后，我们需要一个信号来告诉我们网络连接成功。添加控制类积木 <span style="background-color:#FFAB19;color:white;padding:4px; font-size: 12px;border-radius:3px;">等待（）</span>，拖入Wi-Fi类积木 <span style="background-color:#76CE14;color:white;padding:4px; font-size: 12px;border-radius:3px;">网络已连接？</span>，添加灯光类积木 <span style="background-color:#8521D3;color:white;padding:4px; font-size: 12px;border-radius:3px;">显示（）</span>，让灯环亮起指定图案。

<img src="image/voice-control-2.gif" width="800px;" style="padding:5px 5px 20px 5px;">


## 语音识别

按下按钮，灯环亮起橙色，一秒后，亮起一盏绿色LED，开始语音识别，2秒后，识别结束，灯光熄灭。

3\. 添加事件类积木 <span style="background-color:#FFBF00;color:white;padding:4px; font-size: 12px;border-radius:3px;">当按钮被按下时</span>，添加灯光类积木 <span style="background-color:#8521D3;color:white;padding:4px; font-size: 12px;border-radius:3px;">全部LED显示（）色</span>，选择橙色。再添加控制类积木 <span style="background-color:#FFAB19;color:white;padding:4px; font-size: 12px;border-radius:3px;">等待（1）秒</span>，和灯光类积木 <span style="background-color:#8521D3;color:white;padding:4px; font-size: 12px;border-radius:3px;">显示（）</span>，让灯环亮起一盏绿灯。

<img src="image/voice-control-3.gif" width="800px;" style="padding:5px 5px 20px 5px;">

4\. 添加Wi-Fi类积木 <span style="background-color:#76CE14;color:white;padding:4px; font-size: 12px;border-radius:3px;">识别（汉语）语音（）秒钟</span>，灯光类积木 <span style="background-color:#8521D3;color:white;padding:4px; font-size: 12px;border-radius:3px;">熄灭所有灯光</span>，2秒后语音识别结束，灯光熄灭。

<img src="image/voice-control-4.gif" width="800px;" style="padding:5px 5px 20px 5px;">

## 语音识别蓝色

5\. 添加控制类积木 <span style="background-color:#FFAB19;color:white;padding:4px; font-size: 12px;border-radius:3px;">如果（）那么（）否则（）</span>，拖入运算类积木 <span style="background-color:#59C059;color:white;padding:4px; font-size: 12px;border-radius:3px;">（）包含（）？</span>，添加Wi-Fi类积木 <span style="background-color:#76CE14;color:white;padding:4px; font-size: 12px;border-radius:3px;">语音识别出的文字</span> 至运算类积木前一个变量，修改后一个变量为“蓝”。如果语音识别出“蓝”，则光环板LED亮起蓝色，添加积木 <span style="background-color:#8521D3;color:white;padding:4px; font-size: 12px;border-radius:3px;">全部LED显示（）色</span>。

<img src="image/voice-control-5.gif" width="800px;" style="padding:5px 5px 20px 5px;">

<img src="image/voice-control-6.gif" width="800px;" style="padding:5px 5px 20px 5px;">


## 语音识别红色

6\. 同理，添加控制类积木 <span style="background-color:#FFAB19;color:white;padding:4px; font-size: 12px;border-radius:3px;">如果（）那么（）</span>，复制脚本，将颜色改为红色，并添加灯光类积木 <span style="background-color:#8521D3;color:white;padding:4px; font-size: 12px;border-radius:3px;">全部LED显示（）色</span>，灯环亮起红色。

<img src="image/voice-control-7.gif" width="800px;" style="padding:5px 5px 20px 5px;">

7\. 上传程序。光环板网络连接成功后，按下按钮，在绿灯亮起期间对着麦克风说“红色”，看光环板LED变化。

<p><span style="background-color:#66CDAA;line-height:30px;padding:12px;font-size:16px;border-radius:3px;"><a href="code/语音控制颜色.mblock" download style="color:white;font-weight:bold;">下载代码</a></span></p>

---