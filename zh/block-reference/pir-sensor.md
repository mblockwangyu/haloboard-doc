# 热运动传感器

光环板可以连接 mbuild 的 [热运动传感器](http://docs.makeblock.com/diy-platform/zh/mbuild/hardware/sensors/pir-sensor.html) 模块进行编程。

## 1. 热运动传感器（1）检测到热运动？

如果指定热运动传感器检测到热运动，报告条件成立。

<img src="mbuild-images/pir-1-1.png">

**示例**

<img src="mbuild-images/pir-1-2.png">

按下光环板的按钮，如果热运动传感器1检测到热运动，光环板的LED灯环会显示红色。

---

## 2. 热运动传感器（1）检测到热运动的次数

报告指定热运动传感器检测到热运动的次数。

<img src="mbuild-images/pir-2-1.png">

**示例**

<img src="mbuild-images/pir-2-2.png">

按下光环板的按钮，如果热运动传感器1检测到热运动的次数大于16次，光环板的LED灯环会显示红色。


---

## 3. 热运动传感器（1）重置检测到热运动的次数

重置指定热运动传感器检测到热运动的次数。

<img src="mbuild-images/pir-3-1.png">

**示例**

<img src="mbuild-images/pir-3-2.png">

按下光环板的按钮，重置热运动传感器1检测到热运动的次数。

---