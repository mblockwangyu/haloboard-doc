# 滑动电位器

光环板可以连接 mbuild 的 [滑动电位器](http://docs.makeblock.com/diy-platform/zh/mbuild/hardware/interaction/slide-potentiometer.html) 模块进行编程。

## 滑动电位器（1）读数

报告指定滑动电位器的读数。

<img src="mbuild-images/slider-1-1.png">

**示例**

<img src="mbuild-images/slider-1-2.png">

按下光环板的按钮，滑动电位器1的读数会显示在蓝色LED点阵1上。

---