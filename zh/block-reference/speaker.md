# 扬声器

光环板可以连接 mbuild 的 [扬声器](http://docs.makeblock.com/diy-platform/zh/mbuild/hardware/output-modules/speaker.html) 模块进行编程。

## 1. 扬声器（1）播放音符（C4）以（0.25）拍

指定扬声器播放指定音符，并持续指定拍数。

<img src="mbuild-images/speaker-1-1.png">

**示例**

<img src="mbuild-images/speaker-1-2.png">

按下光环板的按钮，扬声器1会播放音符C4，持续0.25拍。

---

## 2. 扬声器（1）以（700）赫兹播放声音，持续（1）秒

指定扬声器以指定频率播放声音，单位为赫兹，并持续指定时间。

<img src="mbuild-images/speaker-2-1.png">

**示例**

<img src="mbuild-images/speaker-2-2.png">

按下光环板的按钮，扬声器1会播放频率为700赫兹的声音，持续1秒。

---

## 3. 扬声器（1）以（700）赫兹播放声音

指定扬声器以指定频率播放声音，单位为赫兹。

<img src="mbuild-images/speaker-3-1.png">

**示例**

<img src="mbuild-images/speaker-3-2.png">

按下光环板的按钮，扬声器1会持续播放频率为700赫兹的声音。

---

## 4. 扬声器（1）播放（）

指定扬声器播放指定音效。

<img src="mbuild-images/speaker-4-1.png">

**示例**

<img src="mbuild-images/speaker-4-2.png">

按下光环板的按钮，扬声器1会播放情绪音效“哈喽”。

---

## 5. 扬声器（1）播放（）直到结束

指定扬声器播放指定音效，直到播放结束，才执行其他脚本。

<img src="mbuild-images/speaker-5-1.png">

**示例**

<img src="mbuild-images/speaker-5-2.png">

按下光环板的按钮，扬声器1会播放情绪音效“哈喽”，并在播放完成后才执行其他脚本。

---

## 6. 情绪音效（哈喽）

指定情绪音效。

<small>注：该积木为 <a href="http://www.mblock.cc/doc/zh/block-reference/shape.html#3-报告积木">「报告积木」</a>，需搭配其他积木使用。</small>

<img src="mbuild-images/speaker-6-1.png">

**示例**

<img src="mbuild-images/speaker-5-2.png">

按下光环板的按钮，扬声器1会播放情绪音效“哈喽”，并在播放完成后才执行其他脚本。

---

## 7. 电子音效（启动）

指定电子音效。

<small>注：该积木为 <a href="http://www.mblock.cc/doc/zh/block-reference/shape.html#3-报告积木">「报告积木」</a>，需搭配其他积木使用。</small>

<img src="mbuild-images/speaker-7-1.png">

**示例**

<img src="mbuild-images/speaker-7-2.png">

按下光环板的按钮，扬声器1会播放电子音效“启动”，并在播放完成后才执行其他脚本。

---

## 8. 物理音效（金属音）

指定物理音效。

<small>注：该积木为 <a href="http://www.mblock.cc/doc/zh/block-reference/shape.html#3-报告积木">「报告积木」</a>，需搭配其他积木使用。</small>

<img src="mbuild-images/speaker-8-1.png">

**示例**

<img src="mbuild-images/speaker-8-2.png">

按下光环板的按钮，扬声器1会播放物理音效“金属音”，并在播放完成后才执行其他脚本。

---

## 9. 数字及字母音效（0）

指定数字或字母音效。

<small>注：该积木为 <a href="http://www.mblock.cc/doc/zh/block-reference/shape.html#3-报告积木">「报告积木」</a>，需搭配其他积木使用。</small>

<img src="mbuild-images/speaker-9-1.png">

**示例**

<img src="mbuild-images/speaker-9-2.png">

按下光环板的按钮，扬声器1会播放数字音效“0”，并在播放完成后才执行其他脚本。

---

## 10. 英文单词（black）

指定英文单词。

<small>注：该积木为 <a href="http://www.mblock.cc/doc/zh/block-reference/shape.html#3-报告积木">「报告积木」</a>，需搭配其他积木使用。</small>

<img src="mbuild-images/speaker-10-1.png">

**示例**

<img src="mbuild-images/speaker-10-2.png">

按下光环板的按钮，扬声器1会播放英文单词“black”，并在播放完成后才执行其他脚本。

---

## 11. 扬声器（1）停止播放声音

指定扬声器停止播放声音。

<img src="mbuild-images/speaker-11-1.png">

**示例**

<img src="mbuild-images/speaker-11-2.png">

按下光环板的按钮，扬声器1会停止播放声音。

---

## 12. 扬声器（1）增加音量（20）%

将指定扬声器的音量增加指定百分比。

<img src="mbuild-images/speaker-12-1.png">

**示例**

<img src="mbuild-images/speaker-12-2.png">

按下光环板的按钮，将扬声器1的音量增加20%。

---

## 13. 扬声器（1）设置音量为（100）%

将指定扬声器的音量设置为指定百分比。

<img src="mbuild-images/speaker-13-1.png">

**示例**

<img src="mbuild-images/speaker-13-2.png">

按下光环板的按钮，将扬声器1的音量设为100%。

---

## 14. 扬声器（1）的音量（%）

报告指定扬声器的音量。

<img src="mbuild-images/speaker-14-1.png">

**示例**

<img src="mbuild-images/speaker-14-2.png">

按下光环板的按钮，如果扬声器1的音量大于50%，光环板的LED灯环会显示红色。

---

## 15. 扬声器（1）是否在播放歌曲？

如果指定扬声器在播放歌曲，报告条件成立。

<img src="mbuild-images/speaker-15-1.png">

**示例**

<img src="mbuild-images/speaker-15-2.png">

按下光环板的按钮，如果扬声器1在播放歌曲，光环板的LED灯环会显示红色。

---