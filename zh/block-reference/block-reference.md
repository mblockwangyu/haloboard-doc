# 积木说明

本章节包含了每个积木类别的详细介绍。

### 默认积木

* [灯光类](lighting.md)
* [传感器类](sensing.md)
* [引脚类](pins.md)
* [Wi-Fi类](wifi.md)
* [局域网类](lan.md)
* [事件类](events.md)
* [控制类](control.md)
* [运算类](operators.md)

### 扩展积木

* [扬声器](speaker.md)
* [舵机驱动](servo-driver.md)
* [热运动传感器](pir-sensor.md)
* [电机驱动](motor-driver.md)
* [测距传感器](ranging-sensor.md)
* [8×16 蓝色 LED 点阵](8-16-led-matrix.md)
* [彩灯驱动](led-driver.md)
* [滑动电位器](slider.md)
* [温湿度传感器](humiture-sensor.md)
* [双路颜色传感器](dual-rgb.md)
* [超声波传感器](ultrasonic.md)
* [视觉模块](smart-camera.md)