# 超声波传感器

光环板可以连接 mbuild 的 [超声波传感器](http://docs.makeblock.com/diy-platform/zh/mbuild/hardware/sensors/ultrasonic-sensor.html) 模块进行编程。

## 1. 超声波传感器（1）与障碍物的距离

报告指定超声波传感器与障碍物的距离。

<img src="mbuild-images/ultrasonic-1-1.png">

**示例**

<img src="mbuild-images/ultrasonic-1-2.png">

按下光环板的按钮，超声波传感器1检测到的与障碍物的距离会显示在蓝色LED点阵1上。

---


## 2. 超声波传感器（1）超出量程？

如果指定超声波传感器超出量程，报告条件成立。

<img src="mbuild-images/ultrasonic-2-1.png">

**示例**

<img src="mbuild-images/ultrasonic-2-2.png">

按下光环板的按钮，如果超声波传感器1超出量程，那么光环板的LED灯环会亮起红色。

---