# 运算类积木

## 1. （）+（）

执行加法运算。

![](images/operators-1-1.png)

**示例：**

![](images/operators-1-2.png)

光环板启动后，LED灯环按指定颜色顺序显示，等待3（1+2）秒后，熄灭所有LED。

---

## 2. （）-（）

执行减法运算。

![](images/operators-2-1.png)

**示例：**

![](images/operators-2-2.png)

光环板启动后，LED灯环按指定颜色顺序显示，等待2（3-1）秒后，熄灭所有LED。

---

## 3.（）*（）

执行乘法运算。

![](images/operators-3-1.png)

**示例：**

![](images/operators-3-2.png)

光环板启动后，LED灯环按指定颜色顺序显示，等待6（2&times;3）秒后，熄灭所有LED。

---

## 4. （）/（）

执行除法运算。

![](images/operators-4-1.png)

**示例：**

![](images/operators-4-2.png)

光环板启动后，LED灯环按指定颜色顺序显示，等待3（6&divide;2）秒后，熄灭所有LED。

---

## 5. 在（）和（）之间取随机数

在指定区间内取随机数。

![](images/operators-5-1.png)

**示例：**

![](images/operators-5-2.png)

光环板启动后，LED灯环按指定颜色顺序显示，等待1~5间随机数秒后，熄灭所有LED。

---

## 6. （）>（）

如果指定参数的值大于指定值，报告条件成立。

![](images/operators-6-1.png)

**示例：**

![](images/operators-6-2.png)

光环板启动后，如果麦克风检测到的声音响度大于50，所有的LED会亮起红色。

---

## 7. （）<（）

如果指定参数的值小于指定值，报告条件成立。

![](images/operators-7-1.png)

**示例：**

![](images/operators-7-2.png)

光环板启动后，如果麦克风检测到的声音响度小于50，所有的LED会亮起蓝色。

---

## 8. （）=（）

如果指定参数的值等于指定值，报告条件成立。

![](images/operators-8-1.png)

**示例：**

![](images/operators-8-2.png)

光环板启动后，如果绕x轴转过的角度等于360°，则重置绕x轴转过的角度。

---

## 9. （）与（）

指定两个条件同时成立，报告条件成立。

![](images/operators-9-1.png)

**示例：**

![](images/operators-9-2.png)

光环板启动后，如果网络已连接，并且光环板被摇晃，就播放LED动画“彩虹”。

---

## 10. （）或（）

指定两个条件其中一个成立，报告条件成立。

![](images/operators-10-1.png)

**示例：**

![](images/operators-10-2.png)

光环板启动后，如果按钮被按下，或者被摇晃，都播放LED动画“彩虹”。

---

## 11. （）不成立

指定条件不成立，报告条件成立。

![](images/operators-11-1.png)

**示例：**

![](images/operators-11-2.png)

光环板启动后，如果按钮没被按下，LED灯环显示指定颜色顺序。

---

## 12. 连接（）和（）

报告两个字符串合并结果。

![](images/operators-12-1.png)

**示例：**

![](images/operators-12-2.png)

报告语音识别的结果。

---

## 13. （）的第（）个字符

报告指定字符串的指定位置字符。

![](images/operators-13-1.png)

**示例：**

![](images/operators-13-2.png)

报告语音识别的第一个字符。

---

## 14. （）的字符数

报告指定字符串的字符数。

![](images/operators-14-1.png)

**示例：**

![](images/operators-14-2.png)

报告语音识别出的文字的字符数。

---

## 15. （）包含（）？

如果指定字符串包含另一指定字符串，报告条件成立。

![](images/operators-15-1.png)

**示例：**

![](images/operators-15-2.png)

光环板启动后，如果语音识别出的文字包含“红”，所有LED灯亮起红色。

---

## 16. （）除以（）的余数

报告指定两数相除的余数。

![](images/operators-16-1.png)

**示例：**

![](images/operators-16-2.png)

计算5除以2的余数。

---

## 17. 四舍五入（）

报告指定数字四舍五入的值。

![](images/operators-17-1.png)

**示例：**

![](images/operators-17-2.png)

将计时器值四舍五入。

---

## 18. （）（）

报告指定数字的指定数学运算结果，包括绝对值、向下取整、向上取整、平方根、sin、cos、tan、asin、acos、atan、ln、log、e^、10^，共15选项。

![](images/operators-18-1.png)

**示例：**

![](images/operators-18-2.png)

将计时器值向上取整。

---