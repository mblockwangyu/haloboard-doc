# 电机驱动

光环板可以连接 mbuild 的 [电机驱动](http://docs.makeblock.com/diy-platform/zh/mbuild/hardware/motion/motor-driver.html) 模块进行编程。

## 1. 电机驱动（1）输出动力（50）%，持续（1）秒

指定电机驱动输出指定动力，并持续指定时间。

<img src="mbuild-images/motor-driver-1-1.png">

**示例**

<img src="mbuild-images/motor-driver-1-2.png">

按下光环板的按钮，让电机驱动1输出50%动力，持续1秒。

---

## 2. 电机驱动（1）输出动力（50）%

指定电机驱动输出指定动力。

<img src="mbuild-images/motor-driver-2-1.png">

**示例**

<img src="mbuild-images/motor-driver-2-2.png">

按下光环板的按钮，让电机驱动1输出50%动力。

---

## 3. 电机驱动（1）输出动力增大（20）%

将指定电机驱动的输出动力增加指定值。

<img src="mbuild-images/motor-driver-3-1.png">

**示例**

<img src="mbuild-images/motor-driver-3-2.png">

按下光环板的按钮，将电机驱动1的输出动力增加20%。

---


## 4. 电机驱动（1）停止输出动力

让指定电机驱动停止输出动力。

<img src="mbuild-images/motor-driver-4-1.png">

**示例**

<img src="mbuild-images/motor-driver-4-2.png">

按下光环板的按钮，让电机驱动1停止输出动力。

---

## 5. 所有电机驱动停止输出动力

所有电机驱动停止输出动力。

<img src="mbuild-images/motor-driver-5-1.png">

**示例**

<img src="mbuild-images/motor-driver-5-2.png">

按下光环板的按钮，让所有电机驱动停止输出动力。

---

## 6. 电机驱动（1）的输出动力（%）

报告指定电机驱动的输出动力。

<img src="mbuild-images/motor-driver-6-1.png">

**示例**

<img src="mbuild-images/motor-driver-6-2.png">

按下光环板的按钮，如果电机驱动1的输出动力大于80%，光环板的LED灯环会显示红色。


---