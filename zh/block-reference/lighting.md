# 灯光类积木

光环板12颗LED灯的编号和位置如下图：

![](../python-api/led-id.png)

## 1. 播放LED动画（）直到结束

播放指定LED动画，包括：彩虹、浪花、流星、萤火虫。

![](images/lighting-1-1.png)

**示例：**

![](images/lighting-1-2.png)

光环板启动后，播放LED动画“彩虹”直到完成。

---

## 2. 显示（）

点击红色框框位置即可设置，设置光环板LED灯环按图示的颜色显示。

![](images/lighting-2-1.png)

![](images/lighting-2-2.png)

**示例：**

![](images/lighting-2-3.png)

光环板启动后，LED灯环按图示的颜色显示。

---

## 3. 显示旋转（）格后的（）

设置光环板LED灯环显示为设置的颜色顺时针旋转一格。

![](images/lighting-3-1.png)

**示例：**

![](images/lighting-3-2.png)

光环板启动后，LED灯环按指定颜色顺序显示，1秒后，顺时针旋转1格。

---

## 4. 全部LED显示（）色

设置光环板所有LED灯为指定颜色。

![](images/lighting-4-1.png)

**示例：**

![](images/lighting-4-2.png)

光环板启动后，所有LED灯显示绿色。

---

## 5. 全部LED显示（）色，亮度（）%

设置光环板所有LED灯为指定颜色和亮度。

![](images/lighting-5-1.png)

**示例：**

![](images/lighting-5-2.png)

光环板启动后，所有LED灯显示80%亮度的绿色。

---

## 6. 熄灭所有灯光

熄灭所有LED。

![](images/lighting-6-1.png)

**示例：**

![](images/lighting-6-2.png)

光环板启动后，所有LED灯显示绿色，并在1秒后熄灭。

---

## 7. 所有LED显示颜色R（）G（）B（）

设置光环板所有LED灯为指定颜色，由RGB色值混合而成，取值范围为0-255。

![](images/lighting-7-1.png)

**示例：**

![](images/lighting-7-2.png)

光环板启动后，所有LED灯显示红色。

---

## 8. 第（）颗LED显示颜色R（）G（）B（）

设置光环板指定单颗LED灯颜色，由RGB色值混合而成，取值范围为0-255。

![](images/lighting-8-1.png)

**示例：**

![](images/lighting-8-2.png)

光环板启动后，第3颗LED灯显示红色。

---

## 9. 熄灭第（）颗灯

熄灭指定单颗LED灯。

![](images/lighting-9-1.png)

**示例：**

![](images/lighting-9-2.png)

光环板启动后，所有LED灯显示绿色，1秒后，熄灭第3颗LED灯。

---

## 10. 显示LED环形图（）%

用光环板的LED灯环状态显示百分比。

![](images/lighting-10-1.png)

**示例：**

![](images/lighting-10-2.png)

光环板启动后，LED灯环显示当前响度。

---