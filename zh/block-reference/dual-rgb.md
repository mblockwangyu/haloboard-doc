# 双路颜色传感器

光环板可以连接 mbuild 的 [双路颜色传感器](http://docs.makeblock.com/diy-platform/zh/mbuild/hardware/sensors/dual-color-sensor.html) 模块进行编程。

## 1. 双路颜色传感器（1）探头（1）检测到（白）？

如果指定双路颜色传感器的指定探头检测到指定颜色，报告条件成立。

<img src="mbuild-images/dual-rgb-1-1.png">

**示例**

<img src="mbuild-images/dual-rgb-1-2.png">

按下光环板的按钮，如果双路颜色传感器1的探头1检测到红色，那么光环板的LED灯环会显示红色。

---

## 2. 双路颜色传感器（1）探头（1）检测到环境光强度

报告指定双路颜色传感器指定探头检测到的环境光强度。

<img src="mbuild-images/dual-rgb-2-1.png">

**示例**

<img src="mbuild-images/dual-rgb-2-2.png">

按下光环板的按钮，双路颜色传感器1的探头1检测到的环境光强度会通过光环板的LED灯环显示。

---