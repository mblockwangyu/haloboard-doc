# 测距传感器

光环板可以连接 mbuild 的 [测距传感器](http://docs.makeblock.com/diy-platform/zh/mbuild/hardware/sensors/ranging-sensor.html) 模块进行编程。

## 1. 测距传感器（1）与障碍物的距离（cm）

报告指定测距传感器与障碍物的距离。

<img src="mbuild-images/ranging-1-1.png">

**示例**

<img src="mbuild-images/ranging-1-2.png">

按下光环板的按钮，如果测距传感器1与障碍物的距离小于15cm，那么光环板的LED灯环会显示红色。

---

## 2. 测距传感器（1）超出量程？

如果指定测距传感器超出量程，报告条件成立。

<img src="mbuild-images/ranging-2-1.png">

**示例**

<img src="mbuild-images/ranging-2-2.png">

按下光环板的按钮，如果测距传感器1超出量程，那么光环板的LED灯环会显示红色。

---
