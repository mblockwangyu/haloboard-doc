# 扩展积木

光环板支持数十种电子模块扩展，你可以在光环板的硬件角色中，点击“扩展中心”添加对应电子模块的积木扩展。

* [扬声器](speaker.md)
* [舵机驱动](servo-driver.md)
* [热运动传感器](pir-sensor.md)
* [电机驱动](motor-driver.md)
* [测距传感器](ranging-sensor.md)
* [8×16 蓝色 LED 点阵](8-16-led-matrix.md)
* [彩灯驱动](led-driver.md)
* [滑动电位器](slider.md)
* [温湿度传感器](humiture-sensor.md)
* [双路颜色传感器](dual-rgb.md)
* [超声波传感器](ultrasonic.md)
* [视觉模块](smart-camera.md)