# 8×16 蓝色LED点阵

光环板可以连接 mbuild 的 [8×16 蓝色LED点阵](http://docs.makeblock.com/diy-platform/zh/mbuild/hardware/display/led-matrix.html) 模块进行编程。

## 1. 8×16蓝色LED点阵（1）显示图案（）（1）秒

在指定蓝色LED点阵显示指定图案，并持续指定时间。

<img src="mbuild-images/led-matrix-1-1.png">

**示例**

<img src="mbuild-images/led-matrix-1-2.png">

按下光环板的按钮，在蓝色LED点阵1显示指定图案，持续1秒。

---

## 2. 8×16蓝色LED点阵（1）显示图案（）

在指定蓝色LED点阵显示指定图案。

<img src="mbuild-images/led-matrix-2-1.png">

**示例**

<img src="mbuild-images/led-matrix-2-2.png">

按下光环板的按钮，在蓝色LED点阵1显示指定图案。

---

## 3. 8×16蓝色LED点阵（1）显示图案（）在 x：（0）y：（0）

在指定蓝色LED点阵的指定位置显示指定图案。

<img src="mbuild-images/led-matrix-3-1.png">

**示例**

<img src="mbuild-images/led-matrix-3-2.png">

按下光环板的按钮，在蓝色LED点阵1的（0，0）显示指定图案。

---

## 4. 8×16蓝色LED点阵（1）显示（Hello）

在指定蓝色LED点阵显示指定文本。

<img src="mbuild-images/led-matrix-4-1.png">

**示例**

<img src="mbuild-images/led-matrix-4-2.png">

按下光环板的按钮，在蓝色LED点阵1显示“Hello”。

---

## 5. 8×16蓝色LED点阵（1）滚动显示（Hello）直到结束

在指定蓝色LED点阵滚动显示指定文本直到结束。

<img src="mbuild-images/led-matrix-5-1.png">

**示例**

<img src="mbuild-images/led-matrix-5-2.png">

按下光环板的按钮，在蓝色LED点阵1滚动显示“Hello”。

---

## 6. 8×16蓝色LED点阵（1）以 x：（0）y：（0）为起点显示（Hello）

在指定蓝色LED点阵以指定位置为起点显示指定文本。

<img src="mbuild-images/led-matrix-6-1.png">

**示例**

<img src="mbuild-images/led-matrix-6-2.png">

按下光环板的按钮，在蓝色LED点阵1以（0，0）为起点显示“Hello”。

---

## 7. 8×16蓝色LED点阵（1）熄灭

熄灭指定蓝色LED点阵。

<img src="mbuild-images/led-matrix-7-1.png">

**示例**

<img src="mbuild-images/led-matrix-7-2.png">

按下光环板的按钮，将蓝色LED点阵1熄灭。

---

## 8. 8×16蓝色LED点阵（1）点亮 x：（0）y：（0）

点亮指定蓝色LED点阵的指定位置的LED。

<img src="mbuild-images/led-matrix-8-1.png">

**示例**

<img src="mbuild-images/led-matrix-8-2.png">

按下光环板的按钮，点亮蓝色LED点阵1（0，0）的LED。

---

## 9. 8×16蓝色LED点阵（1）熄灭 x：（0）y：（0）

熄灭指定蓝色LED点阵的指定位置的LED。

<img src="mbuild-images/led-matrix-9-1.png">

**示例**

<img src="mbuild-images/led-matrix-9-2.png">

按下光环板的按钮，熄灭蓝色LED点阵1（0，0）的LED。

---

## 10. 8×16蓝色LED点阵（1）切换 x：（0）y：（0）的点亮状态

切换指定蓝色LED点阵的指定位置的LED的点亮状态。

<img src="mbuild-images/led-matrix-10-1.png">

**示例**

<img src="mbuild-images/led-matrix-10-2.png">

按下光环板的按钮，切换蓝色LED点阵1（0，0）的LED的点亮状态。

---

## 11. 8×16蓝色LED点阵（1）x：（0）y：（0）被点亮？

如果指定蓝色LED点阵的指定位置的LED被点亮，报告条件成立。

<img src="mbuild-images/led-matrix-11-1.png">

**示例**

<img src="mbuild-images/led-matrix-11-2.png">

按下光环板的按钮，如果蓝色LED点阵1（0，0）的LED被点亮，那么光环板的LED灯环会显示红色。

---
