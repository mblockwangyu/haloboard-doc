# 视觉模块

光环板可以连接 mbuild 的 [视觉模块](../mbuild/hardware/sensors/smart-camera.md) 模块进行编程。

## 色块识别
 
 ### 1.视觉模块（1）切换到色块识别模式。

指定视觉模块切换到色块识别模式。

<img src="mbuild-images/vision-1-1.png">

**示例**

<img src="mbuild-images/vision-1-2.png">

按下光环板的按钮，视觉模块（1）切换到色块识别模式。

---

### 2. 视觉模块（1）开始学习色块（1）（直到按钮被按下）

指定视觉模块在制定操作执行后开始学习色块。

<img src="mbuild-images/vision-2-1.png">

**示例**

<img src="mbuild-images/vision-2-2.png">

按下光环板的按钮，视觉模块（1）开始学习色块（1）（直到按钮被按下）。

---

### 3. 视觉模块（1）识别到色块（1）？

指定视觉模块识别到指定色块？

<img src="mbuild-images/vision-3-1.png">

**示例**

<img src="mbuild-images/vision-3-2.png">

按下光环板的按钮，当视觉模块（1）识别到色块（1）时，光环板全部LED灯显示红色

---


### 4. 视觉模块（1）识别到色块（1）位于画面（中央）？

指定视觉模块识别到指定色块位于画面指定位置？

<img src="mbuild-images/vision-4-1.png">

**示例**

<img src="mbuild-images/vision-4-2.png">

按下光环板的按钮，当视觉模块（1）识别到色块（1）位于画面（中央）时，光环板全部LED灯显示红色。

---

### 5. 视觉模块（1）识别到色块（1）的（x坐标）

指定视觉模块识别到指定色块的位置数据。

<img src="mbuild-images/vision-5-1.png">

**示例**

<img src="mbuild-images/vision-5-2.png">

按下光环板的按钮，当视觉模块（1）识别到色块（1）的（x坐标）大于20时，光环板全部LED灯显示红色。

---

### 6. 视觉模块（1）(打开)补光灯

打开或关闭指定视觉模块的补光灯。

<img src="mbuild-images/vision-6-1.png">

**示例**

<img src="mbuild-images/vision-6-2.png">

按下光环板的按钮，视觉模块（1）（打开）补光灯。

---
<br>

## 线/条码追踪

### 7. 视觉模块（1）切换到 线/条码 追踪模式

指定视觉模块切换到 线/条码 追踪模式。

![](mbuild-images/vision-7-1.png)

**示例：**

![](mbuild-images/vision-7-2.png)

按下光环板的按钮，视觉模块（1）切换到 线/条码 追踪模式。

---

### 8. 视觉模块（1）检测到条码（1前进）？

指定视觉模块检测到指定条码？

![](mbuild-images/vision-8-1.png)

**示例：**

![](mbuild-images/vision-8-2.png)

光环板启动后，当视觉模块检测到条码（1前进）时，所有LED灯亮起红色。

---

### 9. 视觉模块（1）检测到条码（1前进）的（x坐标）

指定视觉模块检测到指定条码的指定位置数据。

![](mbuild-images/vision-9-1.png)

**示例：**

![](mbuild-images/vision-9-2.png)

按下光环板的按钮，若视觉模块（1）检测到条码（1前进）的（x坐标）小于30，那么全部LED灯亮起绿色。

---

### 10. 视觉模块（1）当前所在线段的（起始x）坐标

指定视觉模块当前所在线段的指定坐标数据。

![](mbuild-images/vision-10-1.png)

**示例：**

![](mbuild-images/vision-10-2.png)

当光环板按钮按下时，如果视觉模块（1）当前所在线段的（起始x）坐标小于30，所有LED灯亮起红色。

---

### 11. 视觉模块（1）检测到交叉点？

指定视觉模块是否检测到交叉点？

![](mbuild-images/vision-11-1.png)

**示例：**

![](mbuild-images/vision-11-2.png)

光环板启动后，如果视觉模块（1）检测到交叉点，那么全部LED灯亮起绿色。

---

### 12. 视觉模块（1）检测到交叉点的（x）坐标

指定视觉模块检测到交叉点的指定坐标数据

![](mbuild-images/vision-12-1.png)

**示例：**

![](mbuild-images/vision-12-2.png)

当光环板按钮按下时，如果视觉模块（1）检测到交叉点的（x）坐标小于30，所有LED灯亮起绿色。

---

### 13. 视觉模块（1）检测到岔路的数量

指定视觉模块检测到岔路的数量。

![](mbuild-images/vision-13-1.png)

**示例：**

![](mbuild-images/vision-13-2.png)

当光环板按钮按下时，如果视觉模块（1）检测到岔路的数量大于2，所有LED灯亮起红色。

---

### 14. 视觉模块（1）检测到第（1）个岔路的角度

指定视觉模块检测到某个岔路的角度。

![](mbuild-images/vision-14-1.png)

**示例：**

![](mbuild-images/vision-14-2.png)

当光环板按钮按下时，如果视觉模块（1）检测到第（1）个岔路的角度大于30，所有LED灯亮起红色。

---

### 15. 视觉模块（1）设置线追踪模式为（浅底深线）

设置指定视觉模块的线追踪模式。

![](mbuild-images/vision-15-1.png)

**示例：**

![](mbuild-images/vision-15-2.png)

当光环板按钮按下时，视觉模块（1）设置线追踪模式为（浅底深线）。

---

### 16. 视觉模块（1）当前偏好角度

指定视觉模块的当前偏好角度。

![](mbuild-images/vision-16-1.png)

**示例：**

![](mbuild-images/vision-16-2.png)

当光环板按钮按下时，如果视觉模块（1）当前偏好角度大于30，那么全部LED显示绿色。

---

## 赛事专用

### 17.视觉模块（1）设置电机差速计算系数Kp为0.5

指定视觉模块设置电机差速计算系数Kp为特定值。

<img src="mbuild-images/vision-17-1.png">

**示例**

<img src="mbuild-images/vision-17-2.png">

当光环板启动时，视觉模块（1）设置电机差速计算系数Kp为0.5。

---

### 18. 视觉模块（1）计算电机差速（自动跟随色块（1）至（x）轴（100））

指定视觉模块计算（自动跟随色块（1）至（x）轴（100））所需的电机差速。

<img src="mbuild-images/vision-18-1.png">

**示例**

<img src="mbuild-images/vision-18-2.png">

按下光环板的按钮，如果视觉模块（1）到达（自动跟随色块（1）至（x）轴（100））所需的电机差速大于30，那么全部LED显示红色。

---

### 19. 视觉模块（1）计算电机差速（自动跟随条码（1前进）至（x）轴（100））

指定视觉模块计算（自动跟随条码（1前进）至（x）轴（100））所需的电机差速。

<img src="mbuild-images/vision-19-1.png">

**示例**

<img src="mbuild-images/vision-19-2.png">

按下光环板的按钮，如果视觉模块（1）到达（自动跟随条码（1前进）至（x）轴（100））所需的电机差速大于30，那么全部LED显示红色。

---


### 20. 视觉模块（1）计算电机差速（对准巡线线段）

指定视觉模块计算对准巡线线段所需的电机差速。

<img src="mbuild-images/vision-20-1.png">

**示例**

<img src="mbuild-images/vision-20-2.png">

按下光环板的按钮，如果视觉模块（1）（对准巡线线段）所需的电机差速大于30，那么全部LED显示红色。

---

### 21. 视觉模块（1）锁定色块（1）到（x）轴100附近？

判断指定视觉模块中的指定色块是否在（x）轴100附近？

<img src="mbuild-images/vision-21-1.png">

**示例**

<img src="mbuild-images/vision-21-2.png">

按下光环板的按钮，如果视觉模块（1）锁定色块（1）到x为100附近时，光环板全部LED灯显示红色。

---

### 22. 视觉模块（1）锁定条形码（1前进）到（x）轴100附近？

判断指定视觉模块中的指定条形码是否在（x）轴100附近？

<img src="mbuild-images/vision-22-1.png">

**示例**

<img src="mbuild-images/vision-22-2.png">

按下光环板的按钮，如果视觉模块（1）锁定条形码（1前进）到x为100附近时，光环板全部LED灯显示红色。

---
