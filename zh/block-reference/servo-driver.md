# 舵机驱动

光环板可以连接 mbuild 的 [舵机驱动](http://docs.makeblock.com/diy-platform/zh/mbuild/hardware/motion/servo-driver.html) 模块进行编程。

## 1. 舵机驱动（1）设置角度为（90）°

将指定的舵机驱动设置为指定旋转角度。

<img src="mbuild-images/servo-driver-1-1.png">

**示例**

<img src="mbuild-images/servo-driver-1-2.png">

按下光环板的按钮，将舵机驱动1的旋转角度设置为90°。

---

## 2. 舵机驱动（1）的设置角度增加（20）°

将指定的舵机驱动设置的旋转角度增加指定值。

<img src="mbuild-images/servo-driver-2-1.png">

**示例**

<img src="mbuild-images/servo-driver-2-2.png">

按下光环板的按钮，将舵机驱动1的旋转角度增加20°。

---

## 3. 舵机驱动（1）回复到零点

将指定的舵机驱动回复到零点。

<img src="mbuild-images/servo-driver-3-1.png">

**示例**

<img src="mbuild-images/servo-driver-3-2.png">

按下光环板的按钮，将舵机驱动1回复到零点。

---

## 4. 舵机驱动（1）当前设置的角度（°）

报告指定舵机驱动当前设置的角度。

<img src="mbuild-images/servo-driver-4-1.png">

**示例**

<img src="mbuild-images/servo-driver-4-2.png">

按下光环板的按钮，如果舵机驱动1设置的角度大于90°，光环板的LED灯环会显示红色。

---