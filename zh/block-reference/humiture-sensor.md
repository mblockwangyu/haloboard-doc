# 温湿度传感器

光环板可以连接 mbuild 的 [温湿度传感器](http://docs.makeblock.com/diy-platform/zh/mbuild/hardware/sensors/humiture-sensor.html) 模块进行编程。


## 1. 温湿度传感器（1）温度（℃）

报告指定温湿度传感器检测到的温度，单位为摄氏度。

<img src="mbuild-images/humiture-1-1.png">

**示例**

<img src="mbuild-images/humiture-1-2.png">

按下光环板的按钮，温湿度传感器1检测到的温度值会显示在蓝色LED点阵1上。

---

## 2. 温湿度传感器（1）温度（℉）

报告指定温湿度传感器检测到的温度，单位为华氏度。

<img src="mbuild-images/humiture-2-1.png">

**示例**

<img src="mbuild-images/humiture-2-2.png">

按下光环板的按钮，温湿度传感器1检测到的温度值会显示在蓝色LED点阵1上。

---

## 3. 温湿度传感器（1）空气湿度（%）

报告指定温湿度传感器检测到的空气湿度。

<img src="mbuild-images/humiture-3-1.png">

**示例**

<img src="mbuild-images/humiture-3-2.png">

按下光环板的按钮，温湿度传感器1检测到的空气湿度值会显示在蓝色LED点阵1上。

---