# 彩灯驱动

光环板可以连接 mbuild 的 [彩灯驱动](http://docs.makeblock.com/diy-platform/zh/mbuild/hardware/light/led-driver.html) 模块进行编程。

## 1. 彩灯驱动（1）亮起（）

指定彩灯驱动连接的灯带或灯环以指定颜色顺序亮起。

<img src="mbuild-images/led-driver-1-1.png">

**示例**

<img src="mbuild-images/led-driver-1-2.png">

按下光环板的按钮，彩灯驱动1连接的灯带会以指定颜色顺序亮起。

---

## 2. 彩灯驱动（1）第（1）个灯设置颜色为（）

设置指定彩灯驱动连接的灯带或灯环上指定LED灯为指定颜色。

<img src="mbuild-images/led-driver-2-1.png">

**示例**

<img src="mbuild-images/led-driver-2-2.png">

按下光环板的按钮，彩灯驱动1连接的灯带上第一个LED会亮起红色。

---

## 3. 彩灯驱动（1）第（1）个灯设置颜色为R（255）G（0）B（0）

设置指定彩灯驱动连接的灯带或灯环上指定LED灯为指定颜色，通过RGB三色合成。

<img src="mbuild-images/led-driver-3-1.png">

**示例**

<img src="mbuild-images/led-driver-3-2.png">

按下光环板的按钮，彩灯驱动1连接的灯带上第一个LED会亮起红色。

---

## 4. 彩灯驱动（1）第（1）灯熄灭

熄灭指定彩灯驱动连接的灯带或灯环上的指定LED灯。

<img src="mbuild-images/led-driver-4-1.png">

**示例**

<img src="mbuild-images/led-driver-4-2.png">

按下光环板的按钮，彩灯驱动1连接的灯带上第一个LED会熄灭。

---


## 5. 彩灯驱动（1）所有灯熄灭

熄灭指定彩灯驱动连接的灯带或灯环。

<img src="mbuild-images/led-driver-5-1.png">

**示例**

<img src="mbuild-images/led-driver-5-2.png">

按下光环板的按钮，彩灯驱动1连接的灯带会熄灭。

---

## 6. 彩灯驱动（1）第（1）个灯的（红）色值设置为（255）

设置指定彩灯驱动连接的灯带或灯环上指定LED灯的颜色，通过设置指定颜色的色值。

<img src="mbuild-images/led-driver-6-1.png">

**示例**

<img src="mbuild-images/led-driver-6-2.png">

按下光环板的按钮，彩灯驱动1连接的灯带上第一个LED会亮起，其中红色色值为255。

---

## 7. 彩灯驱动（1）第（1）个灯的（红）色值增加（20)

将指定彩灯驱动连接的灯带或灯环上指定LED的指定颜色色值增加指定值。

<img src="mbuild-images/led-driver-7-1.png">

**示例**

<img src="mbuild-images/led-driver-7-2.png">

按下光环板的按钮，将彩灯驱动1连接的灯带上第一个LED红色色值增加20。

---

## 8. 彩灯驱动（1）设置灯效为（静态）

将指定彩灯驱动连接的灯带或灯环设置为指定灯效。

<img src="mbuild-images/led-driver-8-1.png">

**示例**

<img src="mbuild-images/led-driver-8-2.png">

按下光环板的按钮，将彩灯驱动1连接的灯带的灯效设置为“静态”。

---