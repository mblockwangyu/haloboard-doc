# 电机驱动

电机驱动能驱动各类直流电机转动，并控制其速度和转动方向。

<img src="images/motor-driver.png" style="padding:3px 0px 12px 3px;width:200px;">

### 生活实例

- 特斯拉汽车使用电机驱动<br>
<img src="images/motor-driver-2.jpg" style="padding:8px 0px 12px 3px;width:300px;">
- 吹风机的风由电机产生<br>
<img src="images/motor-driver-1.png" style="padding:8px 0px 12px 3px;width:300px;">


### 参数

- 尺寸：24×24mm
- 工作电流：小于1A