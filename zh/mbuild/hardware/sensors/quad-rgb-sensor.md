# 四路颜色传感器

<img src="images/quad-rgb-sensor-1.png" style="padding:3px 0px 12px 3px;width:300px;">

## 概述

四路颜色传感器使用可见光进行补光，大幅增强了对环境光的抗干扰能力，并且支持在巡线检测的同时进行颜色识别。新的环境光校准功能还能降低环境光对巡线效果的干扰。此外，传感器的数量由两个增加至四个，大大提高了该模块的编程潜力以应用场景。

<img src="images/quad-rgb-sensor-2.png" style="padding:3px 0px 12px 3px;width:300px;">


<table  cellpadding="10px;" cellspacing="15px;" style="text-align:left;;margin-bottom:20px;">
<tr>
<th width="33%;" style="border: 1px solid black;">   </th>
<th width="33%;" style="border: 1px solid black;">四路颜色传感器</th>
<th width="34%;" style="border: 1px solid black;">巡线传感器</th>
</tr>

</td>
<td style="border: 1px solid black;">塑料外壳<br>
提升耐用性和质量</td>
<td style="border: 1px solid black;">有</td>
<td style="border: 1px solid black;">无</td>
</tr>

<tr>
<td style="border: 1px solid black;">巡线传感器</td>
<td style="border: 1px solid black;">4 个</td>
<td style="border: 1px solid black;">2 个</td>
</tr>

<tr>
<td style="border: 1px solid black;">颜色传感器</td>
<td style="border: 1px solid black;">4 个<br>（与巡线传感器复用）</td>
<td style="border: 1px solid black;">无</td>
</tr>

<tr>
<td style="border: 1px solid black;">光线传感器</td>
<td style="border: 1px solid black;">4 个<br>（与巡线传感器复用）</td>
<td style="border: 1px solid black;">无</td>
</tr>

<tr>
<td style="border: 1px solid black;">补光灯</td>
<td style="border: 1px solid black;">可见光补光灯</td>
<td style="border: 1px solid black;">红外补光灯</td>
</tr>

<tr>
<td style="border: 1px solid black;">环境光校准<br>该功能能够显著降低环境光对巡线结果的干扰。</td>
<td style="border: 1px solid black;">有</td>
<td style="border: 1px solid black;">无</td>
</tr>

</table>


## 按键学习功能说明

* 双击： 触发巡线学习。将传感器两个探头置于巡线地图背景上双击按键， 观察到两个巡线状态LED灯快速闪烁时，在背景和线上水平定高晃动传感器，直到LED灯停止快闪（时间约2.5秒）。学习参数会自动存储，若学习失败，两个巡线状态灯会转为慢闪，需要重新学习。
* 长按： 切换巡线补光灯颜色（正常补光灯颜色不需要切换，学习成功后自动设定）。