# 新超声波模块

<img src="images/ultrasonic-sensor-2-1.png" style="padding:3px 0px 12px 3px;width:200px;">

## 概述

超声波模块进行了全面的改进，使用了注塑外壳对模块进行保护，并为其增加了氛围灯。氛围灯的引入大大提升了机器人在情绪表达上的潜力。

<img src="images/ultrasonic-sensor-2-2.png" style="padding:3px 0px 12px 3px;width:500px;">

如下是新旧超声波的对比：

<table  cellpadding="10px;" cellspacing="15px;" style="text-align:left;;margin-bottom:20px;">
<tr>
<th width="33%;" style="border: 1px solid black;">   </th>
<th width="33%;" style="border: 1px solid black;">新超声波</th>
<th width="34%;" style="border: 1px solid black;">超声波</th>
</tr>

</td>
<td style="border: 1px solid black;">塑料外壳<br>
提升耐用性和质量</td>
<td style="border: 1px solid black;">有</td>
<td style="border: 1px solid black;">无</td>
</tr>

<tr>
<td style="border: 1px solid black;">自带芯片<br>提升读值稳定性</td>
<td style="border: 1px solid black;">有</td>
<td style="border: 1px solid black;">无</td>
</tr>

<tr>
<td style="border: 1px solid black;">氛围灯<br>
额外的功能</td>
<td style="border: 1px solid black;">8 颗</td>
<td style="border: 1px solid black;">无</td>
</tr>

</table>