
# 视觉模块

视觉模块能够识别条码和线条，也可以学习和识别颜色鲜艳的物体，实现诸如垃圾分类、智慧交通、物体追踪、智能巡线等功能。

<img src="images/vision-1.png" style="padding:3px 0px 12px 3px;width:500px;">

## 连接主控板
通过不同的连线方式，可以将视觉模块作为一个 RJ25 电子模块或 mBuild 电子模块，连接到 mBot 或光环板，然后使用 mBot 或光环板控制视觉模块。

### 连接到 mBot
与 mBot 连接时，可以使用 3.7V 锂电池或 mBuild 电源模块连接到视觉模块，为其供电。

#### 方式一 使用 mBuild 电源模块供电（推荐）
<img src="images/vision-5.png" style="padding:3px 0px 12px 3px;width:700px;">

#### 方式二 使用 3.7V 锂电池供电
<img src="images/vision-6.png" style="padding:3px 0px 12px 3px;width:700px;">

### 连接到光环板
注意：仅通过 USB 供电可能会影响模块的正常使用，建议使用 mBuild 电源模块供电。

<img src="images/vision-7.png" style="padding:3px 0px 12px 3px;width:700px;">

## 功能介绍
### 色块学习
视觉模块能够学习颜色鲜艳的物体，并在学习后识别色块，返回它们的坐标及长宽信息。

**学习步骤如下：**

1. 长按学习按钮，直到识别指示灯亮红色（橙、黄、绿、青、蓝、紫灯亦可，不同颜色代表学习不同物体）时松开。<br>
2. 将要学习的色块放到摄像头正前方。<br>
3. 观察视觉模块正面或背面的识别指示灯，同时缓慢移动需要学习的物体直至指示灯的颜色与被学习物体的颜色一致。<br>
4. 短按学习按钮记录当前学习的物体。<br>
5. 学习成功，当摄像头识别到已学习的物体时，彩色指示灯颜色会与被识别到物体的颜色一致。<br>

这样的方式最多支持学习并记录 7 种物体。<br>

<img src="images/vision-2.png" style="padding:3px 0px 12px 3px;width:800px;">

学习完成后，可以使用如下示例代码完成简单的色块（色块1）追踪功能。

<img src="images/vision-8.png" style="padding:3px 0px 12px 3px;width:600px;">

**注意事项**

1、学习的物体需要具备鲜艳的颜色，颜色越鲜艳，识别的准确率越高。

错误示范：学习熊猫人偶

<img src="images/vision-9.png" style="padding:3px 0px 12px 3px;width:150px;">

2、视觉模块支持学习并记录 7 种物体，这 7 种物体的特征点差别应尽可能大。

错误示范：学习黄球和小黄帽

<img src="images/vision-10.png" style="padding:3px 0px 12px 3px;width:300px;">

正确示范：学习橙球和绿球

<img src="images/vision-11.png" style="padding:3px 0px 12px 3px;width:300px;">

3、在程序中，视觉模块需要被切换到“色块识别模式”，才能够正常获得识别到色块的坐标及长宽信息。

4、视觉模块对光线变化具备一定抗干扰能力，但多变的光线仍然会影响识别率。在新的光线环境下，旧的学习结果将无法适用。以下方法有助于解决这一问题：
- 重置白平衡
- 开启补光灯后再学习和识别
- 在 PixyMon 软件中设置细节参数
- 为硬件设计合适的遮光罩


### 识别条码和巡线
视觉模块支持同时识别条码、线条和岔路，且无需学习。<br>
但视觉模块需要先进入“线/条码识别模式”才能够识别并返回条码、线、岔路的坐标信息。<br>

**条码识别**<br>
视觉模块包装内包含了条码卡片和贴纸，点击可<a href="images/barcodes.rar" download >下载条码文件</a>。

<img src="images/vision-3.png" style="padding:3px 0px 12px 3px;width:800px;">

您可以将卡片放在巡线地图上：

<img src="images/vision-12.png" style="padding:3px 0px 12px 3px;width:400px;">

**识别线和岔路**<br>
视觉模块能够识别线条并判断岔路数量，返回必要的信息（坐标、方向、岔路数量）。

<img src="images/vision-4.png" style="padding:3px 0px 12px 3px;width:600px;">

**注意事项**

1、您可以在程序中设置“浅底深线”或是“深底浅线”。<br>
2、视觉模块默认过滤掉了宽度较低的线条，若您需要视觉模块识别细线，请查看 PixyMon 的使用指南。


### PixyMon
视觉模块支持 PixyMon 软件，通过 PixyMon 软件可以查看摄像头画面、调试视觉模块功能以及对一些参数做微调，探索更多更复杂的功能。<br>

可直接点击以下链接下载：

[PixyMon Windows 版本下载](images/pixymon_v2_windows-3.0.24.rar)<br>
[PixyMon Mac 版本下载](images/pixymon_v2_mac-3.0.24.rar)<br>



**注意事项**

考虑到模块的易用性，我们并未在积木块上开发很多功能，如您希望通过积木块控制视觉模块完成更多功能，请使用扩展编辑器。<br>
[扩展编辑器帮助文档](https://www.mblock.cc/doc/zh/developer-documentation/developer-documentation.html?home)

## 参数
* 尺寸：48*48mm
* 分辨率：640 * 480
* 视场角：65.0度
* 有效焦距：4.65±5% mm
* 识别速度：60帧/s
* 识别距离：0.25-1.2m范围最佳
* 抗跌落能力：1m
* 供电方式：3.7V锂电池 或 5V mBuild 电源模块
* 功耗范围：0.9-1.3W
* 工作温度：-10℃~55℃
