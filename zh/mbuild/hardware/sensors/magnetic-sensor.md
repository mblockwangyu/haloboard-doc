# 磁敏传感器

磁敏传感器可以检测模块周围是否有磁体。

<img src="images/magnetic-sensor.png" style="padding:3px 0px 12px 3px;width:200px;">

### 生活实例

扫地机器人的虚拟墙使用了磁场检测来判断能否通行。

<img src="images/magnetic-sensor-2.png" style="padding:3px 0px 12px 3px;width:400px;">

### 参数

- 尺寸：24×20mm
- 检测距离：<1cm
- 工作电流：15mA