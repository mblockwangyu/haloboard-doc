# 科学传感器

<img src="images/science-sensor-1.png" style="padding:3px 0px 12px 3px;width:100px;">

## 概述
科学传感器是童心制物（Makeblock）自主开发的一款多合一传感器，集成了MQ2气体、地磁、火焰、气压、温湿度、热运动、触摸以及土壤湿度传感器元件，可方便且高效地应用于多种数据收集、科学探寻和物联网项目。

<img src="images/science-sensor-2.png" style="padding:3px 0px 12px 3px;width:400px;">

## 功能与参数

### 整体参数

* 尺寸：78.2 × 24 × 1.6（mm）
* 工作电压：5.0 V
* 工作电流：250 mA

### 各元件功能与参数

#### MQ2气体传感器

* 功能：可灵敏地检测到空气中的烟雾、液化气、丁烷、丙烷、甲烷、酒精、氢气等气体。
* 测量浓度：300~10000ppm（可燃气体）
* 加热阻抗：33Ω
* 预热消耗：< 950mW

<div style="background-color:#EAF6FD;padding:10px;border-left:5px solid #BAE3F9;margin-bottom:30px;border-radius:5px;line-height:28px;font-size:14px;"><strong>注意：</strong><br> 
上电后使用误差较大，需预热5分钟。
</div>


#### 地磁传感器

* 功能：通过校准，可在水平方向检测所处方向与北极点（北纬 90 度）的偏移角度。
* 角度读值范围：–180°~+180°（0°指向磁北）
* 磁通量读值范围：±30G（单位：高斯）

<div style="background-color:#EAF6FD;padding:10px;border-left:5px solid #BAE3F9;margin-bottom:30px;border-radius:5px;line-height:28px;font-size:14px;"><strong>注意：</strong><br> 
请将传感器校准后置于水平位置进行测量，以避免倾斜导致角度测量偏差过大。<br>
测量时需确保附近无磁铁等强磁物体，以免导致测量错误。
</div>

#### 火焰传感器

* 功能：可通过检测红外光来检测火焰及其大小。
* 检测波长：600~1000nm
* 火焰大小读值范围：1~100

<div style="background-color:#EAF6FD;padding:10px;border-left:5px solid #BAE3F9;margin-bottom:30px;border-radius:5px;line-height:28px;font-size:14px;"><strong>注意：</strong><br> 
请勿在阳光直射下使用，否则会出现严重干扰，无法正常工作。
</div>

#### 气压传感器

* 功能：可通过检测大气压强粗略估算目前海拔高度。
* 气压读值范围：300~1100 hPa（单位：百帕）
* 海拔读值范围：–500m~+6000m

<div style="background-color:#EAF6FD;padding:10px;border-left:5px solid #BAE3F9;margin-bottom:30px;border-radius:5px;line-height:28px;font-size:14px;"><strong>注意：</strong><br> 
请确保传感器表面无灰尘等颗粒沉积，以避免传感器失效。
</div>

#### 温湿度传感器

* 功能：可检测空气的湿度和温度。
* 温度读值范围：–40°C ~+125°C
* 温度读值误差：±0.5°C（在 0°C ~50°C 环境中）；±1°C （在–20°C ~85°C 环境中）
* 湿度读值范围：0~100%
* 湿度读值误差：±3%（在 50%RH 环境中）；±5% （在 20%~80%RH 环境中）

<div style="background-color:#EAF6FD;padding:10px;border-left:5px solid #BAE3F9;margin-bottom:30px;border-radius:5px;line-height:28px;font-size:14px;"><strong>注意：</strong><br> 
请确保传感器表面无灰尘等颗粒沉积，以避免导致测量偏差较大或传感器失效。
</div>

#### 热运动传感器

* 功能：可检测环境中是否有人或恒温动物经过。
* 检测范围：< 2m
* X 轴检测角度：80°

  <img src="images/science-sensor-3.png" style="padding:3px 0px 12px 3px;width:200px;">

* Y 轴检测角度：55°

  <img src="images/science-sensor-4.png" style="padding:3px 0px 12px 3px;width:200px;">

* 触发后持续检测时间：2 秒

<div style="background-color:#EAF6FD;padding:10px;border-left:5px solid #BAE3F9;margin-bottom:30px;border-radius:5px;line-height:28px;font-size:14px;"><strong>注意：</strong><br> 
请勿拆掉白色的菲涅尔滤光片，避免影响传感器的检测效果。
</div>

#### 触摸传感器

* 功能：可检测是否有人手触摸。
* 触摸范围：指纹图案区

  <img src="images/science-sensor-5.png" style="padding:3px 0px 12px 3px;width:200px;">

* 电阻读值范围：0~102300000kΩ

<div style="background-color:#EAF6FD;padding:10px;border-left:5px solid #BAE3F9;margin-bottom:30px;border-radius:5px;line-height:28px;font-size:14px;"><strong>注意：</strong><br> 
请保持指纹图案区干净，无灰尘、水珠，以避免误触。
</div>

#### 土壤湿度传感器

* 功能：可检测土壤湿度。
* 检测范围：极耳图案区

  <img src="images/science-sensor-6.png" style="padding:3px 0px 12px 3px;width:200px;">

* 湿度读值范围：0~100%RH
* 电阻读值范围：0~102200kΩ

<div style="background-color:#EAF6FD;padding:10px;border-left:5px solid #BAE3F9;margin-bottom:30px;border-radius:5px;line-height:28px;font-size:14px;"><strong>注意：</strong><br> 
插入土壤中的深度切勿超过丝印名，避免损坏其他传感器。
</div>

## 使用示例

<p><span style="background-color:#66CDAA;line-height:15px;padding:6px;font-size:12px;border-radius:3px;"><a href="images/science-sensor-example-programs.rar" download style="color:white;font-weight:bold;">科学传感器示例程序</a></span></p>

### 示例1：人体感应灯（热运动传感器）
通过热运动传感器对童芯派的LED灯进行控制，并在屏幕上显示热运动传感器检测的状态。热运动传感器的工作原理可参考 [mBuild 热运动传感器](pir-sensor.md)。

<img src="images/science-sensor-7.png" style="padding:3px 0px 12px 3px;width:400px;">

### 示例2：环境检测仪（温湿度传感器、大气压传感器）

科学传感器上的温湿度传感器可以获取当前环境的温度值和湿度值，大气压传感器可以获取当前的压强和粗略估算目前海拔高度。通过编程将传感器获取到的数值显示在童芯派的显示屏上，实现传感器数值的可视化。

<img src="images/science-sensor-8.png" style="padding:3px 0px 12px 3px;width:600px;">

### 示例3：火灾预警器（MQ2气体传感器、火焰传感器）

科学传感器集成了MQ2气体传感器和火焰传感器。这两个传感器可以用于家庭安全方面，如气体的泄露、检测火焰等。综合应用这两个传感器可以实现一个简易可燃气体与火焰检测的警报装置，当传感器被触发的时候，童心芯派就会响起警报。

<img src="images/science-sensor-9.png" style="padding:3px 0px 12px 3px;width:600px;">

火焰传感器的工作原理是基于红外的原理实现的（见：[mBuild火焰传感器](flame-sensor.md)），即人体产生的红外也可能触发火焰传感器，因此在实际的使用当中也可以火焰传感器检测到的火焰值进行判定。

<img src="images/science-sensor-10.png" style="padding:3px 0px 12px 3px;width:200px;">

### 示例4：指南针（地磁传感器）

指南针的功能需要使用到科学传感器上的地磁传感器。由于不同区域的磁偏角是存在差异的，因此<strong>在使用地磁传感器之前需要对传感器进行校准。在程序一开始需要开启地磁传感器的校准功能。校准方法就是将传感器平面转一圈，然后可以关闭传感器的校准功能。注意，校正需要在无磁环境下进行，比较理想的场景是在户外进行校正。</strong>

<img src="images/science-sensor-11.png" style="padding:3px 0px 12px 3px;width:200px;">

地磁传感器角度读值范围： –180°~+180°（0°指向北极点）

<img src="images/science-sensor-12.png" style="padding:3px 0px 12px 3px;width:300px;">

指南针程序：
<img src="images/science-sensor-13.png" style="padding:3px 0px 12px 3px;width:600px;">

### 示例5：土壤湿度检测仪（土壤湿度传感器）

科学传感器的土壤湿度传感器是电阻式的，当土壤湿度提高时，水分能够溶解部分土壤中包含的离子，从而使得土壤的电阻变小，也因此土壤湿度传感器的读值变大。而当土壤中的含水量进一步提高时（极限的情况下，想象往水里扔一些泥土），土壤中的离子浓度反而会被过量的水稀释，导致导电能力下降，电阻值变高。因此你会发现，该土壤湿度传感器在纯水中的读值将小于在湿润土壤中的读值（尽管前者的含水量明显更高），这是因为湿润土壤中的离子浓度更高，电阻更小。

通过程序你可以观察到土壤湿度变化与传感器电阻值变化。

<img src="images/science-sensor-14.png" style="padding:3px 0px 12px 3px;width:600px;">

### 示例6：触摸灯（触摸传感器）

触摸传感器可以检测是否有人手触摸，用于触摸控制。示例程序通过触摸传感器对童芯派的灯光进行控制。

<img src="images/science-sensor-15.png" style="padding:3px 0px 12px 3px;width:300px;">