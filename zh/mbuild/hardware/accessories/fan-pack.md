# 小风扇

借助不同的连接线，小风扇组件得以适配掌上扩展板、mBot2与直流电机驱动。
小风扇可以在它们的驱动下进行正转和反转，从而控制风力及风向。


<img src="images/fan-pack-1.png" style="padding:3px 0px 12px 3px;width:300px;">

## 兼容性说明

<table  cellpadding="10px;" cellspacing="15px;" style="text-align:left;;margin-bottom:20px;">
<tr>
<th width="33%;" style="border: 1px solid black;">驱动方式</th>
<th width="33%;" style="border: 1px solid black;">使用连接线</th>
</tr>

</td>
<td style="border: 1px solid black;">掌上扩展板-直流电机接口</td>
<td style="border: 1px solid black;"><img src="images/water-pump-2.png" style="padding:3px 0px 12px 3px;width:200px;"></td>
</tr>

<tr>
<td style="border: 1px solid black;">mBot2 扩展板-直流电机接口</td>
<td style="border: 1px solid black;"><img src="images/water-pump-2.png" style="padding:3px 0px 12px 3px;width:200px;"></td>
</tr>

<tr>
<td style="border: 1px solid black;">mBuild 直流电机驱动模块</td>
<td style="border: 1px solid black;"><img src="images/water-pump-3.png" style="padding:3px 0px 12px 3px;width:200px;"></td>
</tr>

<tr>
<td style="border: 1px solid black;">其他支持 5V 电压输出，且峰值输出电流在 1A 以上的直流电机驱动接口</td>
<td style="border: 1px solid black;">自行焊接或匹配的对应驱动的连接线</td>
</tr>

</table>
