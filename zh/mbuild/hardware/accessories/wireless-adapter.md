# 无线适配器

## 使用场景

由于台式电脑本身不带蓝牙硬件，或由于旧款的笔记本蓝牙版本低于 4.0 或驱动异常导致的兼容性问题，电脑端慧编程无法直接与 Makeblock 硬件建立蓝牙连接。

童心制物（Makeblock）无线适配器能够有效解决这一问题，实现电脑与 Makeblock 硬件设备，如童芯派、程小奔、蓝牙版 mBot、mBot Ranger 等的无线连接，从而无线地实现对硬件的在线调试，以及对童芯派、光环板、程小奔硬件的程序烧录。

<img src="images/wireless-adapter-1.png" style="padding:3px 0px 12px 3px;width:200px;">

如需检查您手中的电脑设备是否符合 Makeblock 硬件设备的蓝牙连接要求，请查阅[蓝牙兼容性](accessories/bluetooth-compatibility.md)。

## 指示灯状态说明

<table  cellpadding="10px;" cellspacing="15px;" style="text-align:left;;margin-bottom:20px;">

</td>
<td width="20%;" style="border: 1px solid black;">慢闪</td>
<td style="border: 1px solid black;">待机状态
</td>
<td style="border: 1px solid black;">适配器搜寻上一次配对成功的设备并自动自行配对</td>
</tr>
<tr>
<td style="border: 1px solid black;">快闪</td>
<td style="border: 1px solid black;">配对状态</td>
<td style="border: 1px solid black;">适配器将搜寻新设备进行配对</td>
</tr>
<tr>
<td style="border: 1px solid black;">常亮</td>
<td style="border: 1px solid black;">配对成功</td>
<td style="border: 1px solid black;"></td>
</tr>

</table>

## 硬件连接

1. 揭开保护盖。

    <img src="images/wireless-adapter-2.png" style="padding:3px 0px 12px 3px;width:200px;">

2. 将无线适配器插入电脑主机的 USB 接口， 指示灯慢闪。

    <img src="images/wireless-adapter-3.png" style="padding:3px 0px 12px 3px;width:200px;">

## 设备配对

本文以连接童芯派为例（此例中，童芯派使用掌上扩展板供电）。
1. 童芯派开机。

    <img src="images/wireless-adapter-4.png" style="padding:3px 0px 12px 3px;width:200px;">

2. 按下无线适配器上的按键，指示灯闪烁频率加快，进入配对状态。

    <img src="images/wireless-adapter-5.png" style="padding:3px 0px 12px 3px;width:200px;">

3. 将设备靠近无线适配器，自动进行配对。

    <img src="images/wireless-adapter-6.png" style="padding:3px 0px 12px 3px;width:200px;">

    无线适配器的指示灯常亮，则说明配对成功。

    <img src="images/wireless-adapter-7.png" style="padding:3px 0px 12px 3px;width:200px;">

## 软件连接

设备与无线适配器配对成功后，还需要在慧编程中连接，才能将设备连接到慧编程。

### 慧编程

以童芯派为例，在设备页签下点击“连接”，然后在弹出的“连接设备”对话框中选择无线适配器当前使用的串口。

<img src="images/wireless-adapter-8.png" style="padding:3px 0px 12px 3px;width:500px;">

连接成功后，慧编程会提示连接成功。

<img src="images/wireless-adapter-9.png" style="padding:3px 0px 12px 3px;width:200px;">

<strong>现在，你可以在慧编程中对童芯派进行编程啦！</strong>