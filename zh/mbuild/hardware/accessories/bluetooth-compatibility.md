# 蓝牙兼容性

## 兼容性声明

童芯派、光环板、程小奔要求电脑的蓝牙版本为 4.0 及以上版本，才能通过蓝牙连接模式，直接同电脑建立连接。若您的电脑缺少蓝牙设备或版本过低，可以考虑购买无线适配器或含无线适配器版本的套装。

## 检查电脑蓝牙版本

检查Windows电脑的蓝牙版本，请进行以下操作： 
1. 按 Win+X 打开开始菜单，并选择“设备管理器”。
2. 在“蓝牙”下，你将看到多个蓝牙设备。
3. 选择你的蓝牙品牌，然后点击右键查看“属性”；
4. 点击“高级”查看固件版本。LMP编号显示电脑使用的蓝牙版本。
以下是LMP编号和对应蓝牙版本：
LMP 编号
蓝牙版本
LMP 9.x
Bluetooth 5.0
LMP 8.x
Bluetooth 4.2
LMP 7.x
Bluetooth 4.1
LMP 6.x
Bluetooth 4.0
LMP 5.x
Bluetooth 3.0 + HS
LMP 4.x
Bluetooth 2.1 + EDR
LMP 3.x
Bluetooth 2.0 + EDR
LMP 2.x
Bluetooth 1.2
LMP 1.x
Bluetooth 1.1
LMP 0.x
Bluetooth 1.0b
其他解决方案
您也可以考虑购买第三方蓝牙4.0适配器来连接设备到慧编程。但考虑到 Windows 系统本身的复杂性以及市面上蓝牙适配器可能存在的参数虚标问题，您可能依旧无法建立起正常的无线连接。您可参考如下操作安装和卸载蓝牙 4.0 驱动，也欢迎向我们反馈您在使用如下方法时遇到的问题。
注：安装蓝牙 4.0 驱动会导致 Windows 系统蓝牙无法使用，如要恢复，需卸载蓝牙 4.0 驱动，请参考“卸载蓝牙 4.0 驱动并恢复Windows系统蓝牙”。
安装蓝牙4.0驱动
在Windows电脑安装蓝牙4.0驱动的驱动，请进行以下操作：
1. 下载 Zadig工具。
2. 启动Zadig，选择“Options”并勾选“List All Devices”。
3. 从下拉菜单选择你的设备，然后点击“Replace Driver”
查看电脑蓝牙硬件信息：
1. 按 Win+X 打开开始菜单，并选择“设备管理器”。
2. 找到“通用串行总线设备”并点击。
3. 右键点击"Bluetooth Radio"，选择“属性”。
4. 选择“详细信息”，从“属性”的下拉菜单中选择“硬件Id”。
卸载蓝牙 4.0 驱动并恢复Windows系统蓝牙
如您要恢复系统蓝牙，则需先卸载蓝牙4.0驱动，请按如下步骤操作：
1. 按 Win+X 打开开始菜单，并选择“设备管理器”。
2. 找到“通用串行总线设备”并点击。
3. 右键点击"Bluetooth Radio"，选择“属性”。
4. 选择“驱动程序”，并点击“卸载设备”。
5. 勾选“删除此设备的驱动程序软件”，并点击“卸载”。
6. 卸载完成后，请重启电脑。

<img src="images/connect-1.png" style="padding:3px 0px 12px 3px;width:300px;">

### 快速固定示意

<img src="images/connect-2.png" style="padding:3px 0px 12px 3px;width:300px;">

### 堆叠示意

<img src="images/connect-3.png" style="padding:3px 0px 12px 3px;width:300px;">
