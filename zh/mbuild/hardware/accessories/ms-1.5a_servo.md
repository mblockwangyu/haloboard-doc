# MS-1.5A 舵机

MS-1.5A 是 Makeblock 自主研发的平台舵机，该舵机尺寸与市面常见的 9g 小舵机相当。

<img src="images/ms-1.5a_servo-1.png" style="padding:3px 0px 12px 3px;width:300px;">

## 功能对比

<table  cellpadding="10px;" cellspacing="15px;" style="text-align:left;;margin-bottom:20px;">
<tr>
<th width="33%;" style="border: 1px solid black;"></th>
<th width="33%;" style="border: 1px solid black;">使用连接线</th>
<th width="33%;" style="border: 1px solid black;">使用连接线</th>
</tr>

</td>
<td style="border: 1px solid black;">掌上扩展板-直流电机接口</td>
<td style="border: 1px solid black;"><img src="images/water-pump-2.png" style="padding:3px 0px 12px 3px;width:200px;"></td>
</tr>

<tr>
<td style="border: 1px solid black;">mBot2 扩展板-直流电机接口</td>
<td style="border: 1px solid black;"><img src="images/water-pump-2.png" style="padding:3px 0px 12px 3px;width:200px;"></td>
</tr>

<tr>
<td style="border: 1px solid black;">mBuild 直流电机驱动模块</td>
<td style="border: 1px solid black;"><img src="images/water-pump-3.png" style="padding:3px 0px 12px 3px;width:200px;"></td>
</tr>

<tr>
<td style="border: 1px solid black;">其他支持 5V 电压输出，且峰值输出电流在 1A 以上的直流电机驱动接口</td>
<td style="border: 1px solid black;">自行焊接或匹配的对应驱动的连接线</td>
</tr>

</table>
