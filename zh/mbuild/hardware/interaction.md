# 交互类

* [按钮](interaction/button.md)
* [角度传感器](interaction/angle-sensor.md)
* [滑动电位器](interaction/slide-potentiometer.md)
* [摇杆](interaction/joystick.md)
* [多路触摸](interaction/multi-touch.md)