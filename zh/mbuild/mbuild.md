<img src="mbuild-all.png" style="padding:3px 0px 12px 3px;width:600px;">

# mbuild

mbuild 是 Makeblock（童心制物）研发的新一代电子模块平台，它在极度小巧的同时又高度智能，涵盖丰富的电子模块功能，并能与主流开源硬件结合使用。mbuild 电子模块无需编程即可使用，也能够通过 mBlock 或 MU 以积木块或是 Python 的方式对其编程进行控制。 mbuild 电子模块平台将能够充分满足您在创意物化、综合实践、项目制教学、编程普及、人工智能科普、机器人赛事等场景中对于电子模块的需求。

* [硬件说明](hardware.md)
    * [能源类](hardware/power.md)
    * [通信类](hardware/communication.md)
    * [交互类](hardware/interaction.md)
    * [传感器类](hardware/sensors.md)
    * [显示类](hardware/display.md)
    * [灯效类](hardware/light.md)
    * [播放类](hardware/sound.md)
    * [运动类](hardware/motion.md)
    * [外设类](hardware/peripheral.md)
    * [配件类](hardware/accessories.md)