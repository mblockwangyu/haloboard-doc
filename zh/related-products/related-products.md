# 相关产品

* [光环板标准套件](standard-kit.md)
* [人工智能大班教学套件](classroom.md)
* [人工智能创意实践扩展包](creator.md)
* [人工智能科学探究扩展包](scientist.md)
* [人工智能教育工具箱扩展包](education-toolbox.md)