# 人工智能大班教学套件

![](images/parts-list.jpg)

人工智能大班教学套装提供光环板主控和mBuild电源模块，以及扬声器、电机、测距、LED点阵屏、光线传感器，旨在帮助零基础的学生了解人工智能，体验人工智能在生活的简单应用，学习并掌握图形化编程能力。套装配备《人工智能编程入门》、《人工智能体验》、《人工智能与物联网创意项目制作》课程以及丰富的教学案例，能够激发学生们的好奇心和想象力，建立学生对人工智能的认识和感悟。

## 主要模块

<table style="margin-left: auto; margin-right: auto" width="800px"  align="center">
        <tr>
            <td>
<strong>主机：</strong><br>         
<a href=../README.md> 光环板</a><br>
<strong>传感器：</strong><br> 
<a href=../mbuild/hardware/sensors/ranging-sensor.md> 测距传感器</a><br>
<a href=../mbuild/hardware/sensors/light-sensor.md> 光线传感器</a><br>
</td>

<td>


<strong>输出：</strong><br> 
<a href=../mbuild/hardware/output-modules/speaker.md> 扬声器</a><br>
<a href=../mbuild/hardware/motion/motor-driver.md> 电机驱动</a><br>
<a href=../mbuild/hardware/display/led-matrix.md> 8×16 蓝色LED点阵</a><br>

<strong>其他：</strong><br> 
<a href=../mbuild/hardware/power/power.md> 电源模块</a><br>

</td>
        </tr>
    </table>
    

 ## 了解光环板和 mBuild 电子模块

有关光环板的简介和快速上手指南，参见[光环板帮助文档](http://docs.makeblock.com/halocode/zh)。

有关 mBuild 电子模块的功能和特性，参见[mBuild 硬件说明](http://docs.makeblock.com/halocode/zh/mbuild/mbuild.html)。

## 获取在线课程

我们为此套件提供了配套的在线课程，可登陆[慧课程](https://edu.makeblock.com/)免费获取。

如你已有慧编程账号，可直接使用该账号登陆，然后通过以下步骤获取课程：

1、选择“课程资源” > “官方课程”，然后点击“课程超市”。

<img src="images/lessons.png" width="600px;" style="padding: 5px 5px 15px 0px;">

2、在搜索框中输入“人工智能编程入门”并点击“搜索”。

<img src="images/lessons-1.png" width="600px;" style="padding: 5px 5px 15px 0px;">

3、点击“免费获取”。

<img src="images/lessons-2.png" width="600px;" style="padding: 5px 5px 15px 0px;">

<div style="background-color:#EAF6FD;padding:10px;border-left:5px solid #BAE3F9;margin-bottom:30px;border-radius:5px;line-height:28px;font-size:14px;"><strong>注：</strong><br> 
你可以使用同样的方法搜索并获取《人工智能体验》和《人工智能与物联网创意项目制作》课程。
</div>