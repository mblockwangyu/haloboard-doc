# 人工智能教育工具箱扩展包
## 主要模块
<table style="margin-left: auto; margin-right: auto" width="800px"  align="center">
        <tr>
            <td>


<strong>传感器：</strong><br> 
<a href=../mbuild/hardware/sensors/pir-sensor.md> 热运动传感器</a><br>
<a href=../mbuild/hardware/sensors/ranging-sensor.md> 测距传感器</a><br>
<a href=../mbuild/hardware/sensors/light-sensor.md> 灯光传感器</a><br>
<a href=../mbuild/hardware/sensors/ultrasonic-sensor.md> 超声波传感器</a><br>
<a href=../mbuild/hardware/sensors/sound-sensor.md> 声音传感器</a><br>
<a href=../mbuild/hardware/sensors/temperature-sensor.md> 温度传感器</a><br>
<a href=../mbuild/hardware/sensors/dual-color-sensor.md> 双路颜色传感器</a><br>
<a href=../mbuild/hardware/sensors/motion-sensor.md> 运动传感器</a><br>
<a href=../mbuild/hardware/sensors/gas-sensor.md> MQ2气体传感器</a><br>
<a href=../mbuild/hardware/sensors/humiture-sensor.md> 温湿度传感器</a><br>
<a href=../mbuild/hardware/sensors/magnetic-sensor.md> 磁敏传感器</a><br>
<a href=../mbuild/hardware/sensors/flame-sensor.md> 火焰传感器</a><br>
<a href=../mbuild/hardware/sensors/soil-moisture-sensor.md> 土壤湿度传感器</a><br>
<a href=../mbuild/hardware/sensors/smart-camera.md> 视觉模块</a><br>
</td>

<td>

<strong>交互：</strong><br> 
<a href=../mbuild/hardware/interaction/angle-sensor.md> 角度传感器</a><br>
<a href=../block-reference/slider.md> 滑动电位器</a><br>
<a href=../mbuild/hardware/interaction/button.md> 按钮</a><br>
<a href=../mbuild/hardware/interaction/multi-touch.md> 多路触摸</a><br>
<a href=../mbuild/hardware/interaction/joystick.md> 摇杆</a><br>

<strong>输出：</strong><br> 
<a href=../mbuild/hardware/output-modules/speaker.md> 扬声器</a><br>
<a href=../mbuild/hardware/motion/motor-driver.md> 电机驱动</a><br>
<a href=../mbuild/hardware/motion/servo-driver.md> 舵机驱动</a><br>
<a href=../mbuild/hardware/display/led-matrix.md> 8×16 蓝色LED点阵</a><br>
<a href=../mbuild/hardware/light/led-driver.md> 彩灯驱动</a><br>

<strong>其他：</strong><br> 
<a href=../mbuild/hardware/communication/ir.md> 红外收发</a><br>
<a href=../mbuild/hardware/power/power.md> 电源模块</a><br>
<a href=../mbuild/hardware/accessories/extend-modules.md> 延长模块</a><br>
<a href=../mbuild/hardware/light/rgb-led.md> RGB灯</a><br>
<a href=../mbuild/hardware/communication/bluetooth.md> 蓝牙</a><br>


</td>
        </tr>
    </table>



---