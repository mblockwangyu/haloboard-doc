# 光环板硬件原理图

如果你想深入了解光环板的硬件设计，可以下载光环板的硬件原理图。

<img src="images/reference-design.png" width="600px;" style="padding:3px 3px 20px 3px">

<p><span style="background-color:#66CDAA;line-height:30px;padding:12px;font-size:16px;border-radius:3px;"><a href="halocode_v1_0.rar" download style="color:white;font-weight:bold;">硬件原理图下载</a></span></p>