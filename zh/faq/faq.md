<img src='images/halo-front.png' width='240px;'>

# 光环板FAQ

## 1. 为什么在 PC 端慧编程中连接时，无法发现设备？

光环板用 USB 线连接至电脑后，打开慧编程点击 「**连接**」 按钮，显示 「**未找到可连接设备**」，如下图所示:

<img src="images/no-device-1.png" width="800px;" style="padding:5px 5px 20px 5px;">
 
A：出现这个现象，很可能是以下两种原因造成的，如下针对这两种情况，给出了对应的解决方法。

**1\) USB 数据线导致的问题**

市面上存在一类仅具备电力传输功能的数据线，使用该类数据线连接光环板和电脑，会导致电脑端无法正常检测到光环板。

所以手边有多余的数据线的话，可以多试几根线看看。建议用安卓手机自带的 Micro USB 线（如果有的话），或者[点击购买](https://detail.tmall.com/item.htm?id=565555931655&skuId=3703559825974) Makeblock 官方出品的 「100cm USB 数据线」。如下图所示：

<img src="images/no-device-2.png" width="400px;" style="padding:5px 5px 20px 5px;">


**2\) 驱动文件未正确安装**

由于电脑上运行了防护软件（系统自带或者第三方），导致慧编程在安装时，驱动文件未能正确安装，所以会出现电脑检测不到光环板的情况。

可以先将光环板从电脑端拔下，然后根据电脑操作系统，下载光环板所需要的驱动文件: 
- Windows 驱动文件下载：[http://www.wch.cn/download/CH341SER_EXE.html](http://www.wch.cn/download/CH341SER_EXE.html)
- macOS 驱动文件下载：[http://www.wch.cn/download/CH341SER_MAC_ZIP.html](http://www.wch.cn/download/CH341SER_MAC_ZIP.html)

手动安装驱动文件后，重新用 USB 线将光环板连接至电脑。再次点击慧编程中的「**连接**」 按钮，看是否有 COM 口出现（如下图所示）。

<img src="images/no-device-3.png" width="600px;" style="padding:5px 5px 20px 5px;">

---

## 2. 为什么在慧编程中连接时，显示「扫描串口中」?

在慧编程 PC 端中连接光环板时，可能会遇到下图这种情况，这是因为电脑的系统服务， 阻止了慧编程运行时所需的一些进程导致的。 

<img src="images/scan-serial-1.png" width="400px;" style="padding:5px 5px 20px 5px;">

**Windows 用户**

如果是 Windows 电脑，可尝试退出杀毒软件（360 或电脑管家等⼯具）和系统自带防火墙（防火墙关闭⽅法可网络搜索教程），然后重装慧编程软件。

**MacOS 用户**

如果是苹果电脑，可在「**安全性与隐私**」设置中，打开相应设置 「**系统偏好设置...**」 > 「**安全性与隐私**」 > 「**通用**」。

<img src="images/scan-serial-2.png" width="400px;" style="padding:5px 5px 20px 5px;">

<img src="images/scan-serial-3.png" width="500px;" style="padding:5px 5px 20px 5px;">

<img src="images/scan-serial-4.png" width="600px;" style="padding:5px 5px 20px 5px;">

点击「**允许**」按钮，然后重启电脑，光环板就正常扫描连接。

---

## 3. 如何给光环板供电？

光环板可接受的电压范围为 **DC 3-5V**，超出这一范围将可能引起光环板工作异常。目前建议使用以下两种供电方式：

1. 通过电池接口，外接电池电池盒进行供电（需满足上述电压范围）；
2. 使用 Micro USB 线连接电脑、移动电源或手机充电头进行供电（电压输出规格「5V 1A」左右）。请勿使用带有快充功能的手机充电头，因为其输出电压可能会高于 5V。

---

## 4. 除了 PC 端之外，可以用手机或者平板给光环板进行编程吗？

暂时仅支持 PC、Mac 和 Web 端的编程，请关注官方的信息更新。

---

## 5. 除了慧编程之外，能用其他软件对光环板进行编程么？

光环板目前仅支持用慧编程进行编程，慧编程有着强大的功能体验：虚实结合、积木式编程和 Python 编程都在一款软件统统搞定！！

---

## 6. 除了Scratch之外，可以用Python或者Arduino对光环板进行编程么？

目前光环板支持用慧编程软件进行基于Scratch 3.0的积木式编程和Python文本编程。

---

## 7. 如何使用联网的功能？

在慧编程的「**Wi-Fi**」类积木中找到 <span style="background-color:#76CE14;color:white;padding:3px; font-size: 12px">开始连接无线网络（）密码（）</span> 积木即可.

<img src="images/wifi-1.png" width="800px;" style="padding:5px 5px 20px 5px;">

<small><b>注意：</b><br>部分公共场所的网络无法连接（比如需要验证手机、微信号的酒店网络），只能通过正确的Wi-Fi名称和密码连接网络。</small>

---

## 8. 为什么 PC 端慧编程中，Wi-Fi 类积木块里有些是灰色的？

如果在使用慧编程时，没有登录账号，那么在「**Wi-Fi**」的积木里，会遇到有一些积木块是灰色的情况。如下图所示：

<img src="images/wifi-2.png" width="800px;" style="padding:5px 5px 20px 5px;">

遇到这种情况，请在界面右上角找到 **注册**/**登录** 入口，**注册**/**登录** 慧编程后，就可正常使用这些积木块啦。

<img src="images/wifi-3.png" width="800px;" style="padding:5px 5px 20px 5px;">


---

## 9. 如何使用AI语音识别的功能？

在慧编程的「**Wi-Fi**」类积木中找到 <span style="background-color:#76CE14;color:white;padding:3px; font-size: 12px">识别（）语音（）秒钟</span> 积木即可。

<img src="images/ai.png" width="800px;" style="padding:5px 5px 20px 5px;">

**注意：**<br>

1. 语音识别之前，必须连接网络；
2. 语音识别需要一些时间，速度取决于网络状况，当光环板接收到识别结果之后，才会执行下一个积木；
3. 目前除了汉语、英语之外的语言识别不可用。

---

## 10. 我可以用micro:bit或者RJ25体系的配件来玩光环板么？

那真是太棒了！不过有一些传感器和配件是不支持的，比如使用I2C等串口进行通信的传感器不能使用。

使用外接电子模块，有以下注意事项：  
1、传感器应支持 3.3V 的工作电压  
2、I/O 口的带载能力为10mA  
3、3V3 口的带载能力为500mA  
4、光环板4 Pin 连接器的带载能力为1A，达到此数值的前提为：电源输入电压大于3-5V，输入电流大于3.5A。

---

## 11. 我想接更多的外接扩展进行创作，光环板可以外接哪些扩展呢？

那真是太棒了！不过有一些传感器和配件是不支持的，比如使用I2C等串口进行通信的传感器不能使用。

使用外接电子模块，有以下注意事项：  
1、传感器应支持 3.3V 的工作电压  
2、I/O 口的带载能力为10mA  
3、3V3 口的带载能力为500mA  
4、光环板4 Pin 连接器的带载能力为1A，达到此数值的前提为：电源输入电压大于3V，输入电流大于3.5A。

---

## 12.光环板无法上传或运行程序？

一些错误的操作或异常情况可能会导致光环板无法上传或运行程序，请按如下顺序依次排查并尝试解决，如各类方法均不奏效，请联系当地经销商或 Makeblock 售后获得进一步的帮助。

**1\) 光环板是否供电正常？**

检查光环板背面 micro-USB 左侧的蓝色指示灯是否常亮，当光环板未被供电时，该指示灯会处于熄灭状态。

<img src="images/power.png" width="400px;" style="padding:5px 5px 20px 5px;">

不正确的电源也会导致光环板无法正常工作。目前有两种方式可以为光环板供电：
- 通过电池接口外接电池电池盒进行供电
- 使用 Micro USB 线连接电脑、移动电源或电源适配器进行供电。

但光环板可接受的电压范围为 DC 3-5V，超出这一范围将可能引起光环板的工作异常。因此，一些手机快充头无法被用在光环板上，因为其输出电压可能高于5V。

**2\) 程序本身是否存在BUG？**

你的程序可能并不能如你预想一般被运行，试着写一些简单的程序（比如单纯点亮灯），来确保上传的程序无法运行是程序之外的问题导致的。

**3\) USB数据线是否具备数据传输功能，其连接是否稳定？**

市面上存在一类仅具备电力传输功能的数据线，使用该类数据线会导致程序上传失败。电脑上的接口松动也可能导致程序因为连接问题而无法上传 。

**4\) 固件版本与积木版本是否匹配？**

慧编程中的积木会持续更新以提供用户更多的功能和更好的体验，但新增的积木需要新版本的固件进行配合才能使用，如果你的光环板使用了新的积木却依旧保持在旧的固件版本，那么则很可能导致程序上传失败或上传成功但无法运行的情况。当固件需要更新时，软件界面中一般会有提示。（注意：如果你通过一些渠道获得了非官方的固件版本，这些固件版本在软件中将不会被提示更新）

当固件版本过低时，软件会进行提示：

<img src="images/upgrade-0.png" width="400px;" style="padding:5px 5px 20px 5px;">

**5\) 固件是否被损坏?**

一些小概率的异常情况可能导致光环板的供电电压进入不稳定状态，主芯片在电压不稳定时将有小概率影响光环板的FLASH，这将导致固件存在因异常操作或特定环境下丢失的可能性。此时软件不一定能够识别并提示这一问题，但按如下步骤重新更新固件可以修复该问题。

<img src="images/upgrade-01.png" width="400px;" style="padding:5px 5px 5px 5px;">
<td width="10%;">

<img src="images/upgrade-1.png" width="400px;" style="padding:5px 5px 5px 5px;">

<img src="images/upgrade-2.png" width="300px;" style="padding:5px 5px 20px 5px;">

---

## 13. 光环板的在线模式异常？

由于当前技术瓶颈的限制，一些操作将会降低光环板在在线模式下的体验：

**1\) 切换模式时间较慢**

光环板从一个模式切换到另一个模式会伴随一次重启，这会消耗一定的时间（约5秒），在模式切换期间，光环板属于不可控的状态，这是正常现象。光环板重启时，灯环会以绿色进行闪烁。在烧录有MESH相关代码的情况下，光环板需要重启两次，这会导致更长的切换时长。

**2\) 在线模式的运行速度受限**

在线模式的代码是运行在上位机上的，其速度受限于上位机与硬件的通信延迟，因此在线模式下，积木块的执行速度会慢于将程序直接上传到硬件中进行执行，这在通过蓝牙与光环板建立连接时尤为明显。

**3\) 在线模式的多线程受限**

同样因为程序在在线模式下执行在上位机上，一些多线程代码在在线模式下不能被很好地执行。如想要获得最佳的多线程体验，请使用上传模式。

**4\) Wi-Fi MESH程序与蓝牙功能冲突**

当烧录了 MESH 相关的功能代码后，在线模式会在蓝牙连接的情况下不可用，此时，通过在“上传模式”下重新烧录一段空白代码，可以恢复“在线模式”的功能。

---

FAQ会持续更新
