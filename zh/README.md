# 光环板帮助文档

\* 如有任何技术疑问，请关注微信公众号：“Makeblock售后”

> 搜索微信号：`makeblock2013`  
> 或扫一扫  
> <img src="qr-code.jpg" width="100px;" style="padding:5px 5px 5px 0px;">

感谢您购买使用光环板！  
此帮助文档主要分为以下部分：

* [光环板](README.md)
    * [简介](tutorials/introduction.md)
    * [快速上手](tutorials/get-started.md)
    * [上传模式](tutorials/upload-mode.md)
    * [语音识别](tutorials/voice-recognition.md)
    * [帐号云广播](tutorials/cloud-messaging.md)
    * [局域网](tutorials/mesh.md)
    * [使用Python](tutorials/use-python.md)
* [光环板相关产品](related-products/related-products.md)
* [案例](examples/examples.md)
* [mbuild硬件说明](mbuild.md)
* [积木说明](block-reference/block-reference.md)
* [光环板 Python API](python-api/halocode-python-api.md)
* [mBuild 电子模块平台 Python API](python-api/mbuild-python/mbuild-python-api.md)
* [FAQ](faq/faq.md)
* [开源硬件](open-source/open-source.md)




