# 使用Python

通过使用Python控制HaloCode，您可以：
- 和实物结合，更有趣地学习Python
- 与拖放语句块相比，更方便编写有一定规模的代码，比如实现算法
- 实现积木式编程很难做到的事情，比如使用字典等数据结构

为HaloCode写的Python程序，都需要上传后才能生效。

## 开始使用Python

在脚本区右侧进行模式切换。

<img src="images/use-python-1.png" style="padding:5px 5px 20px 5px;">

**注意：请确保选中的是“设备”下的“光环板”。**

<img src="images/use-python-3.png" style="padding:5px 5px 20px 5px;">

下面是一段简单的示例代码：

```py
import halo
import event

@event.start
def on_start():
    halocode.led.on(211, 32, 32)
    time.sleep(1)
    halo.led.off_all()
```

代码编写完成之后，点击“上传到设备”，编写的Python代码就会执行。

<img src="images/use-python-2.png" style="padding:5px 5px 20px 5px;">

## 将积木块转换为Python代码

在脚本区，点击&nbsp; <img src="images/use-python-5.png" width="20px"> &nbsp;将积木块转换为Python。如下示例：

<img src="images/use-python-4.png" style="padding:5px 5px 20px 5px;">

更多代码功能，请参考[python-api文档](../python-api/halocode-python-api.md)



