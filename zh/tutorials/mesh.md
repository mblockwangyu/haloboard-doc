# 局域网

光环板之间可以组成局域网，组成局域网的设备之间可以相互通讯，使用一个光环板就可以控制其他光环板。

## 用光环板A控制光环板B

让我们新建一个项目，使用两块光环板，实现用光环板A控制光环板B。

### 启动光环板A的局域网

1\. 选择“设备”，添加光环板。用USB数据线将光环板与电脑相连，点击“连接”，连接设备。

<img src="images/mesh-1.gif" width="800px;" style="padding:5px 5px 20px 5px;">

2\. 启用上传模式

<img src="images/toggle-on-upload.gif" width="800px;" style="padding:5px 5px 20px 5px;">

3\. 添加事件类积木 <span style="background-color:#FFBF00;color:white;padding:3px; font-size: 12px">当光环板启动时</span>，和局域网类积木 <span style="background-color:#35D2B2;color:white;padding:3px; font-size: 12px">建立名为（mesh1）的局域网</span>。

<img src="images/mesh-2.gif" width="800px;" style="padding:5px 5px 20px 5px;">

### 光环板A发送局域网广播

4\. 添加事件类积木 <span style="background-color:#FFBF00;color:white;padding:3px; font-size: 12px">当按钮被按下时</span>，和局域网类积木 <span style="background-color:#35D2B2;color:white;padding:3px; font-size: 12px">面向局域网广播消息（）</span>，并将消息命名为“light”。

<img src="images/mesh-3.gif" width="800px;" style="padding:5px 5px 20px 5px;">

5\. 上传程序至光环板A。
   
<img src="images/mesh-4.gif" width="800px;" style="padding:5px 5px 20px 5px;">

### 启动光环板B的局域网

6\. 选择“设备”，点击“+”添加设备，再次选中光环板。添加一根USB数据线将光环板B与电脑相连，点击“连接”。
   
<img src="images/mesh-5.gif" width="800px;" style="padding:5px 5px 20px 5px;">

7\. 启用上传模式

<img src="images/mesh-6.gif" width="800px;" style="padding:5px 5px 20px 5px;">

8\. 添加事件类积木 <span style="background-color:#FFBF00;color:white;padding:3px; font-size: 12px">当光环板启动时</span>，和局域网类积木 <span style="background-color:#35D2B2;color:white;padding:3px; font-size: 12px">加入名为（mesh1）的局域网</span>。

<img src="images/mesh-7.gif" width="800px;" style="padding:5px 5px 20px 5px;">

### 光环板B接收局域网广播

9\. 添加一个局域网类积木 <span style="background-color:#35D2B2;color:white;padding:3px; font-size: 12px">当接收到局域网广播（）</span>，输入“light”。再添加一个灯光类积木 <span style="background-color:#8521D3;color:white;padding:3px; font-size: 12px">全部LED显示（）色</span> 并修改灯效和控制类积木 <span style="background-color:#FFAB19;color:white;padding:3px; font-size: 12px">等待（）秒</span>。最后再添加一个灯光类积木 <span style="background-color:#8521D3;color:white;padding:3px; font-size: 12px">熄灭所有灯光</span>，让所有LED灯在1秒后熄灭。
   
<img src="images/mesh-8.gif" width="800px;" style="padding:5px 5px 20px 5px;">

10\. 上传程序至光环板B。
   
<img src="images/mesh-9.gif" width="800px;" style="padding:5px 5px 20px 5px;">

### 效果展示

11\. 按下光环板A的按钮，看光环板B灯效变化。
    
<img src="images/mesh-11.gif" width="800px;" style="padding:5px 5px 20px 5px;">

**进阶篇**

多个光环板可以加入同一局域网。所以使用一个光环板可以远程控制两个及以上光环板。

想一想下图的效果要怎么实现吧！

<img src="images/mesh-10.gif" width="800px;" style="padding:5px 5px 20px 5px;">

<p><span style="background-color:#66CDAA;line-height:30px;padding:12px;font-size:16px;border-radius:3px;"><a href="../examples/code/局域网广播.mblock" download style="color:white;font-weight:bold;">下载代码</a></span></p>

---