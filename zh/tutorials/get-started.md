# 快速上手

本章将介绍通过慧编程快速使用光环板，开启编程之旅。

## 连接光环板

1\. 选中“设备”，点击“+”添加设备。

<img src="images/get-started-1.png" width="300px;" style="padding:5px 5px 20px 5px;">

2\. 在弹出的设备库页面，选中“光环板”，点击“确定”。

<img src="images/get-started-2.png" width="800px;" style="padding:5px 5px 20px 5px;">

<div style="background-color:#E7F1F9;padding:20px;border-top:5px solid #6EACDB;font-size:14px;line-height:28px;margin-left:10px;margin-bottom:15px;border-radius:5px;width:360px;"><strong>小技巧：</strong><br>
&#9755; 点击左上角的 &star; 将光环板设置为常用设备<br>
<img src="images/frequent-device.png" style="padding:16px 0px 15px 10px;"><br>
</div>

3\. 使用 Micro-USB 数据线将光环板连接到电脑的 USB 口。

**<small>注：光环板独立包装中不包含Micro-USB 数据线，需额外购买。</small>**

<img src="images/get-started-3.png" width="300px;" style="padding:5px 5px 20px 5px;">

4\. 选中“光环板”，点击“连接”。

<img src="images/get-started-4.png" width="300px;" style="padding:5px 5px 20px 5px;">

5\. 连接设备窗口会弹出，慧编程会自动检测光环板的串口，请点击“连接”。

<img src="images/get-started-5.png" width="300px;" style="padding:5px 5px 20px 5px;">

6\. 启用上传模式

<img src="images/upload-mode-1.png" style="padding:5px 5px 20px 5px;">

## 新建一个光环板项目

让我们先从一个简单的项目开始吧。当我们摇晃光环板时，LED灯环会亮起，之后熄灭。

1\. 从事件类积木拖取一个 <span style="background-color:#FFBF00;color:white;padding:3px;font-size:12px;">当光环板摇晃时</span> 积木到脚本区。

<img src="images/get-started-6.gif" width="800px;" style="padding:5px 5px 20px 5px;">

2\. 从灯光类积木拖取一个 <span style="background-color:#8521D3;color:white;padding:3px;font-size:12px;">显示（）</span> 积木，来控制光环板的LED灯环。

<img src="images/get-started-7.gif" width="800px;" style="padding:5px 5px 20px 5px;">

3\. 从控制类积木拖取一个 <span style="background-color:#FFAB19;color:white;padding:3px;font-size:12px;">等待（）秒</span> 积木，再添加一个灯光类积木 <span style="background-color:#8521D3;color:white;padding:3px;font-size:12px;">熄灭所有灯光</span>，让光环板的LED灯环在1秒后熄灭。

<img src="images/get-started-8.gif" width="800px;" style="padding:5px 5px 20px 5px;">

4\. 点击“上传到设备”将程序上传到光环板。

<img src="images/get-started-9.gif" width="800px;" style="padding:5px 5px 20px 5px;">

5\. 试着摇晃一下光环板吧！

![](get-started-10.gif)