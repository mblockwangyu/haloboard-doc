# 语音识别

光环板内置的麦克风和Wi-Fi功能相结合，可以实现语音识别相关的应用。通过接入互联网，可以使用各大主流科技公司提供的语音识别服务，像是微软语音识别服务。使用联网功能需要登陆慧编程账号。

## 注册/登陆慧编程

点击工具栏右侧的登陆/注册按钮，依据提示登陆/注册账号。

<img src="images/login.gif" width="800px;" style="padding:5px 5px 20px 5px;">

## 启用上传模式

点击启用上传模式。

<img src="images/toggle-on-upload.gif" width="800px;" style="padding:5px 5px 20px 5px;">

## 新建语音识别项目

我们将新建一个语音识别项目，使用语音来点亮光环板的LED灯。

### 连接网络

1\. 添加事件类积木 <span style="background-color:#FFBF00;color:white;padding:4px; font-size: 12px;border-radius:3px;">当光环板启动时</span>。使用帐号云广播需要连接网络。添加Wi-Fi类积木 <span style="background-color:#76CE14;color:white;padding:4px; font-size: 12px;border-radius:3px;">开始连接无线网络（）密码（）</span>，输入附近Wi-Fi的名称和密码。

<img src="images/connect-wifi-1.gif" width="800px;" style="padding:5px 5px 20px 5px;">


2\. 网络连接成功后，我们需要一个信号来告诉我们网络连接成功。添加控制类积木 <span style="background-color:#FFAB19;color:white;padding:4px; font-size: 12px;border-radius:3px;">等待（）</span>，拖入Wi-Fi类积木 <span style="background-color:#76CE14;color:white;padding:4px; font-size: 12px;border-radius:3px;">网络已连接？</span>，添加灯光类积木 <span style="background-color:#8521D3;color:white;padding:4px; font-size: 12px;border-radius:3px;">全部LED显示（）色</span>，选择绿色。当光环板亮起绿色，表明网络连接成功。

<img src="images/connect-wifi-2.gif" width="800px;" style="padding:5px 5px 20px 5px;">

### 语音识别

按下按钮，光环板开始语音识别，同时LED亮起白色，3秒后，语音识别结束，同时LED灯熄灭。用白色LED灯亮起时长来提示语音识别时长。

3\. 添加事件类积木 <span style="background-color:#FFBF00;color:white;padding:4px; font-size: 12px;border-radius:3px;">当按钮被按下时</span>，添加灯光类积木 <span style="background-color:#8521D3;color:white;padding:4px; font-size: 12px;border-radius:3px;">全部LED显示（）色</span>，选择白色，添加Wi-Fi类积木 <span style="background-color:#76CE14;color:white;padding:4px; font-size: 12px;border-radius:3px;">识别（）语音（）秒钟</span>，使用默认数值3。

<img src="images/speech-recognition-1.gif" width="800px;" style="padding:5px 5px 20px 5px;">

4\. 添加灯光类积木 <span style="background-color:#8521D3;color:white;padding:4px; font-size: 12px;border-radius:3px;">熄灭所有灯光</span>，3秒后录音结束，音频文件将被上传至云端进行识别（大约2~5秒），当光环板获得识别结果后，灯光熄灭。

<img src="images/speech-recognition-2.gif" width="800px;" style="padding:5px 5px 20px 5px;">

### 语音识别“红色”

5\. 添加控制类积木 <span style="background-color:#FFAB19;color:white;padding:4px; font-size: 12px;border-radius:3px;">如果（）那么（）</span>，拖入运算类积木 <span style="background-color:#59C059;color:white;padding:4px; font-size: 12px;border-radius:3px;">（）包含（）？</span>，添加Wi-Fi类积木 <span style="background-color:#76CE14;color:white;padding:4px; font-size: 12px;border-radius:3px;">语音识别出的文字</span> 至运算类积木前一个变量，修改后一个变量为“红色”。

<img src="images/speech-recognition-3.gif" width="800px;" style="padding:5px 5px 20px 5px;">

6\. 如果语音识别出“红色”，则光环板LED亮起红色，并在1秒后，LED熄灭。依次添加积木 <span style="background-color:#8521D3;color:white;padding:4px; font-size: 12px;border-radius:3px;">全部LED显示（）色</span>，<span style="background-color:#FFAB19;color:white;padding:4px; font-size: 12px;border-radius:3px;">等待（）秒</span>，<span style="background-color:#8521D3;color:white;padding:4px; font-size: 12px;border-radius:3px;">熄灭所有灯光</span>。

<img src="images/speech-recognition-4.gif" width="800px;" style="padding:5px 5px 20px 5px;">

7\. 点击“上传到设备”将程序上传到光环板。

<img src="images/upload-program.gif" width="800px;" style="padding:5px 5px 20px 5px;">

8\. 等光环板的LED全部显示绿色，就按下按钮对光环板说“红色”，看LED是不是就亮起红色啦！

<p><span style="background-color:#66CDAA;line-height:30px;padding:12px;font-size:16px;border-radius:3px;"><a href="语音识别.mblock" download style="color:white;font-weight:bold;">下载代码</a></span></p>

---