# 使用 Python 编辑器 Mu

Mu 是一款面向初学者设计的极简型 Python 集成编辑器。光环板现已支持使用 Mu 编写 Python、Micro Python 代码，对光环板及其配套的电子模块进行编程控制。

除了 Mu 的基础功能外，该版本 Mu 还具有对光环板进行固件升级、代码上传、通过 REPL 控制器在线调试程序、通过绘图器实时查看变量及传感器数值变化等功能。

## 使用前准备
使用前请访问如下网址下载最新版 Mu 编辑器以及光环板最新固件：
https://github.com/FFtust/mu/releases/tag/V0.0.1
<img src="images/download.png" style="padding:5px 5px 20px 5px;">
下载完成后解压 mu-editor.rar 文件。
<img src="images/unzip.png" style="padding:5px 5px 20px 5px;">

## 快速入门指引
软件准备就绪，下面开始学习如何使用吧！
### 1、硬件连接
使用 Micro USB 数据线连接光环板与电脑。
<img src="images/connect.png" style="padding:5px 5px 20px 5px;">

注：您也可以使用 Makeblock 蓝牙适配器无线连接光环板和电脑。

### 2、打开软件
打开解压后的文件夹 **mu-editor**，双击`_main_.exe`打开软件。
<img src="images/open-mu.png" style="padding:5px 5px 20px 5px;">

### 3、选择模式
在弹出的对话框中选择 **Makeblock HaloCode**，然后单击 **OK**。
<img src="images/mode.png" style="padding:5px 5px 20px 5px;">

注：您也可以点击软件左上角**模式**选择模式。

### 4、更新固件
使用 Mu 编辑器需要使用我们尚未发布的光环板固件，在菜单栏点击**更新固件**，在弹出的对话框中选择之前下载的 **firefly_firmware_25_01_004-ht1.bin** 文件，开始升级后再软件左下角可查看固件升级进度。
<img src="images/update-firmware.png" style="padding:5px 5px 20px 5px;">
<img src="images/open-file.png" style="padding:5px 5px 20px 5px;">
<img src="images/update-progress.png" style="padding:5px 5px 20px 5px;">
### 5、开始编写第一段代码
现在一起来完成一个编程小任务吧！让光环板 LED 灯环亮起绿色。示例代码如下：
```py
import halo
halo.led.show_all(0, 255, 0)
```
注：函数说明请查看光环板 [Python API 文档](http://docs.makeblock.com/halocode/zh/python-api/python-api.html)。

### 6、上传代码
点击**上传代码**将程序上传到光环板，上传成功后看看光环板亮绿色了吗？
<img src="images/upload.png" style="padding:5px 5px 20px 5px;">
<img src="images/led-green.png" width="300px" style="padding:5px 5px 20px 5px;">

## 更多功能：使用控制台（REPL）调试程序
### 上报信息
您可以在代码中使用 `print()` 函数，只要光环板与电脑保持连接（通过 USB 数据线有线连线或使用蓝牙适配器无线连接），就可以在控制台中实时查看 `print()` 函数输出的结果。

例如，编写如下代码，将程序上传到光环板，打开**控制台**，就可以实时查看麦克风的读值变化了。
```py
import halo
while True:
    print(halo.microphone.get_loudness("maximum"))
```
<img src="images/controller.png" style="padding:5px 5px 20px 5px;">

### 发送指令
使用控制台还可以实时调试硬件代码。

例如，我们可以在控制台中输入如下代码，按回车键，看看光环板的 LED 灯环变红色了吗？
<img src="images/controller-adjust.png" style="padding:5px 5px 20px 5px;">
还可以继续输入指令，如查看当前麦克风检测到的响度。
<img src="images/controller-adjust-2.png" style="padding:5px 5px 20px 5px;">


## 更多功能：绘图器

### 实时查看变量数值或传感器读数
控制台可以实时查看变量值或传感器读数，但在更多的时候，我们关注的往往是变量或传感器读数在一段时间内的变化，而非是在某一时刻的具体值。绘图器就可以将变量值或传感器读数值绘制成图形，便于观察数值变化情况。

例如，编写如下代码并上传到光环板，打开**绘图器**，实时查看麦克风的读值变化。
```py
import halo
while True:
    halo.print_plot(halo.microphone.get_loudness())
    time.sleep(0.1)
```
<img src="images/plotter.png" style="padding:5px 5px 20px 5px;">

注：使用 Makeblock 蓝牙适配器无线连接光环板与电脑时，绘图器的实时显示速度将受限于蓝牙通信的速度，可能会导致一些延迟。

### 同时查看多个变量或传感器读数变化

绘图器功能还支持用户同时查看多个变量的变化。

例如，编写如下代码并上传到光环板，打开**绘图器**，绿色线显示了麦克风检测到的响度值，而蓝色线则显示了 i%100 的值。

```py
import halo
i=1
while True:
    halo.print_plot(halo.microphone.get_loudness(),i%100)
    i=i+1
    time.sleep(0.1)
```
<img src="images/plotter-2.png" style="padding:5px 5px 20px 5px;">

## 更多文档
### 基础文档
如果想要在 Mu 中实现光环板及其扩展模块的更多功能，您可以访问并查看光环板的 [Python API 文档](http://docs.makeblock.com/halocode/zh/python-api/python-api.html)。

**注：我们正在精简 Python API，希望能够提升拼写及使用体验，我们期望在8月份完成这项工作。但即使有一些改进，我们也会尽可能确保现有 API 能够正常使用。**

如果您对光环板的 API 还不熟悉，那么打开文档[使用Python](http://docs.makeblock.com/halocode/zh/tutorials/use-python.html#%E5%B0%86%E7%A7%AF%E6%9C%A8%E5%9D%97%E8%BD%AC%E6%8D%A2%E4%B8%BApython%E4%BB%A3%E7%A0%81)，查看慧编程的积木块转代码功能介绍将会帮助到你。

### 高级功能 API 文档
光环板具备强大的性能和联网能力，我们提供了一些更为复杂的 API 供您使用，以便您利用光环板和其配套的电子模块创建更激动人心的硬件项目，并从中学习到诸如网络通信、物联网应用设计、App 开发等相关的知识。

注：一些高级 API 会与光环板本身的功能（如局域网、语音识别、扩展模块）争夺硬件资源，导致两者无法同时使用，此时可以使用固件更新功能进行恢复。

**network**

Micropython 已经实现了一个网络模块，并提供了对应的文档，查看如下链接可以了解更多信息：
http://docs.micropython.org/en/latest/library/network.html

使用 network 中的 API 将帮助您充分利用光环板的联网能力。

**network2**

我们在 ESP32 原有的 network 基础上增添一些功能，并做了新的设计，让相关 API 更易用且易于理解。

Default config:
```py
DEFAULT_AP_IP    = "192.168.4.1"
DEFAULT_STA_IP   = "192.168.4.2"
DEFAULT_NETMARK  = "255.255.255.0"
DEFAULT_GATEWAY  = "192.168.1.1"
DEFAULT_DNS      = "172.16.50.20"
DEFAULT_AUTHMODE = AUTH_WPA2_PSK
DEFAULT_PORT     = 5050
```
**API 参考**
```py
network2.config_ap(ssid, password)
```
将光环板设置为一个AP。

注：光环板目前尚不支持同时作为 AP 和 STA 使用。

<table border="1" cellpadding="10px">
<tr>
<td>Parameters:</td>
<td>ssid (str) – wifi ssid
<br/>password(str) – wifi password</td></tr>
<tr>
<td>Returns:</td>
<td>None</td></tr>
</table>

```py
config_sta(ssid, password)
```
将光环板设置为一个 STA。

注：光环板目前尚不支持同时作为 AP 和 STA。

<table border="1" cellpadding="10px">
<tr>
<td>Parameters:</td>
<td>ssid (str) – wifi ssid<br/>
password(str)  – wifi password</td></tr>
<tr>
<td>Returns:</td>
<td>None</td></tr>
</table>

```py
is_connected()
```
检测光环板是否通过 Wi-Fi 与其他设备建立连接，该 API 在光环板作为 AP 或 STA 时均有效。
<table border="1" cellpadding="10px">
<tr>
<td>Returns:</td>
<td>None</td></tr>
</table>

```py
set_ip(ip)
```
设置光环板的 IP 地址，未使用该 API 定义光环板 IP 地址前，光环板使用默认的 IP 地址。
DEFAULT_AP_IP    = "192.168.4.1"<br/>
DEFAULT_STA_IP   = "192.168.4.2"
<table border="1" cellpadding="10px">
<tr>
<td>Parameters:</td>
<td>ip (str) – ipv4</td></tr>
<tr>
<td>Returns:</td>
<td>None</td></tr>
</table>

```py
set_subnet_mark(mark)
```
设置光环板的子网掩码。
<table border="1" cellpadding="10px">
<tr>
<td>Parameters:</td>
<td>mark (str) – wifi mark, 255.255.255.0 as defualt</td></tr>
<tr>
<td>Returns:</td>
<td>None</td></tr>
</table>

```py
get_subnet_mark()
```
获取光环板的子网掩码。

```py
set_gateway(gw)
```
设置光环板的网关。
<table border="1" cellpadding="10px">
<tr>
<td>Parameters:</td>
<td>	
gateway (str) –</td></tr>
<tr>
<td>Returns:</td>
<td>None</td></tr>
</table>

```py
get_gateway()
```
获取光环板的网关。

```py
get_ip()
```
获取光环板的 IP 地址。
<table border="1" cellpadding="10px">
<td>Returns:</td>
<td>ipv4</td></tr>
</table>

```py
create_client()
```
将光环板设置为一个客户端。

```py
client_connect_to(ip_to, port = DEFAULT_PORT)
```
将光环板连接到指定 IP 地址及端口的服务器上。

```py
create_server(port = DEFAULT_PORT)
```
将光环板设置为一个服务器。

```py
server_wait_connection(port = DEFAULT_PORT)
```
服务器等待，直到对应端口连接到了客户端。

```py
server_get_connections(port = DEFAULT_PORT)
```
获得所有已经连接客户端的 IP 地址列表。

```py
server_get_latest_connection(port = DEFAULT_PORT)
```
获得最近一次连接的客户端的 IP 地址。

```py
write(data, mode, ip_to, port = DEFAULT_PORT)
```
发送数据。

```py
write_line(data, mode, ip_to, port = DEFAULT_PORT)
```
发送数据行。

```py
read(mode, ip_from, port = DEFAULT_PORT)
```
接收数据。

```py
read_line(mode, ip_from, port = DEFAULT_PORT)
```
接收数据行。

**communication**

我们开发了光环板的串口通信接口，以便您可以利用这些接口将光环板与各类具备串口通信能力的硬件（如树莓派、一些摄像头模块）搭配使用。
<img src="images/communication.png" style="padding:5px 5px 20px 5px;">

```py
disable_channel_default(channel)
```
光环板通过硬件串口与光环板的电子模块进行通信，两者间使用特定的通信协议，若要关闭该通信协议，使光环板的串口指令能够被“自定义”，请使用本条指令。
<table border="1" cellpadding="10px">
<tr>
<td>Parameters:</td>
<td>	
channel (str) – “uart1/ble”</td></tr>
<tr>
<td>Returns:</td>
<td>None</td></tr>
</table>

```py
enable_channel_default(channel)
```
光环板通过硬件串口与光环板的电子模块进行通信，两者间使用特定的通信协议，若要恢复该通信指令，请使用本条指令。
<table border="1" cellpadding="10px">
<tr>
<td>Parameters:</td>
<td>	
channel (str) – “uart1/ble”</td></tr>
<tr>
<td>Returns:</td>
<td>None</td></tr>
</table>

```py
read(channel)
```
读取特定通道上的数据。
<table border="1" cellpadding="10px">
<tr>
<td>Parameters:</td>
<td>	
channel (str) – “uart1/ble”</td></tr>
<tr>
<td>Returns:</td>
<td>bytearray</td></tr>
</table>

```py
send(channel, data)
```
通过设定的通道发送数据。
<table border="1" cellpadding="10px">
<tr>
<td>Parameters:</td>
<td>	
channel (str) – “uart1/ble”<br/>
data (bytearray) – the data to send</td></tr>
<tr>
<td>Returns:</td>
<td>bytearray</td></tr>
</table>
