# 帐号云广播

帐号云广播功能可以实现远程通信，只要登陆慧编程账号，即使电脑与光环板相隔两地，也可以远程控制光环板。

## 注册/登陆慧编程

点击工具栏右侧的登陆/注册按钮，依据提示登陆/注册账号。

<img src="images/login.gif" width="800px;" style="padding:5px 5px 20px 5px;">

## 启用上传模式

点击启用上传模式。

<img src="images/toggle-on-upload.gif" width="800px;" style="padding:5px 5px 20px 5px;">

## 舞台按钮远程控制光环板

我们新建一个项目，结合舞台编程，用按钮来远程控制光环板。

### 添加帐号云广播积木

选中“角色”，在积木区点击“+”。“扩展中心”页面会弹出，点击添加“帐号云广播”。

<img src="images/user-cloud-message-1.gif" width="800px;" style="padding:5px 5px 20px 5px;">

### 步骤一：舞台按钮程序

1\. 选中“角色”，点击“×”删除熊猫角色（熊猫为默认角色）。

<img src="images/user-cloud-message-2.gif" width="800px;" style="padding:5px 5px 20px 5px;">

2\. 在“角色”下，点击“+”添加角色。在弹出的“角色库”页面，搜索选择Empty button1，点击“确定”。

<img src="images/user-cloud-message-3.gif" width="800px;" style="padding:5px 5px 20px 5px;">

3\. 在“角色”下，选中按钮，拖取一个事件类积木 <span style="background-color:#FFBF00;color:white;padding:4px; font-size: 12px;border-radius:3px;">当角色被点击</span> 到脚本区。

<img src="images/user-cloud-message-4.gif" width="800px;" style="padding:5px 5px 20px 5px;">

4\. 从帐号云广播类积木拖取一个 <span style="background-color:#4EAA83;color:white;padding:4px; font-size: 12px;border-radius:3px;">发送帐号云广播（）</span> 积木，并将云广播命名为“light_on”。

<img src="images/user-cloud-message-04.gif" width="800px;" style="padding:5px 5px 20px 5px;">

5\. 接下来我们为按钮添加一些特效，让它有被点击的感觉。从运动类积木拖取一个 <span style="background-color:#4C97FF;color:white;padding:4px; font-size: 12px;border-radius:3px;">将y坐标增加（）</span> 积木，输入-2，再从外观类积木拖取一个 <span style="background-color:#9966FF;color:white;padding:4px; font-size: 12px;border-radius:3px;">将颜色特效设为（）</span> 积木，输入-10.

<img src="images/user-cloud-message-5.gif" width="800px;" style="padding:5px 5px 20px 5px;">

6\. 从控制类积木拖取一个 <span style="background-color:#FFAB19;color:white;padding:4px; font-size: 12px;border-radius:3px;">等待（）秒</span> 积木，输入0.2。再从从运动类积木拖取一个 <span style="background-color:#4C97FF;color:white;padding:4px; font-size: 12px;border-radius:3px;">将y坐标增加（）</span> 积木，输入2，再从外观类积木拖取一个 <span style="background-color:#9966FF;color:white;padding:4px; font-size: 12px;border-radius:3px;">将颜色特效设为（）</span> 积木，输入0.

<img src="images/user-cloud-message-6.gif" width="800px;" style="padding:5px 5px 20px 5px;">

### 步骤二：光环板程序

接下来我们为光环板添加程序，让光环板接收到的广播名称和按钮发送的一致。

#### 连接网络

1\. 选中“设备”下的光环板。添加事件类积木 <span style="background-color:#FFBF00;color:white;padding:4px; font-size: 12px;border-radius:3px;">当光环板启动时</span>。使用帐号云广播需要连接网络。添加Wi-Fi类积木 <span style="background-color:#76CE14;color:white;padding:4px; font-size: 12px;border-radius:3px;">开始连接无线网络（）密码（）</span>，输入附近Wi-Fi的名称和密码。

<img src="images/connect-wifi-1.gif" width="800px;" style="padding:5px 5px 20px 5px;">


2\. 网络连接成功后，我们需要一个信号来告诉我们网络连接成功。添加控制类积木 <span style="background-color:#FFAB19;color:white;padding:4px; font-size: 12px;border-radius:3px;">等待（）</span>，拖入Wi-Fi类积木 <span style="background-color:#76CE14;color:white;padding:4px; font-size: 12px;border-radius:3px;">网络已连接？</span>，添加灯光类积木 <span style="background-color:#8521D3;color:white;padding:4px; font-size: 12px;border-radius:3px;">全部LED显示（）色</span>，选择绿色。当光环板亮起绿色，表明网络连接成功。

<img src="images/connect-wifi-2.gif" width="800px;" style="padding:5px 5px 20px 5px;">

#### 云广播程序

当光环板接受到云广播消息时，我们要让全部的LED都亮起红色，再熄灭。

1\. 从Wi-Fi类积木拖取一个 <span style="background-color:#76CE14;color:white;padding:4px; font-size: 12px;border-radius:3px;">当接收账号云广播（）</span>，输入刚刚创建的广播light_on。

<img src="images/user-cloud-message-7.gif" width="800px;" style="padding:5px 5px 20px 5px;">

2\. 从灯光类积木拖取一个 <span style="background-color:#8521D3;color:white;padding:4px; font-size: 12px;border-radius:3px;">全部LED显示（）色</span>，设置为红色。

<img src="images/user-cloud-message-8.gif" width="800px;" style="padding:5px 5px 20px 5px;">

3\. 从控制类积木拖取一个 <span style="background-color:#FFAB19;color:white;padding:4px; font-size: 12px;border-radius:3px;">等待（）秒</span> 积木，再从灯光类积木拖取一个 <span style="background-color:#8521D3;color:white;padding:4px; font-size: 12px;border-radius:3px;">熄灭所有灯光</span> 积木，让灯光在1秒后熄灭。

<img src="images/user-cloud-message-9.gif" width="800px;" style="padding:5px 5px 20px 5px;">

4\. 点击“上传到设备”将程序上传到光环板。

<img src="images/user-cloud-message-10.gif" width="800px;" style="padding:5px 5px 20px 5px;">

5\. 试一下用鼠标点击舞台按钮吧！只要光环板接入互联网，在其他的电脑上登陆慧编程，也可以打开同样的程序来远程控制光环板。

<p><span style="background-color:#66CDAA;line-height:30px;padding:12px;font-size:16px;border-radius:3px;"><a href="帐号云广播.mblock" download style="color:white;font-weight:bold;">下载代码</a></span></p>

---

{% note %}
登陆同一慧编程帐号才能实现远程控制。
{% endnote %}
