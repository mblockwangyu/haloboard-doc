![](halocode.png)

# 简介

光环板是一块可无线联网的单板计算机，专为编程教育而设计。它小巧的机身拥有丰富的电子元件，通过简单编程就可以实现各种电子创作，入门新手也能手到拈来。搭配慧编程软件，软硬件结合，由积木式编程到Python，由浅入深，掌握编程逻辑。内置Wi-Fi天线让光环板接入互联网，实现IoT应用，创作简易的智能家居设备，体验万物互联时代。

![](halocode-01.png)
![](halocode-2.png)
![](halocode-3.png)
![](halocode-4.png)

### 使用慧编程

结合慧编程使用，你可以通过编程掌握光环板的各种玩法，像是可穿戴的体感计步器，触摸感应灯箱等等。

<p style="text-align:left"><img style="width:600px;" src="mblock5.png"></p>

光环板支持在PC、Mac、移动端上使用慧编程。

- PC端：访问 [http://www.mblock.cc/mblock-software/](http://www.mblock.cc/mblock-software/) 下载软件
- 网页端：[https://ide.makeblock.com/](https://ide.makeblock.com/)
- 移动端：可以在各大应用商店搜索“慧编程”下载使用

<small>注：网页端需配合 mLink 插件，请参考 <a href="http://www.mblock.cc/doc/zh/part-one-basics/mlink-quick-start-guide.html" target="_blank">mLink 指南</a>。</small>