# 编程模式

光环板有两种编程模式：在线模式和上传模式。

---

## 在线模式

光环板默认的编程模式是在线模式。当光环板链接到慧编程后，在线模式默认启动，如下图：

<img src="images/live-mode-1.png" style="padding:5px 5px 20px 5px;">

“在线模式”下，编程有以下这些特性：

1. 程序不需要上传到设备运行，无“上传到设备”按钮。
    
    <img src="images/live-mode-1.png" style="padding:5px 5px 20px 5px;">

    鼠标单击可直接运行程序，方便你检测程序效果。更改程序后，再次单击运行就可以看到新的效果。

    <div style="background-color:#FFFFCC;padding:10px;border-left:5px solid #FFEB3B;margin-bottom:30px;border-radius:5px;line-height:28px;font-size:14px;"><strong>注：</strong><br>
    鼠标单击运行程序或单个积木。
</div>

2. 程序不能离线运行。光环板必须与慧编程保持连接才可运行程序。
<br>需注意的是，在线模式下，事件类积木 <span style="background-color:#D1D3D5;color:white;padding:4px; font-size: 12px;border-radius:3px;">当光环板启动时</span> 不可使用。

    <img src="images/live-mode-2.png" style="padding:5px 5px 20px 5px;">

    <div style="background-color:#FFFFCC;padding:10px;border-left:5px solid #FFEB3B;margin-bottom:30px;border-radius:5px;line-height:28px;font-size:14px;"><strong>注：</strong><br>
    事件类积木发送的广播只能用在项目内，可以用于：光环板给自身发送、光环板与舞台之间互相通信、舞台内部通信。
</div>

## 上传模式

在上传模式下，编程有以下这些特性：

1. 所有程序必须上传到光环板运行。完成编程后，点击”上传到设备“进行上传。<br>
<img src="images/upload-mode-1.png" style="padding:5px 5px 20px 5px;">
<br>
2. 程序可离线运行。只要光环板有外接电源，与慧编程断开连接后仍会运行程序。
<br><br>
3. 关机再重新开机，光环板仍会运行关机前最后上传的程序。
<br><br>
4. 在“上传模式”下，事件类积木有三个特定积木无法使用，分别是：<span style="background-color:#D1D3D5;color:white;padding:4px; font-size: 12px;border-radius:3px;">当绿色旗帜被点击</span>、<span style="background-color:#D1D3D5;color:white;padding:4px; font-size: 12px;border-radius:3px;">当按下（）键</span>、<span style="background-color:#D1D3D5;color:white;padding:4px; font-size: 12px;border-radius:3px;">广播（）并等待</span>。

    <img src="images/upload-mode-2.png" style="padding:5px 5px 20px 5px;">

    <div style="background-color:#FFFFCC;padding:10px;border-left:5px solid #FFEB3B;border-radius:5px;margin-bottom:30px;line-height:28px;font-size:14px;"><strong>注：</strong><br>
    事件类积木发送的广播只能用在项目内。
</div>

---
