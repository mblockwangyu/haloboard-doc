# 使用光环板上的PIN脚控制开源硬件

该部分 API 将帮助您使用光环上的PIN角控制，诸如舵机、蜂鸣器、按钮这样的开源硬件。

![](images/引脚图.png)


<img src="images/连接方式.png" width= 300>


### 省略代码中的halocode
注意：该部分 API 省略了“halocode.”，本篇提及的所有 API 均省略了“halocode.” ，如 led_driver.off( )实际为halocode.led_driver.off()。mBuild 电子模块平台的所有功能均维护在halocode库中，因此我们推荐如下写法，以便将halocode.进行省略，使得代码更简洁。

``` py
from halocode import *
```
<br>


## pin0.write_digital(val = True)
设置引脚0上的数值输出，参数：
- *val* 数值或布尔值，表示设置的数值输出值。<br>
val 为数值时，有效范围为 0：设置引脚数值输出为低电平；1：设置引脚数值输出为高电平；<br>
val 为布尔值时，True：设置引脚数值输出为低电平；False：设置引脚数值输出为高电平；<br>

## pin1.write_digital(val = True)
设置引脚1上的数值输出。

## pin2.write_digital(val = True)
设置引脚2上的数值输出。

## pin3.write_digital(val = True)
设置引脚3上的数值输出。

## pin0.read_digital()
读取引脚0上的数值输入，参数：<br>
返回数值，可能的值为0或1。

## pin1.read_digital()
读取引脚0上的数值输入，参数：<br>
返回数值，可能的值为0或1。

## pin2.read_digital()
读取引脚0上的数值输入，参数：<br>
返回数值，可能的值为0或1。

## pin3.read_digital()
读取引脚0上的数值输入，参数：<br>
返回数值，可能的值为0或1。

## pin0.write_analog(val)
设置引脚0上的模拟输出（PWM），参数：<br>
- *val* 数值，有效范围为0 - 1023，

## pin1.write_analog(val)
设置引脚1上的模拟输出（PWM）

## pin2.write_analog(val)
设置引脚2上的模拟输出（PWM）

## pin3.write_analog(val)
设置引脚3上的模拟输出（PWM）

## pin2.read_analog()
读取引脚2上的模拟输入。

## pin3.read_analog()
读取引脚3上的模拟输入。

## pin0.servo(angle)
设置引脚0，让其控制与其连接的舵机转到对应角度，参数：
- *angle* 数值，有效范围为0-180，表示控制舵机转动的角度。

## pin1.servo(angle)
设置引脚1，让其控制与其连接的舵机转到对应角度。

## pin2.servo(angle)
设置引脚2，让其控制与其连接的舵机转到对应角度。

## pin3.servo(angle)
设置引脚3，让其控制与其连接的舵机转到对应角度。


## pin0.play_tone(freq)
设置引脚0，让其控制与其连接的蜂鸣器持续鸣叫，参数：
- *freq* 数值，有效值为20 - 5000，单位赫兹（Hz），表示控制蜂鸣器鸣叫的频率，低于或高于有效数值均无法听到蜂鸣器鸣叫（5000的上限设置是考虑人耳的听力健康）。

## pin1.play_tone(freq)
设置引脚1，让其控制与其连接的蜂鸣器持续鸣叫。

## pin2.play_tone(freq)
设置引脚2，让其控制与其连接的蜂鸣器持续鸣叫。

## pin3.play_tone(freq)
设置引脚3，让其控制与其连接的蜂鸣器持续鸣叫。