# 使用 makeblock 提供的云服务

该部分 API 将帮助您使用光环板上基于 Wi-Fi 的相关云功能，包括 Wi-Fi连接，语音识别 ，账号云广播，IoT。

### 省略代码中的halocode
注意：该部分 API 省略了“halocode.”，本篇提及的所有 API 均省略了“halocode.” ，如 led_driver.off( )实际为halocode.led_driver.off()。mbuild 电子模块平台的所有功能均维护在halocode库中，因此我们推荐如下写法，以便将halocode.进行省略，使得代码更简洁。

``` py
from halocode import *
```
<br>

## wifi.connect(ssid, password)
启动wifi连接，该API不阻塞，API 退出不代表Wi-Fi已连接上，需要调用wifi.is_connect()进行判断，参数：
- *ssid* 字符串类型，Wi-Fi账号。<br>
- *password* 字符串类型，Wi-Fi密码。

## wifi.is_connect()
检测 Wi-Fi 连接状态。<br>
返回布尔值，True：表示Wi-Fi已经建立连接；False；表示Wi-Fi尚未建立连接。

## cloud.setkey(key)
认证慧编程账号以使用 makeblock 云服务。参数：
- *key* 字符串类型，在[缺网址]中输入你的慧编程账号及密码来生成令牌。

## speech.listen(language,time=3)
开始进行语音识别，参数：
- *language* 字符串类型，。有效值为：<br>
`"chinese"`：汉语；<br>
`"chinese_taiwan"`：台湾普通话；<br>
`"cantonese"`：粤语；<br>
`"english"`：英语；<br>
`"french"`：法语；<br>
`"german"`：德语；<br>
`"italian"`：意大利语；<br>
`"spanish"`：西班牙语；<br>
表示需要识别语言的种类。
- *time* 数值，默认值为3，表示录音持续的时间，单位为秒。<br>
注意：该 API 将阻塞直至得到语音识别的结果，因此该 API 的阻塞时长为 time + 语音识别的时长（约2-5秒）

## cloud_broadcast.set(message, val)
发送账号云广播并附带值，参数：
- *message* 字符串类型，账号云广播消息的名称。
- *val* 账号云广播消息的值

## cloud_broadcast.get(message)
发送账号云广播并附带值，参数：
- *message* 字符串类型，账号云广播消息的名称。<br>
- *val* 账号云广播消息的值。<br>
返回字符串类型或者数值数据。

## iot.max_temp(location)
获得某地的最高气温（℃），参数：

## iot.min_temp(location)
获得某地的最低气温（℃），参数：

## iot.air(item, location)
获得某地的空气质量数据，参数：

## iot.humidity(location)
获得某地的空气湿度，参数：

## iot.sunrise(location)
获得某地的日出时间，参数：

## iot.sunset(location)
获得某地的日落时间，参数：

## iot.time(item, location)
获得某地的当地时间，参数：
