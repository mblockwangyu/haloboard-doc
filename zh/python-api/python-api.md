# Python API 文档

## `button` &mdash; 板载按键

**功能相关函数**

`button.is_pressed()`<br>
获取按键当前状态。 返回的结果是 `True`：按键被按下， 或者 `False`: 按键未被按下。

**程序示例**

```py
import halo

def loop():
    while True:
        if halo.button.is_pressed():
            print("button is pressed")
loop()
```

## `touchpad0` &mdash; 触摸按键0

**功能相关函数**

`touchpad0.is_touched()`<br>
获取触摸按键0当前状态。返回的结果是 `True`：触摸按键0被触摸， 或者 `False`: 触摸按键0未被触摸。

`touchpad0.get_value()`<br>
获取触摸按键被触摸状态。数值范围为`0-10000`。

`touchpad0.set_touch_threshold(val)`<br>
设定触摸按键的阈值，参数：
- *val* 触摸变化的百分比，检测到变化的幅值大于该百分比时认为被触摸，数值范围为`0-1`。

**程序示例**

```py
import halo
import event

@event.start
def on_start():
    global results
    if halo.touchpad0.is_touched():
        halo.led.show_all(126, 211, 33)
```

## `touchpad1` &mdash; 触摸按键1

**功能相关函数**

`touchpad1.is_touched()`<br>
获取触摸按键1当前状态。返回的结果是 `True`：触摸按键1被触摸， 或者 `False`: 触摸按键1未被触摸。

`touchpad1.get_value()`<br>
获取触摸按键被触摸状态。数值范围为`0-10000`。

`touchpad1.set_touch_threshold(val)`<br>
设定触摸按键的阈值，参数：
- *val* 触摸变化的百分比，检测到变化的幅值大于该百分比时认为被触摸，数值范围为`0-1`。

**程序示例**

```py
import halo
import event

@event.start
def on_start():
    global results
    if halo.touchpad1.is_touched():
        halo.led.show_all(126, 211, 33)
```

## `touchpad2` &mdash; 触摸按键2

**功能相关函数**

`touchpad2.is_touched()`<br>
获取触摸按键2当前状态。返回的结果是 `True`：触摸按键2被触摸， 或者 `False`: 触摸按键2未被触摸。

`touchpad0.get_value()`<br>
获取触摸按键被触摸状态。数值范围为`0-10000`。

`touchpad0.set_touch_threshold(val)`<br>
设定触摸按键的阈值，参数：
- *val* 触摸变化的百分比，检测到变化的幅值大于该百分比时认为被触摸，数值范围为`0-1`。

**程序示例**

```py
import halo
import event

@event.start
def on_start():
    global results
    if halo.touchpad2.is_touched():
        halo.led.show_all(126, 211, 33)
```

## `touchpad3` &mdash; 触摸按键3

**功能相关函数**

`touchpad3.is_touched()`<br>
获取触摸按键3当前状态。返回的结果是 `True`：触摸按键3被触摸， 或者 `False`: 触摸按键3未被触摸。

`touchpad0.get_value()`<br>
获取触摸按键被触摸状态。数值范围为`0-10000`。

`touchpad0.set_touch_threshold(val)`<br>
设定触摸按键的阈值，参数：
- *val* 触摸变化的百分比，检测到变化的幅值大于该百分比时认为被触摸，数值范围为`0-1`。

**程序示例**

```py
import halo
import event

@event.start
def on_start():
    global results
    if halo.touchpad3.is_touched():
        halo.led.show_all(126, 211, 33)
```

## `pin0` &mdash; pin口0

**功能相关函数**

`pin0.is_touched()`<br>
获取pin口当前状态。返回的结果是`True`：pin口被触摸，或者`False`：pin口未被触摸。

`pin0.get_touchpad_value()`<br>
获取pin口被触摸的值。数值范围为`0-10000`。

`pin0.set_touchpad_threshold(val)`<br>
设置pin口触摸触发阈值，参数：
- *val* 变化的百分比，检测到变化的幅值大于该百分比时认为被触摸，数值为`0.0-1`。

`pin0.read_digital()`<br>
获取pin口数字输入，值为`0`或`1`。

`pin0.write_digital(val)`<br>
设置pin口数字输出，参数：
- *val* 数字输出值为`0`或`1`。

`pin0.write_analog(val)`<br>
设置模拟输出（pwm），参数：
- *val* 模拟输出值，数值范围为`0-1023`。

`pin0.read_analog()`<br>
获取模拟输入值（pwm）。数值范围为`0-3300`，单位为`mv`。

`pin0.servo_write(val)`<br>
设置舵机转动的角度，参数：
- *val* 舵机转动的角度，或者舵机控制脉冲的高电平的维持时间，数值为`0-19999`。
    * 当数值小于544 的时候，输入数据如果小于0，会转换为0，如果大于180会转化为180，代表设置的是模拟舵机的转动角度；
    * 当数值大于或等于544时，表示设置的是50Hz PWM波的高电平的时间宽度(单位是 us)，所以最大值是 19999， 将近20ms，如果大于19999的，则转化为19999。

**程序示例**

```py
import halo
import event

@event.start
def on_start():
    global results
    if halo.pin0.is_touched():
        halo.led.show_all(126, 211, 33)
```

## `pin1` &mdash; pin口1

**功能相关函数**

`pin1.is_touched()`<br>
获取pin口当前状态。返回的结果是`True`：pin口被触摸，或者`False`：pin口未被触摸。

`pin1.get_touchpad_value()`<br>
获取pin口被触摸的值。数值范围为`0-10000`。

`pin1.set_touchpad_threshold(val)`<br>
设置pin口触摸触发阈值，参数：
- *val* 变化的百分比，检测到变化的幅值大于该百分比时认为被触摸，数值为`0.0-1`。

`pin1.read_digital()`<br>
获取pin口数字输入，值为`0`或`1`。

`pin1.write_digital(val)`<br>
设置pin口数字输出，参数：
- *val* 数字输出值为`0`或`1`。

`pin1.write_analog(val)`<br>
设置模拟输出（pwm），参数：
- *val* 模拟输出值，数值范围为`0-1023`。

`pin1.read_analog()`<br>
获取模拟输入值（pwm）。数值范围为`0-3300`，单位为mv。

`pin1.servo_write(val)`<br>
设置舵机转动的角度，参数：
- *val* 舵机转动的角度，或者舵机控制脉冲的高电平的维持时间，数值为`0-19999`。
    * 当数值 小于 544 的时候，输入数据如果小于0，会转换为0， 如果大于 180 会转化为 180，代表设置的是模拟舵机的转动角度；
    * 当数值大于或等于 544时，表示设置的是 50Hz PWM波的高电平的时间宽度(单位是 us)，所以最大值是 19999， 将近20ms，如果大于19999的，则转化为19999。

**程序示例**

```py
import halo
import event

@event.start
def on_start():
    global results
    if halo.pin1.is_touched():
        halo.led.show_all(126, 211, 33)
```

## `pin2` &mdash; pin口2

**功能相关函数**

`pin2.is_touched()`<br>
获取pin口当前状态。返回的结果是`True`：pin口被触摸，或者`False`：pin口未被触摸。

`pin2.get_touchpad_value()`<br>
获取pin口被触摸的值。数值范围为`0-10000`。

`pin2.set_touchpad_threshold(val)`<br>
设置pin口触摸触发阈值，参数：
- *val* 变化的百分比，检测到变化的幅值大于该百分比时认为被触摸，数值为`0.0-1`。

`pin2.read_digital()`<br>
获取pin口数字输入，值为`0`或`1`。

`pin2.write_digital(val)`<br>
设置pin口数字输出，参数：
- *val* 数字输出值为`0`或`1`。

`pin2.write_analog(val)`<br>
设置模拟输出（pwm），参数：
- *val* 模拟输出值，数值范围为`0-1023`。

`pin2.read_analog()`<br>
获取模拟输入值（pwm）。数值范围为`0-3300`，单位为mv。

`pin2.servo_write(val)`<br>
设置舵机转动的角度，参数：
- *val* 舵机转动的角度，或者舵机控制脉冲的高电平的维持时间，数值为`0-19999`。
    * 当数值 小于 544 的时候，输入数据如果小于0，会转换为0， 如果大于 180 会转化为 180，代表设置的是模拟舵机的转动角度；
    * 当数值大于或等于 544时，表示设置的是 50Hz PWM波的高电平的时间宽度(单位是 us)，所以最大值是 19999， 将近20ms，如果大于19999的，则转化为19999。

**程序示例**

```py
import halo
import event

@event.start
def on_start():
    global results
    if halo.pin2.is_touched():
        halo.led.show_all(126, 211, 33)
```

## `pin3` &mdash; pin口3

**功能相关函数**

`pin3.is_touched()`<br>
获取pin口当前状态。返回的结果是`True`：pin口被触摸，或者`False`：pin口未被触摸。

`pin3.get_touchpad_value()`<br>
获取pin口被触摸的值。数值范围为`0-10000`。

`pin3.set_touchpad_threshold(val)`<br>
设置pin口触摸触发阈值，参数：
- *val* 变化的百分比，检测到变化的幅值大于该百分比时认为被触摸，数值为`0.0-1`。

`pin3.read_digital()`<br>
获取pin口数字输入，值为`0`或`1`。

`pin3.write_digital(val)`<br>
设置pin口数字输出，参数：
- *val* 数字输出值为`0`或`1`。

`pin3.write_analog(val)`<br>
设置模拟输出（pwm），参数：
- *val* 模拟输出值，数值范围为`0-1023`。

`pin3.read_analog()`<br>
获取模拟输入值（pwm）。数值范围为`0-3300`，单位为mv。

`pin3.servo_write(val)`<br>
设置舵机转动的角度，参数：
- *val* 舵机转动的角度，或者舵机控制脉冲的高电平的维持时间，数值为`0-19999`。
    * 当数值 小于 544 的时候，输入数据如果小于0，会转换为0， 如果大于 180 会转化为 180，代表设置的是模拟舵机的转动角度；
    * 当数值大于或等于 544时，表示设置的是 50Hz PWM波的高电平的时间宽度(单位是 us)，所以最大值是 19999， 将近20ms，如果大于19999的，则转化为19999。

**程序示例**

```py
import halo
import event

@event.start
def on_start():
    global results
    if halo.pin1.is_touched():
        halo.led.show_all(126, 211, 33)
```

## `motion_sensor` &mdash; 板载姿态传感器

**姿态传感器说明**

<img src="motion-sensor.png" style="width:600px;">

如上图所示，roll，pitch（翻滚角，俯仰角）的方向以右手螺旋定则为标准。

光环板水平放置时roll和pitch都为`0°`
- roll的范围：`-90° ~ 90°`
- pitch的范围：`-180° ~ 180°`

**功能相关函数**

`motion_sensor.get_roll()`<br>
获取姿态角的翻滚角，返回的数据范围是 `-90 ~ 90`

`motion_sensor.get_pitch()`<br>
获取姿态角的俯仰角，返回的数据范围是 `-180 ~ 180`

`motion_sensor.get_yaw()`<br>
获取姿态角的偏航角，返回的数据范围是`0 ~ 360`，由于没有电子罗盘,所以实际上偏航角只是使用了Z轴角速度的积分。它存在着积累误差。如果是想获得真实偏航角的，这个API不适合使用。

`motion_sensor.get_acceleration(axis)`<br>
获取三个轴的加速度值，单位是 `m/s^2`，参数：
- *axis* 字符串类型，以 `x`，`y`，`z` 代表光环板定义的坐标轴。

`motion_sensor.get_gyroscope(axis)`<br>
获取三个轴的角速度值，单位是 `°/`秒，参数：
- *axis* 字符串类型，以 `x`，`y`，`z` 代表光环板定义的坐标轴。

`motion_sensor.get_rotation(axis)`<br>
获得光环板在三个轴上转动的角度，以逆时针转动方向为正方向，参数：
- *axis* 字符串类型，以 `x`，`y`，`z` 代表光环板定义的坐标轴。

`motion_sensor.reset_rotation(axis = "all")`<br>
初始化绕三个轴转动的当前角度为0，`get_rotation()` 函数将从 0 开始计算，参数：
- *axis* 字符串类型，以 `x`，`y`，`z` 代表光环板定义的坐标轴, `all` 代表全部的三个轴。也是这个函数的默认值。

`motion_sensor.is_tilted_left()`<br>
检测光环板是否向左倾斜，阈值15°，返回值是布尔值，其中 `True` 表示光环板向左倾斜了，`False` 表示光环板未向左倾斜。

`motion_sensor.is_tilted_right()`<br>
检测光环板是否向右倾斜，阈值15°，返回值是布尔值，其中 `True` 表示光环板向右倾斜了，`False` 表示光环板未向右倾斜。

`motion_sensor.is_arrow_up()`<br>
获取箭头是否朝上状态，阈值15°，返回值是布尔值，其中 `True` 表示箭头朝上，`False` 表示箭头没有朝上。

`motion_sensor.is_arrow_down()`<br>
获取箭头是否朝上状态，阈值15°,返回值是布尔值，其中 `True` 表示箭头朝下，`False` 表示箭头没有朝下。

`motion_sensor.is_shaked()`<br>
检测光环板是否有被摇晃，返回值是布尔值，其中 `True` 表示光环板被晃动了，`False` 表示光环板未被晃动。

`motion_sensor.get_shake_strength()`<br>
如果光环板被摇晃了，这个函数可以获得摇晃的强度，返回值的数值范围是 `0 ~ 100`， 数值越大，晃动的强度就越大。

**程序示例1：**

```py
import halo
import time

while True:
    roll = halo.motion_sensor.get_roll()
    pitch = halo.motion_sensor.get_pitch()
    yaw = halo.motion_sensor.get_yaw()
    print("roll:", end = "")
    print(roll, end = "")
    print("   ,pitch:", end = "")
    print(pitch, end = "")
    print("   ,yaw:", end = "")
    print(yaw)
    time.sleep(0.05)
```

**程序示例2：**

```py
import halo

while True:
    if halo.motion_sensor.is_shaked():
        print("shake_strength:", end = "")
        print(halo.motion_sensor.get_shake_strength())
```

**程序示例3：**

```py
import halo

while True:
    if halo.motion_sensor.is_tilted_left():
        print("tilted_left")
    if halo.motion_sensor.is_tilted_right():
        print("tilted_right")
    if halo.motion_sensor.is_arrow_up():
        print("arrow_up")
    if halo.motion_sensor.is_arrow_down():
        print("arrow_down")
```


## `led` &mdash; 板载全彩LED灯

**功能相关函数**

`led.show_single(led_id, r, g, b)`<br>
设置单颗RGB LED灯的颜色，参数：
- *led_id* 单颗LED的编号，参数范围是`1-12`，对应位置如下图：<br>
<img style="left;" src="led-id.png">
- *r* 全彩LED红色分量的数值，参数范围是 `0 ~ 255`， 0为无红色分量，255是红色分量最亮。
- *g* 全彩LED绿色分量的数值，参数范围是 `0 ~ 255`， 0为无绿色分量，255是绿色分量最亮。
- *b* 全彩LED蓝色分量的数值，参数范围是 `0 ~ 255`， 0为无蓝色分量，255是蓝色分量最亮。<br>
    颜色RGB对应表：
    ```
    red(255 , 0 , 0)

    green(0 , 255 , 0)

    blue(0 , 0 , 255)

    yellow(255 , 255 , 0)

    cyan(0 , 255 , 255)

    purple(255 , 0 , 255)

    white(150 , 150 , 150)

    orange(255 , 50 , 0)

    black(0 , 0 , 0)

    gray(0 , 0 , 0)
    ```

`led.show_all(r, g, b)`<br>
设置所有RGB LED灯的颜色，参数：
- *r* 全彩LED红色分量的数值，参数范围是 `0 ~ 255`， 0为无红色分量，255是红色分量最亮。
- *g* 全彩LED绿色分量的数值，参数范围是 `0 ~ 255`， 0为无绿色分量，255是绿色分量最亮。
- *b* 全彩LED蓝色分量的数值，参数范围是 `0 ~ 255`， 0为无蓝色分量，255是蓝色分量最亮。

`led.off_all()`<br>
熄灭所有RGB LED。

`led.off_single(led_id)`<br>
熄灭单颗RGB LED，参数：
- *led_id* 单颗LED的编号，参数范围是`1-12`。

`led.show_ring(color_str, offset=0)`<br>
同时设置12颗RGB LED为相应的颜色，参数：
- *color_str* 字符串类型，字符串格式需满足“color1 color2 color3 color4”，其中colorx为"red"/"green"/"blue"/"yellow"/"cyan"/"purple"/"white"/"orange"/"black"/"gray"颜色字符以单个空格隔开；当颜色个数大于12时将被截断成12个。
- *offset* 数值型，数值范围`0-12`。

`led.ring_graph(percentage)`<br>
用LED灯环的状态显示百分比，参数：
- *percentage* 数值型，数值范围`0-100`。

`led.show_animation(name)`<br>
显示默认灯效，阻塞型，参数：
- *name* 默认灯效名，有四种：`spoondrift`, `meteor`, `rainbow`,`firefly`。

`led.is_led_ring_up()`<br>
检测LED灯环是否朝上状态，返回布尔值，其中`True`表示灯环朝上，`False`表示灯环未朝上。

`led.is_led_ring_down()`<br>
检测LED灯环是否朝下状态，返回布尔值，其中`True`表示灯环朝下，`False`表示灯环未朝下。

**程序示例1：**

```py
import halo
import time

halo.led.show_all(255,0,0)
time.sleep(2)
halo.led.off_all()
time.sleep(2)
halo.led.show_all(0,0,255)
time.sleep(2)
halo.led.off_all()
time.sleep(2)
halo.led.show_all(0,255,0)
time.sleep(2)
halo.led.off_all()
time.sleep(2)
```

**程序示例2：**

```py
import halo 
import event

@event.button_pressed
def on_button_pressed():
    while True:
        halo.led.show_ring('red orange yellow green cyan blue purple white white white white white')
        time.sleep(1)
        halo.led.off_all()
        halo.led.show_ring('red orange yellow green cyan blue purple white white white white white', 1)
        time.sleep(1)
        halo.led.off_all()
        halo.led.show_ring('red orange yellow green cyan blue purple white white white white white', 2)
        time.sleep(1)
        halo.led.off_all()
        halo.led.show_ring('red orange yellow green cyan blue purple white white white white white', 3)
        time.sleep(1)
        halo.led.off_all()
        halo.led.show_ring('red orange yellow green cyan blue purple white white white white white', 4)
        time.sleep(1)
        halo.led.off_all()
        halo.led.show_ring('red orange yellow green cyan blue purple white white white white white', 5)
        time.sleep(1)
        halo.led.off_all()
        halo.led.show_ring('red orange yellow green cyan blue purple white white white white white', 6)
        time.sleep(1)
        halo.led.off_all()
        halo.led.show_ring('red orange yellow green cyan blue purple white white white white white', 7)
        time.sleep(1)
        halo.led.off_all()
        halo.led.show_ring('red orange yellow green cyan blue purple white white white white white', 8)
        time.sleep(1)
        halo.led.off_all()
        halo.led.show_ring('red orange yellow green cyan blue purple white white white white white', 9)
        time.sleep(1)
        halo.led.off_all()
        halo.led.show_ring('red orange yellow green cyan blue purple white white white white white', 10)
        time.sleep(1)
        halo.led.off_all()
        halo.led.show_ring('red orange yellow green cyan blue purple white white white white white', 11)
        time.sleep(1)
        halo.led.off_all()
        halo.led.show_ring('red orange yellow green cyan blue purple white white white white white', 12)
        time.sleep(1)
        halo.led.off_all()
```


## `microphone` &mdash; 板载麦克风

**功能相关函数**

`microphone.get_loudness(type)`<br>
获取声音的响度，参数：
- *type* 字符串参数，一共有两个：`average`，获得一段时间内的响度平均值；`maximum`，获得一段时间内响度的最大值，为默认参数。返回值范围为`0-100`。

**程序示例**

```py
import halo

halo.microphone.get_loudness("average")
```


## `speech_recognition` &mdash; 语音识别

**功能相关函数**

`speech_recognition.start(server, language, time)`<br>
开始进行语音识别，参数：
- *server* 语音识别服务器，默认为，`SERVER_MICROSOFT`，微软语音识别服务；
- *language* 识别语言：`LAN_CHINESE`, 汉语; `LAN_ENGLISH`, 英语；
- *time* 识别时间，默认为3秒。

`speech_recognition.get_error_code()`<br>
获取结果数据的错误码，如下：<br>
```
0 : 正确返回

3300： 语音输入参数不正确

3301：语音数据不清晰

3302：鉴权失败

3303：原始音频或者服务端问题

3304：用户请求超限(QPS)

3305：用户请求超限(pv-日请求量)

3307: 服务端问题

3308：音频数据过长

3309：音频数据异常

3310：音频文件过大

3311：采样率错误

3312：音频格式错误

3333：未知错误

3334：响应超时
```

`speech_recognition.get_error_message()`<br>
获取错误的具体信息，字符串类型。

`speech_recognition.get_result_code()`<br>
获取识别的结果,如果发生错误或者超时，返回空字符串。

`speech_recognition.get_sn_code()`<br>
获取语音数据唯一标识，由服务器系统内部产生。

`speech_recognition.get_all_respond()`<br>
获取语音识别结果，包含整个回复信息，如错误信息等。

**程序示例**

```py
import halo, event
halo.speech_recognition.set_recognition_url(halo.speech_recognition.SERVER_MICROSOFT, "http://msapi.passport3.makeblock.com/ms/bing_speech/interactive")
halo.speech_recognition.set_token(halo.speech_recognition.SERVER_MICROSOFT, "ed8xubrmidv")

@event.start
def on_start():
    halo.wifi.start(ssid = 'Maker-guest', password = 'makeblock', mode = halo.wifi.WLAN_MODE_STA)
    while not halo.wifi.is_connected():
        pass

    halo.speech_recognition.start(halo.speech_recognition.SERVER_MICROSOFT, halo.speech_recognition.LAN_CHINESE, 3)
```

## `wifi` &mdash; 板载Wi-Fi

**功能相关函数**

`wifi.start(ssid = "wifi_ssid", password = "password", mode = halo.wifi.STA)`<br>
启动wifi连接，该API不阻塞，API退出不代表Wi-Fi已连接上，需要调用 `wifi.is_connected()` 判断，参数：
- *ssid* 字符串类型，Wi-Fi账号。
- *password* 字符串类型，Wi-Fi密码。
- *mode* 启动Wi-Fi的模式，目前只支持WLAN_MODE_STA

`wifi.is_connected()`<br>
检测Wi-Fi连接状态，返回值是布尔值，其中 `True` 表示Wi-Fi已经建立连接，`False` 表示Wi-Fi尚未建立连接。

**程序示例**

```py
import halo
import event

@event.start
def on_start():
    halo.wifi.start(ssid = 'Maker-guest', password = 'makeblock', mode = halo.wifi.WLAN_MODE_STA)
    while not halo.wifi.is_connected():
        pass

    halo.led.show_all(126, 211, 33)
    time.sleep(2)
    halo.led.off_all()
```

## `cloud_message` &mdash; 云广播

**功能相关函数**

`cloud_message.start(topic_head, client_id=None, server="mq.makeblock.com", port=1883, user=None, password=None, keepalive=60, ssl=False)`<br>
开启云广播，需保证Wi-Fi连接，这个开启动作才生效，参数：
- *topic_head* 云广播主题的前缀目录。
- *client_id* 连接到代理时使用的唯一客户端ID字符串，如果`client_id`为零长度或无，则将随机生成一个。在这种情况下，`connect` 函数的`clean_session`参数必须为`True`。
- *server* 远程服务器的主机名或IP地址。
- _\*port\*_ （可选）要连接的服务器主机的网络端口，默认为`1883`。请注意：MQTT over SSL/TLS的默认端口为`8883`。
- _\*user\*_ （可选）在服务器上注册的用户名。
- _\*password\*_ （可选）在服务器上注册的密码。
- _\*keepalive\*_ （可选）客户端的keepalive超时值，默认为60秒。
- _\*ssl\*_ （可选）是否能使SSL/TLS支持。

`cloud_message.get_info(message)`<br>
获取广播信息的附带参数，返回字符串类型或者数值数据，参数：
- *message* 字符串数据，广播的信息名称。

**程序示例**

```py
import halo
import event
halo.cloud_message.start('/USER/1014148/MESSAGE')

@event.start
def on_start():
    halo.wifi.start(ssid = 'Maker-guest', password = 'makeblock', mode = halo.wifi.WLAN_MODE_STA)
    while not halo.wifi.is_connected():
        pass

    halo.led.show_all(126, 211, 33)
    time.sleep(2)
    halo.led.off_all()
    halo.cloud_message.broadcast('hello', '')
```

## `event` &mdash; 事件函数

**两种写法**

方法一：注册方式，如以下示例：

`event.start(test_callback)`<br>
`event.received(callback, 'hello')`

方法二：修饰器写法，如以下示例：

```py
@event.start

def start_callback():

    print(123)

@event.received('hello')

def received_callback():

    print(123)
```

需注意，当函数有`callback`之外的其他参数时，需加上参数。

**函数**

`event.start(callback)`<br>
启动事件，参数：
- *callback* 回调函数。

`event.shaked(callback)`<br>
摇晃事件，参数：
- *callback* 回调函数。

`event.button_pressed(callback)`<br>
按键按下事件，参数：
- *callback* 回调函数。

`event.tilted_left(callback)`<br>
左倾事件，参数：
- *callback* 回调函数。

`event.tilted_right(callback)`<br>
右倾事件，参数：
- *callback* 回调函数。

`event.arrow_up(callback)`<br>
前倾事件，参数：
- *callback* 回调函数。

`event.arrow_down(callback)`<br>
后倾事件，参数：
- *callback* 回调函数。

`event.received(callback, message_str)`<br>
广播事件，参数：
- *callback* 回调函数。
- *message_str* 监听的广播名称。

`event.cloud_message(message)`<br>
云广播事件，参数：
- *message* 字符串数据，广播的信息名称。

`event.mesh_message(message)`<br>
mesh广播事件，参数：
- *message* 字符串数据，广播的信息名称。

`event.greater_than(callback, threshold, type_str)`<br>
阈值比较事件， 超过阈值则触发，参数：
- *callback* 回调函数。
- *threshold* 数值型，触发阈值。
- *type_str* `microphone`/`timer`，分别代表声音传感器和计时器，目前仅支持这两个。

`event.touchpad0_active(callback)`<br>
被触摸按键，参数：
- *callback* 回调函数。

`event.touchpad1_active(callback)`<br>
被触摸按键，参数：
- *callback* 回调函数。

`event.touchpad2_active(callback)`<br>
被触摸按键，参数：
- *callback* 回调函数。

`event.touchpad3_active(callback)`<br>
被触摸按键，参数：
- *callback* 回调函数。

## `timer` &mdash; 计时器

**相关函数**

`get_timer()`<br>
获取系统计时器时间，单位为`秒`。

`reset_timer()`<br>
复位系统计时器时间。

## `variable` &mdash; 通讯变量

**相关函数**

`set_variable(name, value)`<br>
设置通讯变量，参数：
- *name* 通讯变量名称。
- *value* 通讯变量的值。

`get_variable(name)`<br>
获取通讯变量，参数：
- *name* 通讯变量名称。

## `broadcast` &mdash; 广播消息

**相关函数**

`broadcast(message_str)`<br>
广播消息，参数：
- *message_str* 字符串类型，消息值。



