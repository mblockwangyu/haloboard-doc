# 使用 network2 发掘 Wi-Fi 模组的更多应用

该部分 API 将帮助您进一步使用光环板上基于 Wi-Fi 的相关功能，包括将光环板配置成 STA 或 AP 模式，建立服务器或客户端，在服务器与客户端之间进行通信。

### 省略代码中的halocode
注意：该部分 API 省略了“halocode.”，本篇提及的所有 API 均省略了“halocode.” ，如 led_driver.off( )实际为halocode.led_driver.off()。mbuild 电子模块平台的所有功能均维护在halocode库中，因此我们推荐如下写法，以便将halocode.进行省略，使得代码更简洁。

``` py
from halocode import *
```
<br>



如不进行设定，network2 将保有如下默认设置：
``` py
DEFAULT_AP_IP    = "192.168.4.1"
DEFAULT_STA_IP   = "192.168.4.2"
DEFAULT_NETMARK  = "255.255.255.0"
DEFAULT_GATEWAY  = "192.168.1.1"
DEFAULT_DNS      = "172.16.50.20"
DEFAULT_AUTHMODE = AUTH_WPA2_PSK
DEFAULT_PORT     = 5050
DEFAULT_READ_BUFFER_SIZE = 32
```
<br>

## network2.config_ap(ssid, password)
设置光环板为无线接入点（Access Point）。
- *ssid* 字符串类型，有效值为任意字符串，表示设置无线接入点（Access Point）的服务集标识（Service Set Identifier）
- *password* 字符串类型，表示设置无线接入点（Access Point）的密码。

**注意：光环板目前尚不支持同时作为 AP 和 STA。**

## network2.config_sta(ssid, password)
设置光环板为接入点（Station），并将其与指定服务集标识（Service Set Identifier）与密码（password）进行连接。
- *ssid* 字符串类型，有效值为任意字符串，表示设置无线接入点（Access Point）的服务集标识（Service Set Identifier）
- *password* 字符串类型，表示设置无线接入点（Access Point）的密码。

返回布尔值，True：连接无线接入点（Access Point）成功； False: 连接无线接入点（Access Point）失败。

注意：光环板目前尚不支持同时作为 AP和 STA。

## network2.is_connect()
检测光环板是否与无线接入点（Access Point）或接入点（Station）建立了连接。
返回布尔值，True：连接无成功； False: 连接失败。

## network2.set_ip(ip)
设置光环板的 IP 地址，在未使用该 API 前，光环板具备默认的IP地址，而默认的 IP 地址有时会与网络中的已有的设备重名。此时，使用该 API 可以解决 IP 地址的冲突问题。
``` py
DEFAULT_AP_IP    = "192.168.4.1"
DEFAULT_STA_IP   = "192.168.4.2"
```
- *ip* 字符串类型，有效格式为"xx.xx.xx.xx"，表示将光环板的 IP 地址设置为该值。

## network2.get_ip()
获得光环板自身的 IP 地址。
返回字符串。

## network2.set_subnet_mark(mark)`
设置光环板的子网掩码。
- *mark* 字符串类型，有效格式为"xx.xx.xx.xx"，如"255.255.255.0", 表示将光环板的子网掩码设置为该值。

## network2.get_subnet_mark()
获得光环板自身的子网掩码。
返回字符串。

## network2.set_gateway(gateway)
设置光环板的网关。
- *gateway* 字符串类型，有效格式为"xx.xx.xx.xx"，表示将光环板的网关设置为该值。

## network2.get_gateway()
获得光环板自身的网关。
返回字符串。

## network2.create_client()
创建一个客户端。

## network2.client_connect_to(ip, port = DEFAULT_PORT)
客户端尝试与指定 IP 地址，指定端口的服务器连接。
- *ip* 字符串类型，有效值格式为"xx.xx.xx.xx"，表示客户端所在的 IP 地址。
- *port* 数值型，默认值为 DEFAULT_PORT， 表示客户端所在的端口。

返回布尔值，True：连接服务器成功； False: 连接服务器失败。

## network2.create_server(port = DEFAULT_PORT)
在给定的端口上创建一个服务器，一个光环板可以在不同的端口下创建多个不同服务器，而一个服务器可以同时与多个客户端建立连接。
- *port* 数值型，默认值为 DEFAULT_PORT ，表示在该端口上创建一个服务器。

## network2.server_wait_connection(port = DEFAULT_PORT)
等待给定端口上的服务器被连接，该 API 将阻塞线程直到指定端口的服务器与客户端建立了连接。
- *port* 字符串类型，默认值为 DEFAULT_PORT ，表示在该端口上创建一个服务器。

## network2.server_get_connections(port = DEFAULT_PORT)
以 list 格式获得指定端口下服务器连接的所有客户端的 IP 地址。
返回字符串数组，长度取决于服务器连接的客户端数量。

## network2.server_get_latest_connection(port = DEFAULT_PORT)
获得最近一次连接的客户端的 IP 地址。
返回字符串。

## network2.write(data, ip_to, port = DEFAULT_PORT)
向指定的 IP 地址，指定端口发送数据。
- *data*  字符串类型，表示发送的数据。
- *ip_to* 字符串类型，有效格式为"xx.xx.xx.xx"，表示数据发向的 IP 地址。
- *port*  数值型，默认值为 DEFAULT_PORT， 表示数据发向的端口号。

## network2.write_line(data, ip_to, port = DEFAULT_PORT)
向指定的 IP 地址，指定端口发送数据行。
- *data*  字符串类型，表示发送的数据。
- *ip_to* 字符串类型，有效格式为"xx.xx.xx.xx"，表示数据发向的 IP 地址。
- *port*  数值型，默认值为 DEFAULT_PORT， 表示数据发向的端口号。

## network2.set_read_buffer_size(size)
设置 network2.read函数的默认读的取字节长度
- *size* 数值型，默认为32， 表示read函数读取的字节数

## network2.read(ip_from, data_size = None, port = DEFAULT_PORT)
从指定 IP 地址，指定端口，接受数据。
- *ip_to* 字符串类型，有效格式为"xx.xx.xx.xx"，表示数据发向的 IP 地址。
- *data_size* 数值型， 默认为None，使用set_read_buffer_size定义的值，否则，使用data_size的值。
- *port*  数值型，默认值为 DEFAULT_PORT， 表示数据发向的端口号。

## network2.read_line(ip_from, port = DEFAULT_PORT)
从指定 IP 地址，指定端口，接受数据行。
- *ip_to* 字符串类型，有效格式为"xx.xx.xx.xx"，表示接受从该 IP 地址发送的消息。
- *port*  数值型，默认值为 DEFAULT_PORT， 表示接受从该端口号发送的消息。

注意1：write()与write_line()的区别近似print()与println()的关系，前者发送的数据在读取时有概念被黏连在一起，特别是当间隔很短地write()时，write_line()则没有这样的困扰，也因此，我们更推荐使用write_line()。
注意2：使用write_line()发送的数据只能使用read_line()进行读取，而使用write()发送的数据也只能使用read()读取，它们是一一对应使用的。