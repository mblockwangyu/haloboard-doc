# 事件头与广播
该部分 API 将帮助您监听光环板上的各类事件，学习如何使用广播进行线程间的通信，控制脚本的停止，以写出多线程程序。

### 省略代码中的halocode
注意：该部分 API 省略了“halocode.”，本篇提及的所有 API 均省略了“halocode.” ，如 led_driver.off( )实际为halocode.led_driver.off()。mbuild 电子模块平台的所有功能均维护在halocode库中，因此我们推荐如下写法，以便将halocode.进行省略，使得代码更简洁。

``` py
from halocode import *
```
<br>

## 事件
事件头有两种写法：<br>

方法一：注册方式，如以下示例：
```py
from halocode import *
def callback():
    led.on("red")
event.is_pressed(callback)
event.received(callback, 'hello')
```
在此写法下，事件头触发时将调用其参数中指向的函数。如在上方所展示的代码中，光环板在按钮被按下，以及收到广播消息"hello"时，都会变为红色。

方法二：修饰器写法，如以下示例：
```py
@event.start # 你需要监听的事件
def callback():
    led.on("green") # 事件被触发后需要执行的代码 

@event.received('hello') # 你需要监听的带参数的事件 
def callback():
    led.on("blue") # 事件被触发后需要执行的代码 
```
在此写法下，事件头触发时事件下定义的函数将被执行。在此例子中，该函数名称是callback()，但实际上，任何合乎命名规则的名字都是可以的。你也不用担心函数名称是否重名，因为该函数是被定义在事件下的。<br>

因此，你可以看到，尽管在上述例子中，按下按钮或接受到广播消息"hello"都将导致 callback() 被执行，但他们其实都仅指向各自的事件而不至于互相冲突。<br>

我们推荐使用修饰器写法，慧编程的积木块转移和 MU 编辑器中的代码补全都会生成修饰器写法的代码。


## `event.start()`<br>
启动事件，光环板启动时被触发。

## `event.is_shake()`<br>
摇晃事件，光环板检测到摇晃时被触发。

## `event.is_press()`<br>
按键按下事件，光环板板载按钮按下时被触发。

## `event.is_tiltleft()`<br>
左倾事件，光环板向左倾斜时被触发。

## `event.is_tiltright()`<br>
右倾事件，光环板向右倾斜时被触发。

## `event.is_arrowup()`<br>
前倾事件，光环板箭头向前倾时被触发。

## `event.is_arrowdown()`<br>
后倾事件，光环板箭头向后倾时被触发。

## `event.is_clockwise()`<br>
后倾事件，光环板箭头向后倾时被触发。

## `event.is_anticlockwise()`<br>
后倾事件，光环板箭头向后倾时被触发。

## `event.is_freefall()`<br>
后倾事件，光环板箭头向后倾时被触发。

## `event.receive(message)`<br>
广播事件，收到广播消息时被触发，参数：
- *message* 字符串类型，监听的广播消息名称。

## `event.upload_broadcast(message)`<br>
上传模式广播事件，收到上传模式广播消息时被触发，参数：
- *message* 字符串类型，上传模式广播的消息名称。

## `event.cloud_broadcast(message)`<br>
账号云广播事件，收到账号云广播消息时被触发，参数：
- *message* 字符串类型，账号云广播的消息名称。

## `event.mesh_broadcast(message)`<br>
mesh 局域网广播事件，参数：
- *message* 字符串类型，mesh 局域网广播的消息名称。

## `event.greater_than(threshold, type)`<br>
阈值比较事件， 超过阈值则触发，参数：
- *threshold* 数值型，有效范围是0-100，表示触发阈值。
- *type* 字符串类型，有效范围为microphone：代表声音传感器；timer：代表计时器，目前仅支持这两个。

## `event.pin0_is_touch()`<br>
被触摸事件，触摸触点0被触摸时，事件被触发。

## `event.pin1_is_touch()`<br>
被触摸事件，触摸触点1被触摸时，事件被触发。

## `event.pin2_is_touch()`<br>
被触摸事件，触摸触点2被触摸时，事件被触发。

## `event.pin3_is_touch()`<br>
被触摸事件，触摸触点3被触摸时，事件被触发。

## 广播
## `broadcast(message)`
发送广播消息，广播将能被所有光环板内的线程所接受，或在在线模式下被慧编程舞台接受，参数：
- *message* 字符串类型，广播消息的名称。

## 上传模式广播
## `upload_broadcast.set(message, val)`
发送广播消息，上传模式广播将能被所有光环板内的线程所接受，或在上传模式下被慧编程舞台接受，参数：
- *message* 字符串类型，广播消息的名称。
- *val* 局域网广播消息的值。

## `upload_broadcast.get(message)`
读取上传广播的附带值，参数：
- *message* 字符串类型，广播消息的名称。

返回字符串类型或者数值数据。

## 脚本控制
## `stop_all()`
停止所有脚本。

## `stop_this()`
停止当前脚本。

## `stop_other()`
停止当前脚本外的其他脚本。

## `restart()`
重启所有线程。

