# 局域网
该部分 API 将帮助您使用光环上基于 Wi-Fi 的组建局域网，并在局域网内实现广播通信。

### 省略代码中的halocode
注意：该部分 API 省略了“halocode.”，本篇提及的所有 API 均省略了“halocode.” ，如 led_driver.off( )实际为halocode.led_driver.off()。mBuild 电子模块平台的所有功能均维护在halocode库中，因此我们推荐如下写法，以便将halocode.进行省略，使得代码更简洁。

``` py
from halocode import *
```
<br>


## mesh.set(name = "mesh1")
组建一个局域网，参数：
- *name* 字符串，默认值为 "mesh1"，表示要建立的 MESH 局域网名称。

## mesh.join(name = "mesh1")
加入一个局域网，参数：
- *name* 字符串，默认值为 "mesh1"，表示要加入的 MESH 局域网名称。

## mesh_broadcast.set(message, val)
发送局域网广播并附带值，参数：
- *message* 字符串类型，广播消息的名称。
- *val* 局域网广播消息的值。

## mesh_broadcast.get(message)
获取广播信息的附带值，参数：
- *message* 字符串数据，广播的信息名称。

返回局域网广播消息的值。


