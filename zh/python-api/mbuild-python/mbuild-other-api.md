# 其他 mBuild 扩展模块

该部分 API 将帮助您使用其他 mBuild 电子模块。
注意：你需要额外购买包含 mBuild 电子模块扩展包或套装来获得 mBuild 模块以使用这些功能。

### 省略代码中的halocode
注意：该部分 API 省略了“halocode.”，本篇提及的所有 API 均省略了“halocode.” ，如 led_driver.off( )实际为halocode.led_driver.off()。mBuild 电子模块平台的所有功能均维护在halocode库中，因此我们推荐如下写法，以便将halocode.进行省略，使得代码更简洁。

``` py
from halocode import *
```
<br>

### 通用参数`index`
所有的 mbuild 电子模块的 API 均有一个参数index，它代表 API 指向的链上的第几个同类型模块。一般，具有默认值`1`。因此，当链上的每种模块只使用到一个时，你无需关注和独立设置它。
而当链上出现两个同类型模块时，你需要让index等于2、3、4甚至其他数字，以便控制链上的第2个、第3个，甚至第4个同类型模块，比如，motor_driver.set(100, index = 2)表示将链上第二个电机驱动的输出动力变为100。

### 红外收发
`ir.send(message, index = 1)`
发送红外消息，参数：
- *message* 字符串，长度限制为30个，暂时仅支持英文字母、数字标点的组合。

`ir.receive(index = 1)`
获得红外收发模块收到的消息。<br>
返回字符串。

`ir.record(record_id, index = 1)`
录制的红外消息，该 API 会阻塞线程3秒以完成红外信号的录制，参数：
- *record_id* 数值，有效值`1`或`2`，红外收发模块最多可以录制两个信号。

`ir.send_record(record_id, index = 1)`
发送录制的红外消息，参数：
- *record_id* 数值，有效值`1`或`2`，红外收发模块最多可以录制两个信号。

`ir.is_receive(message, index = 1)`
判断红外收发模块是否接受到了特定的红外信号，参数：
- *message* 字符串，也可以填写代表红外遥控信号的字符串变量，有效的变量及其含义有：<br>
<table>
<tr><td>变量</td><td> 含义</td></tr>
<tr><td>IR_REMOTE.up </td><td> ↑</td></tr>
<tr><td>IR_REMOTE.down  </td><td> ↓
<tr><td>IR_REMOTE.left  </td><td> ←
<tr><td>IR_REMOTE.right </td><td> →
<tr><td>IR_REMOTE.set   </td><td>设置
<tr><td>IR_REMOTE.zero   </td><td>0
<tr><td>IR_REMOTE.one    </td><td>1
<tr><td>IR_REMOTE.two    </td><td>2
<tr><td>IR_REMOTE.three  </td><td>3
<tr><td>IR_REMOTE.four   </td><td>4
<tr><td>IR_REMOTE.five   </td><td>5
<tr><td>IR_REMOTE.six    </td><td>6
<tr><td>IR_REMOTE.seven  </td><td>7
<tr><td>IR_REMOTE.eight  </td><td>8
<tr><td>IR_REMOTE.nine   </td><td>9
<tr><td>IR_REMOTE.A      </td><td>A
<tr><td>IR_REMOTE.B     </td><td> B
<tr><td>IR_REMOTE.C     </td><td> C
<tr><td>IR_REMOTE.D     </td><td> D
<tr><td>IR_REMOTE.E     </td><td> E
<tr><td>IR_REMOTE.F     </td><td> F
</table>


