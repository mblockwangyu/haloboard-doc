# 输出类 mBuild 模块
该部分 API 将帮助您使用输出类 mBuild 电子模块。
注意：你需要额外购买包含 mBuild 电子模块扩展包或套装来获得 mBuild 模块以使用这些功能。

![](../images/mbuild-all.png)

### 省略代码中的halocode
注意：该部分 API 省略了“halocode.”，本篇提及的所有 API 均省略了“halocode.” ，如 led_driver.off( )实际为halocode.led_driver.off()。mBuild 电子模块平台的所有功能均维护在halocode库中，因此我们推荐如下写法，以便将halocode.进行省略，使得代码更简洁。

``` py
from halocode import *
```
<br>

### 通用参数`index`
所有的 mbuild 电子模块的 API 均有一个参数index，它代表 API 指向的链上的第几个同类型模块。一般，具有默认值1。因此，当链上的每种模块只使用到一个时，你无需关注和独立设置它。
而当链上出现两个同类型模块时，你需要让index等于2、3、4甚至其他数字，以便控制链上的第2个、第3个，甚至第4个同类型模块，比如，motor_driver.set(100, index = 2)表示将链上第二个电机驱动的输出动力变为100。

## 灯光类
### LED 点阵
### `led_matrix.show(image = "hi", index = 1)`
设置 LED 点阵屏显示图像，参数：
- *image* 字符串，默认值为"hello"，image = "hi" 时字符串对应的点阵图像如下：

![](../images/hi.png)

<div style='display: none'>
//哈哈我是注释，不会在浏览器中显示。
//hello 是动图不好显示，所以这里用hi。
</div>



若需要让点阵屏显示非预置的图像，则需要以字符串形式填写自定义图像对应的十六进制数，生成规则如下：
将每列的8颗LED的亮灭状态看做一个8位的2进制数，将该8位2进制数转换为两位的十六进制数，点阵共16列即可以32位16进制数来表示点阵的亮灭状态。

![](../images/点阵对照表.png)

**程序示例1**
```py
from halocode import *
led_matrix.show(image = "hello", index = 1) #让 LED 点阵屏现实 "hello" 表情。
```
**程序示例2**
```py
from halocode import *
led_matrix.show(image = "00003c7e7e3c000000003c7e7e3c0000", index = 1) 
#让 LED 点阵屏现实 "hello" 表情。
```

### `led_matrix.print(message, index = 1)`
LED 点阵屏滚动显示字符信息，参数：
- *message* 字符串，有效范围为任意英文字母、数字、标点的组合，不支持中文，当需要显示的信息超出 LED 点阵屏的显示范围时，字符串将滚动显示。

### `led_matrix.print_until_done(message, index = 1)`
LED 点阵屏滚动显示字符信息直到结束，参数：
- *message* 字符串，有效范围为任意英文字母、数字、标点的组合，不支持中文，当需要显示的信息超出 LED 点阵屏的显示范围时，字符串将滚动显示，在滚动结束前，线程将阻塞。

### `led_matrix.print_at(message, x, y, index = 1)`
LED 点阵屏以给定的 x、y 坐标为起点显示字符串信息，参数：
- *message* 字符串，有效范围为任意英文字母、数字、标点的组合，不支持中文，表示需要 LED 点阵显示的信息，当需要显示的信息超出 LED 点阵屏的显示范围时，字符串将滚动显示，在滚动结束前，线程将阻塞。
- *x* 数值，有效范围0 - 15，表示显示字符串的起始 x 轴坐标。
- *y* 数值，有效范围0 - 7，表示显示字符串的起始 y 轴坐标。

### `led_matrix.on(x, y, index = 1)`
LED 点阵屏点亮特定位置的灯，参数：
- *x* 数值，有效范围0 - 15，表示要点亮的灯珠的 x 轴坐标。
- *y* 数值，有效范围0 - 7，表示要点亮的灯珠的 y 轴坐标。

### `led_matrix.off(x, y, index = 1)`
LED 点阵屏熄灭特定位置的灯，参数：
- *x* 数值，有效范围0 - 15，表示要熄灭的灯珠的 x 轴坐标。
- *y* 数值，有效范围0 - 7，表示要熄灭的灯珠的 y 轴坐标。

### `led_matrix.toggle(x, y, index = 1)`
LED 点阵屏切换特定位置的灯的点亮或熄灭状态，参数：
- *x* 数值，有效范围0 - 15，表示切换状态的灯珠的 x 轴坐标。
- *y* 数值，有效范围0 - 7，表示切换状态的灯珠的 y 轴坐标。

### `led_matrix.clear(index = 1)`
将整个 LED 点阵屏上的所有灯都熄灭。

### RGB 灯
### `rgb_led.on(r, g, b, index = 1)`
设置 RGB 灯模块的颜色，参数：
- *r* 数值或字符串类型。<br>
r 为数值时，有效范围是 0 - 255，表示全彩LED红色分量的数值，0时无红色分量，255时红色分量最亮。<br>
r 为字符串类型时，表示颜色名称或缩写。颜色名称及其缩写对照表：<br>

```
红 red r

橙 orange o

黄 yellow y

绿 green g

青 cyan c

蓝 blue b

紫 purple p

白 white w

黑 black k
```


- *g* 数值，全彩LED绿色分量的数值，参数范围是 0 ~ 255， 0为无绿色分量，255时绿色分量最亮。<br>
- *b* 数值，全彩LED蓝色分量的数值，参数范围是 0 ~ 255， 0为无蓝色分量，255时蓝色分量最亮。<br>
- *pct* 数值，灯环的亮度的大小，参数范围是0 ~ 100，默认值为 100。 0时灯环熄灭，100时灯环的按照设定的RGB值点亮。<br>

### `rgb_led.off(index = 1)`
熄灭 RGB 灯。

### `rgb_led.set_red(val, index = 1)`
改变 RGB 灯的R值，参数：
- *val* 数值，有效范围0 -255，表示 RGB 灯模块R值改变后的大小。

### `rgb_led.set_green(val, index = 1)`
改变 RGB 灯的G值，参数：
- *val* 数值，有效范围0 -255，表示 RGB 灯模块G值改变后的大小。

### `rgb_led.set_blue(val, index = 1)`
改变 RGB 灯的B值，参数：
- *val* 数值，有效范围0 -255，表示 RGB 灯模块B值改变后的大小。

### `rgb_led.add_red(val, index = 1)`
增加 RGB 灯的R值，参数：
- *val* 数值，有效范围-255 -255，表示 RGB 灯模块R值增加的大小。

### `rgb_led.add_green(val, index = 1)`
增加 RGB 灯的G值，参数：
- *val* 数值，有效范围-255 -255，表示 RGB 灯模块G值增加的大小。

### `rgb_led.add_blue(val, index = 1)`
增加 RGB 灯的B值，参数：
- *val* 数值，有效范围-255 -255，表示 RGB 灯模块B值增加的大小。

### `rgb_led.get_red(val, index = 1)`
获得 RGB 灯模块当前颜色的R值。
返回数值，可能的范围为0 -255`。

### `rgb_led.get_green(index = 1)`
获得 RGB 灯模块当前颜色的G值。
返回数值，可能的范围为0 -255。

### `rgb_led.get_blue(index = 1)`
获得 RGB 灯模块当前颜色的B值。
返回数值，可能的范围为0 -255。

### 彩灯驱动
### `led_driver.on(r, g, b, id = "all", index = 1)`
设置单颗或全部的灯珠颜色，参数：
- *r* 数值或字符串类型。<br>
r 为数值时，有效范围是 0 - 255，表示全彩LED红色分量的数值，0时无红色分量，255时红色分量最亮。<br>
r 为字符串类型时，表示颜色名称或缩写。颜色名称及其缩写对照表：<br>

```
红 red r

橙 orange o

黄 yellow y

绿 green g

青 cyan c

蓝 blue b

紫 purple p

白 white w

黑 black k
```

- *g* 数值，全彩LED绿色分量的数值，参数范围是 0 ~ 255， 0为无绿色分量，255时绿色分量最亮。<br>
- *b* 数值，全彩LED蓝色分量的数值，参数范围是 0 ~ 255， 0为无蓝色分量，255时蓝色分量最亮。<br>
- *id* 字符串类型或数值，默认值为"all"。
id 为字符串类型时，有效值为"all"，此时设置所有灯珠的颜色。<br>
id 为数值时，有效范围为1-96，此时设置对应位置的灯珠的颜色。

### `led_driver.show(color, index = 1)`
同时设置多颗灯珠为相应的颜色，参数：
- *color* 字符串类型，字符串格式需满足“color1 color2 color3 color4 color5 color6 color7 color8 color9 color10 color11 color12”，其中colorx为 "red"/"green"/"blue"/"yellow"/"cyan"/"purple"/"white"/"orange"/"black" 或是其缩写 "r"/"g"/"b"/"y"/"c"/"p"/"w"/"o"/"k"。颜色字符间以单个空格隔开。当颜色个数大于12时将被截断成12个。

### `led_driver.off(led_id = "all", index = 1)`
熄灭指定 id 的灯珠，参数：
- *id* 字符串类型或数值，默认值为"all"。
id 为字符串类型时，有效值为"all"，此时设置所有灯珠的颜色。<br>
id 为数值时，有效范围为1-96，此时设置对应位置的灯珠的颜色。

### `led_driver.set_red(val, led_id = "all", index = 1)`
改变指定位置灯珠的R值，参数：
- *val* 数值，有效范围0 -255，表示灯珠R值改变后的大小。
- *id* 字符串类型或数值，默认值为"all"。
id 为字符串类型时，有效值为"all"，此时设置所有灯珠的颜色。<br>
id 为数值时，有效范围为1-96，此时设置对应位置的灯珠的颜色。

### `led_driver.set_green(val, led_id = "all", index = 1)`
改变指定位置灯珠的G值，参数：
- *val* 数值，有效范围0 -255，表示灯珠G值改变后的大小。
- *id* 字符串类型或数值，默认值为"all"。
id 为字符串类型时，有效值为"all"，此时设置所有灯珠的颜色。<br>
id 为数值时，有效范围为1-96，此时设置对应位置的灯珠的颜色。

### `led_driver.set_blue(val, led_id = "all", index = 1)`
改变指定位置灯珠的B值，参数：
- *val* 数值，有效范围0 -255，表示灯珠B值改变后的大小。
- *id* 字符串类型或数值，默认值为"all"。
id 为字符串类型时，有效值为"all"，此时设置所有灯珠的颜色。<br>
id 为数值时，有效范围为1-96，此时设置对应位置的灯珠的颜色。

### `led_driver.add_red(val, led_id = "all", index = 1)`
增加指定位置灯珠的R值，参数：
- *val* 数值，有效范围-255 -255，表示灯珠R值增加的大小。
- *id* 字符串类型或数值，默认值为"all"。
id 为字符串类型时，有效值为"all"，此时设置所有灯珠的颜色。<br>
id 为数值时，有效范围为1-96，此时设置对应位置的灯珠的颜色。

### `led_driver.add_green(val, led_id = "all", index = 1)`
增加指定位置灯珠的G值，参数：
- *val* 数值，有效范围-255 -255，表示灯珠G值增加的大小。
- *id* 字符串类型或数值，默认值为"all"。
id 为字符串类型时，有效值为"all"，此时设置所有灯珠的颜色。<br>
id 为数值时，有效范围为1-96，此时设置对应位置的灯珠的颜色。

### `led_driver.add_blue(val, led_id = "all", index = 1)`
增加指定位置灯珠的B值，参数：
- *val* 数值，有效范围-255 -255，表示灯珠B值增加的大小。
- *id* 字符串类型或数值，默认值为"all"。
id 为字符串类型时，有效值为"all"，此时设置所有灯珠的颜色。<br>
id 为数值时，有效范围为1-96，此时设置对应位置的灯珠的颜色。

### `led_driver.set_mode(mode = "steady", index = 50)`
设置彩灯驱动的显示样式，参数：
- *mode* 字符串，有效值为 "steady"：静态，此时灯珠正常显示；"breath"：呼吸，此时灯珠会周期性地发生明亮变化；"marquee"：跑马灯，此时灯条或灯环会循环滚动；


## 播放类

### 扬声器
### `speaker.mute(index = 1)`
扬声器停止播放。

### `speaker.play_tone(freq, index = 1)`
设置扬声器以特定频率持续鸣叫，参数：
- *freq* 数值，有效范围为20-20000，超出这一范围，蜂鸣器不发声，另外出于对人耳的保护（长时间处于过高频率声音的环境下会对耳朵造成损失），该蜂鸣器被限制了最高频率5000Hz，因此当freq的值介于5000-20000之间时，扬声器仍只播放5000Hz的声音。
另外，若希望以扬声器通过频率改变播放曲子，我们需要知道不同频率与音符的对应关系，以下是给到的参考：

<table border="1"  align="center" cellpadding="2">

<tr><td></td><th width= "10%">do / C</th><th width= "10%">re / D</th><th width= "10%"> mi / E</th><th width= "10%">fa / F</th><th width= "10%"> sol / G</th><th width= "10%">la / A</th><th width= "10%"> si / B</th></tr>

<tr><th><b>2</th>
<td>65Hz</td><td>73Hz</td><td>82Hz</td><td>87Hz</td><td>98Hz</td><td>110Hz</td><td>123Hz</td></tr>

<tr><th><b>3</th>
<td>131Hz</td><td>147Hz</td><td>165Hz</td><td>175Hz</td><td>196Hz</td><td>220Hz</td><td>247Hz</td></tr>

<tr><th><b>4(标准中音)</th>
<td>262Hz</td><td>294Hz</td><td>330Hz</td><td>349Hz</td><td>392Hz</td><td>440Hz</td><td>494Hz</td></tr>

<tr><th><b>5</th>
<td>523Hz</td><td>587Hz</td><td>659Hz</td><td>698Hz</td><td>784Hz</td><td>880Hz</td><td>988Hz</td></tr>

<tr><th><b>6</th>
<td>1047Hz</td><td>1175Hz</td><td>1319Hz</td><td>1397Hz</td><td>1568Hz</td><td>1760Hz</td><td>1976Hz</td></tr>

<tr><th><b>7</th>
<td>2093Hz</td><td>2349Hz</td><td>2637Hz</td><td>2794Hz</td><td>3136Hz</td><td>3520Hz</td><td>3951Hz</td></tr>

<tr><th><b>8</th>
<td>4186Hz</td><td>4699Hz</td><td></td><td></td><td></td><td></td><td></td></tr>
</table>


例：标准中音音高（第一国际高度）为： **A4 = 440Hz**。

<div style='display: none'>
哈哈我是注释，不会在浏览器中显示。

['C2','65'],   ['D2','73'],   ['E2','82'],   ['F2','87'],  ['G2','98'],   ['A2','110'],  ['B2','123'],

['C3','131'],  ['D3','147'],  ['E3','165'],  ['F3','175'], ['G3','196'],  ['A3','220'],  ['B3','247'],

['C4','262'],  ['D4','294'],  ['E4','330'],  ['F4','349'], ['G4','392'],  ['A4','440'],  ['B4','494'],

['C5','523'],  ['D5','587'],  ['E5','659'],  ['F5','698'], ['G5','784'],  ['A5','880'],  ['B5','988'],

['C6','1047'], ['D6','1175'], ['E6','1319'], ['F6','1397'],['G6','1568'], ['A6','1760'], ['B6','1976'],

['C7','2093'], ['D7','2349'], ['E7','2637'], ['F7','2794'],['G7','3136'], ['A7','3520'], ['B7','3951'], 
['C8','4186'], ['D8','4699'],
</div>

### `speaker.play_music(music, index = 1)`
扬声器播放预置的或用户自定义的音频文件，参数：
- *music* 字符串，有效范围为任意4字节长度的英文数字、字母、标点的组合，代表需要播放的音频在扬声器中的文件名，扬声器只支持存放mp3类型的音频文件，该 API 不会持续阻塞当前线程。
该扬声器预置了大量短音频可供使用，它们的文件名和音频内容请点击下载 <a href="../images/预置音效列表.xlsx" download>预置音效列表</a>；若希望通过扬声器播放自定义的音频文件，请查看此链接：http://docs.makeblock.com/diy-platform/zh/mbuild/hardware/output-modules/speaker.html

### `speaker.play_music_until_done(music, index = 1)`
扬声器播放预置的或用户自定义的音频文件，参数：
- *music* 字符串，有效范围为任意4字节长度的英文数字、字母、标点的组合，代表需要播放的音频在扬声器中的文件名，扬声器只支持存放mp3类型的音频文件，该 API 会持续阻塞当前线程直到音频播放完成。

### `speaker.set_vol(val, index = 1)`
设置扬声器播放音乐的音量大小，参数：
- *vol* 数值，有效范围 0 - 100，表示设定的扬声器音量值。

### `speaker.add_vol(val, index = 1)`
改变扬声器播放音乐的音量大小（扬声器播放特定频率声音的大小不会被改变），参数：
- *vol* 数值，有效范围-100 - 100，表示扬声器音量的改变值。

### `speaker.get_vol(index = 1)`
获得扬声器的当前音量。<br>
返回数值，可能范围为0-100，当范围值为100时，表示扬声器达到了其音量的最大值。

### `speaker.is_play(index = 1)`
检测扬声器是否正在播放音乐。<br>
返回布尔值，True：扬声器正在播放歌曲，False：扬声器不在播放歌曲。

## 动力类
### 电机驱动
### `motor_driver.set(power, index = 1)`
设置电机驱动的输出功率，参数：
- *power* 数值，有效范围为-100 -100，表示电机驱动输出的功率大小，单位`%`。电机驱动功率为100%时，对应1024的PWM波形。一般的，电机驱动输出正功率会使得电机以逆时针旋转，负功率会使电机以顺时针旋转。

### `motor_driver.add(power, index = 1)`
增加电机驱动的输出功率，参数：
- *power* 数值，有效范围为-200 /~200，表示电机驱动增加的输出的功率大小，单位`%`。电机驱动功率为100%时，对应1024的PWM波形。

### `motor_driver.get(index = 1)`
获得电机驱动的当前的输出功率大小。<br>
返回数值，可能范围为-100 - 100。

### `motor_driver.get_load(index = 1)`
获得电机驱动的当前负载状况，负载会随着电机的实际转动的情况发生变化，当电机大负载运行或堵转时（短路也有可能造成高负载），电机驱动将检测到一个较大的负载值。<br>
返回数值，可能范围为0 - 1024。

### `motor_driver.stop(index = 1)`
电机驱动停止输出功率。

### 舵机驱动
### `servo_driver.set(angle, index = 1)`
舵机驱动设置舵机的度数，参数：
- *angle* 数值，有效范围为0 - 180，单位`°`，表示舵机转动到的角度。

### `servo_driver.add(angle, index = 1)`
舵机驱动增加舵机的度数，参数：
- *angle* 数值，有效范围为-180 - 180，单位`°`，表示舵机从当前位置转动的角度。

### `servo_driver.get(index = 1)`
舵机驱动获得当前设置的度数。<br>
返回数值，可能值为0 - 180，单位为`°`。

### `servo_driver.get_load(index = 1)`
舵机驱动驱动获得当前负载，负载会随着舵机的实际转动的情况发生变化，当舵机以大负载运行或堵转时（短路也有可能造成高负载），舵机驱动将检测到一个较大的负载值。<br>
返回数值，可能范围为0 - 1024。

### `servo_driver.release(index = 1)`
舵机驱动释放角度，当舵机驱动释放角度后，舵机将可以在外力作用下被拧动，但此时 servo_driver.get(index)将无法正确获得舵机的度数。

### 智能伺服电机（12公斤级）
### `smart_servo.turn(angle, speed, index = 1)`
智能舵机转动指定角度，该积木块会阻塞线程直至动作完成或被其他指令打断，参数：
- *angle* 数值，表示舵机在当前位置基础上转过的角度，当该数值为正时，舵机顺时针转动，当该数值为负时，舵机逆时针转动。<br>

### `smart_servo.turn_to(angle, speed, index = 1 )`
智能舵机转动到指定角度，该积木块会阻塞线程直至动作完成或被其他指令打断，参数：
- *angle* 数值，表示舵机最终转到的角度，数值为正时，舵机顺时针转动，当该数值为负时，舵机逆时针转动。<br>

### `smart_servo.run(power, index = 1)`
设定智能舵机转动的速度，参数 ：
- *power* 数值，有效范围为0 - 100

### `smart_servo.stop(index = "all")`
智能舵机停止运动，参数：
- *index* 字符串或数值，默认为"all"，此时指令指向链上所有智能舵机。

### `smart_servo.get_angle(index = 1)`
获得智能舵机当前所在的角度。
返回数值，表示智能舵机自初始位置转过的角度。

### `smart_servo.get_speed(index = 1)`
获得智能舵机当前的转动速度，无论是智能舵机自己的转速，抑或是智能舵机在外力作用下被转动的度数。
返回数值，代表转动的速度，单位为`°/s`。

### `smart_servo.release_angle(index = "all")`
释放智能舵机，此时智能舵机将不会被锁定在当前的角度，因此可以在外力下被转动。智能舵机将保持释放角度状态直到接收到smart_servo.turn(angle)，smart_servo.turn_to(angle)， smart_servo.drive(power, t = "forever")，smart_servo.lock_angle()中任意一条指令的执行

### `smart_servo.lock_angle(index = "all")`
锁定智能舵机度数，此时智能舵机的角度将不可在外力作用下被改变。

### `smart_servo.reset(index = "all")`
重置智能舵机，重置后智能舵机将以当前位置为舵机零点。由于智能舵机的可重置次数有限，故该指令会阻塞线程3秒以防止该指令被重复执行。