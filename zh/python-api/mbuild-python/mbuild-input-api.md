# 输入类 mBuild 模块
该部分 API 将帮助您使用输入类 mBuild 电子模块。
注意：你需要额外购买包含 mBuild 电子模块扩展包或套装来获得 mBuild 模块以使用这些功能。

### 省略代码中的halocode
注意：该部分 API 省略了“halocode.”，本篇提及的所有 API 均省略了“halocode.” ，如 led_driver.off( )实际为halocode.led_driver.off()。mBuild 电子模块平台的所有功能均维护在halocode库中，因此我们推荐如下写法，以便将halocode.进行省略，使得代码更简洁。

``` py
from halocode import *
```
<br>

### 通用参数`index`
所有的 mbuild 电子模块的 API 均有一个参数index，它代表 API 指向的链上的第几个同类型模块。一般，具有默认值1。因此，当链上的每种模块只使用到一个时，你无需关注和独立设置它。
而当链上出现两个同类型模块时，你需要让index等于2、3、4甚至其他数字，以便控制链上的第2个、第3个，甚至第4个同类型模块，比如，motor_driver.set(100, index = 2)表示将链上第二个电机驱动的输出动力变为100。


## 交互类
### 按钮
### `button.is_press(index = 1)`
检测按钮是否被按下。<br>
返回布尔值，True：按钮被按下；False：按钮未被按下。

### `button.get_count(index = 1)`
获得按钮自通电后被按下的次数。<br>
返回数值。

### `button.reset_count(index = 1)`
重置按钮被按下的次数，重置后次数归零。

### 角度传感器
### `angle_sensor.get(index = 1)`
获得角度传感器转过的角度（相对于通电瞬间所在的位置）。<br>
返回数值，单位`°`，角度传感器顺时针旋转时角度增大，逆时针旋转时角度减小。

### `angle_sensor.reset(index = 1)`
角度传感器重置转过的角度。

### `angle_sensor.get_speed(index = 1)`
获得角度传感器转动的速度。<br>
返回数值，单位`°/s`，角度传感器顺时针旋转时返回值为正，逆时针旋转时返回值为负。

### `angle_sensor.is_clockwise(index = 1).`
检测角度传感器是否正顺时针转动。<br>
返回布尔值，True：正顺时针转动；False：未顺时针转动。

### `angle_sensor.is_anticlockwise(index = 1)`
检测角度传感器是否正逆时针转动。<br>
返回布尔值，True：正逆时针转动；False：未逆时针转动。

### 滑动电位器
## `slider.get(index = 1)`
获得滑动电位器的位置。<br>
返回数值，可能值为0-100，单位为`%`，表示滑杆所在的位置。

### 摇杆
### `joystick.get_x(index = 1)`
获得摇杆的 x 轴读数。<br>
返回数值，可能的范围为-100 - 100。

### `joystick.get_y(index = 1)`
获得摇杆的 y 轴读数。<br>
返回数值，可能的范围为-100 - 100。

### `joystick.is_up(index = 1)`
判断摇杆是否摇到上方。
返回布尔值，True：摇杆向上摇动；False：摇杆未向上摇动。

### `joystick.is_down(index = 1)`
判断摇杆是否摇到下方。
返回布尔值，True：摇杆向下摇动；False：摇杆未向下摇动。

### `joystick.is_left(index = 1)`
判断摇杆是否摇到左方。
返回布尔值，True：摇杆向左摇动；False：摇杆未向左摇动。

### `joystick.is_right(index = 1)`
判断摇杆是否摇到右方。
返回布尔值，True：摇杆向右摇动；False：摇杆未向右摇动。

### 多路触摸
### `multi_touch.is_touch(ch = "any", index = 1)`
检测多路触摸对应触点是否被触摸，多路触摸主要根据触点的电容值变化检测物体，因此在合适的阈值设置下，隔着纸、木板、塑料等绝缘体也可以被检测到，因为导体的靠近终归是改变了触点的电容值，参数：
- *ch* 字符串或数值变量，默认为"any"。
ch 为字符串类型时，有效值为"any"，此时任意触点被触摸都会被检测到。<br>
ch 为数值时，有效范围为1-8，对应同序号的触摸触点。
返回布尔值，True：对应触点被触摸；False：对应触点未被触摸。

### `multi_touch.reset(level = "middle", index = 1)`
设置多路触摸的触摸阈值，参数：
- *level* 字符串，有效值为 low：低灵敏度；middle：中灵敏度；high：高灵敏度。


## 感知类
### 光线传感器
### `light_sensor.get(index = 1)`
获得光线传感数值。<br>
返回数值，可能范围为0 - 100。

### 双路颜色传感器
### `dual_rgb_sensor.is_color(color = "white", ch, index = 1)`
判断颜色传感器是否检测到了特定的颜色，参数：
- *color* 字符串，表示需要检测的颜色，颜色名称及其缩写对照表：<br>

```
红 red r

黄 yellow y

绿 green g

青 cyan c

蓝 blue b

紫 purple p

白 white w

黑 black k
```

- *ch* 数值，有效范围为1-2，1对应 RGB1，2对应 RGB2。<br>
返回布尔值，True：检测到指定颜色；False：未检测到指定颜色。

### `dual_rgb_sensor.get_red(ch = 1, index = 1)`
获取颜色传感器检测到颜色的R值。参数：
- *ch* 数值，默认值1，有效范围为1-2，1对应 RGB1，2对应 RGB2。<br>
返回数值，可能值为0 -255。

### `dual_rgb_sensor.get_green(ch = 1, index = 1)`
获取颜色传感器检测到颜色的G值，参数：<br>
- *ch* 数值，默认值1，有效范围为1-2，1对应 RGB1，2对应 RGB2。<br>
返回数值，可能值为0 -255。

### `dual_rgb_sensor.get_blue(ch = 1, index = 1)`
获取颜色传感器检测到颜色的B值。参数：
- *ch* 数值，默认值1，有效范围为1-2，1对应 RGB1，2对应 RGB2。<br>
返回数值，可能值为0 -255。

### `dual_rgb_sensor.get_light(ch = 1, index = 1)`
获得颜色传感器检测到环境光强度，颜色传感器自身的补光灯也是环境光的一部分，如有必要请关闭补光灯后使用该 API。参数：
- *ch* 数值，默认值1，有效范围为1-2，1对应 RGB1，2对应 RGB2。<br>
返回数值，可能范围为0 -100。

### `dual_rgb_sensor.get_gray_level(ch = 1, index = 1)`
获得颜色传感器检测到物体反射光强度，该读值来自开启白色补光灯的环境光强度与不开启补光灯时环境光强度的差值，参数：
- *ch* 数值，默认值1，有效范围为1-2，1对应 RGB1，2对应 RGB2。<br>
返回数值，可能范围为0 -100。

### `dual_rgb_sensor.set_led(color = "white", index = 1)`
设置补光灯的颜色。参数：
- *color* 字符串，表示补光灯的点亮颜色，颜色名称及其缩写对照表：<br>

```
红 red r

黄 yellow y

绿 green g

青 cyan c

蓝 blue b

紫 purple p

白 white w

黑 black k
```

- *ch* 数值，有效范围为1-2，1对应 RGB1，2对应 RGB2。<br>

### `dual_rgb_sensor.off_led(index = 1)`
关闭补光灯。需要注意，颜色传感器在调用检测颜色 API 时会自动将补光灯设置为所需的颜色。

### 声音传感器
### `sound_sensor.get(index = 1)`
获得声音传感器读值。<br>
返回数值，可能范围为0 - 100。

### 热运动传感器
### `pir.is_detect(index = 1)`
检测热运动传感器是否检测到人体运动。热运动传感器被触发后，指示灯将被点亮，传感器将保持触发状态3秒，若之后的3秒都未继续检测到人体运动，则传感器退出触发状态，指示灯熄灭。<br>
返回布尔值，True：检测到人体运动;False：未检测到人体运动。

### `pir.get_count(index = 1)`
获得热运动传感器至上电后被触发的次数。<br>
返回数值，可能值为任意正整数。

### `pir.reset_count(index = 1)`
重置热运动传感器被触发的次数为0。<br>

### 超声波
### `ultrasonic.get(index = 1)`
获得测距传感器与障碍物之间的距离。<br>
返回数值，可能的范围为3 - 300，单位是cm，误差为±5%。

### 测距传感器
### `ranging_sensor.get(index = 1)`
获得测距传感器与障碍物之间的距离。<br>
返回数值，可能的范围为2 - 200，单位是cm，误差为±5%。

### 运动传感器
### `motion_sensor.is_shake(index = 1)`
检测运动传感器模块是否有被摇晃。<br>
返回值布尔值，True：表示运动传感器模块被晃动了；False：表示运动传感器模块未被晃动。

### `motion_sensor.get_shakeval(index = 1)`
获得运动传感器模块的摇晃强度。<br>
返回数值，可能范围是0-100，数值越大，晃动的强度就越大。

### `motion_sensor.get_accel(axis, index = 1)`
获取三个轴的加速度值，参数：
- *axis* 字符串类型，可能的值为 "x"/"y"/"z" 代表运动传感器模块定义的坐标轴。<br>
返回数值，单位是 m/s²。

### `motion_sensor.is_tiltleft(index = 1)`
检测运动传感器模块是否向左倾斜，阈值15°。<br>
返回布尔值，True：表示运动传感器模块向左倾斜了；False：表示运动传感器模块未向左倾斜。

### `motion_sensor.is_tiltright(index = 1)`
检测运动传感器模块是否向右倾斜，阈值15°。<br>
返回布尔值，True：表示运动传感器模块向右倾斜了；False：表示运动传感器模块未向右倾斜。

### `motion_sensor.is_tiltup(index = 1)`
检测运动传感器模块是否前倾，阈值15°。<br>
返回布尔值，True：表示模块前倾，False：表示模块没有前倾。

### `motion_sensor.is_tiltdown(index = 1)`
检测运动传感器模块是否后倾，阈值15°。<br>
返回布尔值，True：表示模块后倾；False：表示模块没有后倾。

### `motion_sensor.is_faceup(index = 1)`
检测模块是否朝上状态。<br>
返回布尔值，True：表示模块朝上；False：表示模块未朝上。

### `motion_sensor.is_facedown(index = 1)`
检测模块是否朝下状态。<br>
返回布尔值，True：表示模块朝下；False：表示模块未朝下。

### `motion_sensor.get_roll(index = 1)`
获取姿态角的翻滚角。<br>
返回数值，可能范围为-90 - 90，单位是°。

### `motion_sensor.get_pitch(index = 1)`
获取姿态角的俯仰角。<br>
返回数值，可能范围为-180 - 180，单位是°。

### `motion_sensor.get_yaw(index = 1)`
获取姿态角的偏航角。<br>
返回数值，可能范围为0 - 360，单位是°。<br>
注意：由于没有电子罗盘,所以实际上偏航角只是使用了Z轴角速度的积分。它存在着积累误差。如果是想获得真实偏航角的，这个 API 不适合使用。

### `motion_sensor.get_gyro(axis, index = 1)`
获取三个轴的角速度值，参数：
- *axis* 字符串类型，有效值为"x"/"y"/"z"，代表运动传感器模块定义的坐标轴。<br>
返回数值，单位是 °/秒。

### `motion_sensor.get_rotation(axis, index = 1)`
获得运动传感器模块在三个轴上转动的角度，以逆时针转动方向为正方向，参数：
- *axis* 字符串类型，有效值为"x"/"y"/"z"，代表运动传感器模块定义的坐标轴。<br>
返回数值，单位是 °。

### `motion_sensor.reset_rotation(axis= "all", index = 1)`
初始化绕三个轴转动的当前角度为0，get_rotation() 函数将从 0 开始计算，参数：
- *axis* 字符串类型，有效值是 "x"/"y"/"z"/"all"。"x"/"y"/"z"代表运动传感器模块定义的坐标轴。默认值为"all" 代表全部的三个轴。

### 土壤湿度传感器
### `soil_sensor.get(index = 1)`
获得土壤湿度传感器读值。<br>
返回数值，可能范围为0 - 100。

### 温度传感器
### `temp_sensor.get(index = 1)`
获得温度传感器读值。<br>
返回数值，可能范围为-55 - 125，单位℃。

### 温湿度传感器
### `humiture.get_humidity(index = 1)`
获得温湿度传感器的湿度读值。<br>
返回数值，可能范围为0 - 100，单位%。

### `humiture.get_temp(index = 1)`
获得温湿度传感器的温度读值。<br>
返回数值，可能范围为-40 - 125，单位℃。

### MQ2气体传感器
### `mq2.is_detect(level = ”high“, index = 1)`
检测可燃气体浓度是否超过阈值，参数：
- *level* 字符串，有效范围为"high"：以高灵敏度检测可燃气体，此时传感器更易被触发；"low"：以低灵敏度检测可燃气体，此时传感器不易被触发；

### `mq2.get(index = 1)`
获得 MQ2 气体传感器读值。<br>
返回数值，可能范围为0 - 100，数值越大，可燃性气体浓度越高。

### 火焰传感器
### `flame_sensor.is_detect(index = 1)`
检测是否有火焰。
返回布尔值，True：检测到火焰；False：未检测到火焰。

### `flame_sensor.get(index = 1)`
获得火焰传感器读值。<br>
返回数值，可能范围为0 - 100。

### 磁敏传感器
### `magnetic_sensor.is_detect(index = 1)`
检测模块附近是否有磁体。
返回布尔值，True：检测到磁体；False：未检测到磁体。

### `magnetic_sensor.get_count(index = 1)`
获得磁敏模块自通电后被触发的次数。<br>
返回数值。

### `magnetic_sensor.reset_count(index = 1)`
重置磁敏模块被触发的次数，重置后次数归零。

### 视觉模块
### `smart_camera.set_mode(mode = "color", index = 1)`
设置视觉模块的识别模式，参数：
- *mode* 字符串，有效值为 color：色块识别模式，此时可以识别和追踪色块；"line"：线追踪模式，此时可以识别线条和条形码。

### `smart_camera.learn(sign = 1, t = "until_button", index = 1)`
视觉模块学习色块标记，参数：
- *sign* 数值，有效范围为1 - 7的整数，表示对应序号的色块标记。<br>
- *t* 数值或字符串，默认值为"until_button"。
t 为字符串是，有效值为"until_button"：当板载按钮按下时结束学习。
t 为数值时，表示视觉模块在倒计时结束后学习并记录色块标记，在此之前，线程将持续阻塞。

### `smart_camera.detect_sign(sign, index = 1)`
判断视觉模块是否检测到特定色块标记，参数：
- *sign* 数值，有效范围为1 - 7的整数，表示对应序号的色块标记。<br>
返回布尔值，True：检测到特定色块标记；False：未检测到特定色块标记。

### `smart_camera.detect_sign_location(sign, location, index = 1)`
判断指定的色块标记是否位于视觉模块画面的某一区域，参数：
- *sign* 数值，有效范围为1 - 7的整数，表示对应序号的色块标记。<br>
- *location* 字符串，有效值为 "up"：色块是否位于画面上方；"down"：色块是否位于画面下方；"left"：色块是否位于画面左边；"right"：色块是否位于画面右边；"middle"：色块是否位于画面中央。<br>
各区域示意图如下：




![](../images/视觉模块识别区.png)<br>



返回布尔值，True：检测到特定色块标记在指定区域；False：未检测到特定色块标记在指定区域。

### `smart_camera.get_sign_x(sign, index = 1)`
获得特定色块标记在画面上的 x 坐标，参数：
- *sign* 数值，有效范围为1 - 7的整数，表示对应序号的色块标记。<br>
返回值数值，可能的范围为0 - 319，若画面上检测不到指定的色块标记，则返回0值。

### `smart_camera.get_sign_y(sign, index = 1)`
获得特定色块标记在画面上的 y 坐标，参数：
- *sign* 数值，有效范围为1 - 7的整数，表示对应序号的色块标记。<br>
返回值数值，可能的范围为0 - 239，若画面上检测不到指定的色块标记，则返回0值。

### `smart_camera.get_sign_wide(sign, index = 1)`
获得特定色块标记在画面上的宽度，参数：
- *sign* 数值，有效范围为1 - 7的整数，表示对应序号的色块标记。<br>
返回值数值，可能的范围为0 - 319，若画面上检测不到指定的色块标记，则返回0值。

### `smart_camera.get_sign_hight(sign, index = 1)`
获得特定视觉标记在画面上的高度，参数：
- *sign* 数值，有效范围为1 - 7的整数，表示对应序号的色块标记。<br>
返回值数值，可能的范围为0 - 239，若画面上检测不到指定的色块标记，则返回0值。

### `smart_camera.open_light(index = 1)`
打开视觉模块上的两颗补光灯。

### `smart_camera.close_light(index = 1)`
关闭视觉模块上的两颗补光灯。

### `smart_camera.reset(index = 1)`
重置视觉模块的白平衡，该 API 将阻塞当前线程5秒。

### `smart_camera.detect_label(label, index = 1)`
判断视觉模块是否检测到特定条形码标记，参数：
- *label* 数值，有效范围为1 - 15的整数，表示对应序号的条形码标记。<br>
返回吧布尔值，True：检测到特定条形码标记；False：未检测到特定条形码标记。

### `smart_camera.get_label_x(label, index = 1)`
获得特定条形码标记在画面上的 x 坐标，参数：
- *label* 数值，有效范围为1 - 15的整数，表示对应序号的条形码标记。<br>
返回值数值，可能的范围为0 - 319，若画面上检测不到指定的条形码标记，则返回0值。

### `smart_camera.get_label_y(sign, index = 1)`
获得特定条形码标记在画面上的 x 坐标，参数：
- *label* 数值，有效范围为1 - 15的整数，表示对应序号的条形码标记。<br>
返回值数值，可能的范围为0 - 239，若画面上检测不到指定的条形码标记，则返回0值。

### `smart_camera.detect_cross(index = 1)`
判断画面中是否存在交叉路口。<br>

### `smart_camera.get_cross_x(index = 1)`
获得交叉点的 x 轴坐标。<br>
返回值数值，可能的范围为0 - 319，若画面上检测不到交叉点，则返回0值。

### `smart_camera.get_cross_y(index = 1)`
获得交叉点的 y 轴坐标。<br>
返回值数值，可能的范围为0 - 239，若画面上检测不到交叉点，则返回0值。

### `smart_camera.get_cross_road(index = 1)`
获得交叉点上的岔路数量。<br>
返回值数值。

### `smart_camera.get_cross_angle(sn = 1, index = 1)`
获得指定岔路与vector的夹角大小，vector指代视觉模块当前所在的线段，参数：
- *sn* 数值，表示vector是与岔路上第几个岔路形成夹角。

### `smart_camera.set_line(mode = "black, index = 1)`
设置线追踪模式的色彩偏好，参数：
- *mode* 字符串，有效范围为 "black"：识别浅色背景下的深色线；"white"：识别深色背景下的浅色线。

### `smart_camera.get_vector_start_x(index = 1)`
获得当前巡线"vector"的起始x坐标值。<br>
返回数值，可能的范围为0 - 319。

### `smart_camera.get_vector_start_y(index = 1)`
获得当前巡线"vector"的起始y坐标值。<br>
返回数值，可能的范围为0 - 239。

### `smart_camera.get_vector_end_x(index = 1)`
获得当前巡线"vector"的终止x坐标值。<br>
返回数值，可能的范围为0 - 319。

### `smart_camera.get_vector_end_y(index = 1)`
获得当前巡线"vector"的终止y坐标值。<br>
返回数值，可能的范围为0 - 239。

### `smart_camera.set_vector_angle(angle, index = 1)`
设置vector的偏好角度，参数：
- *angle* 数值，表示设置vector的偏好角度，视觉模块在遇到岔路口时，会将最接近angle值的岔路视为下一个vector。

### `smart_camera.get_vector_angle(index = 1)`
获得当前设置的偏好角度。<br>
返回数值，可能范围为-180 - 180，单位°。

### `smart_camera.set_kp(kp, index = 1)`
设置视觉模块计算电机差速的 Kp 值大小，参数：
- *kp* 数值，表示要设置的 Kp 值大小。

### `smart_camera.get_sign_diff_speed(sign, axis, axis_val, index = 1)`
计算将指定色块标记锁定到指定 x，y坐标上所需要的电机差速值，该差数值大小同时受Kp大小的变化，Kp越大，该值越大，参数：
- *sign* 数值，有效范围为1 - 7的整数，表示对应序号的色块标记。<br>
- *axis* 字符串，有效值为"x"：计算将色块移动到 x = axis_val 轴上所需要的电机差速；"y"：计算将色块移动到 y = axis_val 轴上所需要的电机差速。
- *axis_val* 数值，若 axis = "x"，有效范围0 - 319；若 axis = "y"，有效范围0 - 239。<br>
返回数值，可能值范围0 - 100。

### `smart_camera.get_label_diff_speed(label, axis, axis_val, index = 1)`
计算将指定条形码标记锁定到指定 x，y坐标上所需要的电机差速值，该差数值大小同时受Kp大小的变化，Kp越大，该值越大，参数：
- *label* 数值，有效范围为1 - 15的整数，表示对应序号的条形码标记。<br>
- *axis* 字符串，有效值值得"x"：计算将条形码移动到 x = axis_val 轴上所需要的电机差速；"y"：计算将条形码移动到 y = axis_val 轴上所需要的电机差速。
- *axis_val* 数值，若 axis = "x"，有效范围0 - 319；若 axis = "y"，有效范围0 - 239。<br>
返回数值，可能值范围0 - 100。

### `smart_camera.get_follow_vector_diff_speed(index = 1)`
计算将保持vector位于画面中心所需要的电机差速值，该差数值大小同时受Kp大小的变化，Kp越大，该值越大。
返回数值，可能值范围0 - 100。

### `smart_camera.is_lock_sign(sign, axis, axis_val, index = 1)`
判断色块标记是否位于目标 x，y 附近，参数：
- *sign* 数值，有效范围为1 - 7的整数，表示对应序号的色块标记。<br>
- *axis* 字符串，有效值为"x"：判断色块是否移动到 x = axis_val 的轴附近；"y"：判断色块是否移动到 y = axis_val 的轴附近；
- *axis_val* 数值，若 axis = "x"，有效范围0 - 319；若 axis = "y"，有效范围0 - 239。<br>
返回布尔值，True：位于目标点附近；False：不在目标点附近。

### `smart_camera.is_lock_label(sign, axis, axis_val index = 1)`
判断条形码标记是否位于目标 x，y 附近，参数：
 *label* 数值，有效范围为1 - 15的整数，表示对应序号的条形码标记。<br>
- *axis* 字符串，有效值为"x"：判断条形码是否移动到 x = axis_val 的轴附近；"y"：判断条形码是否移动到 y = axis_val 的轴附近；
- *axis_val* 数值，若 axis = "x"，有效范围0 - 319；若 axis = "y"，有效范围0 - 239。<br>
返回布尔值，True：位于目标点附近；False：不在目标点附近。