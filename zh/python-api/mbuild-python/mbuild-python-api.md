# mBuild 电子模块平台 Python API

该部分 API 将帮助您使用 mBuild 电子模块平台以扩展光环板的功能和使用场景。
注意：你需要额外购买包含 mBuild 电子模块扩展包或套装来获得 mBuild 模块以使用这些功能。

<img src="../images/mbuild-all.png" style="padding:3px 0px 12px 3px;width:500px;">

* [输入类 mbuild 模块](mbuild-input-api.md)
* [输出类 mbuild 模块](mbuild-output-api.md)
* [其他 mBuild 扩展模块](mbuild-other-api.md)
