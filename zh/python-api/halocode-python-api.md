# 光环板 Python API 文档
欢迎使用光环板学习 Python 编程，光环板具备丰富的输入输出能力，能够让你和你的代码互动起来，并直观地展现撰写代码的效果。
光环板具备蓝牙及 Wi-Fi，您可以通过他们组建本地局域网，或连接至因特网。在实际项目进行动手实践，并由此了解和掌握诸如物联网、人工智能、计算机网络方面的相关知识和技能。
光环板还拥有30余种扩展电子模块以及小车扩展板，能够支持诸如智慧农场、智能家居、自动驾驶车等项目制作。
上述这些功能均被维护在了`halocode`库中。

注意：以下提及的所有 API 均省略了 `halocode.`，如`led.off()`实际为`halocode.led.off()`。
本文档涉及的所有 API 均维护在`halocode`库中，因此我们推荐如下写法，以便将`halocode.`进行省略，使得代码更简洁。
``` py
from halocode import *
```

恢复出厂程序
你可以通过上传如下指令恢复出厂时自带的程序。
`import factory_script`

## 光环板 API
该部分 API 将帮助您使用光环板自身的诸多功能。
### 灯光类
`led.on(r,g,b,pct = 100, id = "all")`
`led.play(name = "rainbow")`
`led.show(color, offset = 0)`
`led.move(offset = 1)`
`led.chart(pct)`
`led.off(id = "all")`

### 感知类
#### 按钮
`is_press()`
#### 麦克风
`get_loudness(type = "maximum")`
#### 触摸引脚
`pin0.is_touch()`
`pin1.is_touch()`
`pin2.is_touch()`
`pin3.is_touch()`

`pin0.get_touch()`
`pin1.get_touch()`
`pin2.get_touch()`
`pin3.get_touch()`

`pin0.set_touch(pct = "middle")`
`pin1.set_touch(pct = "middle")`
`pin2.set_touch(pct = "middle")`
`pin3.set_touch(pct = "middle")`

#### 运动传感器
`is_shake()`
`get_shakeval()`
`is_freefall()`
`get_accel(axis)`
`is_tiltleft()`
`is_tiltright()`
`is_arrowup()`
`is_arrowdown()`
`is_faceup()`
`is_facedown()`
`is_clockwise()`
`is_anticlockwise()`

`get_roll()`
`get_pitch()`
`get_yaw()`
`get_gyro(axis)`
`get_rotation(axis)`
`reset_rotation(axis= "all")`

#### 计时器
`timer.get()`
`timer.reset()`

### 引脚
`pin0.write_digital(val = True)`
`pin1.write_digital(val = True)`
`pin2.write_digital(val = True)`
`pin3.write_digital(val = True)`

`pin0.read_digital(type = "boolean")`
`pin1.read_digital(type = "boolean")`
`pin2.read_digital(type = "boolean")`
`pin3.read_digital(type = "boolean")`

`pin0.write_analog(val)`
`pin1.write_analog(val)`
`pin2.write_analog(val)`
`pin3.write_analog(val)`

`pin2.read_analog()`
`pin3.read_analog()`

`pin0.servo(angle)`
`pin1.servo(angle)`
`pin2.servo(angle)`
`pin3.servo(angle)`

`pin0.play_tone(freq, time = "none")`
`pin1.play_tone(freq, time = "none")`
`pin2.play_tone(freq, time = "none")`
`pin3.play_tone(freq, time = "none")`

### Wi-Fi
`wifi.connect(ssid, password)`
`wifi.is_connect()`

 <!-- `cloud.setkey(key)`//等互联网实现，期望9月25号之前 -->

`speech.listen(LAN,time=3)`

`cloud_broadcast.set(message, val)`
`cloud_broadcast.get(message)`

`iot.weather(index, location)`
`iot.air(index, location)`
 <!-- `iot.time(index, location)`//007，互联网，互联网9月25日 -->

`tts.say(message)`

### 局域网
`mesh.set(name = "mesh1")`
`mesh.join(name = "mesh1")`
`mesh_broadcast.set(message, val)`
`mesh_broadcast.get(message)`

### network2（高级）
`network2.config_ap(ssid, password)`
`network2.config_sta(ssid, password)`

`network2.is_connect()`

`network2.set_ip(ip)`
`network2.get_ip()`

`network2.set_subnet_mark(mark)`
`network2.get_subnet_mark()`

`network2.set_gateway(gw)`
`network2.get_gateway()`

`network2.create_client()`
`network2.client_connect_to(ip, port)`

`network2.create_server(port = DEFAULT_PORT)`
`network2.server_wait_connection(port = DEFAULT_PORT)`
`network2.server_get_connections(port = DEFAULT_PORT)`
`network2.server_get_latest_connection(port = DEFAULT_PORT)`

`network2.write(data, mode, ip_to, port = DEFAULT_PORT)`
`network2.write_line(data, mode, ip_to, port = DEFAULT_PORT)`
`network2.read(mode, ip_from, port = DEFAULT_PORT)`
`network2.read_line(mode, ip_from, port = DEFAULT_PORT)`

### 事件及其他
#### 事件
`event.start()`
`event.is_shake()`
`event.is_press()`
`event.is_tiltedleft()`
`event.is_tiltedright()`
`event.is_arrowup()`
`event.arrowdown()`
`event.received(message)`
`event.upload_broadcast(message)`
`event.cloud_broadcast(message)`
`event.mesh_broadcast(message)`
`event.greater_than(threshold, type)`
`event.pin0_is_touch()`
`event.pin1_is_touch()`
`event.pin2_is_touch()`
`event.pin3_is_touch()`

#### 广播
`broadcast(message)`

#### 上传模式广播
`upload_broadcast.set(message, val)`
`upload_broadcast.get(message)`

#### 通信变量
`set_var(name, val)`
`get_var(name)`

#### 脚本控制
`stop_all()`
`stop_this()`
`stop_other()`

## mbuild 电子模块 API
该部分 API 将帮助您使用 mbuild 电子模块平台以扩展光环板的功能和使用场景。
注意：你需要额外购买包含 mbuild 电子模块扩展包或套装来获得 mbuild 模块以使用这些功能。

![](images/mbuild-all.png)

### 通用参数`index`
所有的 mbuild 电子模块的 API 均有一个参数`index`，它代表 API 指向的链上的第几个同类型模块。一般，具有默认值`1`。因此，当链上的每种模块只使用到一个时，你无需关注和独立设置它。
而当链上出现两个同类型模块时，你需要让`index`等于2、3、4甚至其他数字，以便控制链上的第2个、第3个，甚至第4个同类型模块，比如，`motor_driver.set(100, index = 2)`表示将链上第二个电机驱动的输出动力变为`100`。

### 输入-交互类
#### 按钮
`button.is_press(index = 1)`
`button.get_count(index = 1)`
`button.reset_count(index = 1)`

#### 角度传感器
`angle_sensor.get(index = 1)`
`angle_sensor.reset(index = 1)`
`angle_sensor.get_speed(index = 1)`
`angle_sensor.is_clockwise(index = 1).`
`angle_sensor.is_anticlockwise(index = 1)`

#### 滑动电位器
`slider.get(index = 1)`

#### 摇杆
`joystick.get_x(index = 1)`
`joystick.get_y(index = 1)`

#### 多路触摸
`multi_touch.is_touch(ch = "any", index = 1)`
`multi_touch.set(index = 1)`

### 输入-传感器类
#### 光线传感器
`light_sensor.get(index = 1)`

#### 双路颜色传感器
`dual_rgb_sensor.is_color(color, ch = "any", index = 1)`
`dual_rgb_sensor.get_red(ch = "any", index = 1)`
`dual_rgb_sensor.get_green(ch = "any", index = 1)`
`dual_rgb_sensor.get_blue(ch = "any", index = 1)`
`dual_rgb_sensor.get_light(ch = "any", index = 1)`
`dual_rgb_sensor.get_gray_level(ch = "any", index = 1)`
`dual_rgb_sensor.set_led(color, index = 1)`
`dual_rgb_sensor.off_led(index = 1)`

#### 声音传感器
`sound_sensor.get(index = 1)`

#### 热运动传感器
`pir.is_detect(index = 1)`
`pir.get_count(index = 1)`
`pir.reset_count(index = 1)`

#### 超声波
`ultrasonic.get(index = 1)`

#### 测距传感器
`ranging_sensor.get(index = 1)`

#### 运动传感器
`motion_sensor.is_tiltleft(index = 1)`
`motion_sensor.is_tiltright(index = 1)`
`motion_sensor.is_tiltup(index = 1)`
`motion_sensor.is_tiltdown(index = 1)`
`motion_sensor.is_faceup(index = 1)`
`motion_sensor.is_facedown(index = 1)`
`motion_sensor.is_freefall(index = 1)`
`motion_sensor.is_clockwise(index = 1)`
`motion_sensor.is_anticlockwise(index = 1)`

`motion_sensor.is_shake(index = 1)`
`motion_sensor.get_shakeval(index = 1)`

`motion_sensor.get_roll(index)`
`motion_sensor.get_pitch(index)`
`motion_sensor.get_yaw(index)`
`motion_sensor.get_accel(axis, index)`
`motion_sensor.get_gyro(axis, index)`
`motion_sensor.get_rotation(axis, index)`
`motion_sensor.reset_rotation(axis= "all", index)`

#### 土壤湿度传感器
`soil_sensor.get(index = 1)`

#### 温度传感器
`temp_sensor.get(index = 1)`

#### 温湿度传感器
`humiture.get_humidity(index = 1)`
`humiture.get_temp(index = 1)`

#### MQ2气体传感器
`mq2.is_detect(pct = "middle", index = 1)`
`mq2.get(index = 1)`

#### 火焰传感器
`flame_sensor.is_detect(index = 1)`
`flame_sensor.get(index = 1)`

#### 磁敏传感器
`magnetic_sensor.is_detect(index = 1)`
`magnetic_sensor.get(index = 1)`

### 输出-显示类
#### LED 点阵
`led_matrix.show(image = "hello", time = "none", index = 1)`
`led_matrix.print(message, index = 1)`
`led_matrix.on(x, y, index = 1)`
`led_matrix.off(x, y, index = 1)`
`led_matrix.toggle(x, y, index = 1)`
`led_matrix.clear(index = 1)`

### 输出-灯光类
#### RGB 灯
`rgb_led.on(R, G, B, pct = 100, index = 1)`
`rgb_led.off(index = 1)`
`rgb_led.set_red(val, index = 1)`
`rgb_led.set_green(val, index = 1)`
`rgb_led.set_blue(val, index = 1)`
`rgb_led.add_red(val, index = 1)`
`rgb_led.add_green(val, index = 1)`
`rgb_led.add_blue(val, index = 1)`
`rgb_led.get_red(val, index = 1)`
`rgb_led.get_green(index = 1)`
`rgb_led.get_blue(index = 1)`

#### 彩灯驱动
`led_driver.on(R, G, B, pct = 100, led_id = "all", index = 1)`
`led_driver.off(led_id = "all", index = 1)`
`led_driver.set_red(val, led_id = "all", index = 1)`
`led_driver.set_green(val, led_id = "all", index = 1)`
`led_driver.set_blue(val, led_id = "all", index = 1)`
`led_driver.add_red(val, led_id = "all", index = 1)`
`led_driver.add_green(val, led_id = "all", index = 1)`
`led_driver.add_blue(val, led_id = "all", index = 1)`
`led_driver.set_mode(mode = "steady", speed = 50)`
`led_driver.show(led.show('color','color','color'...., index = 1)`

### 输出-播放类
#### 扬声器
`speaker.mute(index = 1)`
`speaker.set_vol(val, index = 1)`
`speaker.add_vol(val, index = 1)`
`speaker.get_vol(index = 1)`
`speaker.play_tone(frq, index = 1)`
`speaker.play(music_name, index = 1)`
`speaker.is_play(index = 1)`


## 输出-动力类
### 电机驱动
`motor_driver.set(power = 80, index = 1)`
`motor_driver.add(power = 80, index = 1)`
`motor_driver.get(index = 1)`
`motor_driver.get_load()`
`motor_driver.stop`

#### 舵机驱动
`servo_driver.set()`
`servo_driver.add()`
`servo_driver.get()`
`servo_driver.get_load()`
`servo_driver.release()`

#### 智能舵机
`smart_servo.turn(angle, speed, index = 1)`
`smart_servo.turn_to(angle, speed, index = 1 )`
`smart_servo.drive(speed, time = "none", index = 1)`
`smart_servo.stop(index = "all")`
`smart_servo.run(power, index = 1)`
`smart_servo.get_angle(index = 1)`
`smart_servo.get_speed(index = 1)`
`smart_servo.release_angle(index = 1)`
`smart_servo.lock_angle(index = 1)`
`smart_servo.reset(index = 1)`

### 其他
#### 红外收发
`ir.receive(index = 1)`
`ir.send(message, index = 1)`
`ir.send_record(record_id, index = 1)`
`ir.record(record_id, index = 1)`
`ir.is_receive(message, index = 1)`

#### 视觉模块
`smart_camera.set_mode(mode = "color", index = 1)`
`smart_camera.learn(sign = 1, time = "until_button", index = 1)`
`smart_camera.detect_sign(sign, index = 1)`
`smart_camera.get_sign_location(sign, index = 1)`
`smart_camera.get_sign_x(sign, index = 1)`
`smart_camera.get_sign_y(sign, index = 1)`
`smart_camera.get_sign_wide(sign, index = 1)`
`smart_camera.get_sign_hight(sign, index = 1)`

`smart_camera.open_light(index = 1)`
`smart_camera.close_light(index = 1)`

`smart_camera.reset(index = 1)`

`smart_camera.detect_label(label, index = 1)`
`smart_camera.get_label_x(sign, index = 1)`
`smart_camera.get_label_y(sign, index = 1)`

`smart_camera.detect_cross(index = 1)`
`smart_camera.get_cross_x(index = 1)`
`smart_camera.get_cross_y(index = 1)`
`smart_camera.get_cross_road(index = 1)`
`smart_camera.get_cross_angle(sn = 1, index = 1)`

`smart_camera.set_line(sn = 1, index = 1)`
`smart_camera.set_vector_angle(index = 1)`
`smart_camera.get_vector_angle(index = 1)`

`smart_camera_1_kp = 0.5`






