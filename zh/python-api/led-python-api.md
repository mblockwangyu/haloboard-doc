# 控制全彩 LED 灯环
该部分 API 将帮助您使用光环上的板载全彩 LED 灯环。

<img src="images/led-1.png" width="200px;" style="padding:5px 5px 20px 5px;">

### 省略代码中的halocode
注意：该部分 API 省略了“halocode.”，本篇提及的所有 API 均省略了“halocode.” ，如 led_driver.off( )实际为halocode.led_driver.off()。mBuild 电子模块平台的所有功能均维护在halocode库中，因此我们推荐如下写法，以便将halocode.进行省略，使得代码更简洁。

``` py
from halocode import *
```
<br>

## led.on(r,g,b, id = "all")

设置灯环上单颗或全部的 RGB LED 颜色，参数：
- *r* 数值或字符串类型。<br>
r 为数值时，有效范围是 0 - 255，表示全彩LEDR值，0时无R值，255时R值最亮。<br>
r 为字符串类型时，表示颜色名称或缩写。颜色名称及其缩写对照表：<br>

```
红 red r

橙 orange o

黄 yellow y

绿 green g

青 cyan c

蓝 blue b

紫 purple p

白 white w

黑 black k
```

- *g* 数值，全彩LEDG值，参数范围是 0 ~ 255， 0为无G值，255时G值最亮。<br>
- *b* 数值，全彩LEDB值，参数范围是 0 ~ 255， 0为无B值，255时B值最亮。<br>
- *id* 字符串类型或数值，默认值为"all"。
id 为字符串类型时，有效值为"all"，此时设置所有灯珠的颜色。<br>
id 为数值时，有效范围为1-12，此时设置对应位置的灯珠的颜色。灯珠对应位置如下图：<br>

<img src="images/led-id.png" width="200px;" style="padding:5px 5px 20px 5px;">

**程序示例1**

```py
from halocode import *
led.on(255,0,0) #将整个灯环点亮为红色
```

**程序示例2**

```py
from halocode import *
led.on(255,0,0,pct = 50) #将整个灯环点亮为红色，亮度为50%
```

**程序示例3**

```py
from halocode import *
led.on(255,0,0,pct = 50, id = 1) #将灯环上1位置的灯珠点亮为红色，亮度为50%
```

**程序示例4**

```py
from halocode import *
led.on('red', pct = 50, id = 1) #将灯环上1位置的灯珠点亮为红色，亮度为50%
```

**程序示例5**
```py
from halocode import *
led.on('r', pct = 50, id = 1) #将灯环上1位置的灯珠点亮为红色，亮度为50%
```
<br>


## led.play(name = "rainbow")

显示预置灯效，阻塞型，参数：
- *name* 字符串类型，默认值为 "rainbow"，有效值为 "rainbow"/"spindrift"/"meteor_blue"/"meteor_green"/"flash_red"/"flash_orange"/"flyfire"，表示播放的预置灯效名称，该 API 会阻塞所在线程直至灯效被播放完，灯效及其效果对照如下：

<img src="images/led-2.gif" width="200px;" style="padding:5px 5px 20px 5px;">

## led.show(color, offset = 0)

同时设置12颗 RGB LED为相应的颜色，参数：
- *color* 字符串类型，字符串格式需满足“color1 color2 color3 color4 color5 color6 color7 color8 color9 color10 color11 color12”，其中colorx为 red/green/blue/yellow/cyan/purple/white/orange/black 或是其缩写 r/g/b/y/c/p/w/o/k。颜色字符间以单个空格隔开。当颜色个数大于12时将被截断成12个。
- *offset* 数值，灯环显示的图像需要在参数color设定的基础上顺时针偏移的格数，数值范围为任意整数，默认值为`0`

**程序示例1**

```py
from halocode import *
led.show("red") #在整个灯环上显示红色
```
预计效果：

<img src="images/led-3.png" width="200px;" style="padding:5px 5px 20px 5px;">


<br>

## led.move(offset = 1)

将灯环当前图像按顺时针旋转.
- *offset* 数值，默认为1，有效范围为任意整数，表示灯环基于当前显示的图像顺时针旋转的格数。

## led.chart(pct)

用LED灯环的状态显示百分比，参数：
- *pct* 数值，有效范围为0-100。

**程序示例1**

```py
from halocode import *
led.chart(80)
```
预计效果如下

<img src="images/led-5.png" width="200px;" style="padding:5px 5px 20px 5px;">

## led.off(id = "all")

熄灭灯环上单颗或全部的 RGB LED，参数：
- *id* 字符串类型或数值，默认值为"all"。
id 为字符串类型时，有效值为"all"，此时设置所有灯珠的颜色。<br>
id 为数值时，有效范围为1-12，此时设置对应位置的灯珠的颜色。<br>

**程序示例1**p

```py
from halocode import *
led.off() #熄灭整个灯环
```

**程序示例2**

```py
from halocode import *
led.off(id = 1) #熄灭灯环上1位置的灯珠
```
<br>