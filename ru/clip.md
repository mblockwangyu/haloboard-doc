# Использование зажимов «крокодил»

Металлические части на концах зажимов «крокодил» проводят электричество. С помощью датчика касания и токопроводящей медной фольги, можно без труда создавать увлекательные проекты, например палитру HaloCode. И самое главное: HaloCode может взаимодействовать с mBlock 5.

Давайте создадим простой проект с использованием HaloCode. В рамках этого проекта вы узнаете, как использовать зажимы «крокодил».

Благодаря картону и медной фольге светодиодные индикаторы могут загораться определенным цветом при касании зажима «крокодил».

<img src="../fr/images/clip-1.png" width="300px;" style="padding:5px 5px 20px 5px;">

### Шаг 1. Найдите необходимые материалы.

<img src="images/clip-2.png" width="800px;" style="padding:5px 5px 20px 5px;">

### Шаг 2. Прикрепите зажимы «крокодил».

<span style="color:white;font-size:18;border-radius:100%;background-color:lightblue;font-wieght:bold;">&nbsp; 1 &nbsp;</span> Прикрепите красный зажим «крокодил» к тактильному датчику 0.

<img src="../fr/images/clip-3.png" width="300px;" style="padding:5px 5px 20px 5px;">

<span style="color:white;font-size:18;border-radius:100%;background-color:lightblue;font-wieght:bold;">&nbsp; 2 &nbsp;</span> Прикрепите желтый, синий и зеленый зажимы «крокодил» к тактильным датчикам 1, 2 и 3 соответственно.

<img src="../fr/images/clip-4.png" width="300px;" style="padding:5px 5px 20px 5px;">

<div style="background-color:#FFFFCC;padding:10px;border-left:5px solid #FFEB3B;margin:20px;">НЕ используйте зажим «крокодил» для подсоединения контакта 3,3 В к контакту GND, так как это может привести к короткому замыканию и повреждению HaloCode.
</div>

<br>

<span style="color:white;font-size:18;border-radius:100%;background-color:lightblue;font-wieght:bold;">&nbsp; 3 &nbsp;</span> Согните картон из упаковки, как показано на рисунке.

<img src="../fr/images/clip-5.png" width="800px;" style="padding:5px 5px 20px 5px;">

<span style="color:white;font-size:18;border-radius:100%;background-color:lightblue;font-wieght:bold;">&nbsp; 4 &nbsp;</span> Прикрепите медную фольгу к картону.

<img src="../fr/images/clip-6.png" width="300px;" style="padding:5px 5px 20px 5px;">

<div style="background-color:#FFFFCC;padding:10px;border-left:5px solid #FFEB3B;margin:20px;">* Обе стороны медной фольги проводят ток. Прикрепление зажимов «крокодил» к фольге расширяет проводящую область.
</div>

<br>

<span style="color:white;font-size:18;border-radius:100%;background-color:lightblue;font-wieght:bold;">&nbsp; 5 &nbsp;</span> Снимите защитную пленку с медной фольги.

<img src="../fr/images/clip-7.png" width="300px;" style="padding:5px 5px 20px 5px;">

<div style="background-color:#FFFFCC;padding:10px;border-left:5px solid #FFEB3B;margin:20px;">* Защитная пленка используется для защиты пленки от окисления.
</div>

<br>

<span style="color:white;font-size:18;border-radius:100%;background-color:lightblue;font-wieght:bold;">&nbsp; 6 &nbsp;</span> Прикрепите другой конец зажимов «крокодил» к медной фольге.

<img src="../fr/images/clip-8.png" width="500px;" style="padding:5px 5px 20px 5px;">

### Шаг 3. Подключите HaloCode к компьютеру с помощью кабеля Micro USB.

<img src="../fr/images/clip-9.png" width="600px;" style="padding:5px 5px 20px 5px;">

### Шаг 4. Напишите код.

<img src="images/clip-11.png" width="600px;" style="padding:5px 5px 20px 5px;">

### Вот и все!

Коснитесь металлической части красного зажима «крокодил» или медной фольги, чтобы светодиодные индикаторы загорелись красным цветом.

<img src="../fr/images/clip-10.png" width="600px;" style="padding:5px 5px 20px 5px;">

Теперь вы знаете, как использовать зажимы «крокодил». Дайте волю своему воображению, чтобы придумать другие интересные проекты.