# Краткое руководство пользователя

## 1. Загрузка программного обеспечения

Чтобы загрузить последнюю версию mBlock 5, посетите сайт [http://www.mblock.cc](http://www.mblock.cc).

<img src="../fr/images/download.png" width="600px;" style="padding:5px 5px 20px 5px;">

## 2. Подключение HaloCode к компьютеру

Подключите HaloCode к компьютеру с помощью кабеля Micro USB.

<img src="../fr/images/connect.png" width="600px;" style="padding:5px 5px 20px 5px;">

## 3. Программирование

<span style="color:white;font-size:18;border-radius:100%;background-color:lightblue;font-wieght:bold;">&nbsp; 1 &nbsp;</span> Откройте mBlock и добавьте HaloCode из библиотеки устройств.

<table style="table-layout:fixed;width:100%;text-align:center;" cellpadding=20px; cellspacing=12px;>
<tr>
<td width="50%"><img src="images/connect-halo-1.png" height="120px;" style="padding:5px 5px 5px 5px;"></td>
<td width="10%;"><span style="font-size:30px;padding:5px;color:lightblue;">&rarr;</span></td>
<td width="40%"><img src="../fr/images/connect-halo-2.png" height="120px;" style="padding:5px 5px 5px 5px;"></td>
</tr>
</table>

<span style="color:white;font-size:18;border-radius:100%;background-color:lightblue;font-wieght:bold;">&nbsp; 2 &nbsp;</span> Подключите устройство.

<table style="table-layout:fixed;width:100%;text-align:center;" cellpadding=20px; cellspacing=12px;>
<tr>
<td width="50%"><img src="images/connect-halo-3.png" height="120px;" style="padding:5px 5px 5px 5px;"></td>
<td width="10%;"><span style="font-size:30px;padding:5px;color:lightblue;">&rarr;</span></td>
<td width="40%"><img src="images/connect-halo-4.png" height="120px;" style="padding:5px 5px 5px 5px;"></td>
</tr>
</table>

<span style="color:white;font-size:18;border-radius:100%;background-color:lightblue;font-wieght:bold;">&nbsp; 3 &nbsp;</span> Можно переходить к программированию. Попробуем сделать так, чтобы светодиодные индикаторы HaloCode загорались красным цветом.

<div style="margin:20px;">
<p>1) Добавьте блок события (Event), как показано ниже.</p>
<img src="images/program-1.png" width="600px;" style="padding:0px 5px 15px 5px;">
<p>2) Добавьте приведенный ниже фиолетовый блок из категории блоков освещения (Lighting). Затем измените цвет на красный.</p>
<img src="images/program-2.png" width="600px;" style="padding:0px 5px 15px 5px;">
</div>

## 4. Запуск программы

Программу можно загрузить в HaloCode или выполнить в mBlock 5.

### Загрузка программы в HaloCode: выполнение без программного обеспечения.

Включите **режим загрузки** и щелкните **Upload** (Загрузить). После успешной загрузки нажмите синюю кнопку на HaloCode, чтобы светодиодные индикаторы загорелись красным.

<img src="images/program-3.png" style="padding:5px 5px 5px 5px;">

### Выполнение программы в mBlock: загрузка не требуется. Вы сможете оценить, как работает код, в режиме реального времени.

Выключите **режим загрузки и измените цвет в программе с красного на зеленый**. Нажмите синюю кнопку на HaloCode, чтобы светодиодные индикаторы изменили цвет.

<img src="images/program-4.png" style="padding:5px 5px 5px 5px;">