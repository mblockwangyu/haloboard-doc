# Перечень компонентов

<table style="table-layout:fixed;width:100%;text-align:center;" cellpadding=20px; cellspacing=12px;>

<tr>
<td><img src="../fr/images/halo.png" width="300px;" style="padding:5px 5px 5px 5px;"><br>HaloCode x 1</td>
<td><img src="../fr/images/battery-box.png" width="300px;" style="padding:5px 5px 5px 5px;"><br>Батарейный отсек x 1</td>
<td><img src="../fr/images/battery.png" width="300px;" style="padding:5px 5px 5px 5px;"><br>Батарейка AAA x 3</td>
</tr>

<tr>
<td><img src="../fr/images/clip.png" width="300px;" style="padding:5px 5px 5px 5px;"><br>Зажим «крокодил» x 4</td>
<td><img src="../fr/images/tape.png" width="300px;" style="padding:5px 5px 5px 5px;"><br>Медная фольга x 1</td>
<td><img src="../fr/images/strap.png" width="300px;" style="padding:5px 5px 5px 5px;"><br>Ремешок с липучкой x 1</td>
</tr>

<td><img src="../fr/images/fastener.png" width="300px;" style="padding:5px 5px 5px 5px;"><br>Застежка-липучка x 4</td>
<td><img src="../fr/images/usb.png" width="300px;" style="padding:5px 5px 5px 5px;"><br>Кабель Micro USB x 1</td>
</tr>

</table>