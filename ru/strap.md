# Создайте с помощью крючка и петли

С помощью ремешка с крючком и петлей вы можете прикрепить HaloCode к вашему запястью. Кроме того, светодиодные фонари вместе с сенсорным датчиком HaloCode позволяют создавать портативные устройства, например шагомер.

Давайте превратим HaloCode в портативное устройство. В этом проекте вы узнаете, как создать проект с ремешком с крючком и петлей.

Вы достигнете следующего: Используйте HaloCode на вашем запястье. Как запрограммировано, светодиодные фонари будут загораться случайным цветом, когда датчик движения HaloCode обнаруживает движения вашей руки.

<img src="../fr/images/strap-1.png" width="300px;" style="padding:5px 5px 20px 5px;">

### Шаг 1: Найдите предмет, который вам нужен.

<img src="../fr/images/strap-2.png" width="800px;" style="padding:5px 5px 20px 5px;">

### Шаг 2: Напишите программу и загрузите ее на HaloCode.

<img src="images/strap-3.png" width="800px;" style="padding:5px 5px 20px 5px;">

### Шаг 3: Поместите батареи в батарейный отсек.

<img src="../fr/images/strap-4.png" width="800px;" style="padding:5px 5px 20px 5px;">

### Шаг 4: Используйте HaloCode на вашем запястье, как показано ниже (левая рука в качестве примера).

<span style="color:white;font-size:18;border-radius:100%;background-color:lightblue;font-wieght:bold;">&nbsp; 1 &nbsp;</span> Подсоедините батарейный отсек к HaloCode.

<img src="images/strap-5.png" width="600px;" style="padding:5px 5px 20px 5px;">

<span style="color:white;font-size:18;border-radius:100%;background-color:lightblue;font-wieght:bold;">&nbsp; 2 &nbsp;</span> Прикрепите липучку к задней части батарейного отсека и задней части HaloCode.

<img src="images/strap-6.png" width="600px;" style="padding:5px 5px 20px 5px;">

<span style="color:white;font-size:18;border-radius:100%;background-color:lightblue;font-wieght:bold;">&nbsp; 3 &nbsp;</span>

<img src="images/strap-7.png" width="800px;" style="padding:5px 5px 20px 5px;">

<span style="color:white;font-size:18;border-radius:100%;background-color:lightblue;font-wieght:bold;">&nbsp; 4 &nbsp;</span>

<img src="images/strap-8.png" width="600px;" style="padding:5px 5px 20px 5px;">

<span style="color:white;font-size:18;border-radius:100%;background-color:lightblue;font-wieght:bold;">&nbsp; 5 &nbsp;</span>

<img src="images/strap-9.png" width="800px;" style="padding:5px 5px 20px 5px;">

<span style="color:white;font-size:18;border-radius:100%;background-color:lightblue;font-wieght:bold;">&nbsp; 6 &nbsp;</span>

<img src="images/strap-10.png" width="600px;" style="padding:5px 5px 20px 5px;">

### Готово!

Теперь включите выключатель аккумулятора. Пожмите руку, чтобы увидеть, как включаются светодиодные фонари.

<img src="images/strap-11.png" width="600px;" style="padding:5px 5px 20px 5px;">

Теперь вы знаете, как использовать ремешок с крючком и петлей. Используйте свое воображение, чтобы создавать больше интересных проектов.
