# Summary

## Docs

* [Стандартный комплект Makeblock HaloCode](README.md)
    * [Перечень компонентов](parts.md)
    * [Краткое руководство пользователя](quick-start.md)
    * [Описание внешнего вида](appearance.md)
    * [Батарейка](battery.md)
    * [Создайте с помощью крючка и петли](strap.md)
    * [Использование зажимов «крокодил»](clip.md)
    * [Вопросы и ответы](faq.md)
    * [Предупреждение](caution.md)