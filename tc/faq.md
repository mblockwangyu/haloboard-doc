# 常見問題解答

### 上傳程式後，光環板為何未正常運作？

這可能是因為韌體不是最新版本所致，請參照下圖點選更新韌體來升級韌體。

<img src="images/update-halo.png" width="300px;" style="padding:5px 5px 20px 5px;"> <span style="font-size:30px;padding:5px;color:lightblue;">&rarr;</span><img src="images/update-halo-1.png" width="300px;" style="padding:5px 5px 20px 5px;">

### 如何為光環板供電？

有兩種方式可以為光環板供電：

1. 在電池接口外接電池盒
2. 使用 Micro USB 傳輸線連接電腦或行動電源

供電電壓要求：DC 3～5V
