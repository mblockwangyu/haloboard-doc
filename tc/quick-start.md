# 快速使用指南

## 1. 軟體下載

請前往網址 [http://www.mblock.cc](http://www.mblock.cc) 下載並安裝最新版 mBlock 5 軟體。

<img src="../fr/images/download.png" width="600px;" style="padding:5px 5px 20px 5px;">

## 2. 連接光環板到電腦

使用 Micro USB 傳輸線將光環板連接到電腦。

<img src="../fr/images/connect.png" width="600px;" style="padding:5px 5px 20px 5px;">

## 3. 開始編寫程式

<span style="color:white;font-size:18;border-radius:100%;background-color:lightblue;font-wieght:bold;">&nbsp; 1 &nbsp;</span> 開啟 mBlock 5，從設備庫加入光環板。

<table style="table-layout:fixed;width:100%;text-align:center;" cellpadding=20px; cellspacing=12px;>
<tr>
<td width="50%"><img src="images/connect-halo-1.png" height="120px;" style="padding:5px 5px 5px 5px;"></td>
<td width="10%;"><span style="font-size:30px;padding:5px;color:lightblue;">&rarr;</span></td>
<td width="40%"><img src="../fr/images/connect-halo-2.png" height="120px;" style="padding:5px 5px 5px 5px;"></td>
</tr>
</table>

<span style="color:white;font-size:18;border-radius:100%;background-color:lightblue;font-wieght:bold;">&nbsp; 2 &nbsp;</span> 連​​接設備。

<table style="table-layout:fixed;width:100%;text-align:center;" cellpadding=20px; cellspacing=12px;>
<tr>
<td width="50%"><img src="images/connect-halo-3.png" height="120px;" style="padding:5px 5px 5px 5px;"></td>
<td width="10%;"><span style="font-size:30px;padding:5px;color:lightblue;">&rarr;</span></td>
<td width="40%"><img src="images/connect-halo-4.png" height="120px;" style="padding:5px 5px 5px 5px;"></td>
</tr>
</table>

<span style="color:white;font-size:18;border-radius:100%;background-color:lightblue;font-wieght:bold;">&nbsp; 3 &nbsp;</span> 現在一起來完成一個程式編寫小任務吧！試試讓光環板所有的 LED 燈都亮起紅色。

<div style="margin:20px;">
<p>1) 參照下圖加入一個事件積木。</p>
<img src="images/program-1.png" width="600px;" style="padding:0px 5px 15px 5px;">
<p>2) 從燈光積木區域加入下列紫色積木，更改燈光顏色為紅色，完成程式。</p>
<img src="images/program-2.png" width="600px;" style="padding:0px 5px 15px 5px;">
</div>

## 4. 執行程式

您可以選擇將程式上傳到光環板，或是直接在 mBlock 5 軟體內執行程式。

### 上傳程式到光環板：跳脫軟體來執行程式

開啟**上傳模式**，點選**上傳到設備**。上傳成功後，按下光環板上的藍色按鈕，看看 LED 燈是不是亮起紅色了？

<img src="images/program-3.png" style="padding:5px 5px 5px 5px;">

### 在軟體內執行程式：無需上傳，即時查看結果

關閉**上傳模式，將程式中的顏色改為綠色**，按下光環板上的藍色按鈕，看看 LED 燈的顏色是不是改變了？

<img src="images/program-4.png" style="padding:5px 5px 5px 5px;">