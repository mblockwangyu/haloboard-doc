# Summary

## Docs

* [光環板標準套件](README.md)
    * [包裝清單](parts.md)
    * [快速使用指南](quick-start.md)
    * [外觀介紹](appearance.md)
    * [電池](battery.md)
    * [使用魔鬼氈綁帶進行創作](strap.md)
    * [使用鱷魚夾進行創作](clip.md)
    * [常見問題解答](faq.md)
    * [注意事項](caution.md)