# 使用鱷魚夾進行創作

鱷魚夾兩端的金屬部分可以導電，再搭配光環板的觸碰感應器以及雙面導電的銅箔膠帶，就能製作各種有趣的觸碰專案，比如調色盤、與 mBlock 5 舞台互動等。

下面讓我們一起來完成一個簡單的專案吧！透過這個專案，可以學習如何使用鱷魚夾進行創作。

利用包裝盒內的卡紙、銅箔膠帶等，做出只要觸碰任一鱷魚夾，彩色 LED 燈就會亮起對應顏色的效果。

<img src="../fr/images/clip-1.png" width="300px;" style="padding:5px 5px 20px 5px;">

### 1 找到需要的物品。

<img src="images/clip-2.png" width="800px;" style="padding:5px 5px 20px 5px;">

### 2 安裝鱷魚夾。

<span style="color:white;font-size:18;border-radius:100%;background-color:lightblue;font-wieght:bold;">&nbsp; 1 &nbsp;</span> 將紅色鱷魚夾夾在 0 號觸碰感應器上。

<img src="../fr/images/clip-3.png" width="300px;" style="padding:5px 5px 20px 5px;">

<span style="color:white;font-size:18;border-radius:100%;background-color:lightblue;font-wieght:bold;">&nbsp; 2 &nbsp;</span> 將黃色、藍色及綠色的鱷魚夾依序夾在 1 號、2 號及 3 號觸碰感應器上。 

<img src="../fr/images/clip-4.png" width="300px;" style="padding:5px 5px 20px 5px;">

<div style="background-color:#FFFFCC;padding:10px;border-left:5px solid #FFEB3B;margin:20px;">切勿使用鱷魚夾將 3.3V 引腳和 GND 引腳直接相接，這樣可能會引起短路，導致光環板損壞。
</div>

<br>

<span style="color:white;font-size:18;border-radius:100%;background-color:lightblue;font-wieght:bold;">&nbsp; 3 &nbsp;</span> 按圖示裁剪並折疊包裝盒內的卡紙。

<img src="../fr/images/clip-5.png" width="800px;" style="padding:5px 5px 20px 5px;">

<span style="color:white;font-size:18;border-radius:100%;background-color:lightblue;font-wieght:bold;">&nbsp; 4 &nbsp;</span> 取銅箔膠帶黏貼在卡紙上。

<img src="../fr/images/clip-6.png" width="300px;" style="padding:5px 5px 20px 5px;">

<div style="background-color:#FFFFCC;padding:10px;border-left:5px solid #FFEB3B;margin:20px;">* 銅箔膠帶雙面都導電，配合鱷魚夾可以延長、擴大觸碰面積。
</div>

<br>

<span style="color:white;font-size:18;border-radius:100%;background-color:lightblue;font-wieght:bold;">&nbsp; 5 &nbsp;</span> 撕下銅箔膠帶上的保護膜。

<img src="../fr/images/clip-7.png" width="300px;" style="padding:5px 5px 20px 5px;">

<div style="background-color:#FFFFCC;padding:10px;border-left:5px solid #FFEB3B;margin:20px;">* 保護膜可以防止銅箔膠帶氧化。
</div>

<br>

<span style="color:white;font-size:18;border-radius:100%;background-color:lightblue;font-wieght:bold;">&nbsp; 6 &nbsp;</span> 將鱷魚夾另一頭夾在銅箔膠帶上。

<img src="../fr/images/clip-8.png" width="500px;" style="padding:5px 5px 20px 5px;">

### 3 使用 Micro USB 傳輸線將光環板連接到電腦。

<img src="../fr/images/clip-9.png" width="600px;" style="padding:5px 5px 20px 5px;">

### 4 開始編寫程式。

<img src="images/clip-11.png" width="600px;" style="padding:5px 5px 20px 5px;">

### 完成！

觸碰紅色鱷魚夾金屬部分或是銅箔膠帶，看看彩色 LED 燈是不是亮起紅色了？

<img src="../fr/images/clip-10.png" width="600px;" style="padding:5px 5px 20px 5px;">

相信您已經學會如何使用鱷魚夾了，現在發揮想像力，看看還能做出什麼有趣的專案吧！

