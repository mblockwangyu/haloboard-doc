# 使用魔鬼氈綁帶進行創作

魔鬼氈綁帶可用於將光環板綁在手腕上，再搭配彩色 LED 燈、觸碰感應器等，就能製作各種有趣的穿戴式專案，比如計步器。

下面讓我們一起來完成一個簡單的穿戴式專案吧！透過這個專案，可以學習如何使用魔鬼氈綁帶進行創作。

做出當光環板戴在手上時，只要動作感應器偵測到光環板搖晃，彩色 LED 燈就會隨機亮起一個顏色的效果。


<img src="../fr/images/strap-1.png" width="300px;" style="padding:5px 5px 20px 5px;">

### 1 找到需要的物品。

<img src="../fr/images/strap-2.png" width="800px;" style="padding:5px 5px 20px 5px;">

### 2 編寫程式並上傳到光環板。

<img src="images/strap-3.png" width="800px;" style="padding:5px 5px 20px 5px;">

### 3 安裝電池。

<img src="../fr/images/strap-4.png" width="800px;" style="padding:5px 5px 20px 5px;">

### 4 參照下列步驟完成穿戴過程（以左手佩戴為例）。

<span style="color:white;font-size:18;border-radius:100%;background-color:lightblue;font-wieght:bold;">&nbsp; 1 &nbsp;</span> 連接電池和光環板。

<img src="images/strap-5.png" width="600px;" style="padding:5px 5px 20px 5px;">

<span style="color:white;font-size:18;border-radius:100%;background-color:lightblue;font-wieght:bold;">&nbsp; 2 &nbsp;</span> 將魔鬼氈綁帶反向纏繞後黏貼。 

<img src="images/strap-6.png" width="600px;" style="padding:5px 5px 20px 5px;">

<span style="color:white;font-size:18;border-radius:100%;background-color:lightblue;font-wieght:bold;">&nbsp; 3 &nbsp;</span>

<img src="images/strap-7.png" width="800px;" style="padding:5px 5px 20px 5px;">

<span style="color:white;font-size:18;border-radius:100%;background-color:lightblue;font-wieght:bold;">&nbsp; 4 &nbsp;</span>

<img src="images/strap-8.png" width="600px;" style="padding:5px 5px 20px 5px;">

<span style="color:white;font-size:18;border-radius:100%;background-color:lightblue;font-wieght:bold;">&nbsp; 5 &nbsp;</span>

<img src="images/strap-9.png" width="800px;" style="padding:5px 5px 20px 5px;">

<span style="color:white;font-size:18;border-radius:100%;background-color:lightblue;font-wieght:bold;">&nbsp; 6 &nbsp;</span>

<img src="images/strap-10.png" width="600px;" style="padding:5px 5px 20px 5px;">

### 完成！

開啟電池開關，晃晃手，看看彩色 LED 燈是不是亮了？

<img src="images/strap-11.png" width="600px;" style="padding:5px 5px 20px 5px;">

相信您已經學會如何使用魔鬼氈綁帶了，現在發揮想像力，看看還能做出什麼有趣的專案吧！

