# 光環板標準套件

* [包裝清單](parts.md)
* [快速使用指南](quick-start.md)
* [外觀介紹](appearance.md)
* [電池](battery.md)
* [使用魔鬼氈綁帶進行創作](strap.md)
* [使用鱷魚夾進行創作](clip.md)
* [常見問題解答](faq.md)
* [注意事項](caution.md)

### 更多幫助及資源

我們非常在意您的使用體驗，若有任何問題或建議，請與我們聯繫。

技術支持：<support.cn@makeblock.com>

Makeblock 教育網站：[http://education.makeblock.com/](http://education.makeblock.com/)
