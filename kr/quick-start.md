# 사용자 가이드

## 1. 소프트웨어 다운로드

[http://www.mblock.cc](http://www.mblock.cc) 에 접속해 mBlock 5 소프트웨어의 최신 버전을 다운로드 하십시오.

<img src="../fr/images/download.png" width="600px;" style="padding:5px 5px 20px 5px;">

## 2. 할로코드를 컴퓨터에 연결합니다.

Micro USB선을 이용하여 할로코드를 컴퓨터에 연결합니다.

<img src="../fr/images/connect.png" width="600px;" style="padding:5px 5px 20px 5px;">

## 3. 프로그래밍 시작하기

<span style="color:white;font-size:18;border-radius:100%;background-color:lightblue;font-wieght:bold;">&nbsp; 1 &nbsp;</span> mBlock 5를 켜고 기기 라이브러리에서 할로코드를 추가합니다.

<table style="table-layout:fixed;width:100%;text-align:center;" cellpadding=20px; cellspacing=12px;>
<tr>
<td width="50%"><img src="images/connect-halo-1.png" height="120px;" style="padding:5px 5px 5px 5px;"></td>
<td width="10%;"><span style="font-size:30px;padding:5px;color:lightblue;">&rarr;</span></td>
<td width="40%"><img src="../fr/images/connect-halo-2.png" height="120px;" style="padding:5px 5px 5px 5px;"></td>
</tr>
</table>

<span style="color:white;font-size:18;border-radius:100%;background-color:lightblue;font-wieght:bold;">&nbsp; 2 &nbsp;</span>  기기를 연결합니다.

<table style="table-layout:fixed;width:100%;text-align:center;" cellpadding=20px; cellspacing=12px;>
<tr>
<td width="50%"><img src="images/connect-halo-3.png" height="120px;" style="padding:5px 5px 5px 5px;"></td>
<td width="10%;"><span style="font-size:30px;padding:5px;color:lightblue;">&rarr;</span></td>
<td width="40%"><img src="images/connect-halo-4.png" height="120px;" style="padding:5px 5px 5px 5px;"></td>
</tr>
</table>

<span style="color:white;font-size:18;border-radius:100%;background-color:lightblue;font-wieght:bold;">&nbsp; 3 &nbsp;</span> 이제 간단한 프로그래밍 작업을 함께 해볼까요? 할로코드의 모든 LED 램프에 빨간 불이 켜지도록 해봅시다. 

<div style="margin:20px;">
<p>1) 아래의 그림을 따라 "이벤트"에서 블록을 추가합니다.</p>
<img src="images/program-1.png" width="600px;" style="padding:0px 5px 15px 5px;">
<p>2) 램프 블록코딩 영역에서 다음과 같은 보라색 블록코딩을 추가하고, 램프 색상을 빨간색으로 변경하면 프로그래밍이 끝납니다.</p>
<img src="images/program-2.png" width="600px;" style="padding:0px 5px 15px 5px;">
</div>

## 4. 프로그램 실행

코딩한 것을 할로코드에 업로드하거나 mBlock5 소프트웨어에서 직접 실행할 수 있습니다 

### 코딩한 것을 할로코드에 업로드하면 언플러그드 상태로도 코딩한 것을 실행할 수 있습니다. 

업로드 모드를 켜고， 기기에 업로드하기를 클릭합니다. 업로드가 성공하면, 할로코드 위의 파란색 버튼을 눌러 보세요. LED 램프에 빨간 불이 들어오나요?

<img src="images/program-3.png" style="padding:5px 5px 5px 5px;">

### mBlock5에서 코딩을 실행하면 업로드 없이 실시간으로 결과를 확인할 수 있습니다.

업로드 모드를 끄고, 프로그램에서 색상을 녹색으로 바꾸고, 할로코드 위의 파란색 버튼을 눌러 보세요. LED 램프의 색상이 바뀌나요?

<img src="images/program-4.png" style="padding:5px 5px 5px 5px;">