# Summary

## Docs

* [할로코드 스탠다드 키트](README.md)
    * [부품 목록](parts.md)
    * [사용자 가이드](quick-start.md)
    * [제품 외관 소개](appearance.md)
    * [배터리](battery.md)
    * [매직스트랩으로 만들기](strap.md)
    * [악어클립으로 작품 만들기](clip.md)
    * [FAQ](faq.md)
    * [주의 사항](caution.md)