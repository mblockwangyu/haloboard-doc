# FAQ

### 프로그램을 업로드한 후 할로코드의 작동에 이상이 생기면?

최신 펌웨어 버전이 아닐 수 있으므로 아래 그림을 참조하여 **펌웨어 업데이**트를 클릭하고 펌웨어를 업그레이드합니다.

<img src="images/update-halo.png" width="300px;" style="padding:5px 5px 20px 5px;"> <span style="font-size:30px;padding:5px;color:lightblue;">&rarr;</span><img src="images/update-halo-1.png" width="300px;" style="padding:5px 5px 20px 5px;">

### 할로코드에 어떻게 전원을 공급합니까?

할로코드에 전원을 공급하는 방법은 두 가지가 있습니다.

1. 배터리선을 이용하여 배터리 박스를 외부로 연결합니다.
2. Micro USB선으로 컴퓨터 또는 모바일 전원에 연결합니다.

전압 : DC 3-5V
