# 할로코드 스탠다드 키트

* [부품 목록](parts.md)
* [사용자 가이드](quick-start.md)
* [제품 외관 소개](appearance.md)
* [배터리](battery.md)
* [매직스트랩으로 만들기](strap.md)
* [악어클립으로 작품 만들기](clip.md)
* [FAQ](faq.md)
* [주의 사항](caution.md)

### 더 많은 정보를 보려면 

사용에 있어서 문의나 건의사항이 있으시면 연락바랍니다.

기술 지원: <support@makeblock.com>

Makeblock Education website：[http://education.makeblock.com/](http://education.makeblock.com/)
