# 악어클립으로 작품 만들기

악어클립 양끝의 금속 부분은 전기가 통하므로, 할로코드의 터치 센서와 양면에 전기가 통하는 구리테이프를 활용하여 다양하고 흥미로운 터치 프로젝트(예, 팔레트, mBlock5 스프라이트 활용 등)를 만들 수 있습니다.

이제 간단한 프로젝트를 함께 만들어 볼까요! 이 프로젝트를 통해 악어클립으로 작품을 만드는 방법을 배울 수 있습니다.

포장 박스의 켄트지, 구리테이프 등으로 악어클립에 터치될 때 컬러 LED 램프에 상응하는 색깔이 표시됩니다.

<img src="../fr/images/clip-1.png" width="300px;" style="padding:5px 5px 20px 5px;">

### 1. 필요한 물건을 찾는다.

<img src="images/clip-2.png" width="800px;" style="padding:5px 5px 20px 5px;">

### 2. 악어클립을 알맞는 자리에 위치한다.

<span style="color:white;font-size:18;border-radius:100%;background-color:lightblue;font-wieght:bold;">&nbsp; 1 &nbsp;</span> 빨간색 악어클립을 0번 터치 센서에 집는다.

<img src="../fr/images/clip-3.png" width="300px;" style="padding:5px 5px 20px 5px;">

<span style="color:white;font-size:18;border-radius:100%;background-color:lightblue;font-wieght:bold;">&nbsp; 2 &nbsp;</span> 노랑색, 파란색, 녹색 악어클립을 차례대로 1, 2, 3번
터치 센서에 집는다.

<img src="../fr/images/clip-4.png" width="300px;" style="padding:5px 5px 20px 5px;">

<div style="background-color:#FFFFCC;padding:10px;border-left:5px solid #FFEB3B;margin:20px;">악어클립으로 3.3V핀과 GND핀을 직접 연결하지 마십시오. 합선으로 인해 할로코드가 손상될 수 있습니다.
</div>

<br>

<span style="color:white;font-size:18;border-radius:100%;background-color:lightblue;font-wieght:bold;">&nbsp; 3 &nbsp;</span> 그림대로 포장 박스의 켄트지를 자르고 접는다.

<img src="../fr/images/clip-5.png" width="800px;" style="padding:5px 5px 20px 5px;">

<span style="color:white;font-size:18;border-radius:100%;background-color:lightblue;font-wieght:bold;">&nbsp; 4 &nbsp;</span> 구리테이프를 켄트지에 붙인다.

<img src="../fr/images/clip-6.png" width="300px;" style="padding:5px 5px 20px 5px;">

<div style="background-color:#FFFFCC;padding:10px;border-left:5px solid #FFEB3B;margin:20px;">* 구리테이프는 양면에 전기가 통하므로, 악어클립을 이용해 터치 면적을 확대할 수 있습니다.
</div>

<br>

<span style="color:white;font-size:18;border-radius:100%;background-color:lightblue;font-wieght:bold;">&nbsp; 5 &nbsp;</span> 구리테이프의 보호 필름을 벗긴다.

<img src="../fr/images/clip-7.png" width="300px;" style="padding:5px 5px 20px 5px;">

<div style="background-color:#FFFFCC;padding:10px;border-left:5px solid #FFEB3B;margin:20px;">* 보호 필름은 구리테이프의 산화를 막을 수 있습니다.
</div>

<br>

<span style="color:white;font-size:18;border-radius:100%;background-color:lightblue;font-wieght:bold;">&nbsp; 6 &nbsp;</span> 악어클립의 다른 한쪽은 구리테이프에 집는다.

<img src="../fr/images/clip-8.png" width="500px;" style="padding:5px 5px 20px 5px;">

### 3. Micro USB 선을 이용하여 컴퓨터에 연결한다.

<img src="../fr/images/clip-9.png" width="600px;" style="padding:5px 5px 20px 5px;">

### 4. 프로그래밍을 시작한다.

<img src="images/clip-11.png" width="600px;" style="padding:5px 5px 20px 5px;">

### 완성! 

빨간색 악어클립의 금속 부분 또는 구리테이프에 터치될 때, 컬러 LED 램프가 빨갛게 켜지나요?

<img src="../fr/images/clip-10.png" width="600px;" style="padding:5px 5px 20px 5px;">

악어클립의 사용법을 다 마스터 하셨나요? 이제 상상력을 발휘해 다른 재미있는 프로젝트를 완성해 보세요!
