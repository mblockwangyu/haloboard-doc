# 매직스트랩으로 만들기

매직스트랩으로 할로보드를 손목에 두르고 컬러 LED, 터치 센서 등과 함께 이용하여 만보기와 같은 다양하고 웨어러블 프로젝트를  만들 수 있습니다.

이제 간단한 웨어러블 프로젝트를 함께 만들어 볼까요! 이 프로젝트를 통해 매직스트랩으로 새로운 작품을 만드는 방법을 배울 수 있습니다.

할로코드를 손에 차고 움직여 모션 센서가 할로보드의 움직임을 감지했을 때, 컬러 LED 램프가 무작위로 한 가지 색상을 표시합니다.


<img src="../fr/images/strap-1.png" width="300px;" style="padding:5px 5px 20px 5px;">

### 1. 필요한 물건을 찾는다.

<img src="../fr/images/strap-2.png" width="800px;" style="padding:5px 5px 20px 5px;">

### 2. 프로그래밍을 하고 할로보드에 업로드한다.

<img src="images/strap-3.png" width="800px;" style="padding:5px 5px 20px 5px;">

### 3. 배터리를 넣는다.

<img src="../fr/images/strap-4.png" width="800px;" style="padding:5px 5px 20px 5px;">

### 4. 다음의 단계를 따라 착용한다(왼손 착용 예시).

<span style="color:white;font-size:18;border-radius:100%;background-color:lightblue;font-wieght:bold;">&nbsp; 1 &nbsp;</span> 배터리와 할로코드를 연결한다.

<img src="images/strap-5.png" width="600px;" style="padding:5px 5px 20px 5px;">

<span style="color:white;font-size:18;border-radius:100%;background-color:lightblue;font-wieght:bold;">&nbsp; 2 &nbsp;</span> 배터리 박스 백슬라이딩커버와 할로코드 뒷면에 매직벨크로를 붙인다.

<img src="images/strap-6.png" width="600px;" style="padding:5px 5px 20px 5px;">

<span style="color:white;font-size:18;border-radius:100%;background-color:lightblue;font-wieght:bold;">&nbsp; 3 &nbsp;</span>

<img src="images/strap-7.png" width="800px;" style="padding:5px 5px 20px 5px;">

<span style="color:white;font-size:18;border-radius:100%;background-color:lightblue;font-wieght:bold;">&nbsp; 4 &nbsp;</span>

<img src="images/strap-8.png" width="600px;" style="padding:5px 5px 20px 5px;">

<span style="color:white;font-size:18;border-radius:100%;background-color:lightblue;font-wieght:bold;">&nbsp; 5 &nbsp;</span>

<img src="images/strap-9.png" width="800px;" style="padding:5px 5px 20px 5px;">

<span style="color:white;font-size:18;border-radius:100%;background-color:lightblue;font-wieght:bold;">&nbsp; 6 &nbsp;</span>

<img src="images/strap-10.png" width="600px;" style="padding:5px 5px 20px 5px;">

### 완성! 

배터리 스위치를 켜고 손을 흔들어 보세요, 컬러 LED 램프가 켜지나요?

<img src="images/strap-11.png" width="600px;" style="padding:5px 5px 20px 5px;">

매직스트랩의 사용법을 다 마스터 하셨나요, 이제 상상력을 발휘해 다른 흥미로운 프로젝트를 완성해 보세요!

