# Attenzione

- Collega HaloCode esclusivamente ad un alimentatore a CC da 3 Volt a 5 Volt.
- Evita di maneggiare HaloCode mentre è collegato a un alimentatore. HaloCode deve essere spento quando viene collegato a un circuito I/O esterno.
- La corrente massima fornita in sicurezza a un circuito esterno utilizzando il cavo I/O è di 10 mA.
- La corrente massima fornita in sicurezza a un circuito esterno utilizzando l'interfaccia a 3.3 V è di 500mA. Assicurati di non superare tali limiti.
- La corrente massima fornita in sicurezza a un circuito esterno utilizzando l'interfaccia di espansione di blocchi elettronici è di 1 A: solo quando la corrente in ingresso è maggiore di 3,5 A, sarà raggiunta la corrente massima. In questo caso, è possibile collegare più blocchi elettronici a HaloCode.
- Non utilizzare HaloCode in acqua o con le mani bagnate.
- Utilizza HaloCode in una stanza ben ventilata.
- Non conservare o utilizzare HaloCode in ambienti estremamente caldi o freddi.
- Non posizionare oggetti metallici sui circuiti stampati della scheda poiché ciò può causare un cortocircuito che può danneggiare HaloCode. Ciò può causare il rischio di danni permanenti.
- Il collegamento di HaloCode a periferiche non approvate potrebbe danneggiare HaloCode.
- Non lasciare HaloCode collegato a un computer o ad un altro dispositivo senza supervisione.
- NON utilizzare la clip a coccodrillo per collegare il cavetto a 3.3 V al cavetto GND, altrimenti potrebbe verificarsi un cortocircuito e HaloCode potrebbe rimanere danneggiato. 
