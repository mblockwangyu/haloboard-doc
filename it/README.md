# Makeblock HaloCode Kit standard

* [Elenco dei pezzi](parts.md)
* [Guida rapida per l'utente](quick-start.md)
* [Introduzione all'aspetto](appearance.md)
* [Batteria](battery.md)
* [Crea con il cinturino a strappo](strap.md)
* [Crea con le clip a coccodrillo](clip.md)
* [Domande frequenti](faq.md)
* [Attenzione](caution.md)

### Assistenza e risorse

Ci interessa come utilizzi i nostri prodotti e come ti trovi con essi. Non esitare a contattarci se riscontri problemi o hai bisogno di suggerimenti.

Supporto tecnico: <support@makeblock.com>

Makeblock Education: 
[http://education.makeblock.com/](http://education.makeblock.com/)















