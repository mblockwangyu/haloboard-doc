# Guida rapida per l'utente

## 1. Scaricare il software

Per scaricare l'ultima versione di mBlock 5, visita: [http://www.mblock.cc](http://www.mblock.cc).

<img src="../fr/images/download.png" width="600px;" style="padding:5px 5px 20px 5px;">

## 2. Collegare HaloCode a un computer

Collega HaloCode al computer tramite il cavo micro USB.

<img src="../fr/images/connect.png" width="600px;" style="padding:5px 5px 20px 5px;">

## 3. Iniziare a programmare

<span style="color:white;font-size:18;border-radius:100%;background-color:lightblue;font-wieght:bold;">&nbsp; 1 &nbsp;</span> Apri mBlock 5 e aggiungi HaloCode dalla Libreria del dispositivo.

<table style="table-layout:fixed;width:100%;text-align:center;" cellpadding=20px; cellspacing=12px;>
<tr>
<td width="50%"><img src="images/connect-halo-1.png" height="120px;" style="padding:5px 5px 5px 5px;"></td>
<td width="10%;"><span style="font-size:30px;padding:5px;color:lightblue;">&rarr;</span></td>
<td width="40%"><img src="../fr/images/connect-halo-2.png" height="120px;" style="padding:5px 5px 5px 5px;"></td>
</tr>
</table>

<span style="color:white;font-size:18;border-radius:100%;background-color:lightblue;font-wieght:bold;">&nbsp; 2 &nbsp;</span> Collega il dispositivo

<table style="table-layout:fixed;width:100%;text-align:center;" cellpadding=20px; cellspacing=12px;>
<tr>
<td width="50%"><img src="images/connect-halo-3.png" height="120px;" style="padding:5px 5px 5px 5px;"></td>
<td width="10%;"><span style="font-size:30px;padding:5px;color:lightblue;">&rarr;</span></td>
<td width="40%"><img src="images/connect-halo-4.png" height="120px;" style="padding:5px 5px 5px 5px;"></td>
</tr>
</table>

<span style="color:white;font-size:18;border-radius:100%;background-color:lightblue;font-wieght:bold;">&nbsp; 3 &nbsp;</span> Completiamo ora un'attività di programmazione! Prova a far diventare rossi i LED su HaloCode.

<div style="margin:20px;">
<p>1) Aggiungi un Blocco eventi come mostrato di seguito.</p>
<img src="images/program-1.png" width="600px;" style="padding:0px 5px 15px 5px;">
<p>2) Aggiungi il blocco viola in basso dalla categoria Blocchi di illuminazione. Quindi cambia il colore in rosso.</p>
<img src="images/program-2.png" width="600px;" style="padding:0px 5px 15px 5px;">
</div>

## 4. Eseguire il programma

Puoi scegliere di caricare il programma su HaloCode o di eseguire il programma su mBlock 5.

### Caricare il programma su HaloCode 

Esegui il programma senza software.

Attiva la modalità **di caricamento** e fai clic su **Carica**. Dopo che il caricamento è andato a buon fine, premi il pulsante blu su HaloCode per verificare se i LED diventano rosso fisso.

<img src="images/program-3.png" style="padding:5px 5px 5px 5px;">

### Eseguire il programma su mBlock

Il caricamento non sarà necessario. Puoi vedere in tempo reale il modo in cui il tuo codice funziona.

Disattiva la **Modalità di caricamento** e **cambia il colore del tuo programma da rosso a verde**. Premi il pulsante blu su HaloCode per verificare se i LED cambiano colore.

<img src="images/program-4.png" style="padding:5px 5px 5px 5px;">

