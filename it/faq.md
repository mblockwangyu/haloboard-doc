# Domande frequenti

### Perché il programma non funziona come previsto dopo che ho caricato il programma su HaloCode?

Il motivo potrebbe essere che la versione del firmware non è aggiornata. Fai clic su **Aggiorna** per aggiornare il firmware.

<img src="images/update-halo.png" width="300px;" style="padding:5px 5px 20px 5px;"> <span style="font-size:30px;padding:5px;color:lightblue;">&rarr;</span><img src="images/update-halo-1.png" width="300px;" style="padding:5px 5px 20px 5px;">

### In che modo alimento HaloCode?

Ci sono due metodi per alimentare HaloCode: 

1. Collegalo a una batteria tramite l'interfaccia per la batteria. 

2. Collegalo a un computer o a una power bank tramite un cavo micro USB.

Requisiti di tensione di alimentazione: CC 3-5 V