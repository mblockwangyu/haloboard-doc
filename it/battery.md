# Batteria

### Installare la batteria

<img src="../fr/images/install-battery.png" width="600px" style="padding:5px 5px 5px 5px;">

### Avvertenze

- Per rimuovere la batteria, solleva il connettore con le dita. Non rimuovere tirando i cavi.
- Non usare insieme tipi diversi di batterie o batterie nuove e usate.
- Inserisci le batterie nel modo corretto (con la polarità corretta).
- Rimuovi le batterie usate dal portabatterie.
- Non posizionare oggetti metallici lungo la presa della batteria HaloCode poiché ciò potrebbe causare cortocircuiti e surriscaldare la batteria.
- Non provare a ricaricare batterie normali (non ricaricabili).
