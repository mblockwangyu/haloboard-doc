# Crea con il cinturino a strappo

Con il cinturino a strappo, puoi legare HaloCode al polso. I LED e il sensore tattile su HaloCode ti permettono di creare un dispositivo indossabile, come un contapassi. 

Trasformiamo HaloCode in un dispositivo indossabile. In questo progetto, imparerai a creare un progetto utilizzando il cinturino a strappo.

Questo è quello che otterrai: Indossa HaloCode al polso. Come programmato, i LED si illuminano di un colore casuale quando il sensore di movimento su HaloCode rileva i movimenti della mano.

<img src="../fr/images/strap-1.png" width="300px;" style="padding:5px 5px 20px 5px;">

### Fase 1 Trova i materiali di cui hai bisogno.

<img src="../fr/images/strap-2.png" width="800px;" style="padding:5px 5px 20px 5px;">

### Fase 2 Scrivi il codice e caricalo su HaloCode.

<img src="images/strap-3.png" width="800px;" style="padding:5px 5px 20px 5px;">

### Fase 3 Inserisci le batterie nel portabatterie.

<img src="../fr/images/strap-4.png" width="800px;" style="padding:5px 5px 20px 5px;">

### Fase 4 Indossa HaloCode al polso come mostrato di seguito (mano sinistra nell'esempio).

<span style="color:white;font-size:18;border-radius:100%;background-color:lightblue;font-wieght:bold;">&nbsp; 1 &nbsp;</span> Collega il portabatterie a HaloCode.

<img src="images/strap-5.png" width="600px;" style="padding:5px 5px 20px 5px;">

<span style="color:white;font-size:18;border-radius:100%;background-color:lightblue;font-wieght:bold;">&nbsp; 2 &nbsp;</span> Attacca i dispositivi di fissaggio a strappo alla custodia scorrevole del portabatterie e alla parte posteriore di HaloCode. 

<img src="images/strap-6.png" width="600px;" style="padding:5px 5px 20px 5px;">

<span style="color:white;font-size:18;border-radius:100%;background-color:lightblue;font-wieght:bold;">&nbsp; 3 &nbsp;</span>

<img src="images/strap-7.png" width="800px;" style="padding:5px 5px 20px 5px;">

<span style="color:white;font-size:18;border-radius:100%;background-color:lightblue;font-wieght:bold;">&nbsp; 4 &nbsp;</span>

<img src="images/strap-8.png" width="600px;" style="padding:5px 5px 20px 5px;">

<span style="color:white;font-size:18;border-radius:100%;background-color:lightblue;font-wieght:bold;">&nbsp; 5 &nbsp;</span>

<img src="images/strap-9.png" width="800px;" style="padding:5px 5px 20px 5px;">

<span style="color:white;font-size:18;border-radius:100%;background-color:lightblue;font-wieght:bold;">&nbsp; 6 &nbsp;</span>

<img src="images/strap-10.png" width="600px;" style="padding:5px 5px 20px 5px;">

### Completato!

Ora, accendi l'interruttore della batteria. Scuoti la mano per vedere i LED accesi!

<img src="images/strap-11.png" width="600px;" style="padding:5px 5px 20px 5px;">

Ora sai come usare il cinturino a strappo. Usa la tua immaginazione per creare altri progetti divertenti.