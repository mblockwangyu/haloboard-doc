# Summary

## Docs

* [Makeblock HaloCode Kit standard](README.md)
    * [Elenco dei pezzi](parts.md)
    * [Guida rapida per l'utente](quick-start.md)
    * [Introduzione all'aspetto](appearance.md)
    * [Batteria](battery.md)
    * [Crea con il cinturino a strappo](strap.md)
    * [Crea con le clip a coccodrillo](clip.md)
    * [Domande frequenti](faq.md)
    * [Attenzione](caution.md)
