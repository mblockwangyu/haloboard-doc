# Elenco dei pezzi

<table style="table-layout:fixed;width:100%;text-align:center;" cellpadding=20px; cellspacing=12px;>

<tr>
<td><img src="../fr/images/halo.png" width="300px;" style="padding:5px 5px 5px 5px;"><br>1 HaloCode</td>
<td><img src="../fr/images/battery-box.png" width="300px;" style="padding:5px 5px 5px 5px;"><br>Porta batterie x 1</td>
<td><img src="../fr/images/battery.png" width="300px;" style="padding:5px 5px 5px 5px;"><br>Batteria AAA x 3</td>
</tr>

<tr>
<td><img src="../fr/images/clip.png" width="300px;" style="padding:5px 5px 5px 5px;"><br>Clip a coccodrillo x 4</td>
<td><img src="../fr/images/tape.png" width="300px;" style="padding:5px 5px 5px 5px;"><br>Nastro laminato in rame<br>x 1</td>
<td><img src="../fr/images/strap.png" width="300px;" style="padding:5px 5px 5px 5px;"><br>Cinturino Hook & Loop x 1</td>
</tr>

<td><img src="../fr/images/fastener.png" width="300px;" style="padding:5px 5px 5px 5px;"><br>Fermaglio Hook & Loop<br>x 4</td>
<td><img src="../fr/images/usb.png" width="300px;" style="padding:5px 5px 5px 5px;"><br>Cavo Micro USB x 1</td>
</tr>

</table>