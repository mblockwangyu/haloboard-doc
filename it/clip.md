# Crea con le clip a coccodrillo

Le parti metalliche all'estremità delle clip a coccodrillo conducono elettricità. Utilizzando il sensore tattile e il nastro conduttore in rame, puoi facilmente creare progetti stimolanti, come una tavolozza HaloCode. La cosa ancora più interessante è che HaloCode può anche interagire con la fase mBlock 5.

Creiamo ora un semplice progetto utilizzando HaloCode. In questo progetto, imparerai come utilizzare le clip a coccodrillo per creare un progetto.

Con il cartone all'interno della confezione e il nastro di rame, puoi fare in modo che i LED possono si illuminino del colore corrispondente quando tocchi una determinata clip a coccodrillo.

<img src="../fr/images/clip-1.png" width="300px;" style="padding:5px 5px 20px 5px;">

### Fase 1 Trova i materiali di cui hai bisogno.

<img src="images/clip-2.png" width="800px;" style="padding:5px 5px 20px 5px;">

### Fase 2  Fissa le clip a coccodrillo.

<span style="color:white;font-size:18;border-radius:100%;background-color:lightblue;font-wieght:bold;">&nbsp; 1 &nbsp;</span> Fissa la clip a coccodrillo rossa al sensore tattile 0.

<img src="../fr/images/clip-3.png" width="300px;" style="padding:5px 5px 20px 5px;">

<span style="color:white;font-size:18;border-radius:100%;background-color:lightblue;font-wieght:bold;">&nbsp; 2 &nbsp;</span> Fissa le clip a coccodrillo gialla, blu e verde rispettivamente i sensori tattili 1, 2 e 3.

<img src="../fr/images/clip-4.png" width="300px;" style="padding:5px 5px 20px 5px;">

<div style="background-color:#FFFFCC;padding:10px;border-left:5px solid #FFEB3B;margin:20px;">NON utilizzare la clip a coccodrillo per collegare il cavetto a 3.3 V al cavetto GND, altrimenti potrebbe verificarsi un cortocircuito e HaloCode potrebbe rimanere danneggiato.
</div>

<br>

<span style="color:white;font-size:18;border-radius:100%;background-color:lightblue;font-wieght:bold;">&nbsp; 3 &nbsp;</span> Adatta il cartone all'interno della confezione e piegalo come mostrato.

<img src="../fr/images/clip-5.png" width="800px;" style="padding:5px 5px 20px 5px;">

<span style="color:white;font-size:18;border-radius:100%;background-color:lightblue;font-wieght:bold;">&nbsp; 4 &nbsp;</span> Fissa il nastro di rame al cartone.

<img src="../fr/images/clip-6.png" width="300px;" style="padding:5px 5px 20px 5px;">

<div style="background-color:#FFFFCC;padding:10px;border-left:5px solid #FFEB3B;margin:20px;">*Il nastro di rame è conduttivo su entrambi i lati. Fissando le clip a coccodrillo ai nastri puoi espandere l'area conduttiva. 
</div>

<br>

<span style="color:white;font-size:18;border-radius:100%;background-color:lightblue;font-wieght:bold;">&nbsp; 5 &nbsp;</span> Rimuovi le pellicole protettive dal nastro di rame.

<img src="../fr/images/clip-7.png" width="300px;" style="padding:5px 5px 20px 5px;">

<div style="background-color:#FFFFCC;padding:10px;border-left:5px solid #FFEB3B;margin:20px;">* La pellicola protettiva serve a proteggere il nastro dall'ossidazione.
</div>

<br>

<span style="color:white;font-size:18;border-radius:100%;background-color:lightblue;font-wieght:bold;">&nbsp; 6 &nbsp;</span> Fissa l'altra estremità delle clip a coccodrillo ai nastri in lamina di rame. Rimuovi le pellicole protettive dal nastro di rame.

<img src="../fr/images/clip-8.png" width="500px;" style="padding:5px 5px 20px 5px;">

### Fase 3  Collega HaloCode a un computer con il cavo micro USB.

<img src="../fr/images/clip-9.png" width="600px;" style="padding:5px 5px 20px 5px;">

### Fase 4  Inizia a programmare.

<img src="images/clip-11.png" width="600px;" style="padding:5px 5px 20px 5px;">

### Completato!

Tocca la parte metallica della clip a coccodrillo rosso o il nastrodi rame per vedere se i LED diventano rossi.

<img src="../fr/images/clip-10.png" width="600px;" style="padding:5px 5px 20px 5px;">

Ora sai come usare le clip a coccodrillo. Usa la tua immaginazione per creare altri progetti divertenti.