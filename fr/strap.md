# Effectuer votre création en utilisant une bande magique

En combinant avec des LED de couleur, des capteurs tactiles et similaires, cette bande magique peut fixer votre HaloCode au poignet pour constituer les articles portables intéressantes telles qu’un podomètre.

Constituons un article portable tout simple ! Et vous pourrez connaître la constitution d’un article en utilisant la bande magique.

Portez votre HaloCode au poignet, lorsque le capteur de mouvement détecte un mouvement du HaloCode, la LED de couleur présente une couleur de manière aléatoire.

<img src="images/strap-1.png" width="300px;" style="padding:5px 5px 20px 5px;">

### 1. Trouvez les objets que vous avez besoin.

<img src="images/strap-2.png" width="800px;" style="padding:5px 5px 20px 5px;">

### 2. Programmez et émettez le programme vers votre HaloCode.

<img src="images/strap-3.png" width="800px;" style="padding:5px 5px 20px 5px;">

### 3. Installez la batterie.

<img src="images/strap-4.png" width="800px;" style="padding:5px 5px 20px 5px;">

### 4. Effectuez le processus de port en suivant les étapes ci-dessous (un exemple de porter à la main gauche).

<span style="color:white;font-size:18;border-radius:100%;background-color:lightblue;font-wieght:bold;">&nbsp; 1 &nbsp;</span> Connectez la batterie et le HaloCode.

<img src="images/strap-5.png" width="600px;" style="padding:5px 5px 20px 5px;">

<span style="color:white;font-size:18;border-radius:100%;background-color:lightblue;font-wieght:bold;">&nbsp; 2 &nbsp;</span> Mettez les velcros au couvercle coulissant arrière du compartiment à batterie et à l’arrière du HaloCode.

<img src="images/strap-6.png" width="600px;" style="padding:5px 5px 20px 5px;">

<span style="color:white;font-size:18;border-radius:100%;background-color:lightblue;font-wieght:bold;">&nbsp; 3 &nbsp;</span>

<img src="images/strap-7.png" width="800px;" style="padding:5px 5px 20px 5px;">

<span style="color:white;font-size:18;border-radius:100%;background-color:lightblue;font-wieght:bold;">&nbsp; 4 &nbsp;</span>

<img src="images/strap-8.png" width="600px;" style="padding:5px 5px 20px 5px;">

<span style="color:white;font-size:18;border-radius:100%;background-color:lightblue;font-wieght:bold;">&nbsp; 5 &nbsp;</span>

<img src="images/strap-9.png" width="800px;" style="padding:5px 5px 20px 5px;">

<span style="color:white;font-size:18;border-radius:100%;background-color:lightblue;font-wieght:bold;">&nbsp; 6 &nbsp;</span>

<img src="images/strap-10.png" width="600px;" style="padding:5px 5px 20px 5px;">

### Voilà, c’est fini！ 

Fermez l'interrupteur de la batterie et secouez la main pour vérifier que la LED de couleur s’allume.

<img src="images/strap-11.png" width="600px;" style="padding:5px 5px 20px 5px;">

Je crois que vous pouvez utiliser la bande magique maintenant, alors bien profitez votre imagination pour constituer plus d’articles intéressants!
