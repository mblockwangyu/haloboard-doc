# Questions fréquentes

### Le HaloCode ne peut pas fonctionner normalement après avoir émis le programme?

Ça peut être parce que la version du matériel n'est pas mise à jour, cliquez sur mise à jour du matériel pour mettre le matériel à jour en référant la figure ci-dessous.

<img src="images/update-halo.png" width="300px;" style="padding:5px 5px 20px 5px;"> <span style="font-size:30px;padding:5px;color:lightblue;">&rarr;</span><img src="images/update-halo-1.png" width="300px;" style="padding:5px 5px 20px 5px;">

### Comment alimenter le HaloCode?

Le HaloCode peut être alimenté à deux manières suivantes:

1. Branchez le compartiment à batterie depuis l’extérieur via l'interface de batterie

2. Branchez un ordinateur ou une source d’alimentation portable via un câble micro USB

Tension d'alimentation spécifiée : DC 3-5V