# Summary

## Docs

* [Kit standard de HaloCode](README.md)
    * [Liste des pièces détachées](parts.md)
    * [Guide d’emploi facile](quick-start.md)
    * [Présentation de l’aspect](appearance.md)
    * [Batterie](battery.md)
    * [Effectuer votre création en utilisant une bande magique](strap.md)
    * [Constituez un article en utilisant votre pince crocodile](clip.md)
    * [Questions fréquentes](faq.md)
    * [Précautions](caution.md)
