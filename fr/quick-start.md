# Guide d’emploi facile

## 1. Téléchargement du logiciel

Téléchargez et installez le logiciel de dernier version mBlock 5 sur [http://www.mblock.cc](http://www.mblock.cc).

<img src="images/download.png" width="600px;" style="padding:5px 5px 20px 5px;">

## 2. Connectez le HaloCode à l'ordinateur

Connectez le HaloCode à l'ordinateur via un câble micro USB.

<img src="images/connect.png" width="600px;" style="padding:5px 5px 20px 5px;">

## 3. Commencez la programmation

<span style="color:white;font-size:18;border-radius:100%;background-color:lightblue;font-wieght:bold;">&nbsp; 1 &nbsp;</span> Démarrez mBlock 5 et ajoutez le HaloCode à partir de la base d’équipement.

<table style="table-layout:fixed;width:100%;text-align:center;" cellpadding=20px; cellspacing=12px;>
<tr>
<td width="50%"><img src="images/connect-halo-1.png" height="120px;" style="padding:5px 5px 5px 5px;"></td>
<td width="10%;"><span style="font-size:30px;padding:5px;color:lightblue;">&rarr;</span></td>
<td width="40%"><img src="images/connect-halo-2.png" height="120px;" style="padding:5px 5px 5px 5px;"></td>
</tr>
</table>

<span style="color:white;font-size:18;border-radius:100%;background-color:lightblue;font-wieght:bold;">&nbsp; 2 &nbsp;</span> Connectez l’équipement

<table style="table-layout:fixed;width:100%;text-align:center;" cellpadding=20px; cellspacing=12px;>
<tr>
<td width="50%"><img src="images/connect-halo-3.png" height="120px;" style="padding:5px 5px 5px 5px;"></td>
<td width="10%;"><span style="font-size:30px;padding:5px;color:lightblue;">&rarr;</span></td>
<td width="40%"><img src="images/connect-halo-4.png" height="120px;" style="padding:5px 5px 5px 5px;"></td>
</tr>
</table>

<span style="color:white;font-size:18;border-radius:100%;background-color:lightblue;font-wieght:bold;">&nbsp; 3 &nbsp;</span> Faisons maintenant un programme tout simple ! Opérez le HaloCode pour vérifier que toutes LED de couleur s’allument et présentent la couleur rouge.

<div style="margin:20px;">
<p>1) Ajoutez un bloc d’événement en référant la figure ci-dessous.</p>
<img src="images/program-1.png" width="600px;" style="padding:0px 5px 15px 5px;">
<p>2) Ajoutez un bloc violet comme ci-dessous à partir de la zone de bloc d'éclairage et changez la couleur d'éclairage en rouge pour terminer le programme.</p>
<img src="images/program-2.png" width="600px;" style="padding:0px 5px 15px 5px;">
</div>

## 4. Exécutez le programme

Vous pouvez émettre le programme vers HaloCode ou exécuter directement le programme dans le logiciel mBlock 5.

### Pour émettre le programme vers HaloCode

Il est possible d’éviter d’exécuter le programme exécutif dans le logiciel

Passez en **mode d’émettre**, appuyez sur la touche pour **émettre à l’équipement**. A la fin d’émettre, appuyez sur la touche bleue sur le HaloCode pour vérifier que la LED de couleur s’allume et présente la couleur rouge.

<img src="images/program-3.png" style="padding:5px 5px 5px 5px;">

### Pour exécuter le programme dans le logiciel mblock

Vous pouvez consulter les résultats en temps réel sans émettre le programme.

Quittez le **mode d’émettre et changez la couleur du programme en vert**, appuyez sur la touche bleue sur le HaloCode pour vérifier que la couleur de la LED se change.

<img src="images/program-4.png" style="padding:5px 5px 5px 5px;">