# Précautions

- Le HaloCode ne peut être alimentée que par une source d’alimentation 3V - 5V DC.

- Ne branchez/débranchez pas le circuit sous tension. En d'autres termes, assurez-vous que le HaloCode se trouve à l’état hors tension pour brancher le circuit avec l'interface d'extension E/S.

- Chacune des interfaces d’extension 0, 1, 2, et 3 a une capacité de charge de 10 mA.

- La broche 3.3V a une capacité de charge de 500mA.

- L’interface d’extension du module électronique a une capacité de charge de 1 A. Notez que la capacité de charge de cette interface d’extension ne peut arriver à 1A que la tension d'entrée de la source l'alimentation est supérieure à 3 V et que le courant d'entrée est supérieur à 3,5 A, en ce cas, il est possible de brancher plusieurs modules électroniques.

- N'exposez pas votre HaloCode à l'eau ou à l'humidité.

- Opérez votre HaloCode dans un environnement bien ventilé.

- Ne stockez pas votre HaloCode dans un environnement trop froid ou trop chaud.

- Ne placez aucun objet métallique au-dessus de votre HaloCode, vous risqueriez de provoquer une panne de votre HaloCode même un endommagement irréversible.

- La connexion votre HaloCode à un équipement non approuvé risque de provoquer un endommagement de votre HaloCode.

- Ne branchez pas votre HaloCode dans un équipement en cas d’absence de surveillance.

- Ne connectez pas directement la broche 3,3 V à la broche GND par la pince crocodile, ce qui risque de provoquer un court-circuit et un endommagement du HaloCode.
