# Kit standard de HaloCode

* [Liste des pièces détachées](parts.md)
* [Guide d’emploi facile](quick-start.md)
* [Présentation de l’aspect](appearance.md)
* [Batterie](battery.md)
* [Effectuer votre création en utilisant une bande magique](strap.md)
* [Constituez un article en utilisant votre pince crocodile](clip.md)
* [Questions fréquentes](faq.md)
* [Précautions](caution.md)

### Pour plus d’aide et de ressource

Votre expérience d’utilisation est très importante pour nous, et n’hésitez pas de nous contacter pour toutes questions ou suggestions.

Support technique: <support@makeblock.com>

Site d'éducation Makeblock
[http://education.makeblock.com/](http://education.makeblock.com/)















