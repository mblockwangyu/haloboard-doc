# Batterie

### Installez la batterie.

<img src="images/install-battery.png" width="600px" style="padding:5px 5px 5px 5px;">

### Informations importantes

- Pour débrancher la batterie, ne tirez pas forcement la câble de la batterie, pincez le connecteur avec les doigts et retirez-le doucement.
- N'utilisez pas des batteries de marques différentes, ni des batteries anciennes et des piles neuves en même temps.
- Assurez-vous que la batterie est placée dans le bon sens.
Toujours retirez les batteries épuisées de son compartiment.
- Ne placez aucun objet métallique entre les bornes du compartiment de la batterie, vous risqueriez de provoquer un court-circuit.
- Ne chargez pas une batterie ordinaire (batterie non rechargeable).
