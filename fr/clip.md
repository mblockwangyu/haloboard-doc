#  Constituez un article en utilisant votre pince crocodile

Les parties métalliques aux extrémités de la pince crocodile sont conductives et peuvent être en combinaison avec le capteur tactile du HaloCode et le ruban adhésif couvert de papier en cuivre conductif à deux surfaces pour constituer divers articles tactiles intéressants, tels que la palette de couleurs, l'interaction avec l’étape mBlock 5 etc.

Constituons un article tout simple ! Vous pourrez connaître la constitution d’un article en utilisant la pince crocodile.

Utilisez les objets fournis dans la boîte de conditionnement, tel que le carton, le ruban adhésif couvert de papier en cuivre, afin que la LED de couleur s’allume et présente la couleur correspondante lorsque vous touchez une pince de crocodile.

<img src="images/clip-1.png" width="300px;" style="padding:5px 5px 20px 5px;">

### 1. Trouvez les objets que vous avez besoin.

<img src="images/clip-2.png" width="800px;" style="padding:5px 5px 20px 5px;">

### 2. Installez la pince crocodile.

<span style="color:white;font-size:18;border-radius:100%;background-color:lightblue;font-wieght:bold;">&nbsp; 1 &nbsp;</span> Fixez la pince crocodile rouge au capteur tactile 0.

<img src="images/clip-3.png" width="300px;" style="padding:5px 5px 20px 5px;">

<span style="color:white;font-size:18;border-radius:100%;background-color:lightblue;font-wieght:bold;">&nbsp; 2 &nbsp;</span> Fixez les pinces crocodiles jaune, bleu et vert un par un aux capteurs tactiles 1, 2 et 3.

<img src="images/clip-4.png" width="300px;" style="padding:5px 5px 20px 5px;">

<div style="background-color:#FFFFCC;padding:10px;border-left:5px solid #FFEB3B;margin:20px;">Ne connectez pas directement la broche 3,3 V à la broche GND par la pince crocodile, ce qui risque de provoquer un court-circuit et un endommagement du HaloCode.
</div>

<br>

<span style="color:white;font-size:18;border-radius:100%;background-color:lightblue;font-wieght:bold;">&nbsp; 3 &nbsp;</span> Coupez et pliez le carton fourni dans la boîte de conditionnement.

<img src="images/clip-5.png" width="800px;" style="padding:5px 5px 20px 5px;">

<span style="color:white;font-size:18;border-radius:100%;background-color:lightblue;font-wieght:bold;">&nbsp; 4 &nbsp;</span> Adhérez le ruban adhésif couvert de papier en cuivre au carton.

<img src="images/clip-6.png" width="300px;" style="padding:5px 5px 20px 5px;">

<div style="background-color:#FFFFCC;padding:10px;border-left:5px solid #FFEB3B;margin:20px;">* Le ruban adhésif couvert de papier en cuivre est conductive à deux surfaces, ce qui permet de prolonger et d’étendre la surface de contact en combinaison avec la pince crocodile.
</div>

<br>

<span style="color:white;font-size:18;border-radius:100%;background-color:lightblue;font-wieght:bold;">&nbsp; 5 &nbsp;</span> Enlevez le film protecteur sur ruban adhésif couvert de papier en cuivre.

<img src="images/clip-7.png" width="300px;" style="padding:5px 5px 20px 5px;">

<div style="background-color:#FFFFCC;padding:10px;border-left:5px solid #FFEB3B;margin:20px;">* Ce film protecteur est utilisé pour empêcher l’oxydation du ruban adhésif couvert de papier en cuivre.
</div>

<br>

<span style="color:white;font-size:18;border-radius:100%;background-color:lightblue;font-wieght:bold;">&nbsp; 6 &nbsp;</span> Fixez l’autre extrémité de la pince crocodile sur le ruban adhésif couvert de papier en cuivre.

<img src="images/clip-8.png" width="500px;" style="padding:5px 5px 20px 5px;">

### 3. Connectez le HaloCode à l'ordinateur via un câble micro USB.

<img src="images/clip-9.png" width="600px;" style="padding:5px 5px 20px 5px;">

### 4. Commencez la programmation.

<img src="images/clip-11.png" width="600px;" style="padding:5px 5px 20px 5px;">

### Voilà, c’est fini！ 

Touchez la partie métallique de la pince crocodile rouge ou le ruban adhésif couvert de papier en cuivre pour vérifier que la LED de couleur s’allume et présente la couleur rouge.

<img src="images/clip-10.png" width="600px;" style="padding:5px 5px 20px 5px;">

Je crois que vous pouvez utiliser la pince crocodile maintenant, alors bien profitez votre imagination pour constituer plus d’articles intéressants!
















