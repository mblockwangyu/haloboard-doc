# クイックスタートガイド

## 1. ソフトウェアをダウンロードする

mBlockの最新バージョンを [http://www.mblock.cc](http://www.mblock.cc) からダウンロードしてください

<img src="../fr/images/download.png" width="600px;" style="padding:5px 5px 20px 5px;">

## 2. HaloCodeをコンピュータに接続する

HaloCodeをマイクロUSBケーブル経由でコンピュータに接続します。

<img src="../fr/images/connect.png" width="600px;" style="padding:5px 5px 20px 5px;">

## 3. プログラミングを始める

<span style="color:white;font-size:18;border-radius:100%;background-color:lightblue;font-wieght:bold;">&nbsp; 1 &nbsp;</span> mBlock 5を開き、HaloCodeをデバイスライブラリから追加します。

<table style="table-layout:fixed;width:100%;text-align:center;" cellpadding=20px; cellspacing=12px;>
<tr>
<td width="50%"><img src="images/connect-halo-1.png" height="120px;" style="padding:5px 5px 5px 5px;"></td>
<td width="10%;"><span style="font-size:30px;padding:5px;color:lightblue;">&rarr;</span></td>
<td width="40%"><img src="../fr/images/connect-halo-2.png" height="120px;" style="padding:5px 5px 5px 5px;"></td>
</tr>
</table>

<span style="color:white;font-size:18;border-radius:100%;background-color:lightblue;font-wieght:bold;">&nbsp; 2 &nbsp;</span>  デバイスを接続する。

<table style="table-layout:fixed;width:100%;text-align:center;" cellpadding=20px; cellspacing=12px;>
<tr>
<td width="50%"><img src="images/connect-halo-3.png" height="120px;" style="padding:5px 5px 5px 5px;"></td>
<td width="10%;"><span style="font-size:30px;padding:5px;color:lightblue;">&rarr;</span></td>
<td width="40%"><img src="images/connect-halo-4.png" height="120px;" style="padding:5px 5px 5px 5px;"></td>
</tr>
</table>

<span style="color:white;font-size:18;border-radius:100%;background-color:lightblue;font-wieght:bold;">&nbsp; 3 &nbsp;</span> さあ、プログラミングを完成させましょう！HaloCodeのLEDライトを赤色に点灯させます。

<div style="margin:20px;">
<p>1) イベントブロックを追加します。</p>
<img src="images/program-1.png" width="600px;" style="padding:0px 5px 15px 5px;">
<p>2) LEDライトブロックから紫色のブロックを追加します。点灯する色を赤色に変更します。</p>
<img src="images/program-2.png" width="600px;" style="padding:0px 5px 15px 5px;">
</div>

## 4. プログラムを実行する

HaloCodeにプログラムをアップロードすることも、mBlock 5上でプログラムを実行することもできます。

### HaloCodeにプログラムをアップロードします。ソフトウェアから接続を切断して、プログラムを実行してみましょう。

**アップロードモード**をオンにし、**アップロード**をクリックしてください。プログラムのアップロードが完了したら、HaloCodeの青いボタンを押してLEDライトが赤色に点灯するか確認しましょう。

<img src="images/program-3.png" style="padding:5px 5px 5px 5px;">

### mBlock 5でプログラムを実行します。アップロードは必要ありません。プログラムが実際に動くかどうかリアルタイムで確認できます。

**アップロードモード**をオフにして、**プログラムの色を赤から緑に変更しましょう**。HaloCodeの青いボタンを押してLEDの色が変わるか確認しましょう。

<img src="images/program-4.png" style="padding:5px 5px 5px 5px;">