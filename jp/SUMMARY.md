# Summary

## Docs

* [Makeblock HaloCode スタンダードキット](README.md)
    * [パーツリスト](parts.md)
    * [クイックスタートガイド](quick-start.md)
    * [外観の紹介](appearance.md)
    * [電池の使用](battery.md)
    * [マジックベルトの使用例](strap.md)
    * [ワニ口クリップの使用例](clip.md)
    * [FAQ](faq.md)
    * [注意](caution.md)