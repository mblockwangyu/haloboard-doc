# Makeblock HaloCode スタンダードキット

* [パーツリスト](parts.md)
* [クイックスタートガイド](quick-start.md)
* [外観の紹介](appearance.md)
* [電池の使用](battery.md)
* [マジックベルトの使用例](strap.md)
* [ワニ口クリップの使用例](clip.md)
* [FAQ](faq.md)
* [注意](caution.md)

### ヘルプ・詳細情報

お使いになった感想はいかがでしょうか。ご質問やご意見がありましたら、お気軽にお寄せください。

- 住所：東京都千代田区外神田6-3-8 ACN秋葉原ビル 7階
[www.makeblock.com/jp](www.makeblock.com/jp)

- サポート：<jp@makeblock.com>

- Makeblcok Educationサイト：
[http://education.makeblock.com/](http://education.makeblock.com/)
