# マジックベルトを使って創作する

マジックベルトでHaloCodeを腕に巻きつけることができます。RGB LED、タッチセンサーなどと組み合わせて、歩数計のような様々な面白いウェアラブルデバイスをつくることが可能です。

さあ、簡単なウェアラブルデバイスを作ってみましょう！このプロジェクトを通して、マジックベルトを使った創作を学ぶことができます。

HaloCodeを腕につけ、モーションセンサーを使って揺れを検知した時に、RGB LEDがランダムに1つの色を示すようにします。


<img src="../fr/images/strap-1.png" width="300px;" style="padding:5px 5px 20px 5px;">

### 1 必要なパーツを見つける。

<img src="../fr/images/strap-2.png" width="800px;" style="padding:5px 5px 20px 5px;">

### 2 プログラムを編集し、HaloCodeにアップロードする。

<img src="images/strap-3.png" width="800px;" style="padding:5px 5px 20px 5px;">

### 3 電池を取り付ける。

<img src="../fr/images/strap-4.png" width="800px;" style="padding:5px 5px 20px 5px;">

### 4 以下の手順を参照して着用する（左手への着用例）。

<span style="color:white;font-size:18;border-radius:100%;background-color:lightblue;font-wieght:bold;">&nbsp; 1 &nbsp;</span> 電池ケースとHaloCodeを接続します。

<img src="images/strap-5.png" width="600px;" style="padding:5px 5px 20px 5px;">

<span style="color:white;font-size:18;border-radius:100%;background-color:lightblue;font-wieght:bold;">&nbsp; 2 &nbsp;</span> マジックベルトを反対向きに巻きつけてから、貼り付けます。 

<img src="images/strap-6.png" width="600px;" style="padding:5px 5px 20px 5px;">

<span style="color:white;font-size:18;border-radius:100%;background-color:lightblue;font-wieght:bold;">&nbsp; 3 &nbsp;</span>

<img src="images/strap-7.png" width="800px;" style="padding:5px 5px 20px 5px;">

<span style="color:white;font-size:18;border-radius:100%;background-color:lightblue;font-wieght:bold;">&nbsp; 4 &nbsp;</span>

<img src="images/strap-8.png" width="600px;" style="padding:5px 5px 20px 5px;">

<span style="color:white;font-size:18;border-radius:100%;background-color:lightblue;font-wieght:bold;">&nbsp; 5 &nbsp;</span>

<img src="images/strap-9.png" width="800px;" style="padding:5px 5px 20px 5px;">

<span style="color:white;font-size:18;border-radius:100%;background-color:lightblue;font-wieght:bold;">&nbsp; 6 &nbsp;</span>

<img src="images/strap-10.png" width="600px;" style="padding:5px 5px 20px 5px;">

### 完成です！

電池のスイッチをオンにし、手を動かすとRGB LEDが光るか試してみましょう。

<img src="images/strap-11.png" width="600px;" style="padding:5px 5px 20px 5px;">

マジックベルトの使い方がわかりましたね。さあ、想像力を発揮して、他にどんな面白いアイテムができるかやってみましょう！

