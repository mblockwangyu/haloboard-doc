# 注意

- HaloCodeは、3V-5VのDC電源からのみ給電できます。
- HaloCodeを電源に接続した状態での取り扱いは避けてください。外部のI/O回路に接続するときはHaloCodeをオフにしてください。
- 4つのI/Oピンを使用して外部回路に安全に供給される最大電流は10mAです。
- 3.3Vインタフェースを使用して外部回路に安全に供給される最大電流は500mAです。この制限を超えないようにしてください。
- 電子ブロックの拡張インタフェースを使用して外部回路に安全に供給される最大電流は1Aです。電源入力電流が3.5Aより大きい場合に限り、最大電流に達します。この場合、より多くの電子ブロックをHaloCodeに接続することができます。
- HaloCodeを水の中や濡れた手で使用しないでください。
- HaloCodeは換気の良い部屋で操作してください。
- HaloCodeを極端に高温または低温の環境で保管または使用しないでください。
- HaloCodeの基板上に金属物を置かないでください。誤動作や永久的な損傷の原因となる可能性があります。
- HaloCodeを許可されていない周辺機器に接続すると、破損する可能性があります。
- HaloCodeをコンピュータや他の機器に接続したまま放置しないでください。
- ワニ口クリップを使って3.3VピンとGNDピンとを直接接続しないでください。ショートし、HaloCodeが破損する恐れがあります。
