# ワニ口クリップを使って創作する

ワニ口クリップの両端の金属部分は電気を通します。HaloCodeのタッチセンサーと両面が電気を通す銅箔テープと組み合わせることで、HaloCodeパレットのような様々な面白いタッチアイテムを製作したり、mBlock 5のステージと相互に作用させることができます。

さあ、一緒に簡単なアイテムを作ってみましょう！このプロジェクトを通して、ワニ口クリップを使った創作を学ぶことができます。

ボール紙（自分で準備する）、銅箔テープなどを使って、どれか1つのワニ口クリップに触った時、RGB LEDが対応する色を示すようにします。


<img src="../fr/images/clip-1.png" width="300px;" style="padding:5px 5px 20px 5px;">

### 1 必要なパーツを見つける。

<img src="images/clip-2.png" width="800px;" style="padding:5px 5px 20px 5px;">

### 2 ワニ口クリップを取り付ける。

<span style="color:white;font-size:18;border-radius:100%;background-color:lightblue;font-wieght:bold;">&nbsp; 1 &nbsp;</span> 赤、黄、青、緑の4色のワニ口クリップで順に0、1、2、3番のタッチセンサーを挟みます。

<img src="../fr/images/clip-3.png" width="300px;" style="padding:5px 5px 20px 5px;"> 
<img src="../fr/images/clip-4.png" width="300px;" style="padding:5px 5px 20px 5px;">

<div style="background-color:#FFFFCC;padding:10px;border-left:5px solid #FFEB3B;margin:20px;">ワニ口クリップを使って3.3VピンとGNDピンとを直接接続しないでください。ショートし、HaloCodeが破損する恐れがあります。
</div>

<br>

<span style="color:white;font-size:18;border-radius:100%;background-color:lightblue;font-wieght:bold;">&nbsp; 2 &nbsp;</span> ボール紙を1枚準備し、銅箔テープをボール紙の上に貼ります。

<img src="../fr/images/clip-5.png" width="800px;" style="padding:5px 5px 20px 5px;">

<img src="../fr/images/clip-6.png" width="300px;" style="padding:5px 5px 20px 5px;">

<div style="background-color:#FFFFCC;padding:10px;border-left:5px solid #FFEB3B;margin:20px;">※銅箔テープは両面が電気を通すので、ワニ口クリップと組み合わせて延長しするとタッチ面積を拡大できます。
</div>

<br>

<span style="color:white;font-size:18;border-radius:100%;background-color:lightblue;font-wieght:bold;">&nbsp; 3 &nbsp;</span> 銅箔テープの保護フィルムをはがします。

<img src="../fr/images/clip-7.png" width="300px;" style="padding:5px 5px 20px 5px;">

<div style="background-color:#FFFFCC;padding:10px;border-left:5px solid #FFEB3B;margin:20px;">※保護フィルムは銅箔テープの酸化を防ぐことができます。
</div>

<br>

<span style="color:white;font-size:18;border-radius:100%;background-color:lightblue;font-wieght:bold;">&nbsp; 4 &nbsp;</span> ワニ口クリップのもう1つの先で銅箔テープを挟みます。

<img src="../fr/images/clip-8.png" width="500px;" style="padding:5px 5px 20px 5px;">

### 3 マイクロUSBケーブルでHaloCodeをパソコンに接続する。

<img src="../fr/images/clip-9.png" width="600px;" style="padding:5px 5px 20px 5px;">

### 4 プログラミングを始める。

<img src="images/clip-11.png" width="600px;" style="padding:5px 5px 20px 5px;">

### 完成です！

赤色のワニ口クリップの金属部分か銅箔テープをタッチして、RGB LEDが赤く光るか見てみましょう。

<img src="../fr/images/clip-10.png" width="600px;" style="padding:5px 5px 20px 5px;">

ワニ口クリップの使い方がわかりましたね。さあ、想像力を発揮して、他にどんな面白いアイテムができるかやってみましょう！

