# FAQ

### HaloCodeにプログラムをアップロードしたのに、予想通りにプログラムが実行されないのはなぜですか？

ファームウェアが最新に更新されていない可能性があります。**アップデート**をクリックし、最新のファームウェアに更新してください。

<img src="images/update-halo.png" width="300px;" style="padding:5px 5px 20px 5px;"> <span style="font-size:30px;padding:5px;color:lightblue;">&rarr;</span><img src="images/update-halo-1.png" width="300px;" style="padding:5px 5px 20px 5px;">

### HaloCodeの給電方法

HaloCodeの給電方法は2つあります：

1. 電池ケース（パッケージには含まれていません）を接続する。
2. マイクロUSBケーブル経由でコンピュータや充電器に接続する。

供給電圧条件：DC 3-5V
